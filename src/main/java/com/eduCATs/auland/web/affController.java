package com.eduCATs.auland.web;


import com.eduCATs.common.auth.auth;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.List;

/**
 * 학생 > 검사결과 > 진로탐색결과 > 진로탐색결과상세 > 진로적성검사결과
 */
@Controller
@RequestMapping({"/au/aff", "/mng/aff"})
public class affController{


	@Autowired
	private SqlSession sqlSession;


	@Autowired
	private auth auth;


	/**
	 * 검사 결과 처리 ( 계열 및 학과 )
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "doResult")
	public String doAffResult(HttpServletRequest request, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
		}

		if(!request.getParameter("ctgry3_id").toString().equals("Q3")) {
			return "redirect:/au/survey?msg=N";
		}

		CommonMap map = new CommonMap();
		map.put("user_id", user.getUser_id());
		map.put("pri_tme", user.getPri_tme());
		map.put("schorg_id", user.getSchorg_id());
		map.put("ctgry3_id", "Q2");

		// 계열 정보
		CommonMap affMap = (CommonMap)sqlSession.selectOne("auMapper.cmAnswer", map);

		if(affMap != null && affMap.get("anst_typ_cd").equals("02")) {

			/* ----------- 계열 START ----------- */
			affMap.put("code_set", "TCSO");
			affMap.put("ctgry1_id", "AFFMSTR");
			// 계열 정보
			List<CommonMap> affList = sqlSession.selectList("auAffMapper.affCodeList", affMap);
			// 답변 항목 리스트
			List<CommonMap> ansList = sqlSession.selectList("auAffMapper.answerList", affMap);
			// 문항설정변수
			CommonMap cfgval = (CommonMap)sqlSession.selectOne("auAffMapper.affCfgValList");

			// 값 분포 모형 초기화
			String[][] vlist = new String [affList.size()][affList.size()];
			// 종(세로) 값들의 합
			double[] slist = new double [affList.size()];
			// 횡(가로) 표준 편차 값들의 평균
			double[] alist = new double [affList.size()];
			// 가중치 평균
			double[] glist = new double [affList.size()];
			// 행의 합 평균
			double avg = 0;
			// 일관성 지수
			double const_idx = 0;
			// 일관성 비율
			double const_rate = 0;

			// 값 분포모형 생성
			for(int i=0; i<ansList.size(); i++) {
				int p1 = Integer.parseInt(ansList.get(i).get("position_x").toString());
				int p2 = Integer.parseInt(ansList.get(i).get("position_y").toString());
				vlist[p1-1][p2-1] = ansList.get(i).get("reply_value").toString();
				double up = (1 / Double.parseDouble(ansList.get(i).get("reply_value").toString()));
				vlist[p2 - 1][p1 - 1] = String.valueOf(up);
			}

			// 종(세로) 값들의 합
			for(int y=0; y<affList.size(); y++) {
				for(int x=0; x<affList.size(); x++) {
					if(x == y) {
						vlist[x][y] = String.valueOf(1);
					}
					slist[x] += Double.valueOf(vlist[x][y]);
				}
			}

			// 횡(가로) 표준편차 값들의 평균 ( 순위 및 가중치 )
			for(int y=0; y<affList.size(); y++) {
				for(int x=0; x<affList.size(); x++) {
					alist[y] += (Double.valueOf(vlist[x][y]) / slist[x]);
				}
				alist[y] = Double.valueOf(String.format("%.2f", alist[y] / affList.size()));
			}

			// 가중치 평균
			for(int y=0; y<affList.size(); y++) {
				for(int x=0; x<affList.size(); x++) {
					glist[y] += Double.valueOf(String.format("%.3f", (Double.valueOf(vlist[x][y]) * alist[x])));
				}
				avg += Double.valueOf(String.format("%.3f", glist[y] / alist[y]));
			}
			avg = Double.valueOf(String.format("%.2f", avg / affList.size()));

			// 일관성 지수
			const_idx = Double.valueOf(String.format("%.2f", ((avg - affList.size()) / Double.valueOf(cfgval.get("AFF_CON_M").toString()))));

			// 일관성 비율
			const_rate = Double.valueOf(String.format("%.2f", const_idx / Double.valueOf(cfgval.get("AFF_CON_RT_M").toString())));

			CommonMap result = new CommonMap();
			for(int t=0; t<affList.size(); t++) {
				String values = "result" + String.valueOf(t + 1);
				result.put(values, alist[t]);
			}
			result.put("seq_answer_master", affMap.get("seq_answer_master"));
			result.put("const_idx", const_idx);
			result.put("const_rate", const_rate);

			int idx = sqlSession.insert("auAffMapper.insertAffResult", result);

			/* ----------- 학과 START ----------- */
			map.put("ctgry3_id", "Q3");
			// 학과 정보
			CommonMap majMap = (CommonMap)sqlSession.selectOne("auMapper.cmAnswer", map);
			majMap.put("user_id", user.getUser_id());
			majMap.put("seq_answer_master2", affMap.get("seq_answer_master"));

			List<CommonMap> majList = sqlSession.selectList("auAffMapper.selectMajRank", majMap);

			int midx = sqlSession.insert("auAffMapper.insertMajResult", majList);

			// 일관성검사결과 산출
			CommonMap constValue = (CommonMap)sqlSession.selectOne("auAffMapper.answerConst", majMap);
			// 일관성검사결과 입력
			int cidx = sqlSession.insert("auAffMapper.insertMajConst", constValue);
			// 학업성향결과 산출
			List<CommonMap> ppsList = sqlSession.selectList("auAffMapper.selectPpsRank", majMap);
			// 학업성향결과 입력
			int pidx = sqlSession.insert("auAffMapper.insertPpsRank", ppsList);

			/* ----------- 클론 START ----------- */
			// 설문 정보 산출
			CommonMap clonMap = (CommonMap)sqlSession.selectOne("auMapper.userAnsMaster", majMap);
			CommonMap clonInfo = (CommonMap)sqlSession.selectOne("auCloneMapper.cloneCode", clonMap);
			clonInfo.put("user_id", user.getUser_id());
			clonInfo.put("seq_tme", majMap.get("seq_tme"));
			// 설문 정보 입력
			int xidx = sqlSession.insert("auCloneMapper.insertCloneCode", clonInfo);
		}
		return "redirect:/au/survey?msg=Y";
	}


	/**
	 * 결과요약
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "summary")
	public String affSummary(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 계열 & 학과 검사
			model.addAttribute("majResultBrif", sqlSession.selectList("auAffMapper.majResultBrif", seqInfo));

			return "auland/aff/summary";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 학업성향분석
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "analys")
	public String analys(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 계열 & 학과 검사 ( 학업성향 1순위 )
			model.addAttribute("majResultBrif", sqlSession.selectList("auAffMapper.majResultBrif", seqInfo));
			// 학업성향 순위
			model.addAttribute("ppsResultDetail", sqlSession.selectList("auAffMapper.ppsResultDetail", seqInfo));
			return "auland/aff/analys";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 학과 선택 가이드
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "majAnalys")
	public String majAnalys(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 계열 & 학과 검사
			model.addAttribute("majResultBrif", sqlSession.selectList("auAffMapper.majResultBrif", seqInfo));

			return "auland/aff/majAnalys";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 진로·직업 선택 가이드
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "auGuide")
	public String auGuide(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 계열 & 학과 검사
			model.addAttribute("majResultBrif", sqlSession.selectList("auAffMapper.majResultBrif", seqInfo));

			return "auland/aff/auGuide";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}
}
