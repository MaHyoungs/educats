package com.eduCATs.auland.web;

import com.eduCATs.common.auth.auth;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.util.ArrayList;

@Controller
public class auController {

	private final Logger logger = LoggerFactory.getLogger(auController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private auth auth;

	@Value("#{prop['perPages']}")
	private String perPages;

	@Value("#{prop['affPages']}")
	private String affPages;

	@Value("#{prop['majPages']}")
	private String majPages;


	/**
	 * 학생 > 검사하기 > 진로탐색검사
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/survey")
	public String survey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.survey !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap maps = new CommonMap();
		model.addAttribute("msg", request.getParameter("msg"));

		// 개인회원 체크
		if(user.getPri_tme() != null) {
			CommonMap priChk = (CommonMap)sqlSession.selectOne("auMapper.priSurveyChk", user);
			if(priChk.get("next_yn").equals("true") && priChk.get("max_yn").equals("false")) {
				CommonMap nextInfo = (CommonMap)sqlSession.selectOne("planMapper.insertPriNextTme", user);
				priChk.put("seq_tme", nextInfo.toString().replace("{=", "").replace("}", ""));
				priChk.put("seq_user", user.getSeq_user());
				// 쿠폰사용이력에 저장
				sqlSession.insert("planMapper.insertCouponNextVal", priChk);
				sqlSession.update("planMapper.updateBeforeTme", user);

				user.setPri_tme(nextInfo.toString().replace("{=", "").replace("}", ""));
				session.removeAttribute("user");
				session.setAttribute("user", user);
			}
		}

		CommonMap tmeChk = (CommonMap)sqlSession.selectOne("auMapper.auTmeChk", user);

		// 라이센스 및 회차 체크
		if(tmeChk != null){
//			System.out.println("01 >>> tmeChk != null");
			tmeChk.put("user_id", user.getUser_id());

			model.addAttribute("tmeChk", "Y");

			// 인성검사
			tmeChk.put("ctgry3_id", "Q1");
			model.addAttribute("perChk", sqlSession.selectOne("auMapper.auCurrent", tmeChk));
			// 계열검사
			tmeChk.put("ctgry3_id", "Q2");
			model.addAttribute("affChk", sqlSession.selectOne("auMapper.auCurrent", tmeChk));

			if(model.get("affChk") != null){
				model.addAttribute("affInfo", sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk));
				model.addAttribute("cnt", sqlSession.selectOne("auMapper.surveyCnt", tmeChk));
			}else{

			}

			// 학과검사
			tmeChk.put("ctgry3_id", "Q3");
			model.addAttribute("majChk", sqlSession.selectOne("auMapper.auCurrent", tmeChk));

			// 없을 시, 최신 이력
		}else{
//			System.out.println("02 >>> tmeChk == null");
			maps.put("user_id", user.getUser_id());

			// 인성검사
			maps.put("ctgry3_id", "Q1");
			model.addAttribute("perChk", sqlSession.selectOne("auMapper.auHistory", maps));
			// 계열검사
			maps.put("ctgry3_id", "Q2");
			model.addAttribute("affChk", sqlSession.selectOne("auMapper.auHistory", maps));
			// 학과검사
			maps.put("ctgry3_id", "Q3");
			model.addAttribute("majChk", sqlSession.selectOne("auMapper.auHistory", maps));
		}
		return "auland/survey";
	}


	/**
	 * 학생 > 검사하기 > 진로탐색검사 > 검사시작(인성검사)
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/perSurvey")
	public String perSurvey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.perSurvey !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		CommonMap tmeChk = (CommonMap)sqlSession.selectOne("auMapper.auTmeChk", user);

		// 라이센스 및 회차 체크
		if(tmeChk != null) {
			int recordsPerPage = Integer.parseInt(perPages);
			CommonMap maps = new CommonMap(request);
			maps.put("seq_version", tmeChk.get("seq_version"));
			tmeChk.put("user_id", user.getUser_id());
			tmeChk.put("ctgry3_id", "Q1");
			tmeChk.put("recordsPerPage", recordsPerPage);

			CommonMap perChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);
			model.addAttribute("perChk", perChk);

			// 총 문제 수
			int cnt = (Integer)sqlSession.selectOne("auMapper.surveyCnt", tmeChk);

			// 검사 완료일 경우
			if(perChk != null && perChk.get("anst_typ_cd").equals("02")) {
				return "redirect:/au/survey?msg=C";

				// 현재 풀은문항 보다 많은 페이지 이동
			} else if(maps.get("currentPage") != null && (Integer.parseInt(maps.get("currentPage").toString()) > Integer.parseInt(perChk.get("currentPage").toString()))){
				return "redirect:/au/survey?msg=N";
			}

			// 기존 검사 답변 조회
			if(perChk != null) {
				maps.put("seq_answer_master", perChk.get("seq_answer_master"));
				model.addAttribute("info", perChk);
				// 신규
			} else {
				tmeChk.put("survey_typ_cd", "SV01");
				tmeChk.put("st_grade_cd", user.getSt_grade_cd());
				tmeChk.put("st_class", user.getSt_class());
				tmeChk.put("st_number_cd", user.getSt_number_Cd());

				sqlSession.insert("auMapper.insertMaster", tmeChk);
				CommonMap newPerChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);
				maps.put("seq_answer_master", newPerChk.get("seq_answer_master"));
				model.addAttribute("info", newPerChk);
			}

			// 페이징 처리
			if(maps.get("currentPage") == null) {
				if(perChk!= null) {
					if(Integer.parseInt(perChk.get("totalCnt").toString()) == cnt) {
						maps.put("currentPage", (Integer.parseInt(perChk.get("currentPage").toString()) -1));
					} else {
						maps.put("currentPage", perChk.get("currentPage"));
					}
				} else {
					maps.put("currentPage", "1");
				}
				model.addAttribute("currentPage", maps.get("currentPage"));
			}
			request.setAttribute("totalRecordCount", cnt);
			request.setAttribute("recordsPerPage", recordsPerPage);
			maps.put("recordsPerPage", recordsPerPage);
			maps.put("type", "per");
			model.addAttribute("perSurvey", sqlSession.selectList("auMapper.surveys", maps));
			model.addAttribute("cnt", cnt);

			return "auland/perSurvey";
		}
		return "redirect:/au/survey";
	}


	/**
	 * 학생 > 검사하기 > 진로탐색검사 > 검사시작(진로적성검사) > 계열
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/affSurvey")
	public String affSurvey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.affSurvey !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		CommonMap tmeChk = (CommonMap)sqlSession.selectOne("auMapper.auTmeChk", user);

		// 라이센스 및 회차 체크
		if(tmeChk != null) {
			int recordsPerPage = Integer.parseInt(affPages);
			CommonMap maps = new CommonMap(request);
			maps.put("seq_version", tmeChk.get("seq_version"));
			tmeChk.put("user_id", user.getUser_id());
			tmeChk.put("ctgry3_id", "Q2");
			tmeChk.put("recordsPerPage", recordsPerPage);

			// 계열 답변
			CommonMap affChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);

			// 총 문제 수
			int cnt = (Integer)sqlSession.selectOne("auMapper.surveyCnt", tmeChk);

			if(affChk != null){
				if(cnt == Integer.parseInt(affChk.get("totalCnt").toString())){
					affChk.put("currentPage", Integer.parseInt(affChk.get("currentPage").toString()) - 1);
				}
			}

			// 검사 완료일 경우
			if(affChk != null && affChk.get("anst_typ_cd").equals("02")) {
				return "redirect:/au/survey?msg=C";

				// 현재 풀은문항 보다 많은 페이지 이동
			} else if(maps.get("currentPage") != null && (Integer.parseInt(maps.get("currentPage").toString()) > Integer.parseInt(affChk.get("currentPage").toString()))){
				return "redirect:/au/survey?msg=N";
			}

			// 기존 검사 답변 조회
			if(affChk != null) {
				maps.put("seq_answer_master", affChk.get("seq_answer_master"));
				model.addAttribute("info", affChk);
			} else {
				tmeChk.put("survey_typ_cd", "SV01");
				tmeChk.put("st_grade_cd", user.getSt_grade_cd());
				tmeChk.put("st_class", user.getSt_class());
				tmeChk.put("st_number_cd", user.getSt_number_Cd());

				sqlSession.insert("auMapper.insertMaster", tmeChk);
				CommonMap newPerChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);
				maps.put("seq_answer_master", newPerChk.get("seq_answer_master"));
				model.addAttribute("info", newPerChk);
			}

			// 페이징 처리
			if(maps.get("currentPage") == null) {
				if(affChk!= null) {
					maps.put("currentPage", affChk.get("currentPage"));
				} else {
					maps.put("currentPage", "1");
				}
				model.addAttribute("currentPage", maps.get("currentPage"));
			}
			request.setAttribute("totalRecordCount", cnt);
			request.setAttribute("recordsPerPage", recordsPerPage);
			maps.put("recordsPerPage", recordsPerPage);
			maps.put("type", "aff");
			model.addAttribute("affSurvey", sqlSession.selectList("auMapper.surveys", maps));
			model.addAttribute("cnt", cnt);
			return "auland/affSurvey";
		}
		return "redirect:/au/survey";
	}


	/**
	 * 학생 > 검사하기 > 진로탐색검사 > 검사시작(진로적성검사) > 학과
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/majSurvey")
	public String majSurvey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.majSurvey !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		CommonMap tmeChk = (CommonMap)sqlSession.selectOne("auMapper.auTmeChk", user);

		// 라이센스 및 회차 체크
		if(tmeChk != null){
			int recordsPerPage = Integer.parseInt(majPages);
			CommonMap maps = new CommonMap(request);
			maps.put("seq_version", tmeChk.get("seq_version"));
			tmeChk.put("user_id", user.getUser_id());
			tmeChk.put("ctgry3_id", "Q3");
			tmeChk.put("recordsPerPage", recordsPerPage);

			// 계열 검사 정보
			CommonMap affChk = new CommonMap();
			affChk.put("ctgry3_id", "Q2");
			affChk.put("user_id", user.getUser_id());
			affChk.put("seq_tme", tmeChk.get("seq_tme"));
			CommonMap affInfo = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", affChk);
			int affCnt = (Integer)sqlSession.selectOne("auMapper.surveyCnt", affChk);
			int affPer = Integer.parseInt(affPages);
			int affPages = (affCnt / affPer);
			model.addAttribute("affPages", affPages);

			// 학과 답변
			CommonMap majChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);

			// 총 문제 수
			int cnt = (Integer)sqlSession.selectOne("auMapper.surveyCnt", tmeChk);


			// 검사 완료일 경우
			if(majChk != null && majChk.get("anst_typ_cd").equals("02")){
				return "redirect:/au/survey?msg=C";

				// 현재 풀은문항 보다 많은 페이지 이동
			}else if(maps.get("currentPage") != null && (Integer.parseInt(maps.get("currentPage").toString()) > Integer.parseInt(majChk.get("currentPage").toString()))){
				return "redirect:/au/survey?msg=N";

				// 계열 검사 모두 완료하지 않고 페이지 이동시
			}else if(affInfo.get("totalCnt") != null && affCnt > Integer.parseInt(affInfo.get("totalCnt").toString())) {
				return "redirect:/au/survey?msg=N";
			} else {

			}

			// 기존 검사 답변 조회
			if(majChk != null) {
				maps.put("seq_answer_master", majChk.get("seq_answer_master"));
				model.addAttribute("info", majChk);
			} else {
				tmeChk.put("survey_typ_cd", "SV01");
				tmeChk.put("st_grade_cd", user.getSt_grade_cd());
				tmeChk.put("st_class", user.getSt_class());
				tmeChk.put("st_number_cd", user.getSt_number_Cd());

				sqlSession.insert("auMapper.insertMaster", tmeChk);
				CommonMap newPerChk = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);
				maps.put("seq_answer_master", newPerChk.get("seq_answer_master"));
				model.addAttribute("info", newPerChk);
			}

			// 페이징 처리
			if(maps.get("currentPage") == null) {
				if(majChk!= null) {
					if(Integer.parseInt(majChk.get("totalCnt").toString()) == cnt) {
						maps.put("currentPage", (Integer.parseInt(majChk.get("currentPage").toString()) -1));
					} else {
						maps.put("currentPage", majChk.get("currentPage"));
					}
				} else {
					maps.put("currentPage", "1");
				}
				model.addAttribute("currentPage", maps.get("currentPage"));
			}
			request.setAttribute("totalRecordCount", cnt);
			request.setAttribute("recordsPerPage", recordsPerPage);
			maps.put("recordsPerPage", recordsPerPage);
			maps.put("type", "maj");
			model.addAttribute("majSurvey", sqlSession.selectList("auMapper.surveys", maps));
			model.addAttribute("cnt", cnt);
			return "auland/majSurvey";
		}
		return "redirect:/au/survey";
	}


	/**
	 * 학생 > 검사하기 > 진로탐색검사 > 검사 > 임시저장 Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/doTempSaveAjax", method = RequestMethod.POST)
	public void doTempSave(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.doTempSave !!!");

		String val = request.getParameter("valList");
		String seq = request.getParameter("seqList");
		String seqAnswer = request.getParameter("seqAnswerMaster");
		String ctgryId = request.getParameter("ctgryId");
		CommonMap times = new CommonMap(request);
		String[] seqList = seq.split(",");
		String[] valList = val.split(",");

		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		response.setHeader("Content-Type", "text/html;charset=utf-8");
		PrintWriter out = new PrintWriter(new OutputStreamWriter(response.getOutputStream(),"UTF-8"));

		try{
			for(int i = 0; i < seqList.length; i++){
				CommonMap map = new CommonMap();
				map.put("seq", seqList[i]);
				map.put("val", valList[i]);
				map.put("seqAnswer", seqAnswer);
				list.add(map);
			}
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			insMap.put("ctgryId", ctgryId);

			CommonMap delMap = new CommonMap();

			delMap.put("seqQFirst", seqList[0]);
			delMap.put("seqQLast", seqList[seqList.length -1]);
			delMap.put("seq_answer_master", seqAnswer);
			delMap.put("ctgryId", ctgryId);
			sqlSession.delete("auMapper.deleteAnswer", delMap);
			sqlSession.insert("auMapper.insertAnswer", insMap);
			sqlSession.update("auMapper.timeUpdate", times);
		} catch(Exception e) {
			System.out.println(e);
		}
		out.print("succ");
		out.flush();
	}


	/**
	 * 학생 > 검사하기 > 진로탐색검사 > 검사 > 검사완료
	 * @param request
	 * @param response
	 * @param model
	 * @throws Exception
	 */
	@RequestMapping(value = "/au/doComplete")
	public String doComplete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.doComplete !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		CommonMap map = new CommonMap(request);

		if(map.get("ctgry3_id").equals("Q3")) {
			CommonMap tmeChk = (CommonMap)sqlSession.selectOne("auMapper.auTmeChk", user);
			tmeChk.put("user_id", user.getUser_id());
			tmeChk.put("ctgry3_id", "Q2");
			CommonMap getAff = (CommonMap)sqlSession.selectOne("auMapper.auSurveyAnswer", tmeChk);
			map.put("ctgry3_ids", getAff.get("ctgry3_id"));
			map.put("seq_answer_masters", getAff.get("seq_answer_master"));
		}
		sqlSession.update("auMapper.updateMaster", map);

		if(map.get("ctgry3_id").equals("Q1")) {
			return "redirect:/au/per/doResult?ctgry3_id=Q1";
		} else if(map.get("ctgry3_id").equals("Q3")) {
			return "redirect:/au/aff/doResult?ctgry3_id=Q3";
		} else {
			return "redirect:/au/survey?msg=T";
		}
	}


	/**
	 * 학생 > 검사결과 > 진로탐색결과 프레임셋
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/au/aulandResult", "/mng/aulandResult"})
	public String aulandResult(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.aulandResult !!!");

		return "auland/aulandResult";
	}


	/**
	 * 학생 > 검사결과 > 진로탐색결과 프레임셋 PDF다운로드
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/au/pdfDownload","/mng/pdfDownload"})
	public String pdfDownload(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.pdfDownload !!!");

/*
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
		model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
*/

		CommonMap map = new CommonMap(request);
		CommonMap usrInfo = (CommonMap)sqlSession.selectOne("auMapper.getUsrInfo", map);
		model.addAttribute("usrInfo",usrInfo);
		CommonMap userPdfInfo = (CommonMap)sqlSession.selectOne("auMapper.userPdfInfo", map);
		model.addAttribute("userPdfInfo",userPdfInfo);
//		System.out.println(">>> " + userPdfInfo.get("reg_dt"));

		return "auland/pdfDownload";
	}


	/**
	 * 학생 > 검사결과 > 진로탐색결과
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = {"/au/surveyResult", "/mng/surveyResult"})
	public String surveyResult(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("auController.surveyResult !!!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo.get("adms") != null) {
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
//			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}

		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 인성검사 > 하위인성결과
			model.addAttribute("perResultBrif", sqlSession.selectList("auPerMapper.perResultBrif", seqInfo));
			// 인성검사 > 주요인성결과
			model.addAttribute("mainperResultBrif", sqlSession.selectList("auPerMapper.mainperResultBrif", seqInfo));
			// 계열 & 학과 검사
			model.addAttribute("majResultBrif", sqlSession.selectList("auAffMapper.majResultBrif", seqInfo));
			// 클론대학생활정보
			model.addAttribute("cloneProfile", sqlSession.selectOne("auCloneMapper.cloneProfile", usrInfo));
			// 클론합격기업직무
			model.addAttribute("clonePassbiz", sqlSession.selectList("auCloneMapper.clonePassbiz", usrInfo));

			return "auland/surveyResult";
		}

		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}

}