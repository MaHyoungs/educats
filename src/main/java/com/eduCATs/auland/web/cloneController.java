package com.eduCATs.auland.web;


import com.eduCATs.common.auth.auth;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;

/**
 * 학생 > 검사결과 > 진로탐색결과 > 진로탐색결과상세 > 클론매칭
 */
@Controller
@RequestMapping({"/au/clone", "/mng/clone"})
public class cloneController{


	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private auth auth;


	/**
	 * 결과지 소개
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "resultInfo")
	public String resultInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){

			return "auland/clone/resultInfo";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 대학생활 정보
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "univInfo")
	public String univInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 클론대학생활정보
			model.addAttribute("cloneProfile", sqlSession.selectOne("auCloneMapper.cloneProfile", usrInfo));

			return "auland/clone/univInfo";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 취업정보
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "jobInfo")
	public String jobInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 클론합격기업직무
			model.addAttribute("clonePassbiz", sqlSession.selectList("auCloneMapper.clonePassbiz", usrInfo));

			return "auland/clone/jobInfo";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}
}
