package com.eduCATs.auland.web;

import com.eduCATs.common.auth.auth;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.net.URLEncoder;
import java.util.List;

/**
 * 학생 > 검사결과 > 진로탐색결과 > 진로탐색결과상세 > 인성검사결과
 */
@Controller
@RequestMapping({"/au/per", "/mng/per"})
public class perController{


	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private auth auth;


	/**
	 * 검사 결과 처리 ( 인성 )
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "doResult")
	public String doPerResult(HttpServletRequest request, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
		}

		if(!request.getParameter("ctgry3_id").toString().equals("Q1")) {
			return "redirect:/au/survey?msg=N";
		}

		CommonMap map = new CommonMap();
		map.put("user_id", user.getUser_id());
		map.put("pri_tme", user.getPri_tme());
		map.put("schorg_id", user.getSchorg_id());
		map.put("ctgry3_id", "Q1");
		// 인성 정보
		CommonMap cmMap = (CommonMap)sqlSession.selectOne("auMapper.cmAnswer", map);

		if(cmMap != null && cmMap.get("anst_typ_cd").equals("02")){
			// 하위인성검사결과 산출
			List<CommonMap> answerResult = sqlSession.selectList("auPerMapper.answerResult", cmMap);
			// 하위인성검사결과 입력
			int ridx = sqlSession.insert("auPerMapper.insertPerResult", answerResult);
			// 일관성검사결과 산출
			CommonMap constValue = (CommonMap)sqlSession.selectOne("auPerMapper.answerConst", cmMap);

			// 주요인성검사결과 산출
			int[] mainSum = new int[3];
			for(int i = 0; i < answerResult.size(); i++){
				if(answerResult.get(i).get("ctgry2_id") != null && answerResult.get(i).get("ctgry2_id").toString().equals("PERMAIN_01")){
					mainSum[0] += Integer.parseInt(answerResult.get(i).get("factor_score").toString());
				}else if(answerResult.get(i).get("ctgry2_id") != null && answerResult.get(i).get("ctgry2_id").toString().equals("PERMAIN_02")){
					mainSum[1] += Integer.parseInt(answerResult.get(i).get("factor_score").toString());
				}else if(answerResult.get(i).get("ctgry2_id") != null && answerResult.get(i).get("ctgry2_id").toString().equals("PERMAIN_03")){
					mainSum[2] += Integer.parseInt(answerResult.get(i).get("factor_score").toString());
				}else{
					constValue.put("scpr_score", answerResult.get(i).get("factor_score"));
//					System.out.println("factor_score" + answerResult.get(i).get("factor_score"));
				}
			}

			// 일관성검사결과 입력
			int cidx = sqlSession.insert("auPerMapper.insertPerConst", constValue);

			CommonMap mainInfo = new CommonMap();
			mainInfo.put("seq_answer_master", cmMap.get("seq_answer_master"));
			mainInfo.put("seq_version", cmMap.get("seq_version"));

			for(int x = 0; x < mainSum.length; x++){
				mainInfo.put("ctgry2_id" + (x + 1), "PERMAIN_0" + (x + 1));
				mainInfo.put("factor_score" + (x + 1), mainSum[x]);
			}
			List<CommonMap> mainResult = sqlSession.selectList("auPerMapper.answerMainResult", mainInfo);

			// 주요인성검사결과 입력
			int midx = sqlSession.insert("auPerMapper.insertPerMainResult", mainResult);
			// 하위인성검사 순위
			int prank = sqlSession.update("auPerMapper.updatePerRank", cmMap);
		}
		return "redirect:/au/survey?msg=Y";
	}


	/**
	 * 결과요약
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "summary")
	public String perSummary(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 인성검사 > 하위인성결과
			model.addAttribute("perResultBrif", sqlSession.selectList("auPerMapper.perResultBrif", seqInfo));
			// 인성검사 > 주요인성결과
			model.addAttribute("mainperResultBrif", sqlSession.selectList("auPerMapper.mainperResultBrif", seqInfo));
			// 인성검사 > 일관성지수
			model.addAttribute("perConst", sqlSession.selectOne("auPerMapper.selectPerConst", seqInfo));

			return "auland/per/summary";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 하위 인성별 수준분석
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "analysL")
	public String analysL(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 인성검사 > 하위인성결과 상세
			model.addAttribute("perResultDetail", sqlSession.selectList("auPerMapper.perResultDetail", seqInfo));

			return "auland/per/analysL";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 주요 인성군별 수준분석
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "analysM")
	public String analysM(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 인성검사 > 주요인성결과
			model.addAttribute("mainperResultDetail", sqlSession.selectList("auPerMapper.mainperResultDetail", seqInfo));

			return "auland/per/analysM";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}


	/**
	 * 검사신뢰도
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "trust")
	public String trust(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		adminVo adm = new adminVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap usrInfo = new CommonMap(request);
		model.addAttribute("adms", usrInfo.get("adms"));
		if(usrInfo != null) {
			model.addAttribute("usrInfo", sqlSession.selectOne("auMapper.getUsrInfo", usrInfo));
			model.addAttribute("userPdfInfo", sqlSession.selectOne("auMapper.userPdfInfo", usrInfo));
		} else {
			String message = "비정상적인 접속입니다.";
			return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
		}
		String auths = "";

		if(adm != null) {
			auths = auth.admAuth(adm, usrInfo);
		} else if(user != null) {
			auths = auth.usrAuth(user, usrInfo);
		}

		if(auths.equals("Y")){
			// 검사 시퀀스 번호
			CommonMap seqInfo = (CommonMap)sqlSession.selectOne("auMapper.ansMasterSeq", usrInfo);
			// 인성검사 > 일관성지수
			model.addAttribute("perConst", sqlSession.selectOne("auPerMapper.selectPerConst", seqInfo));

			return "auland/per/trust";
		}
		String message = "비정상적인 접속입니다.";
		return "redirect:/index?msg=" + URLEncoder.encode(message, "UTF-8");
	}

}
