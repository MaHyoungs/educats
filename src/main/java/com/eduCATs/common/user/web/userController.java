package com.eduCATs.common.user.web;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/usr")
public class userController {

	private final Logger logger = LoggerFactory.getLogger(userController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 검사하기 & 검사결과
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "choice")
	public String choice(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		String type = request.getParameter("type");
		model.addAttribute("type", type);

		// 개인회원(쿠폰등록자) 처리
		if(user.getPri_tme() != null)
		{
			if(type.equals("s")){
				if(user.getPri_typ().equals("SV01")){
					return "redirect:/au/survey";
				}else{
					return "redirect:/self/survey";
				}
			}else{
				CommonMap chk = (CommonMap)sqlSession.selectOne("valueMapper.priSurveyYn", user);
				if(chk == null){
					return "redirect:/au/survey?msg=D";
				}else{
					if(user.getPri_typ().equals("SV01")){
//						return "redirect:/au/surveyResult?seq_tme=" + user.getPri_tme() + "&who=" + user.getUser_id();
						return "redirect:/au/aulandResult?seq_tme=" + chk.get("seq_tme") + "&who=" + user.getUser_id();
					}else{
						return "redirect:/self/survey";
					}
				}
			}
		}
		else
		{
			if(user.getConfm_yn() == null){
				return "redirect:/index?msg=B";
			}else if(user.getConfm_yn().equals("N")){
				return "redirect:/index?msg=R";
			}

			if(type.equals("s")){
				boolean sv01 = false;
				boolean sv02 = false;

				if(user.getSV01YN().equals("Y") || user.getSV01HISYN().equals("Y")){
					sv01 = true;
				}
				if(user.getSV02YN().equals("Y") || user.getSV02HISYN().equals("Y")){
					sv02 = true;
				}

				if(sv01 == true && sv02 == false){
					return "redirect:/au/survey";
				}else if(sv01 == false && sv02 == true){
					return "redirect:/self/survey";
				}else{
					return "common/user/choice";
				}
			}else{
				// 진로탐색 검사 여부
				CommonMap auRecent = (CommonMap)sqlSession.selectOne("auMapper.auRecentResult", user);
				model.addAttribute("auRecent", auRecent);

				boolean sv01 = false;
				boolean sv02 = false;
				if(user.getSV01YN().equals("Y") || user.getSV01HISYN().equals("Y")){
					sv01 = true;
				}
				if(user.getSV02YN().equals("Y") || user.getSV02HISYN().equals("Y")){
					sv02 = true;
				}

				if(sv01 == true && sv02 == true){
					return "common/user/choice";
				}else if(sv01 == true && sv02 == false){
					if(auRecent != null){
//						return "redirect:/au/surveyResult?seq_tme=" + auRecent.get("seq_tme") + "&who=" + user.getUser_id();
						return "redirect:/au/aulandResult?seq_tme=" + auRecent.get("seq_tme") + "&who=" + user.getUser_id();
					}
				}else if(sv01 == false && sv02 == true){
					return "common/user/choice";
				}else{
					return "common/user/choice";
				}
				return "common/user/choice";
			}
		}
	}


	/**
	 * 마이페이지 > 내정보관리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "info")
	public String usrInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("userController.usrInfo !!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap map = new CommonMap(request);

		// 비밀번호
		logger.info("비밀번호 >>> classMapper.getPassword");
		CommonMap pas = (CommonMap)sqlSession.selectOne("classMapper.getPassword", user);
		pas.put("password", aes.decrypt(pas.get("password").toString()));
		model.addAttribute("pas", pas);
		model.addAttribute("msg", request.getParameter("msg"));

		// 쿠폰 사용 회원일 경우
		if(user.getPri_typ() != null){
			return "common/user/priInfo";
		}

		// 년도
		map.put("code_set", "cm_year");
		logger.info("년도 >>> valueMapper.codeList");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));

		// 번호
		map.put("code_set", "cm_number");
		logger.info("번호 >>> valueMapper.codeList");
		model.addAttribute("numList", sqlSession.selectList("valueMapper.codeList", map));

		//
		logger.info(" >>> joinMapper.schorgDetail");
		model.addAttribute("schorgDetail", sqlSession.selectOne("joinMapper.schorgDetail", user));

		// 기관일때 학급
		if(user.getSchorg_typ_cd().equals("ORG01")){
			logger.info("학급 >>> classMapper.gradeToClass");
			model.addAttribute("classList", sqlSession.selectList("classMapper.gradeToClass", user));
		}
		Calendar c = Calendar.getInstance();
		String nowYear = String.valueOf(c.get(Calendar.YEAR));
		map.put("user_id", user.getUser_id());
		map.put("schorg_typ_cd", user.getSchorg_typ_cd());
		map.put("st_year_cd", nowYear);
		map.put("type", "now");
		logger.info(" >>> classMapper.userHistory");
		model.addAttribute("userCurrent", sqlSession.selectOne("classMapper.userHistory", map));
		map.put("type", "fut");
		logger.info(" >>> classMapper.userHistory");
		model.addAttribute("userHistory", sqlSession.selectOne("classMapper.userHistory", map));

		return "common/user/info";
	}


	/**
	 * 내정보관리 > 수정 > 비밀번호 확인
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "passwordChk", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String passwordChk(HttpServletRequest request) throws Exception {
		CommonMap map = new CommonMap(request);
		map.put("chkPassword", aes.encrypt(map.get("chkPassword").toString()));
		int cidx = Integer.parseInt(sqlSession.selectOne("accountMapper.passwordChk", map).toString());
		if(cidx > 0){
			return "Y";
		}else{
			return "N";
		}
	}


	/**
	 * 내정보관리 > 수정
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "infoUpdate")
	public String infoUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap info = new CommonMap(request);
		info.put("new_password", aes.encrypt(info.get("newPassword").toString()));
		int uidx = sqlSession.update("classMapper.updateTch", info);

		return "redirect:/usr/info?msg=Y";
	}


	/**
	 * 내정보관리 현재 학급 수정 및, 다음 학급 재신청 및, 다음 학년도 학급 신청 Ajax
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "usrUpdateAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String usrUpdateAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap map = new CommonMap(request);
		map.put("user_id", user.getUser_id());
		int yea = Integer.parseInt(map.get("userYea").toString()); // 다음학년도
		// 최초 가입 재수정 ( C )
		if(map.get("subtyp").equals("C")) {
			int aidx = sqlSession.update("classMapper.updateUserConfm", map);
			int pidx = sqlSession.update("classMapper.updateUserClass", map);
			user.setConfm_yn(null);
			if(!map.get("newGra").equals("")){
				user.setSt_grade_cd(map.get("newGra").toString());
				user.setSt_number_Cd(map.get("newNum").toString());
			}
			user.setSt_class(map.get("newCla").toString());
			session.removeAttribute("user");
			session.setAttribute("user", user);
			return "up";
		// 다음 학년도 재신청 ( N )
		} else if(map.get("subtyp").equals("N")) {
			map.put("userYea", yea);
			int xidx = sqlSession.update("classMapper.cmNextYearClass", map);
			return "up";
		// 다음 학년도 신청 ( T )
		} else {
			map.put("userYea", yea);
			int iidx = sqlSession.insert("classMapper.submitClass", map);
			return "up";
		}
	}


	/**
	 * 학년도에 따른 학급리스트 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classListAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classListAjax(HttpServletRequest request) throws Exception {
		List<CommonMap> list = sqlSession.selectList("classMapper.gradeToClass", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 마이페이지 > 내검사보관함
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "surveyHistory")
	public String surveyHistory(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		map.put("user_id", user.getUser_id());
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("classMapper.userSurveysCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		model.addAttribute("userSurveys", sqlSession.selectList("classMapper.userSurveys", map));

		return "common/user/surveyHistory";
	}


	/**
	 * 사용자 검사 판단 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "chkResultAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String chkResultAjax(HttpServletRequest request) throws Exception {
		CommonMap map = new CommonMap(request);
		int chk = (Integer.parseInt(sqlSession.selectOne("auMapper.chkResult", map).toString()));
		if(chk > 0){
			return "Y";
		}else{
			return "N";
		}
	}

	/**
	 * 쿠폰사용자 유효기간 판단 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "chkValidAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String chkValidAjax(HttpServletRequest request) throws Exception {
		CommonMap map = new CommonMap(request);
		CommonMap valid = (CommonMap)sqlSession.selectOne("loginMapper.priUserVaildChk", map);
		if(valid.get("use_yn").equals("N")) {
			return "N";
		}else{
			return "Y";
		}
	}

}