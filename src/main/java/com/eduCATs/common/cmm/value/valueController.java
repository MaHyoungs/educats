package com.eduCATs.common.cmm.value;

import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class valueController{


	@Autowired
	SqlSession sqlSession;


	/**
	 * 권한별 소속 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/values/authSelectAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String authSelectAjax(HttpServletRequest request) throws Exception{

		List<CommonMap> list = sqlSession.selectList("valueMapper.authSelect", new CommonMap(request));
		String result = list.toString();

	    return result;
	}
}
