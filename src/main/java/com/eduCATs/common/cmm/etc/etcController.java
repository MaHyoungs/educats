package com.eduCATs.common.cmm.etc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/etc")
public class etcController{

	private final Logger logger = LoggerFactory.getLogger(etcController.class);
	/**
	 * 이용약관
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/introduce")
	public String introduce(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("etcController.introduce !!");
		return "common/cmm/etc/introduce";
	}


	/**
	 * 개인정보취급방침
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/privatePolicy")
	public String privatePolicy(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("etcController.privatePolicy !!");
		return "common/cmm/etc/privatePolicy";
	}


	/**
	 * 이메일집단수집거부
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/emailPolicy")
	public String emailPolicy(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("etcController.emailPolicy !!");
		return "common/cmm/etc/emailPolicy";
	}


	/**
	 * 법적고지
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/lawPolicy")
	public String lawPolicy(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("etcController.lawPolicy !!");
		return "common/cmm/etc/lawPolicy";
	}

}