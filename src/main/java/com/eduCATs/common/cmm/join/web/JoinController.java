package com.eduCATs.common.cmm.join.web;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Random;

@Controller
@RequestMapping("/join")
public class JoinController {

	private final Logger logger = LoggerFactory.getLogger(JoinController.class);

	@Autowired
	private SqlSession sqlSession;
	
	@Autowired
	private SqlSession sqlSessionSms;

	@Autowired
	private aes aes;

	/**
	 * 이용약관
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "lawConfirm")
	public String lawConfirm(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		if(user != null){
			return "redirect:/index";
		}

		return "common/cmm/join/lawConfirm";
	}


	/**
	 * 회원정보입력
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "userInsert")
	public String userInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("JoinController.userInsert !!");

		CommonMap map = new CommonMap(request);
		if(map.get("perinfo") != null && map.get("agree") != null && map.get("perinfo").equals("on") && map.get("agree").equals("on")) {
			model.addAttribute("returnUrl", request.getRequestURI().toString());

			// 번호
			map.put("code_set", "cm_number");
			model.addAttribute("numList", sqlSession.selectList("valueMapper.codeList", map));

			return "common/cmm/join/userInsert";
		} else {
			return "redirect:/index";
		}
	}


	/**
	 * 기관 선택 Layer
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "schoolSelect")
	public String schoolSelect(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);
		// 지역
		map.put("code_set", "area");
		model.addAttribute("areaList", sqlSession.selectList("valueMapper.codeList", map));
		// 구분
		map.put("code_set", "schorg");
		model.addAttribute("schList", sqlSession.selectList("valueMapper.codeList", map));
		// 학교 기관
		model.addAttribute("schorgList", sqlSession.selectList("joinMapper.schorgList"));

		return "common/cmm/join/joinLayer";
	}


	/**
	 * 학교/기관 검색 Ajax ( 회원가입 )
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchOrgan", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String searchOrgan(HttpServletRequest request) throws Exception {
		List<CommonMap> list = sqlSession.selectList("joinMapper.schorgList", new CommonMap(request));

		return list.toString();
	}


	/**
	 * 학교/기관 상세 Ajax ( 회원가입 )
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "organDetail", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String organDetail(HttpServletRequest request) throws Exception {
		CommonMap map = (CommonMap)sqlSession.selectOne("joinMapper.schorgDetail", new CommonMap(request));

		return map.toString();
	}


	/**
	 * 특정 학년에 대한 학급정보 Ajax ( 회원가입 )
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classSet", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classSet(HttpServletRequest request) throws Exception {
		CommonMap map = new CommonMap(request);
		List<CommonMap> list = null;
		
		if ("Y".equals(map.get("yn"))) {
			list = sqlSession.selectList("joinMapper.clasInfoList", map);
		} else {
			list = sqlSession.selectList("classMapper.gradeToClass", map);
		}

		return list.toString();
	}


	/**
	 * 회원ID 중복여부 검사 Ajax (회원가입)
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "checkDupUseridAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String checkDupUseridAjax(HttpServletRequest request, ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);
		
		int chk = (Integer.parseInt(sqlSession.selectOne("joinMapper.joinIdChk", map).toString()));
		if (chk == 0) {
			return "succ";
		} else {
			return "fail";
		}
	}


	/**
	 * 회원가입
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinInsert")
	public String joinInsert(HttpServletRequest request, HttpServletResponse response,  ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		
		CommonMap map = new CommonMap(request);
		map.put("password", aes.encrypt(map.get("password").toString()));
		// 중복 찾기
		int chk = (Integer.parseInt(sqlSession.selectOne("joinMapper.joinIdChk", map).toString()));
		if(chk > 0) {
			return "redirect:/login?msg=S";
		}
		// SMS인증번호 오류여부 확인
		if ("Y".equals(map.get("confrmYn"))) {
			String serverNumber = (String)session.getAttribute("authNumber");
			if (!serverNumber.equals(map.get("confrmNum"))) {
				return "redirect:/login?msg=S";
			}
			session.removeAttribute("authNumber");
		}
		// 개인회원인 경우 쿠폰번호 다시 확인
		String couponNumber = map.get("couponNumber").toString();
		map.put("couponNumber", couponNumber);
		CommonMap res = (CommonMap)sqlSession.selectOne("joinMapper.joinCouponChk", map);
		if (map.get("memClass").equals("priMem") && "Y".equals(map.get("couponChk"))) {
			if(!("1".equals(res.get("use_cnt").toString()))) {
				return "redirect:/login?msg=S";
			}
		}
		int midx = sqlSession.insert("joinMapper.insertMember", map);
		int uidx = sqlSession.insert("joinMapper.insertUser", map);

		if(map.get("memClass").equals("priMem")) {
			CommonMap couponInfo = (CommonMap)sqlSession.selectOne("joinMapper.couponInfo", map);
			CommonMap couponLicInfo = (CommonMap)sqlSession.selectOne("joinMapper.couponLicInfo", map);
			couponLicInfo.put("start_dt", couponInfo.get("valid_start_dt"));
			couponLicInfo.put("end_dt", couponInfo.get("valid_end_dt"));
			couponLicInfo.put("user_id", map.get("user_id"));
			CommonMap iidx = (CommonMap)sqlSession.selectOne("joinMapper.insertCmTmeList", couponLicInfo);
			map.put("seq_coupon", couponInfo.get("seq_coupon"));
			map.put("seq_tme", iidx.toString().replace("{=", "").replace("}", ""));
			// 개인회원인 경우 쿠폰사용이력에 저장
			sqlSession.insert("joinMapper.insertCouponUselist", map);
			// 쿠폰 사용 처리
			sqlSession.update("joinMapper.updateCouponState", map);

			return "redirect:/join/joinComplete?success=P";
		} else if(map.get("memClass").equals("orgMem") && map.get("insertMemClass").equals("02")) {
			return "redirect:/join/joinComplete?success=S";
		} else if(map.get("memClass").equals("orgMem") && map.get("insertMemClass").equals("04")) {
			return "redirect:/join/joinComplete?success=T";
		}
		return "redirect:/join/joinComplete?success=Y";
	}


	/**
	 * 가입완료
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinComplete")
	public String joinComplete(HttpServletRequest request, HttpServletResponse response,  ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);
		model.addAttribute("success", map.get("success"));

		return "common/cmm/join/joinComplete";
	}

	
	/**
	 * SMS 인증번호발송 Ajax (회원가입)
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "sendSmsAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String sendSmsAjax(HttpServletRequest request, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		session.removeAttribute("authNumber");
		
		CommonMap map = new CommonMap(request);
		String phoneNumber = map.get("phoneNumber").toString();
		
		Random random = new Random();
		String authNumber = String.format("%06d", random.nextInt(999999));
		String msgString = "에듀캣 인증번호 " + authNumber;
		
		map.put("msgString", msgString);
		
		int cnt = sqlSessionSms.insert("joinMapper.insertSmsMessage", map);
		if (cnt > 0) {
			session.setAttribute("authNumber", authNumber);
			return "succ";
		} else {
			return "fail";
		}
	}
	
	
	/**
	 * SMS 인증번호확인 Ajax (회원가입)
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "checkSmsAuthnumberAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String checkSmsAuthnumberAjax(HttpServletRequest request, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
				
		CommonMap map = new CommonMap(request);
		
		String serverNumber = (String)session.getAttribute("authNumber");
		if (serverNumber.equals(map.get("authNumber").toString())) {
			return "succ";
		} else {
			return "fail";
		}
	}


	/**
	 * 쿠폰번호 검사 Ajax (회원가입)
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "checkCouponAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String checkCouponAjax(HttpServletRequest request, ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);
		String couponNumber = map.get("couponNumber").toString();
		
		map.put("couponNumber", couponNumber);
		
		CommonMap res = (CommonMap)sqlSession.selectOne("joinMapper.joinCouponChk", map);

		if("1".equals(res.get("use_cnt").toString())) {
			return "succ";
		} else {
			return "fail";
		}
	}
}