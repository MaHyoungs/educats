package com.eduCATs.common.cmm.join.web;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.util.SimpleEmailClient;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.util.Calendar;
import java.util.Random;

@Controller
public class loginController {

	private final Logger logger = LoggerFactory.getLogger(loginController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['fromMails']}")
	private String fromMails;

	@Value("#{prop['fromNames']}")
	private String fromNames;

	/**
	 * 로그인 페이지
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "login")
	public String loginView(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("loginController.loginView !!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		if(user != null){
			return "redirect:/index";
		}

		return "common/cmm/login/login";
	}


	/**
	 * 로그인 처리
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loginAction", method = RequestMethod.POST)
	public String loginAction(HttpServletRequest request, HttpSession session) throws Exception {
		logger.info("loginController.loginAction !!");

		CommonMap map = new CommonMap(request);
		map.put("password", aes.encrypt(map.get("password").toString()));
		loginVo user = (loginVo)sqlSession.selectOne("loginMapper.userChk", map);

		if(user != null){
			if(user.getUser_typ_cd().equals("02")) {
				CommonMap usef = (CommonMap)sqlSession.selectOne("loginMapper.userCurrentClass", user);
				if(usef != null){
					user.setSt_year_cd(usef.get("st_year_cd").toString());
					user.setSt_class(usef.get("st_class").toString());
					if(!user.getSchorg_typ_cd().equals("ORG01")){
						user.setSt_grade_cd(usef.get("st_grade_cd").toString());
						user.setSt_number_Cd(usef.get("st_number_cd").toString());
					}
				}
			}
			if(user.getSchlvl_cd() != null) {
				CommonMap valid = (CommonMap)sqlSession.selectOne("loginMapper.priUserVaildChk", user);

				if(valid.get("use_yn").equals("N")) {
					session.setAttribute("user", user);
					session.removeAttribute("adm");
					return "redirect:/usr/info?msg=V";
				}
			}

			if (user.getConfm_yn() == null || user.getConfm_yn().equals("")) {
				session.setAttribute("user", user);
				session.removeAttribute("adm");
				if(user.getUser_typ_cd().equals("02")) {
					return "redirect:/usr/info?msg=B";
				} else if(user.getUser_typ_cd().equals("04")) {
					return "redirect:/tch/info?msg=BT";
				}
			} else if (user.getConfm_yn() != null && user.getConfm_yn().equals("N")) {
				session.setAttribute("user", user);
				session.removeAttribute("adm");
				if(user.getUser_typ_cd().equals("02")) {
					return "redirect:/usr/info?msg=R";
				} else if(user.getUser_typ_cd().equals("04")) {
					return "redirect:/tch/info?msg=RT";
				}
			} else if (user.getSV01YN().equals("N") && user.getSV02YN().equals("N") && (user.getSV01HISYN().equals("Y") || user.getSV02HISYN().equals("Y"))) {
				session.setAttribute("user", user);
				session.removeAttribute("adm");
				if(user.getUser_typ_cd().equals("02")) {
					return "redirect:/usr/info?msg=H";
				} else if(user.getUser_typ_cd().equals("04")) {
					return "redirect:/tch/joinState?msg=H";
				}
			} else if (user.getConfm_yn() != null && user.getConfm_yn().equals("Y")) {
				session.setAttribute("user", user);
				session.removeAttribute("adm");
				if(user.getUser_typ_cd().equals("02")) {
					return "redirect:/usr/choice?type=s";
				} else if(user.getUser_typ_cd().equals("04")) {
					return "redirect:/tch/joinState";
				}
			} else {
				session.setAttribute("user", user);
				session.removeAttribute("adm");
				return "redirect:/index";
			}
		}
		return "redirect:/login?msg=D";
	}


	/**
	 * 아이디/비밀번호 찾기
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchInfo")
	public String searchInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("loginController.searchInfo !!");

		CommonMap map = new CommonMap(request);
		
		Calendar c = Calendar.getInstance();
		String nowYear = String.valueOf(c.get(Calendar.YEAR));
		model.addAttribute("nowYear", nowYear);
		
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		
		// 번호
		map.put("code_set", "cm_number");
		model.addAttribute("numList", sqlSession.selectList("valueMapper.codeList", map));
		return "common/cmm/login/searchInfo";
	}


	/**
	 * 아이디 찾기 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchIdAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String searchIdAjax(HttpServletRequest request) throws Exception {
		logger.info("loginController.searchIdAjax !!");

		CommonMap map = new CommonMap(request);
		CommonMap ids = (CommonMap)sqlSession.selectOne("loginMapper.searchId", map);
		if(ids == null) {
			return "N";
		} else{
			return ids.get("user_id").toString();
		}
	}


	/**
	 * 임시 비밀번호를 이메일로 발송  Ajax (비밀번호 찾기)
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "sendPasswordEmailAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String sendPasswordEmailAjax(HttpServletRequest request) throws Exception {
		logger.info("loginController.sendPasswordEmailAjax !!");

		CommonMap map = new CommonMap(request);
//		System.out.println(map);
		CommonMap chks = (CommonMap)sqlSession.selectOne("loginMapper.searchPwChk", map);
		if(chks == null) {
			return "N";
		}
		Random rnd = new Random();
		StringBuffer buf = new StringBuffer();

		for(int i=0;i<10;i++){
			if(rnd.nextBoolean()){
				buf.append((char)((int)(rnd.nextInt(26))+65));
			}else{
				buf.append((rnd.nextInt(10)));
			}
		}
		String changePassword = buf.toString();

		String toId = chks.get("user_id").toString();
		String toEmail = toId;
		String toName = "";
		String fromMail = fromMails;
		String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
		String subject = "[에듀캣] 임시 비밀번호 안내";
		String message = "에듀캣 임시 비밀번호를 알려드립니다.<br/>* 임시 비밀번호 : " + changePassword + "<br/>로그인 후 비밀번호를 변경하시기 바랍니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";

		map.put("changePassword", aes.encrypt(changePassword));
		map.put("user_id", toId);
		int uidx = sqlSession.update("accountMapper.updatePassword", map);
		if(uidx > 0) {
			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
				return "succ";
			} catch (Exception ex) {
				return "fail";
			}
		} else {
			return "fail";
		}
	}


	/**
	 * 기관 선택 Layer
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "schoolSelects")
	public String schoolSelect(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("loginController.schoolSelect !!");

		CommonMap map = new CommonMap(request);
		// 지역
		map.put("code_set", "area");
		model.addAttribute("areaList", sqlSession.selectList("valueMapper.codeList", map));
		// 구분
		map.put("code_set", "schorg");
		model.addAttribute("schList", sqlSession.selectList("valueMapper.codeList", map));
		// 학교 기관
		model.addAttribute("schorgList", sqlSession.selectList("joinMapper.schorgList2"));

		return "common/cmm/login/loginLayer";
	}


	/**
	 * 로그아웃
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "logOut")
	public String logOut(HttpSession session) throws Exception {
		logger.info("loginController.logOut !!");

		session.removeAttribute("adm");
		session.removeAttribute("user");

		return "redirect:/index";
	}

}