package com.eduCATs.common.cmm.join;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class member extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(member.class);

	/**
	 * 사용자 로그인 체크
	 * @param request
	 * @param response
	 * @param handler
	 * @return
	 * @throws Exception
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		logger.info("member.preHandle !!");

		HttpSession session = request.getSession();
		loginVo vo = (loginVo)session.getAttribute("user");
		if(vo == null){
			response.sendRedirect(request.getContextPath() + "/login");
			return false;
		}

		return true;
	}

}