package com.eduCATs.common.cmm.join;

import java.io.Serializable;

@SuppressWarnings("serial")
public class loginVo implements Serializable{

	/** 사용자PK */
	private String seq_user;
	/** 아이디 */
	private String user_id;
	/** 이름 */
	private String user_nm;
	/** 학교/기관 */
	private String schorg_id;
	/** 사용자구분 */
	private String user_typ_cd;
	/** 개인회원학교급 */
	private String schlvl_cd;
	/** 학년도 */
	private String st_year_cd;
	/** 학년 */
	private String st_grade_cd;
	/** 반 */
	private String st_class;
	/** 번호 */
	private String st_number_Cd;
	/** 학교/기관유형 */
	private String schorg_typ_cd;
	/** 학교/기관명 */
	private String schorg_nm;
	/** 승인여부 */
	private String confm_yn;
	/** 진로라이센스여부 */
	private String SV01YN;
	/** 자기주도라이센스여부 */
	private String SV02YN;
	/** 진로라이센스히스토리여부 */
	private String SV01HISYN;
	/** 자기주도라이센스히스토리여부 */
	private String SV02HISYN;
	/** 개인회원검사회차 */
	private String pri_tme;
	/** 개인회원검사타입 */
	private String pri_typ;

	public String getSeq_user(){
		return seq_user;
	}

	public void setSeq_user(String seq_user){
		this.seq_user = seq_user;
	}

	public String getUser_id(){
		return user_id;
	}

	public void setUser_id(String user_id){
		this.user_id = user_id;
	}

	public String getUser_nm(){
		return user_nm;
	}

	public void setUser_nm(String user_nm){
		this.user_nm = user_nm;
	}

	public String getSchorg_id(){
		return schorg_id;
	}

	public void setSchorg_id(String schorg_id){
		this.schorg_id = schorg_id;
	}

	public String getUser_typ_cd(){
		return user_typ_cd;
	}

	public void setUser_typ_cd(String user_typ_cd){
		this.user_typ_cd = user_typ_cd;
	}

	public String getSchlvl_cd(){
		return schlvl_cd;
	}

	public void setSchlvl_cd(String schlvl_cd){
		this.schlvl_cd = schlvl_cd;
	}

	public String getSt_year_cd(){
		return st_year_cd;
	}

	public void setSt_year_cd(String st_year_cd){
		this.st_year_cd = st_year_cd;
	}

	public String getSt_grade_cd(){
		return st_grade_cd;
	}

	public void setSt_grade_cd(String st_grade_cd){
		this.st_grade_cd = st_grade_cd;
	}

	public String getSt_class(){
		return st_class;
	}

	public void setSt_class(String st_class){
		this.st_class = st_class;
	}

	public String getSt_number_Cd(){
		return st_number_Cd;
	}

	public void setSt_number_Cd(String st_number_Cd){
		this.st_number_Cd = st_number_Cd;
	}

	public String getSchorg_typ_cd(){
		return schorg_typ_cd;
	}

	public void setSchorg_typ_cd(String schorg_typ_cd){
		this.schorg_typ_cd = schorg_typ_cd;
	}

	public String getSchorg_nm(){
		return schorg_nm;
	}

	public void setSchorg_nm(String schorg_nm){
		this.schorg_nm = schorg_nm;
	}

	public String getConfm_yn(){
		return confm_yn;
	}

	public void setConfm_yn(String confm_yn){
		this.confm_yn = confm_yn;
	}

	public String getSV01YN(){
		return SV01YN;
	}

	public void setSV01YN(String SV01YN){
		this.SV01YN = SV01YN;
	}

	public String getSV02YN(){
		return SV02YN;
	}

	public void setSV02YN(String SV02YN){
		this.SV02YN = SV02YN;
	}

	public String getSV01HISYN(){
		return SV01HISYN;
	}

	public void setSV01HISYN(String SV01HISYN){
		this.SV01HISYN = SV01HISYN;
	}

	public String getSV02HISYN(){
		return SV02HISYN;
	}

	public void setSV02HISYN(String SV02HISYN){
		this.SV02HISYN = SV02HISYN;
	}

	public String getPri_tme(){
		return pri_tme;
	}

	public void setPri_tme(String pri_tme){
		this.pri_tme = pri_tme;
	}

	public String getPri_typ(){
		return pri_typ;
	}

	public void setPri_typ(String pri_typ){
		this.pri_typ = pri_typ;
	}

}