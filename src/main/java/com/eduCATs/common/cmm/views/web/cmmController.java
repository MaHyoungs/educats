package com.eduCATs.common.cmm.views.web;

import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class cmmController {

	private final Logger logger = LoggerFactory.getLogger(cmmController.class);

	@Autowired
	private SqlSession sqlSession;

	/**
	 * 일반 메인
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/index")
	public String main(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("cmmController.main !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));

		/* 세미나 신청 */
		// 오전 오후
		map.put("code_set", "AMPM");
		model.addAttribute("ampmList", sqlSession.selectList("valueMapper.codeList", map));
		// 시간
		map.put("code_set", "DAYTME");
		model.addAttribute("dayTmeList", sqlSession.selectList("valueMapper.codeList", map));
		/* 세미나 신청 */

		return "common/cmm/main";
	}

}