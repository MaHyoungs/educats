package com.eduCATs.common.cmm.views.web;

import com.eduCATs.common.cmm.join.loginVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class tmpController{

	/**
	 * Common templat admin head
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/top")
	public String templateMngHead(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "common/mng/layout/top";
	}

	/**
	 * Common templat admin left
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/left")
	public String templateMngLeft(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "common/mng/layout/left";
	}

	/**
	 * Common templat admin bottom
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/bottom")
	public String templateMngBottom(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "common/mng/layout/bottom";
	}

	/**
	 * Common templat admin message
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/message")
	public String templateMngMessage(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "common/mng/layout/layerMessage";
	}

	/**
	 * Common templat header
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/header")
	public String templateHead(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		HttpSession session = request.getSession();
		if(session != null){
			model.addAttribute("user", (loginVo)session.getAttribute("user"));
		}

		return "common/cmm/layout/header";
	}


	/**
	 * Common templat footer
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/footer")
	public String templateBottom(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "common/cmm/layout/footer";
	}


	/**
	 * Common templat PDF area
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/pdfArea")
	public String templatePdf(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
		model.addAttribute("templateImport", "Y");

		return "auland/pdfArea";
	}

}