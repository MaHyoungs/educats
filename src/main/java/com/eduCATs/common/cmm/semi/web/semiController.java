package com.eduCATs.common.cmm.semi.web;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.util.SimpleEmailClient;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("cmm/semi")
public class semiController{

	@Autowired
	SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['salesMail']}")
	private String salesMail;

	@Value("#{prop['salesName']}")
	private String salesName;

	@Value("#{prop['fromMails']}")
	private String fromMails;

	@Value("#{prop['fromNames']}")
	private String fromNames;

	/**
	 * 자기주도학습 세미나 신청
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "semiInsert")
	public String semiInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);

		map.put("semiPhone", aes.encrypt(map.get("semiPhone").toString()));
		int iidx = sqlSession.insert("semiMapper.semiInsert", map);

		if (iidx > 0) {

			String toEmail = salesMail;
			String toName = new String(salesName.getBytes("ISO-8859-1"), "UTF-8");
			String fromMail = fromMails;
			String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
			String subject = "[에듀캣] 자기주도학습세미나 신청 등록 안내";

			StringBuffer message = new StringBuffer();
			message.append("신청일자: ");
			message.append(map.get("semiProDt"));
			message.append("<br/><br/>희망시간: ");
			message.append("AM".equals(map.get("semiAmPm")) ? "오전 " : "오후 ");
			message.append(map.get("semiDayTme"));
			message.append("시");
			message.append("<br/><br/>학교/기관명: ");
			message.append(map.get("semiOrgan"));
			message.append("<br/><br/>담당자명: ");
			message.append(map.get("semiRes"));
			message.append("<br/><br/>연락처: ");
			message.append(map.get("semiPhone"));
			message.append("<br/><br/>설명: ");
			message.append(map.get("semiDescription"));

			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message.toString());
			} catch (Exception ex) {
			}
		}

		return "redirect:/index?msg=S";
	}


	/**
	 * 자기주도학습 세미나 사례
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "seSemiEx")
	public String seSemiEx(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "common/cmm/semi/seSemiEx";
	}

}