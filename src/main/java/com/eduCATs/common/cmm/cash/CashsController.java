package com.eduCATs.common.cmm.cash;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.util.SimpleEmailClient;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Controller
@RequestMapping("cmm/cash")
public class CashsController{

	@Autowired
	SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['salesMail']}")
	private String salesMail;

	@Value("#{prop['salesName']}")
	private String salesName;

	@Value("#{prop['fromMails']}")
	private String fromMails;

	@Value("#{prop['fromNames']}")
	private String fromNames;

	/**
	 * 현금영수증 신청
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "cashInsert")
	public String cashInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		CommonMap map = new CommonMap(request);

		int iidx = sqlSession.insert("cashMapper.cashInsert", map);

		if (iidx > 0) {

			SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat ( "yyyy.MM.dd HH:mm:ss", Locale.KOREA );
			Date currentTime = new Date ( );
			String mTime = mSimpleDateFormat.format (currentTime);

			String toEmail = salesMail;
			String toName = new String(salesName.getBytes("ISO-8859-1"), "UTF-8");
			String fromMail = fromMails;
			String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
			String subject = "[에듀캣] 현금영수증 신청 등록 안내";

			StringBuffer message = new StringBuffer();
			message.append("신청일자: ");
			message.append(mTime);
			message.append("<br/><br/>아이디: ");
			message.append(map.get("cashId"));
			message.append("<br/><br/>입금자명: ");
			message.append(map.get("cashNm"));
			message.append("<br/><br/>거래자구분: ");
			message.append(map.get("cashReceiptTypeTxt"));
			message.append("<br/><br/>현금영수증 신청정보: ");
			message.append(map.get("cashTypeTxt") + "  ");
			message.append(map.get("cashNumber"));

			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message.toString());
			} catch (Exception ex) {
			}
		}

		return "redirect:/index?msg=O";
	}
	}