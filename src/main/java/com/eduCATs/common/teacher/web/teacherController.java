package com.eduCATs.common.teacher.web;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.util.SimpleEmailClient;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/tch")
public class teacherController {

	private final Logger logger = LoggerFactory.getLogger(teacherController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	@Value("#{prop['fromMails']}")
	private String fromMails;

	@Value("#{prop['fromNames']}")
	private String fromNames;

	/**
	 * 참여현황
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinState")
	public String joinState(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("teacherController.joinState !!");

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		if(user.getConfm_yn() == null){
			return "redirect:/index?msg=BT";
		}else if(user.getConfm_yn().equals("N")){
			return "redirect:/index?msg=RT";
		}

		CommonMap map = new CommonMap(request);
/*
		// 현재년도 ( Default )
		if(map.get("searchYear") == null) {
			Calendar c = Calendar.getInstance();
			map.put("searchYear", String.valueOf(c.get(Calendar.YEAR)));
			map.put("st_year", String.valueOf(c.get(Calendar.YEAR)));
		}
*/
		if(user.getSV01YN().equals("N") && user.getSV01HISYN().equals("N")){
			model.addAttribute("sv01y", "N");
		}else{
			model.addAttribute("sv01y", "Y");
		}

		if(user.getSV02YN().equals("N") && user.getSV02HISYN().equals("N")){
			model.addAttribute("sv02y", "N");
		}else{
			model.addAttribute("sv02y", "Y");
		}
		model.addAttribute("map", map);

		// 검사분류 목록
		logger.info("map.survey_typ >>> " + map.get("survey_typ"));
		if(map.get("survey_typ") == null){
			map.put("code_set", "survey_typ");
//			logger.info("검사분류 목록 >>> valueMapper.codeList");
			model.addAttribute("surveyList", sqlSession.selectList("valueMapper.codeList", map));
		}

/*
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
*/

		// 담당학급
//		model.addAttribute("manageClassList", sqlSession.selectList("classMapper.manageClassList", user));
		map.put("seq_user", user.getSeq_user());
		map.put("schorg_id", user.getSchorg_id());
		map.put("schorg_typ_cd", user.getSchorg_typ_cd());
		map.put("searchGroup", user.getSchorg_typ_cd());
		map.put("st_class", map.get("searchSeqClass"));
//		logger.info("map.searchYear >>> " + map.get("searchYear"));
		if(map.get("searchYear") != null ){
			map.put("st_year", map.get("searchYear"));
//			logger.info("담당학급 >>> classMapper.yearToClass");
			model.addAttribute("yearToClass", sqlSession.selectList("classMapper.yearToClass", map));
		}

		// 연차
//		logger.info("map.searchTyp >>> " + map.get("searchTyp"));
		if(map.get("searchTyp") != null){
			map.put("survey_typ_cd", map.get("searchTyp"));
//			logger.info("연차 >>> classMapper.surveyToLic");
			model.addAttribute("licList", sqlSession.selectList("classMapper.surveyToLic", map));
		}

//		logger.info("map.schorg_typ_cd >>> " + map.get("schorg_typ_cd"));
//		logger.info("map.st_class >>> " + map.get("st_class"));
//		logger.info("학급존재 >>> classMapper.classDetail");
		CommonMap classInfo = (CommonMap)sqlSession.selectOne("classMapper.classDetail", map);
		model.addAttribute("classInfo", classInfo);

		// 참여현황
		if(classInfo != null){
			if(map.get("searchTme") == null ){
				// 검사회차 max
				map.put("user_id",user.getUser_id());
//				logger.info("검사회차 max >>> planMapper.schorgMaxtme");
				String maxtme = (String)sqlSession.selectOne("planMapper.schorgMaxtme", map);
				map.put("searchTme", maxtme);
			}

//			logger.info("참여현황 통계 >>> planMapper.joinsDetail3");
			List<CommonMap> list = sqlSession.selectList("planMapper.joinsDetail3", map);
			model.addAttribute("classJoinState", list);
			model.addAttribute("classJoinStateCnt", list.size());
			if(map.get("currentPage") == null){
				map.put("currentPage", "1");
			}
			request.setAttribute("currentPage", map.get("currentPage"));
//			logger.info("참여현황 총수량 >>> planMapper.classListCnt");
			request.setAttribute("totalRecordCount", sqlSession.selectOne("planMapper.classListCnt", map));
			request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
			map.put("recordsPerPage", Integer.parseInt(comPages));

//			logger.info("참여현황 목록 >>> planMapper.classList");
			model.addAttribute("classJoinList", sqlSession.selectList("planMapper.classList", map));
		}

		return "common/tch/joinState";
	}


	/**
	 * 검사종류에 따른 연차 목록 Ajax--------------------------
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "surveyToLicAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String surveyToLicAjax(HttpServletRequest request) throws Exception {
		logger.info("teacherController.surveyToLicAjax !!");

		List<CommonMap> list = sqlSession.selectList("classMapper.surveyToLic", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 연차에 따른 학년도 Ajax--------------------------
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "licToYearAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String licToYearAjax(HttpServletRequest request) throws Exception {
		logger.info("teacherController.surveyToLicAjax !!");

		CommonMap lics = (CommonMap)sqlSession.selectOne("classMapper.licToYear", new CommonMap(request));

		return lics.toString();
	}


	/**
	 * 학년도에 따른 담당학급 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "yearToClassAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String yearToClassAjax(HttpServletRequest request) throws Exception {
		List<CommonMap> list = sqlSession.selectList("classMapper.yearToClass", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 학년도에 따른 담당학급 목록 Ajax--------------------------
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classListAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classListAjax(HttpServletRequest request) throws Exception {
		logger.info("teacherController.surveyToLicAjax !!");

		List<CommonMap> list = sqlSession.selectList("classMapper.yearToClass", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 학년도에 따른 학급 목록 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classListAjax2", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classListAjax2(HttpServletRequest request) throws Exception {
		List<CommonMap> list = sqlSession.selectList("classMapper.gradeToClass", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 학년도에 따른 담당학급 Ajax ( 신청리스트 추출 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classSelectAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classSelectAjax(HttpServletRequest request) throws Exception {
		List<CommonMap> list = sqlSession.selectList("classMapper.manageClassList2", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 학생관리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "usrManage")
	public String usrManage(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		if(user.getConfm_yn() == null) {
			return "redirect:/index?msg=BT";
		}else if(user.getConfm_yn().equals("N")) {
			return "redirect:/index?msg=RT";
		}
		CommonMap map = new CommonMap(request);
		// 현재년도 ( Default )
		if(map.get("st_year") == null) {
			Calendar c = Calendar.getInstance();
			map.put("st_year", String.valueOf(c.get(Calendar.YEAR)));
		}
		model.addAttribute("map", map);
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 담당학급
		map.put("seq_user", user.getSeq_user());
		map.put("schorg_id", user.getSchorg_id());
		map.put("schorg_typ_cd", user.getSchorg_typ_cd());
		model.addAttribute("yearToClass", sqlSession.selectList("classMapper.yearToClass", map));
//		model.addAttribute("manageClassList", sqlSession.selectList("classMapper.yearToClass", map));

		if(map.get("st_year") != null && map.get("st_class") != null && !map.get("st_class").equals("a")) {
			CommonMap classInfo = (CommonMap)sqlSession.selectOne("classMapper.classDetail", map);
			map.put("schorg_id", classInfo.get("schorg_id"));
			map.put("st_grade_cd", classInfo.get("st_grade_cd"));
			model.addAttribute("classInfo", classInfo);

			if(map.get("currentPage") == null){
				map.put("currentPage", "1");
			}
			request.setAttribute("currentPage", map.get("currentPage"));
			request.setAttribute("totalRecordCount", sqlSession.selectOne("classMapper.classUserListCnt", map));
			request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
			map.put("recordsPerPage", Integer.parseInt(comPages));
			model.addAttribute("classUserList", sqlSession.selectList("classMapper.classUserList", map));
		}else {

		}
		return "common/tch/usrManage";
	}


	/**
	 * 학생관리 > 승인/반려 처리 Ajax
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "usrUpdateAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String usrUpdate(HttpServletRequest request, ModelMap model) throws Exception {
		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}

		CommonMap map = new CommonMap(request);

		// 라이선스 존재여부 및 라이선스 학생수 체크
		if ("Y".equals(map.get("typ")) || "03".equals(map.get("typ"))){
			CommonMap licConfim = (CommonMap)sqlSession.selectOne("licMapper.licConfirm", user);

			if(licConfim == null){
				return "nolic";
			}else if(licConfim.get("ok_yn").equals("N")){
				return "over";
			}
		}

		// 승인, 반려에 따른 이메일발송처리
		String stdId = (String) sqlSession.selectOne("accountMapper.stdIdSelect", map);
		String toEmail = stdId;
		String toName = "";
		String fromMail = fromMails;
		String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
		String subject = "";
		String message = "";

		CommonMap getCla = (CommonMap)sqlSession.selectOne("classMapper.getClass", map);
		map.put("seq_class", getCla.get("seq_class"));

		int nUpt = 0;

		//최초가입
		if (map.get("usTp").equals("R")) {
			nUpt = sqlSession.update("classMapper.usrUpdate", map);
			if (map.get("typ").equals("Y")) {
				// 라이센스 총 학생 수 판단

				int hidx = sqlSession.insert("classMapper.insertnextYearClass", map);
				int iidx = sqlSession.insert("classMapper.insertClassStudent", map);
			}

			//신청
		} else {
			// 승인
			if(map.get("typ").equals("03")) {
				// 라이센스 총 학생 수 판단

				int uidx = sqlSession.update("classMapper.updateUserClass", map);
				int iidx = sqlSession.insert("classMapper.insertClassStudent", map);
			}
			nUpt = sqlSession.update("classMapper.nextYearStat", map);
		}

		String retValue = "fail";
		if (nUpt > 0) { // 업데이트가 실행된 경우에만 이메일 발송 처리
			if ("Y".equals(map.get("typ")) || "03".equals(map.get("typ"))) {
				// 이용신청 승인 이메일 제목, 내용 작성
				subject = "[에듀캣] 서비스 이용 승인 안내";
				message = "에듀캣 서비스 이용 신청이 승인되었습니다.<br/>로그인하여 모든 서비스를 이용하실 수 있습니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
				retValue = "confirm";
			} else if ("N".equals(map.get("typ")) || "02".equals(map.get("typ"))) {
				// 이용신청 반려 이메일 제목, 내용 작성
				subject = "[에듀캣] 서비스 이용 신청 반려 안내";
				message = "에듀캣 서비스 이용 신청이 반려되었습니다.<br/>신청시 입력한 정보를 다시 확인하시고, 담당 선생님에게 문의하시기 바랍니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
				retValue = "deny";
			}

			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
			} catch (Exception ex) {

			}
		}

		return retValue;
	}


	/**
	 * 내정보관리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "info")
	public String tchInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			Calendar c = Calendar.getInstance();
			user.setSt_year_cd(String.valueOf(c.get(Calendar.YEAR)));
			model.addAttribute("user", user);
		}
		model.addAttribute("msg", request.getParameter("msg"));


		// 담당학급
		model.addAttribute("manageClassList", sqlSession.selectList("classMapper.manageClassList", user));
		// 비밀번호
		CommonMap pas = (CommonMap)sqlSession.selectOne("classMapper.getPassword", user);
		pas.put("password", aes.decrypt(pas.get("password").toString()));
		model.addAttribute("pas", pas);

		return "common/tch/info";
	}


	/**
	 * 내정보관리 > 수정 > 비밀번호 확인
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "passwordChk", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String passwordChk(HttpServletRequest request) throws Exception {
		CommonMap map = new CommonMap(request);
		map.put("chkPassword", aes.encrypt(map.get("chkPassword").toString()));
		int cidx = Integer.parseInt(sqlSession.selectOne("accountMapper.passwordChk", map).toString());
		if(cidx > 0) {
			return "Y";
		} else{
			return "N";
		}
	}


	/**
	 * 내정보관리 > 수정
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "infoUpdate")
	public String infoUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null) {
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap info = new CommonMap(request);
		info.put("new_password", aes.encrypt(info.get("new_password").toString()));
		int uidx = sqlSession.update("classMapper.updateTch", info);

		return "redirect:/tch/info?msg=Y";
	}


	/**
	 * 내정보관리 현재 학급 수정 및, 다음 학급 재신청 및, 다음 학년도 학급 신청 Ajax
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchUpdateAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String tchUpdateAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception{

		HttpSession session = request.getSession();
		loginVo user = new loginVo();
		if(session != null){
			user = (loginVo)session.getAttribute("user");
			model.addAttribute("user", user);
		}
		CommonMap map = new CommonMap(request);
		map.put("user_id", user.getUser_id());
		int yea = Integer.parseInt(map.get("userYea").toString()); // 다음학년도

		// 현재 담당 학급 수정요청
		if(map.get("subtyp").equals("C"))
		{
			// 최초 학급 수정
			if(map.get("userTyp").equals("N")) {
				int uidx = sqlSession.update("classMapper.updateUserClass", map);
				int aidx = sqlSession.update("classMapper.updateUserConfm", map);
				return "up";
				// 기존 학급 수정
			} else {
				int xidx = sqlSession.update("classMapper.cmNextYearClass", map);
				return "up";
			}
		}
		// 다음 담당 학급 재신청
		else if(map.get("subtyp").equals("N"))
		{
			int cidx = Integer.parseInt(sqlSession.selectOne("classMapper.submitChk", map).toString());
			if(cidx > 0) {
				return "duple";
			} else {
				map.put("userYea", yea);
				int xidx = sqlSession.update("classMapper.cmNextYearClass", map);
				return "up";
			}
			// 다음 학년도 학급 신청
		}
		else
		{
			// 기존에 등록했는지 판단
			int cidx = Integer.parseInt(sqlSession.selectOne("classMapper.submitChk", map).toString());
			if(cidx > 0) {
				return "duple";
			} else {
				map.put("userYea", yea);
				int iidx = sqlSession.insert("classMapper.submitClass", map);
				return "up";
			}
		}
	}


	/**
	 * 내정보관리 다음 학년도 학급신청 삭제 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchDeleteAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String tchDeleteAjax(HttpServletRequest request) throws Exception {
		int didx = sqlSession.delete("classMapper.deleteNextClass", new CommonMap(request));
		if(didx > 0) {
			return "succ";
		} else {
			return "fail";
		}
	}

}