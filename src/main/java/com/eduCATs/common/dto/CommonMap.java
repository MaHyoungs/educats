package com.eduCATs.common.dto;

import oracle.sql.CLOB;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.NumberFormat;
import java.util.*;

public class CommonMap extends HashMap {

	private static final long serialVersionUID = 1795122814117884560L;

	private NumberFormat nf = NumberFormat.getInstance();

	private NumberFormat cf = NumberFormat.getCurrencyInstance(Locale.KOREA);

	private static final Logger logger = LoggerFactory.getLogger(CommonMap.class);

	private boolean isSet = false;

	public CommonMap() {
	}

	public CommonMap(int arg0) {
		super(arg0);
	}

	public CommonMap(int arg0, float arg1) {
		super(arg0, arg1);
	}

	public CommonMap(HttpServletRequest request) {
		this.addAll(request.getParameterMap());
	}

	public CommonMap(MultipartHttpServletRequest multi) {
		this.addAll(multi.getParameterMap());
		this.addAll(multi.getFileMap());
	}

	public void set() {
		this.isSet = true;
	}

	public boolean isSet() {
		return this.isSet;
	}

	public void addAll(Map map) {
		if(map == null){
			return;
		}
		Iterator i$ = map.entrySet().iterator();
		do{
			if(!i$.hasNext()){
				break;
			}
			Map.Entry entry = (Map.Entry) i$.next();
			Object value = entry.getValue();
			if(value != null){
				Object toadd;
				if(value instanceof String[]){
					String values[] = (String[]) (String[]) value;
					if(values.length > 1){
						toadd = new ArrayList(Arrays.asList(values));
					}else{
						toadd = values[0];
					}
				}else{
					toadd = value;
				}
				super.put(((String) entry.getKey()).toLowerCase(), toadd);
			}
		}while(true);
	}

	public Object get(Object arg0) {
		arg0 = ((String) arg0).toLowerCase();
		String content = null;
		if(super.get(arg0) instanceof oracle.sql.CLOB){
			try{
				CLOB lob_data = (CLOB) super.get(arg0);
				Reader in = lob_data.getCharacterStream();
				Writer sw = new StringWriter();
				char buffer[] = new char[4096];
				int n;
				while((n = in.read(buffer)) != -1)
					sw.write(buffer, 0, n);
				content = sw.toString();
			}catch(Exception ex){
				ex.printStackTrace();
			}
			return content;
		}else{
			return super.get(arg0);
		}
	}

	public Object put(Object arg0, Object arg1){
		arg0 = ((String) arg0).toLowerCase();
		return super.put(arg0, arg1);
	}

}