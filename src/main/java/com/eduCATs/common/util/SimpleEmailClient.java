package com.eduCATs.common.util;

import com.eduCATs.auland.web.auController;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * @author Hades
 *
 */
public class SimpleEmailClient {
	
	private static final Logger logger = LoggerFactory.getLogger(auController.class);

	// 기본정보 설정
	protected String charSet = "utf8";
	protected String hostSMTP = "localhost";

	// 수신자 정보
	protected String toEmail = "";
	protected String toName = "";

	// 발신자 정보
	protected String fromEmail = "";
	protected String fromName = "";

	protected HtmlEmail email = new HtmlEmail();

	/**
	 * 디폴트 생성자 정의
	 */
	protected SimpleEmailClient() {}


	/**
	 * 사용자정의 생성자 정의
	 * @param toEmail 수신자 이메일주소
	 * @param toName 수신자 이름
	 * @param fromEmail 송신자의 이메일주소
	 * @param fromName 송신자의 이름
	 */
	public SimpleEmailClient(String toEmail, String toName, String fromEmail, String fromName)
	{
		this.toEmail = toEmail;
		this.toName = toName;
		this.fromEmail = fromEmail;
		this.fromName = fromName;
	}


	/**
	 * 메일 발송시 첨부할 HTML 메시지를 작성하는 메소드 정의
	 * @param subject 제목내용
	 * @param htmlContents 본문내용
	 * @throws Exception
	 */
	public void writeHtmlMessage(String subject, String htmlContents)
	{
		try
		{
			email.setDebug(true); // 콘솔창에 송신과정 출력
			email.setCharset(charSet);
			email.setHostName(hostSMTP);

			email.addTo(toEmail, toName, charSet); // 수신자 정보 설정
			email.setFrom(fromEmail, fromName, charSet); // 발신자 정보 설정

			email.setSubject(subject); // 제목 정보 설정

			// HTML 메시지를 작성
			email.setHtmlMsg(htmlContents);

			// HTML 이메일을 지원하지 않는 클라이언트라면 다음 메시지를 출력
			email.setTextMsg("이 메일은 HTML 이메일을 지원하지 않습니다.");
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}


	public void attachFiledata(String filepath) {
		try {
			File f = new File(filepath);
			if (f.exists()) {

				// 첨부할 내용을 EmailAttachment에 저장
				EmailAttachment attachment = new EmailAttachment();
				attachment.setPath(filepath);
				attachment.setDisposition(EmailAttachment.ATTACHMENT);
				attachment.setDescription("");
				attachment.setName(f.getName());

				email.attach(attachment);
			}
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * 이메일을 발송하는 메소드 정의
	 * @return 전송 성공여부
	 */
	public boolean sendEmail()
	{
		boolean bSuccess = true;
		try
		{
			email.send();
		} catch (Exception e)
		{
			bSuccess = false;
			e.printStackTrace();
		}

		return bSuccess;
	}


	/**
	 * @return the toEmail
	 */
	public String getToEmail() {
		return toEmail;
	}


	/**
	 * @param toEmail the toEmail to set
	 */
	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}


	/**
	 * @return the toName
	 */
	public String getToName() {
		return toName;
	}


	/**
	 * @param toName the toName to set
	 */
	public void setToName(String toName) {
		this.toName = toName;
	}


	/**
	 * 파일첨부가 가능한 이메일을 발송한다.
	 * @param fromEmail
	 * @param fromName
	 * @param toEmail
	 * @param toName
	 * @param subject
	 * @param message
	 * @param attachFilepath
	 */
	public static void SimpleMailSend(String fromEmail, String fromName, String toEmail, String toName, String subject, String message, String attachFilepath) {

		SimpleEmailClient ecard = new SimpleEmailClient(toEmail, toName, fromEmail, fromName);

		StringBuffer htmlContents = new StringBuffer();
		htmlContents.append("<HTML>");
		htmlContents.append("<BODY>");
		htmlContents.append(message);
		htmlContents.append("</BODY>");
		htmlContents.append("</HTML>");

		ecard.writeHtmlMessage(subject, htmlContents.toString());

		if (attachFilepath != null)
			ecard.attachFiledata(attachFilepath);

		ecard.sendEmail();
	}


	/**
	 * 고정된 수신이메일계정으로 이메일을 발송한다.
	 * @param toEmail
	 * @param toName
	 * @param subject
	 * @param message
	 */
	public static void EducatMailSend(String toEmail, String toName, String fromMail, String fromName, String subject, String message) throws Exception {

		SimpleEmailClient ecard = new SimpleEmailClient(toEmail, toName, fromMail, fromName);
		
		StringBuffer htmlContents = new StringBuffer();
		htmlContents.append("<HTML>");
		htmlContents.append("<BODY>");
		htmlContents.append(message);
		htmlContents.append("</BODY>");
		htmlContents.append("</HTML>");

		ecard.writeHtmlMessage(subject, htmlContents.toString());
				
		logger.info("SimpleEmailClient.EducatMailSend(toEmail, toName, subject, message)");
		logger.info("toEmail = " + toEmail);
		logger.info("toName = " + toName);
		logger.info("subject = " + subject);
		logger.info("message = " + htmlContents.toString());

		boolean bResult = ecard.sendEmail();
		logger.info("ecard.sendEmail() = " + bResult);
	}

}