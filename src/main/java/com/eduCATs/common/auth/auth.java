package com.eduCATs.common.auth;


import com.eduCATs.common.cmm.join.loginVo;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;

public class auth {

	@Autowired
	SqlSession sqlSession;

	/**
	 * 관리자 권한 체크 ( Y , N )
	 * @param vo
	 * @param usrInfo
	 * @return
	 */
	public String admAuth(adminVo vo, CommonMap usrInfo) {
		// 관리자
		if(vo.getAuth_typ_cd().equals("10"))
		{
			return "Y";
		}
		// 영업사 or 학교/기관
		else
		{
			CommonMap map = new CommonMap();
			usrInfo.put("auth_typ_cd", vo.getAuth_typ_cd());
			usrInfo.put("salescp_id", vo.getSalescp_id());
			usrInfo.put("seq_adm", vo.getSeq_adm());
			map = (CommonMap)sqlSession.selectOne("authMapper.chkAuth", usrInfo);

			return map.get("auth").toString();
		}
	}


	/**
	 * 사용자 권한 체크 ( Y , N )
	 * @param vo
	 * @param usrInfo
	 * @return
	 */
	public String usrAuth(loginVo vo, CommonMap usrInfo) {
		// 학생
		if(vo.getUser_typ_cd().equals("02"))
		{
			if(vo.getUser_id().equals(usrInfo.get("who"))) {
				return "Y";
			} else {
				return "N";
			}
		}
		// 선생님
		else
		{
			return "Y";
		}
	}

}