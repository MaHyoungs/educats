package com.eduCATs.common.mng.layer;

import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/mng/layer")
public class layerController {

	private final Logger logger = LoggerFactory.getLogger(layerController.class);

	@Autowired
	private SqlSession sqlSession;

	/**
	 * 학교/기관 선택 layer ( 검사일정 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "schoolOrgan")
	public String schoolOrgan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.schoolOrgan !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap();
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 구분
		map.put("code_set", "schorg");
		model.addAttribute("schList", sqlSession.selectList("valueMapper.codeList", map));

		return "common/mng/layer/schoolOran";
	}


	/**
	 * 학교/기관 선택 layer ( 학급관리, 선생님관리, 구성원관리 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "schoolOrganB")
	public String schoolOrganB(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.schoolOrganB !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap();
		// 지역
		map.put("code_set", "area");
		model.addAttribute("areaList", sqlSession.selectList("valueMapper.codeList", map));
		// 구분
		map.put("code_set", "schorg");
		model.addAttribute("schList", sqlSession.selectList("valueMapper.codeList", map));

		return "common/mng/layer/schoolOrganB";
	}


	/**
	 * 선생님 등록 layer ( 선생님관리 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherInsert")
	public String teacherInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.teacherInsert !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap();

		return "common/mng/layer/teacherInsert";
	}


	/**
	 * 선생님 선택 Layer
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherSelect")
	public String teacherSelect(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.teacherSelect !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
//		model.addAttribute("teacherSelect", sqlSession.selectList("classMapper.teacherSelect", map));

		return "common/mng/layer/teacherSelect";
	}


	/**
	 * 선생님 상세정보 layer ( 선생님관리 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherDetail")
	public String teacherDetail(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.teacherDetail !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap();

		return "common/mng/layer/teacherDetail";
	}


	/**
	 * 발행대상 선택 layer ( 쿠폰관리 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publisherSelect")
	public String publisherSelect(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("layerController.publisherSelect !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap();

		return "common/mng/layer/choiceSales";
	}

}