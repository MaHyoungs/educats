package com.eduCATs.common.mng.code;

import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/mng/code")
@Controller
public class codeController {

	private final Logger logger = LoggerFactory.getLogger(codeController.class);

	@Autowired
	private SqlSession sqlSession;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "organ")
	public String codeOrgan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.codeOrgan !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		if(!adm.getAuth_typ_cd().equals("10")) {
			// 관리자 아닌경우
			return "redirect:/mng/joins?msg=E";
		}
		CommonMap maps = new CommonMap(request);
		model.addAttribute("msg", maps.get("msg"));

		if(maps.get("currentPage") == null) {
			maps.put("currentPage", "1");
		}
		request.setAttribute("currentPage", maps.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("codeMapper.organCodeCnt"));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		maps.put("recordsPerPage", Integer.parseInt(comPages));

		// 구분
		maps.put("code_set", "schorg");
		model.addAttribute("schorgList", sqlSession.selectList("valueMapper.codeList", maps));
		// 시/도
		maps.put("code_set", "area");
		model.addAttribute("areaList", sqlSession.selectList("valueMapper.codeList", maps));
		// 영업사
		model.addAttribute("salesList", sqlSession.selectList("valueMapper.salesList", maps));
		// 검사
		maps.put("code_set", "survey_typ");
		model.addAttribute("surveyList", sqlSession.selectList("valueMapper.codeList", maps));
		// 학교/기관리스트
		model.addAttribute("organCodeList", sqlSession.selectList("codeMapper.organCodeList", maps));

		return "common/mng/code/organ";
	}


	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 등록
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "insertOrgan")
	public String insertOrgan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.insertOrgan !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap maps = new CommonMap(request);
		maps.put("user_id", adm.getUser_id());

		// 중복 체크
		int cnt = (Integer)sqlSession.selectOne("codeMapper.organCodeChk", maps);
		if(cnt > 0) {
			return "redirect:/mng/code/organ?msg=N";
		}
		// 학교/기관 등록
		int cidx = sqlSession.insert("codeMapper.insertOrganCode", maps);
		// 라이센스&회차 등록
		int lidx = sqlSession.insert("licMapper.insertLicence", maps);

		return "redirect:/mng/code/organ?msg=Y";
	}


	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 수정 Ajax
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "doUpdateOrganAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String doUpdateOrganAjax(HttpServletRequest request) throws Exception {
		logger.info("codeController.doUpdateOrganAjax !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}

		CommonMap maps = new CommonMap(request);
		maps.put("user_id", adm.getUser_id());

		int uidx = sqlSession.update("codeMapper.updateOrganCode", maps);

		if(maps.get("survey_typ_org").equals("SV01")) {
			if(maps.get("survey_typ").equals("SV03")) {
				maps.put("survey_typ", "SV02");
				int lidx = sqlSession.insert("licMapper.insertLicence", maps);
			}
		}
		if(maps.get("survey_typ_org").equals("SV02")) {
			if(maps.get("survey_typ").equals("SV03")) {
				maps.put("survey_typ", "SV01");
				int lidx = sqlSession.insert("licMapper.insertLicence", maps);
			}
		}

		if(uidx > 0){
			CommonMap organCodeInfo = (CommonMap)sqlSession.selectOne("codeMapper.organCodeInfo", maps);
			return organCodeInfo.toString();
		} else {
			return "fail";
		}
	}

	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 수정가능여부 확인 Ajax
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "updateChkOrganAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String updateChkOrganAjax(HttpServletRequest request) throws Exception {
		logger.info("codeController.updateChkOrganAjax !!");

		CommonMap maps = new CommonMap(request);

		int cnt = (Integer)sqlSession.selectOne("codeMapper.organSchorgtypChk", maps);

		if (cnt < 1) {
			return "succ"; // 학교/기관 하위에 개설된 학급이 없어서 구분속성 수정이 가능한 경우
		} else {
			return "fail"; // 학교/기관 하위에 개설된 학급이 없어서 구분속성 수정이 불가능한 경우
		}
	}

	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 아이디 중복여부 확인 Ajax
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "organIdDupChkAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String organIdDupChkAjax(HttpServletRequest request) throws Exception {
		logger.info("codeController.organIdDupChkAjax !!");

		CommonMap maps = new CommonMap(request);

		int cnt = (Integer)sqlSession.selectOne("codeMapper.organCodeChk", maps);

		if (cnt < 1) {
			return "succ"; // 등록하려는 학교/기관코드가 존재하지 않는 경우
		} else {
			return "fail"; // 등록하려는 학교/기관코드가 이미 존재하는 경우
		}
	}

	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 라이선스 등록 여부 Ajax
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "useOrganChkAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String useOrganChkAjax(HttpServletRequest request) throws Exception {
		logger.info("codeController.useOrganChkAjax !!");

		CommonMap maps = new CommonMap(request);

		List<CommonMap> organs = sqlSession.selectList("codeMapper.useOrganChk", maps);
		int cnt = 0;
		for(int i=0; i<organs.size(); i++) {
			if(organs.get(i).get("stat_cd") != null) {
				cnt++;
			} else {
			}
		}
		if (cnt > 0) {
			return "Y"; // 사용중
		} else {
			return "N"; // 미사용중
		}
	}

	/**
	 * 시스템관리 > 코드관리 > 학교/기관코드관리 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "deleteOrgan")
	public String deleteOrgan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.deleteOrgan !!");

		CommonMap maps = new CommonMap(request);

		// 학교/기관코드 삭제
		int uidx = sqlSession.update("codeMapper.deleteOrganCode", maps);
		if (uidx > 0) {
			return "redirect:/mng/code/organ?msg=D";
		} else {
			return "redirect:/mng/code/organ?msg=F";
		}
	}

	/**
	 * 시스템관리 > 코드관리 > 영업사코드관리 목록
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "business")
	public String codeBusiness(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.codeBusiness !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		if(!adm.getAuth_typ_cd().equals("10")) {
			// 관리자 아닌경우
			return "redirect:/mng/joins?msg=E";
		}
		if(!adm.getAuth_typ_cd().equals("10")) {
			// 관리자 아닌경우
			return "redirect:/mng/joins?msg=E";
		}
		CommonMap maps = new CommonMap(request);
		model.addAttribute("msg", maps.get("msg"));

		if(maps.get("currentPage") == null) {
			maps.put("currentPage", "1");
		}
		request.setAttribute("currentPage", maps.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("codeMapper.businessCodeCnt"));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		maps.put("recordsPerPage", Integer.parseInt(comPages));
		model.addAttribute("businessCodeList", sqlSession.selectList("codeMapper.businessCodeList", maps));

		return "common/mng/code/business";
	}


	/**
	 * 시스템관리 > 코드관리 > 영업사코드 등록
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "insertBusiness")
	public String insertBusiness(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.insertBusiness !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap maps = new CommonMap(request);

		// 유효성 체크
		int cnt = (Integer)sqlSession.selectOne("codeMapper.businessCodeChk", maps);
		if(cnt > 0) {
			return "redirect:/mng/code/business?msg=N";
		}

		// 영업사코드 등록
		maps.put("user_id", adm.getUser_id());
		int cidx = sqlSession.insert("codeMapper.insertBusinessCode", maps);

		return "redirect:/mng/code/business?msg=Y";
	}


	/**
	 * 시스템관리 > 코드관리 > 영업사코드 수정 Ajax
	 * @param request
	 * @throws Exception
	 */
	@RequestMapping(value = "doUpdateBusinessAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String doUpdateBusinessAjax(HttpServletRequest request) throws Exception {
		logger.info("codeController.doUpdateBusinessAjax !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}

		CommonMap maps = new CommonMap(request);
		maps.put("user_id", adm.getUser_id());

		int uidx = sqlSession.update("codeMapper.updateBusinessCode", maps);

		if(uidx > 0){
			return "succ";
		} else {
			return "fail";
		}
	}


	/**
	 * 시스템관리 > 코드관리 > 영업사코드 삭제
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "deleteBusiness")
	public String deleteBusiness(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("codeController.deleteBusiness !!");

		CommonMap maps = new CommonMap(request);

		// 영업사코드 삭제
		int uidx = sqlSession.update("codeMapper.deleteBusinessCode", maps);
		if (uidx > 0) {
			return "redirect:/mng/code/business?msg=D";
		} else {
			return "redirect:/mng/code/business?msg=F";
		}
	}

}