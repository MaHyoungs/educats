package com.eduCATs.common.mng.cash;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@RequestMapping("/mng/cash")
@Controller
public class cashController{

	private final Logger logger = LoggerFactory.getLogger(cashController.class);

	@Autowired
	SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 시스템관리 > 현금영수증신청관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "receipt")
	public String receipt(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("cashController.receipt !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("cashMapper.cashListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		model.addAttribute("cashList", sqlSession.selectList("cashMapper.cashList", map));

		return "common/mng/cash/list";
	}


	/**
	 * 시스템관리 > 현금영수증신청관리 > 처리완료
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "receiptComplete", method = RequestMethod.POST)
	public String receiptComplete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("cashController.receiptComplete !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		String val = request.getParameter("valList");
		String[] valList = val.split(",");

		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		try {
			for(int i=0; i<valList.length; i++) {
				CommonMap map = new CommonMap();
				map.put("val", valList[i]);
				list.add(map);
			}
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			insMap.put("upt_id", adm.getUser_id());

			int uidx = sqlSession.update("cashMapper.receiptComplete", insMap);

		} catch(Exception e) {
			System.out.println(e);
		}

		return "redirect:/mng/cash/receipt";
	}
}