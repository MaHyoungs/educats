package com.eduCATs.common.mng.coupon;

import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import sun.misc.BASE64Encoder;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Vector;

@Controller
@RequestMapping("/mng/coupon")
public class couponController{

	private final Logger logger = LoggerFactory.getLogger(couponController.class);

	@Autowired
	SqlSession sqlSession;

	@Value("#{prop['commonPages']}")
	private String comPages;


	/**
	 * 쿠폰회원 참여현황
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joins")
	public String joins(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.joins !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("couponMapper.selectCouponJoinListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		// 학년
		if(map.get("searchSchoolDiv") == null) {
		} else if(map.get("searchSchoolDiv").equals("all")) {
		} else { model.addAttribute("gradeList", sqlSession.selectList("valueMapper.priGradeSelect", map));	}
		model.addAttribute("list", sqlSession.selectList("couponMapper.selectCouponJoinList", map));

		return "common/mng/coupon/joins";
	}


	/**
	 * 쿠폰회원 참여현황 상세정보 Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinsInfoAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String joinsInfoAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.joinsInfoAjax !!");
		CommonMap userInfo = (CommonMap)sqlSession.selectOne("couponMapper.couponUserInfo", new CommonMap(request));
		return userInfo.toString();
	}


	/**
	 * 쿠폰회원 검사이력 Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinsHistoryAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String joinsHistoryAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.joinsHistoryAjax !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> list = sqlSession.selectList("couponMapper.couponSurveyHistoryList", map);
		return list.toString();
	}


	/**
	 * 쿠폰회원 검사이력 페이징 Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinsHistoryCntAjax")
	public String joinsHistoryCntAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.joinsHistoryCntAjax !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map2", map);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("couponMapper.couponSurveyHistoryListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		return "common/mng/paging/couponHisPaging";
	}


	/**
	 * 쿠폰관리
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "list")
	public String list(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.list !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("couponMapper.selectCouponListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		model.addAttribute("couponList", sqlSession.selectList("couponMapper.selectCouponList", map));

		return "common/mng/coupon/list";
	}


	/**
	 * 쿠폰생성 및 관리정책
	 * 1. 쿠폰 1개당 1개의 검사를 할 수 있다.
	 * 2. A 로 시작하면 진로탐색검사, S로 시작하면 자기주도학습역량진단검사 에 해당한다.
	 * 3. 4자리 씩 4칸을 입력하는 방식으로 총 16자리로 구성한다.
	 * 4. 쿠폰의 경우, 검사는 어느 검사든 지정된 횟수에 한해서 검사체험이 가능하다(1개의 쿠폰가입으로 2회 이상 가능).
	 * 5. 쿠폰에 대한 사용기간을 두어서 사용기간 내에만 회원가입 및 검사하기 기능 사용이 가능하다.
	 */
	/**
	 * 쿠폰관리 > 쿠폰발행
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publish")
	public String publish(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.publish !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		List<CommonMap> version = sqlSession.selectList("valueMapper.surveyVersion");

		if(version.get(0).get("survey_typ_cd").equals("SV01")) {
			map.put("oneVersion", version.get(0).get("version"));
			map.put("twoVersion", version.get(1).get("version"));
		} else {
			map.put("oneVersion", version.get(1).get("version"));
			map.put("twoVersion", version.get(0).get("version"));
		}

		CommonMap iidx = (CommonMap)sqlSession.selectOne("couponMapper.insertCouponMaster", map);
		List<CommonMap> cpList = CouponGen(map.get("survey_typ_cd").toString(), Integer.parseInt(map.get("publish_count").toString()));
		CommonMap insMap = new CommonMap();
		insMap.put("list", cpList);
		insMap.put("use_yn", "Y");
		insMap.put("seq_coupon_master", iidx.toString().replace("{=", "").replace("}", ""));

		int cidx = sqlSession.insert("couponMapper.insertCoupon", insMap);

		return "redirect:/mng/coupon/list";
	}


	/**
	 * 쿠폰관리 > 쿠폰발행 > 발행대상 선택 리스트 Layer Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publisherListAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String publisherAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.publisherAjax !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> list = sqlSession.selectList("codeMapper.businessCodeList", map);
		return list.toString();
	}


	/**
	 * 쿠폰관리 > 쿠폰발행 > 발행대상 선택 페이징 Layer Ajax
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publisherListCntAjax")
	public String publisherListCntAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.publisherListCntAjax !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map2", map);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("codeMapper.businessCodeCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		return "common/mng/paging/couponPaging";
	}


	/**
	 * 쿠폰발행 상세내역
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publishHistory")
	public String publishHistory(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.publishHistory !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("couponMapper.selectCouponDetailListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		model.addAttribute("list", sqlSession.selectList("couponMapper.selectCouponDetailList", map));

		return "common/mng/coupon/historyList";
	}


	/**
	 * 쿠폰발행 상세내역 엑셀 다운
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "publishHistoryExcel")
	public String publishHistoryExcel(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("couponController.publishHistoryExcel !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		CommonMap info = (CommonMap)sqlSession.selectOne("couponMapper.couponMasterInfo", map);
		model.addAttribute("company_nm", info.get("company_nm"));
		model.addAttribute("reg_dt", info.get("reg_dt"));
		model.addAttribute("list", sqlSession.selectList("couponMapper.selectCouponDetailListExcel", map));

		return "common/mng/coupon/historyListExcel";
	}


	/**
	 * 검사종류(surveyTyp)에 해당하는 쿠폰을 n개(getMax) 발행하는 메소드
	 * @param surveyTyp 검사종류 - SV01: 진로탐색검사, SV02: 자기주도학습역량진단검사
	 * @param genMax 발행수
	 * @return
	 */
	public static List<CommonMap> CouponGen(String surveyTyp, int genMax) {

		double rand = Math.random(); // 0.0 < rand < 1.0
		long lvalue = (int) (rand * 100000);
		long rvalue = System.currentTimeMillis();
		String prefix = "A";
		if ("SV02".equals(surveyTyp)) prefix = "S";

		Vector cpList = new Vector();

		for (int nCount=1; nCount <= genMax; nCount++)
		{
			try {
				String originalText = String.format("%06d", (lvalue+nCount*nCount)) + String.format("%010d", (rvalue+nCount));
				String key = "educats.co.kr";
				String en = Encrypt(originalText, key);
				String coupon = en.toUpperCase();
				int strLength = coupon.length();
				StringBuffer sb = new StringBuffer();
				for (int t=0; t<strLength; t++) {
					if (coupon.charAt(t) >= 'A' && coupon.charAt(t) <= 'Z' || coupon.charAt(t) >= '0' && coupon.charAt(t) <= '9') {
						sb.append(coupon.charAt(t));
					}
				}
				String cn = sb.toString().substring(0, 15);
				StringBuffer cp = new StringBuffer();
				cp.append(prefix);
				cp.append(cn.substring(0, 3)); // A
				cp.append("-");
				cp.append(cn.substring(3, 7)); // B
				cp.append("-");
				cp.append(cn.substring(7, 11)); // C
				cp.append("-");
				cp.append(cn.substring(11, 15)); // D
				cpList.add(cp.toString());

			} catch (Exception ex) {}
		}
		return cpList;
	}

	public static String Encrypt(String text, String key) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		byte[] keyBytes = new byte[16];
		byte[] b = key.getBytes("UTF-8");
		int len = b.length;
		if (len > keyBytes.length)
			len = keyBytes.length;
		System.arraycopy(b, 0, keyBytes, 0, len);
		SecretKeySpec keySpec = new SecretKeySpec(keyBytes, "AES");
		IvParameterSpec ivSpec = new IvParameterSpec(keyBytes);
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);

		byte[] results = cipher.doFinal(text.getBytes("UTF-8"));
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(results);
	}

}