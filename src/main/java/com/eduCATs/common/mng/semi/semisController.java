package com.eduCATs.common.mng.semi;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/mng/semi")
@Controller
public class semisController {

	private final Logger logger = LoggerFactory.getLogger(semisController.class);

	@Autowired
	SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 시스템관리 > 세미나신청관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "apply")
	public String semiInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("semisController.semiInfo !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		model.addAttribute("proS_dt", map.get("proS_dt"));
		model.addAttribute("proE_dt", map.get("proE_dt"));
		model.addAttribute("proOrgan", map.get("proOrgan"));
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("semiMapper.semiListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> semiList = sqlSession.selectList("semiMapper.semiList", map);
		for(int i=0; i<semiList.size(); i++) {
			semiList.get(i).put("user_cttpc", aes.decrypt(semiList.get(i).get("user_cttpc").toString()));
		}
		model.addAttribute("semiList", semiList);

		return "common/mng/semi/apply";
	}


	/**
	 * 세미나 상세 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "infoDetail", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String infoDetail(HttpServletRequest request) throws Exception{
		logger.info("semisController.infoDetail !!");

		CommonMap info = (CommonMap)sqlSession.selectOne("semiMapper.semiDetail", new CommonMap(request));
//		System.out.println("aaa" + info.get("description").toString());
		info.put("description", info.get("description").toString().replaceAll("\\r\\n", "</br>"));
//		System.out.println(info.get("description").toString());
		info.put("user_cttpc", aes.decrypt(info.get("user_cttpc").toString()));
		return info.toString();
	}

}