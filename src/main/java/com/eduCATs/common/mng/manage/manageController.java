package com.eduCATs.common.mng.manage;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import com.eduCATs.common.util.SimpleEmailClient;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@RequestMapping("/mng/manage")
@Controller
public class manageController {

	private final Logger logger = LoggerFactory.getLogger(manageController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	@Value("#{prop['fromMails']}")
	private String fromMails;

	@Value("#{prop['fromNames']}")
	private String fromNames;

	/**
	 * 학교/기관관리 > 학급관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "class")
	public String manageClass(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.manageClass !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		// 현재년도 기준
		if(map.get("nowYear") == null){
			Calendar c = Calendar.getInstance();
			String nowYear = String.valueOf(c.get(Calendar.YEAR));
			map.put("nowYear", nowYear);
		}else{
			map.put("nowYear", map.get("nowYear"));
		}
		model.addAttribute("map", map);
		map.put("adm_typ_cd", adm.getAuth_typ_cd());
		map.put("adm_salescp_id", adm.getSalescp_id());
		// 학교기관 관리자
		if(adm.getAuth_typ_cd().equals("06")) {
			if(map.get("seq_lic") != null){
				return "redirect:/mng/manage/classConfig?seq_lic=" + map.get("seq_lic") + "&org_id=" + adm.getSchorg_id() + "&nowYear=" + map.get("nowYear");
			} else {
				return "redirect:/mng/manage/classConfig?org_id=" + adm.getSchorg_id() + "&nowYear=" + map.get("nowYear");
			}
		}
		if(map.get("org_id") != null) {
			if(map.get("seq_lic") != null){
				return "redirect:/mng/manage/classConfig?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id") + "&nowYear=" + map.get("nowYear");
			} else {
				return "redirect:/mng/manage/classConfig?&org_id=" + map.get("org_id") + "&nowYear=" + map.get("nowYear");
			}
		} else {
			if(map.get("currentPage") == null){
				map.put("currentPage", "1");
			}
			request.setAttribute("currentPage", map.get("currentPage"));
			request.setAttribute("totalRecordCount", sqlSession.selectOne("searchMapper.searchOrganBCnt", map));
			request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
			map.put("recordsPerPage", Integer.parseInt(comPages));
			// 지역
			map.put("code_set", "area");
			model.addAttribute("areaList", sqlSession.selectList("valueMapper.codeList", map));
			// 구분
			map.put("code_set", "schorg");
			model.addAttribute("schList", sqlSession.selectList("valueMapper.codeList", map));
			model.addAttribute("organList", sqlSession.selectList("searchMapper.searchOrganB", map));

			return "common/mng/manage/class";
		}
	}


	/**
	 * 학교/기관관리 > 학급관리 > 학급설정
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classConfig")
	public String manageClassConfig(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.manageClassConfig !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);

		model.addAttribute("returnUrl", request.getRequestURI().toString());
		CommonMap schInfo = (CommonMap)sqlSession.selectOne("valueMapper.schInfo", map);
		model.addAttribute("schInfo", schInfo);
		// 현재년도 기준
		if(map.get("nowYear") == null){
			Calendar c = Calendar.getInstance();
			String nowYear = String.valueOf(c.get(Calendar.YEAR));
			schInfo.put("nowYear", nowYear);
			model.addAttribute("nowYear", nowYear);
		}else{
			schInfo.put("nowYear", map.get("nowYear"));
			model.addAttribute("nowYear", map.get("nowYear"));
		}
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 학년
		model.addAttribute("gradeist", sqlSession.selectList("valueMapper.codeList2", schInfo));
		model.addAttribute("classList", sqlSession.selectList("classMapper.classList", schInfo));
		model.addAttribute("classTeacherList", sqlSession.selectList("classMapper.classTeacherList", schInfo));

		return "common/mng/manage/classConfig";
	}


	/**
	 * 학교/기관관리 > 학급관리 > 학급설정 > 학급 변경 가능 여부
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classDupleChk", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classDupleChk(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.classDupleChk !!");

		int didx = Integer.parseInt(sqlSession.selectOne("classMapper.classDupleChk", new CommonMap(request)).toString());
		if(didx > 0) {
			return "D";
		}
		int uidx = Integer.parseInt(sqlSession.selectOne("classMapper.classUseChk", new CommonMap(request)).toString());
		if(uidx > 0) {
			return "U";
		}
		return "N";
	}


	/**
	 * 학교/기관관리 > 학급관리 > 학급설정 > 수정 및 저장
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classConfigSave")
	public String manageClassConfigSave(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.manageClassConfigSave !!");

		CommonMap map = new CommonMap(request);

		String grade = "";
		String[] gradeList = {};
		String orgGrade = "";
		String[] orgGradeList = {};
		if(map.get("orgtyp") != null && !map.get("orgtyp").equals("ORG01")){
			grade = request.getParameter("gradeList");
			gradeList = grade.split(",");
			orgGrade = request.getParameter("orgGradeList");
			orgGradeList = orgGrade.replaceAll("-"," ").split(",");
		}
		String seqClass = request.getParameter("seqClassList");
		String classs = request.getParameter("classList");
		String orgClasss = request.getParameter("orgClassList");
		String tchNum1 = request.getParameter("tchNum1");
		String tchNum2 = request.getParameter("tchNum2");
		String tchNum3 = request.getParameter("tchNum3");
		String[] seqClassList = seqClass.split(",");
		String[] classList = classs.split(",");
		String[] orgClassList = orgClasss.replaceAll("-", " ").split(",");
		String[] tchNums1 = tchNum1.replaceAll("-", " ").split(",");
		String[] tchNums2 = tchNum2.replaceAll("-", " ").split(",");
		String[] tchNums3 = tchNum3.replaceAll("-", " ").split(",");

		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		boolean newYn = false;
		// 신규 학급 추가 및 학급 정보 변경
		for(int i = 0; i < seqClassList.length; i++){
			if(seqClassList[i].equals("new")){
				newYn = true;
				CommonMap info = new CommonMap();
				if(map.get("orgtyp") != null && !map.get("orgtyp").equals("ORG01")){
					info.put("gra", gradeList[i]);
				}
					info.put("cnt", i + 1);
					info.put("cla", classList[i]);
					list.add(info);
			}
		}
		// 학급 저장
		if(newYn){
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			insMap.put("typ", map.get("orgtyp"));
			insMap.put("nowYear", map.get("nowYear"));
			insMap.put("orgId", map.get("org_id"));
			int iidx = sqlSession.insert("classMapper.insertCmClass", insMap);
		}
		// 학급 수정
			for(int j =0; j<seqClassList.length; j++){
				if(!seqClassList[j].equals("new")) {
					CommonMap upInfo = new CommonMap();
					if(map.get("orgtyp") != null && !map.get("orgtyp").equals("ORG01")){
						upInfo.put("st_grade_cd", orgGradeList[j].trim());
					}
					upInfo.put("st_class", orgClassList[j].trim());
					if(map.get("orgtyp") != null && !map.get("orgtyp").equals("ORG01")){
						upInfo.put("new_st_grade_cd", gradeList[j].trim());
					}
					upInfo.put("new_st_class", classList[j].trim());
					upInfo.put("st_year_cd", map.get("nowYear"));
					upInfo.put("schorg_typ_cd", map.get("orgtyp"));
					int uuudx = sqlSession.update("classMapper.updateClass", upInfo);
				}
			}
		// 선생님 저장
		List<CommonMap> clist = sqlSession.selectList("classMapper.selectCmClass", map);
		CommonMap delMap = new CommonMap();
		delMap.put("list", clist);
		// 학급 선생님 삭제
		if(clist.size() != 0){
			int tidx = sqlSession.delete("classMapper.deleteCmClassTeacher", delMap);
		}
		for(int y=0; y<classList.length; y++){
			CommonMap tch = new CommonMap();
			tch.put("seq_class", clist.get(y).get("seq_class"));
			if(tchNums1[y].length() != 1){
				tch.put("tchNumsa", tchNums1[y].trim());
			}else{
				tch.put("tchNumsa", "N");
			}
			if(tchNums2[y].length() != 1){
				tch.put("tchNumsb", tchNums2[y].trim());
			}else{
				tch.put("tchNumsb", "N");
			}
			if(tchNums3[y].length() != 1){
				tch.put("tchNumsc", tchNums3[y].trim());
			}else{
				tch.put("tchNumsc", "N");
			}
			sqlSession.insert("classMapper.insertCmClassTeacher", tch);
		}

		if(map.get("seq_lic") != null) {
			return "redirect:/mng/manage/classConfig?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id") + "&nowYear=" + map.get("nowYear");
		} else {
			return "redirect:/mng/manage/classConfig?org_id=" + map.get("org_id") + "&nowYear=" + map.get("nowYear");
		}
	}


	/**
	 * 학교/기관관리 > 학급관리 > 학급설정 > 사용여부Chk
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classUseChk", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classUseChk(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.classUseChk !!");

		CommonMap map = new CommonMap(request);
		int cidx = Integer.parseInt(sqlSession.selectOne("classMapper.classUseChk", map).toString());
		if(cidx > 0) {
			return "Y";
		} else{
			return "N";
		}
	}


	/**
	 * 학교/기관관리 > 학급관리 > 학급설정 > 삭제
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classDelete", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classDelete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.classDelete !!");

		int didx = sqlSession.delete("classMapper.classDelete", new CommonMap(request));
		System.out.println(didx);
		if(didx > 0) {
			return "Y";
		} else {
			return "N";
		}
	}


	/**
	 * 학교/기관관리 > 선생님관리 > 선생님관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacher")
	public String manageTeacher(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.manageTeacher !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		if(adm.getAuth_typ_cd().equals("06")) {
			map.put("auth_typ_cd", adm.getAuth_typ_cd());
			map.put("org_id", adm.getSchorg_id());
		}
		model.addAttribute("returnUrl", request.getRequestURI().toString());
		CommonMap schInfo = (CommonMap)sqlSession.selectOne("valueMapper.schInfo", map);
		model.addAttribute("schInfo", schInfo);
		// 현재년도 기준
		if(map.get("nowYear") == null){
			Calendar c = Calendar.getInstance();
			String nowYear = String.valueOf(c.get(Calendar.YEAR));
			map.put("nowYear", nowYear);
		}else{
			map.put("nowYear", map.get("nowYear"));
		}
		model.addAttribute("map", map);

		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("accountMapper.teacherListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		if(schInfo != null && schInfo.get("schorg_typ_cd") != null){
			map.put("schorg_typ_cd", schInfo.get("schorg_typ_cd"));
		}
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 학년
		model.addAttribute("gradeist", sqlSession.selectList("valueMapper.codeList2", schInfo));
		// 반
		model.addAttribute("classList", sqlSession.selectList("classMapper.gradeToClass3", map));
		model.addAttribute("teacherList", sqlSession.selectList("accountMapper.teacherList", map));

		return "common/mng/manage/teacher";
	}


	/**
	 * 학교/기관관리 > 선생님관리 > 선생님상세
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherDetailAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String teacherDetailAjax(HttpServletRequest request) throws Exception {
		logger.info("manageController.teacherDetailAjax !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> list = sqlSession.selectList("accountMapper.oneTeacherDetail", map);
		return list.toString();
	}


	/**
	 * 학교/기관관리 > 선생님관리 > 선생님상세 페이징
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherDetailAjaxCnt")
	public String teacherDetailAjaxCnt(HttpServletRequest request, ModelMap model) throws Exception {
		logger.info("manageController.teacherDetailAjaxCnt !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map2", map);

		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("accountMapper.oneTeacherDetailCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		return "common/mng/paging/detailPaging";
	}

	
	/**
	 * 학교/기관관리 > 선생님관리 > 선생님반배정 검사
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "teacherClassMaxChkAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String teacherClassMaxChkAjax(HttpServletRequest request) throws Exception {
		logger.info("manageController.teacherClassMaxChkAjax !!");

		CommonMap map = new CommonMap(request);
		
		List<CommonMap> list = sqlSession.selectList("classMapper.classTeacherCnt", map);
		if(list.size() > 2){
			return "fail";
		} else {
			return "succ";
		}
	}
	

	/**
	 * 학교/기관관리 > 선생님관리 > 선생님등록
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchInsert")
	public String tchInsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.tchInsert !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap map = new CommonMap(request);
		int cidx = Integer.parseInt(sqlSession.selectOne("accountMapper.tchChk", map).toString());
		if(cidx > 0) {
			if(map.get("seq_lic") != null){
				return "redirect:/mng/manage/teacher?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id") + "&msg=N";
			} else {
				return "redirect:/mng/manage/teacher?org_id=" + map.get("org_id") + "&msg=N";
			}
		}
		map.put("reg_id", adm.getUser_id());
		map.put("tchPassword", aes.encrypt(map.get("tchPassword").toString()));
		map.put("tchOrg", map.get("org_id"));
		map.put("tchGra", map.get("tchGrade"));
		map.put("tchCla", map.get("tchClass"));
		map.put("thisYea", "Y");
		CommonMap inf = (CommonMap)sqlSession.selectOne("classMapper.classInfo", map);
		List<CommonMap> list = sqlSession.selectList("classMapper.classTeacherCnt2", inf);
		if(list.size() > 2){
			if(map.get("seq_lic") != null){
				return "redirect:/mng/manage/teacher?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id") + "&msg=F";
			} else {
				return "redirect:/mng/manage/teacher?org_id=" + map.get("org_id") + "&msg=F";
			}
		} else {
			if(inf == null) {
				if(map.get("seq_lic") != null){
					return "redirect:/mng/manage/teacher?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id") + "&msg=E";
				} else {
					return "redirect:/mng/manage/teacher?org_id=" + map.get("org_id") + "&msg=E";
				}
			} else{
				map.put("seq_class", inf.get("seq_class"));

				// 담당 번호 매칭
				if(list.size() == 0){
					map.put("tch_num", "1");
				}else if(list.size() == 1){
					int val = Integer.parseInt(list.get(0).get("tch_num").toString());
					if(val == 1){
						map.put("tch_num", "2");
					}else{
						map.put("tch_num", "1");
					}
				}else{
					String vals = list.get(0).get("tch_num").toString() + "," + list.get(1).get("tch_num").toString();
					if(!vals.contains("1")){
						map.put("tch_num", "1");
					}else{
						if(vals.contains("1") && vals.contains("2")){
							map.put("tch_num", "3");
						}else if(vals.contains("1") && vals.contains("3")){
							map.put("tch_num", "2");
						}else if(vals.contains("2") && vals.contains("3")){
							map.put("tch_num", "1");
						}
					}
				}

				int midx = sqlSession.insert("accountMapper.insertTch", map);
				int uidx = sqlSession.insert("accountMapper.insertTchCm", map);
				CommonMap tchInfs = (CommonMap)sqlSession.selectOne("classMapper.getTchInfo", map);
				map.put("tchseq", tchInfs.get("seq_user"));
				int iidx = sqlSession.insert("classMapper.insertClass", map);
			}
		}

		if(map.get("seq_lic") != null){
			return "redirect:/mng/manage/teacher?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id");
		} else {
			return "redirect:/mng/manage/teacher?org_id=" + map.get("org_id");
		}
	}


	/**
	 * 학교/기관관리 > 선생님관리 > 승인/반려 처리 Ajax
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchUpdateAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String tchUpdateAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.tchUpdateAjax !!");

		CommonMap map = new CommonMap(request);
		
		// 승인, 반려에 따른 이메일발송처리
		String tchId = (String)sqlSession.selectOne("accountMapper.tchIdSelect", map);
		String toEmail = tchId;
		String toName = "";
		String fromMail = fromMails;
		String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
		String subject = "";
		String message = "";
		
		// 반려
		if(map.get("tchCon").equals("N")) {
			if(map.get("tchTyp").equals("N")){
				int uidx = sqlSession.update("accountMapper.tchUpdate", map);
			}else {
				map.put("tchCon", "02");
				int oidx = sqlSession.update("accountMapper.nextYearStatCd", map);
			}
			// 이용신청 반려 이메일 발송
			subject = "[에듀캣] 서비스 이용 신청 반려 안내";
			message = "에듀캣 서비스 이용 신청이 반려되었습니다.<br/>신청시 입력한 정보를 다시 확인하시고, 학교/기관 관리자에게 문의하시기 바랍니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
			} catch (Exception ex) {
				
			}
			return "deny";
		// 승인
		} else{
			CommonMap inf = (CommonMap)sqlSession.selectOne("classMapper.classInfo", map);
			List<CommonMap> list = sqlSession.selectList("classMapper.classTeacherCnt", map);
			if(list.size() > 2){
				return "N";
			}else{
				map.put("seq_class", inf.get("seq_class"));

				// 담당 번호 매칭
				if(list.size() == 0){
					map.put("tch_num", "1");
				}else if(list.size() == 1){
					int val = Integer.parseInt(list.get(0).get("tch_num").toString());
					if(val == 1){
						map.put("tch_num", "2");
					}else{
						map.put("tch_num", "1");
					}
				}else{
					String vals = list.get(0).get("tch_num").toString() + "," + list.get(1).get("tch_num").toString();
					if(!vals.contains("1")){
						map.put("tch_num", "1");
					}else{
						if(vals.contains("1") && vals.contains("2")){
							map.put("tch_num", "3");
						}else if(vals.contains("1") && vals.contains("3")){
							map.put("tch_num", "2");
						}else if(vals.contains("2") && vals.contains("3")){
							map.put("tch_num", "1");
						}
					}
				}
				// 신규 가입
				if(map.get("tchTyp").equals("N")){
					int nidx = sqlSession.update("accountMapper.tchUpdate", map);
					int iidx = sqlSession.insert("classMapper.insertClass", map);
					// 기존 신청
				}else if(map.get("tchTyp").equals("H")){
					// 사용자 학년도 && 학급신청이력 && 학급
					map.put("tchCon", "03");
					int sidx = sqlSession.update("accountMapper.updateStYear", map);
					int bidx = sqlSession.update("accountMapper.nextYearStatCd", map);
					int uidx = sqlSession.insert("classMapper.insertClass", map);
				}
			}
			// 이용신청 승인 이메일 발송
			subject = "[에듀캣] 서비스 이용 승인 안내";
			message = "에듀캣 서비스 이용 신청이 승인되었습니다.<br/>로그인하여 모든 서비스를 이용하실 수 있습니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
			} catch (Exception ex) {
				
			}
			return "confrm";
		}
	}

	/**
	 * 학교/기관관리 > 선생님관리 > 승인/반려 처리2 Ajax
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchUpdateAjax2", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String tchUpdateAjax2(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.tchUpdateAjax2 !!");

		CommonMap map = new CommonMap(request);

		// 승인, 반려에 따른 이메일발송처리
		String tchId = (String)sqlSession.selectOne("accountMapper.tchIdSelect", map);
		String toEmail = tchId;
		String toName = "";
		String fromMail = fromMails;
		String fromName = new String(fromNames.getBytes("ISO-8859-1"), "UTF-8");
		String subject = "";
		String message = "";

		// 반려
		if(map.get("tchCon").equals("N")) {
			if(map.get("tchTyp").equals("N")){
				int uidx = sqlSession.update("accountMapper.tchUpdate", map);
			}else {
				map.put("tchCon", "02");
				int oidx = sqlSession.update("accountMapper.nextYearStatCd", map);
			}
			// 이용신청 반려 이메일 발송
			subject = "[에듀캣] 서비스 이용 신청 반려 안내";
			message = "에듀캣 서비스 이용 신청이 반려되었습니다.<br/>신청시 입력한 정보를 다시 확인하시고, 학교/기관 관리자에게 문의하시기 바랍니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
			} catch (Exception ex) {

			}
			return "deny";
			// 승인
		} else{
			CommonMap inf = (CommonMap)sqlSession.selectOne("classMapper.classInfo", map);
			List<CommonMap> list = sqlSession.selectList("classMapper.classTeacherCnt", map);
			if(list.size() > 2){
				return "N";
			}else{
				map.put("seq_class", inf.get("seq_class"));

				// 담당 번호 매칭
				if(list.size() == 0){
					map.put("tch_num", "1");
				}else if(list.size() == 1){
					int val = Integer.parseInt(list.get(0).get("tch_num").toString());
					if(val == 1){
						map.put("tch_num", "2");
					}else{
						map.put("tch_num", "1");
					}
				}else{
					String vals = list.get(0).get("tch_num").toString() + "," + list.get(1).get("tch_num").toString();
					if(!vals.contains("1")){
						map.put("tch_num", "1");
					}else{
						if(vals.contains("1") && vals.contains("2")){
							map.put("tch_num", "3");
						}else if(vals.contains("1") && vals.contains("3")){
							map.put("tch_num", "2");
						}else if(vals.contains("2") && vals.contains("3")){
							map.put("tch_num", "1");
						}
					}
				}
				// 신규 가입
				if(map.get("tchTyp").equals("N")){
					int nidx = sqlSession.update("accountMapper.tchUpdate", map);
					int iidx = sqlSession.insert("classMapper.insertClass", map);
					// 기존 신청
				}else if(map.get("tchTyp").equals("H")){
					// 사용자 학년도 && 학급신청이력 && 학급
					map.put("tchCon", "03");
					int sidx = sqlSession.update("accountMapper.updateStYear", map);
					int bidx = sqlSession.update("accountMapper.nextYearStatCd", map);
					int uidx = sqlSession.insert("classMapper.insertClass", map);
				}
			}
			// 이용신청 승인 이메일 발송
			subject = "[에듀캣] 서비스 이용 승인 안내";
			message = "에듀캣 서비스 이용 신청이 승인되었습니다.<br/>로그인하여 모든 서비스를 이용하실 수 있습니다.<br/><br/><br/>* 에듀캣 바로가기 <a href='http://www.educats.co.kr/' target='_blank'>www.educats.co.kr</a>";
			try {
				SimpleEmailClient.EducatMailSend(toEmail, toName, fromMail, fromName, subject, message);
			} catch (Exception ex) {

			}
			return "confrm";
		}
	}

	/**
	 * 학교/기관관리 > 선생님관리 > 선생님삭제
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchDelete")
	public String tchDelete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.tchDelete !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap map = new CommonMap(request);
		System.out.println(map);

		int diex = sqlSession.delete("classMapper.classTeacherDelete", map);
		int uidx = sqlSession.update("accountMapper.deleteAccount2", map);

		if(map.get("seq_lic") != null){
			return "redirect:/mng/manage/teacher?seq_lic=" + map.get("seq_lic") + "&org_id=" + map.get("org_id");
		} else {
			return "redirect:/mng/manage/teacher?org_id=" + map.get("org_id");
		}
	}

	/**
	 * 학교/기관관리 > 검사일정관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "plan")
	public String managePlan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.managePlan !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		if(adm.getAuth_typ_cd().equals("06")) {
			map.put("org_id", adm.getSchorg_id());
		}
		if(map.get("nowYear") == null){
			Calendar c = Calendar.getInstance();
			String nowYear = String.valueOf(c.get(Calendar.YEAR));
			map.put("nowYear", nowYear);
		}else{
			map.put("nowYear", map.get("nowYear"));
		}
		// 학교/기관이 선택 되어 있을 경우
		// 라이선스 선택 안되어있을 경우
		// 학교관리자일 경우 라이선스가 1개이고 라이선스 회차가 1개일 경우 팝업 없이 진입
		// 학교 기관이 선택되어 있을 경우 라이선스가 1개이고 라이선스 회차가 1개일 경우 팝업
		if(map.get("org_id") != null) {
			List<CommonMap> licInfo = sqlSession.selectList("licMapper.licChk", map);
			if(licInfo.size() == 1) {
				map.put("seq_lic", licInfo.get(0).get("seq_lic"));
			}
		}

		if(map.get("seq_lic") != null){
			if(!map.get("seq_lic").equals("null")) {
				if(!map.get("seq_lic").equals("")) {
					CommonMap schInfo = (CommonMap)sqlSession.selectOne("licMapper.licDetail", map);
					map.put("seq_lic", schInfo.get("seq_lic"));
					model.addAttribute("schInfo", schInfo);
					model.addAttribute("tmeList", sqlSession.selectList("planMapper.tmeList", map));
				}
			}
		}
		model.addAttribute("map", map);

		model.addAttribute("returnUrl", request.getRequestURI().toString());
		map.put("code_set", "proc_typ");
		model.addAttribute("procList", sqlSession.selectList("valueMapper.codeList", map));

		return "common/mng/manage/plan";
	}


	/**
	 * 학교/기관관리 > 검사일정관리 > 검사기간 설정
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "updatePlan")
	public String updatePlan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.updatePlan !!");

		CommonMap map = new CommonMap(request);
		String[] seq_tme = map.get("seq_tme").toString().replace("[", "").replace("]","").replace(" ", "").split(",");
		String[] stat_cd = map.get("proc_typ").toString().replace("[", "").replace("]", "").replace(" ", "").split(",");
		String[] tmeS_dt = map.get("tmeS_dt").toString().replace("[", "").replace("]", "").replace(" ", "").split(",");
		String[] tmeE_dt = map.get("tmeE_dt").toString().replace("[","").replace("]", "").replace(" ", "").split(",");

		try{
			for(int i = 0; i < seq_tme.length; i++){
				CommonMap plans = new CommonMap();
				plans.put("seq_tme", seq_tme[i]);
				plans.put("stat_cd", stat_cd[i]);
				plans.put("tmeS_dt", tmeS_dt[i]);
				plans.put("tmeE_dt", tmeE_dt[i]);
				int uidx = sqlSession.update("planMapper.updatePlan", plans);
			}
		} catch(Exception e) {
			System.out.println(e);
		}
		return "redirect:/mng/manage/plan?seq_lic=" + map.get("lic_id").toString() + "&org_id=" + map.get("org_id") + "&nowYear=" + map.get("nowYear");
	}


	/**
	 * 학교/기관관리 > 구성원관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "member")
	public String manageMember(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.manageMember !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		if(adm.getAuth_typ_cd().equals("06")) {
			map.put("org_id", adm.getSchorg_id());
		}
		model.addAttribute("returnUrl", request.getRequestURI().toString());
		CommonMap schInfo = (CommonMap)sqlSession.selectOne("valueMapper.schInfo", map);
		model.addAttribute("schInfo", schInfo);
		// 현재년도 기준
		if(map.get("nowYear") == null){
			Calendar c = Calendar.getInstance();
			String nowYear = String.valueOf(c.get(Calendar.YEAR));
			map.put("nowYear", nowYear);
		}else{
			map.put("nowYear", map.get("nowYear"));
		}
		model.addAttribute("map", map);

		if(schInfo != null){
			// 년도
			map.put("code_set", "cm_year");
			model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
			// 학년
			model.addAttribute("gradeist", sqlSession.selectList("valueMapper.codeList2", schInfo));
			// 반
			model.addAttribute("classList", sqlSession.selectList("classMapper.gradeToClass3", map));
			if(map.get("currentPage") == null) {
				map.put("currentPage", "1");
			}
			map.put("schorg_typ_cd", schInfo.get("schorg_typ_cd"));
			request.setAttribute("currentPage", map.get("currentPage"));
			request.setAttribute("totalRecordCount", sqlSession.selectOne("accountMapper.memberListCnt", map));
			request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
			map.put("recordsPerPage", Integer.parseInt(comPages));
			model.addAttribute("memberList", sqlSession.selectList("accountMapper.memberList", map));
		}
		return "common/mng/manage/member";
	}


	/**
	 * 학년 따른 학급리스트 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classListAjaxss", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classListAjaxss(HttpServletRequest request) throws Exception {
		logger.info("manageController.classListAjaxss !!");

		List<CommonMap> list = sqlSession.selectList("classMapper.gradeToClass3", new CommonMap(request));
		String result = list.toString();

		return result;
	}


	/**
	 * 학교/기관관리 > 구성원관리 관리자 계정 체크 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "adminChkAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String adminChkAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.adminChkAjax !!");

		CommonMap map = new CommonMap(request);
		map.put("password", aes.encrypt(map.get("password").toString()));
		int cidx = Integer.parseInt(sqlSession.selectOne("accountMapper.adminChk", map).toString());
		if(cidx > 0) {
			return "Y";
		} else {
			return "N";
		}
	}


	/**
	 * 학교/기관관리 > 구성원관리 비밀번호 재설정 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "passChangeAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String passChangeAjax(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("manageController.passChangeAjax !!");

		CommonMap map = new CommonMap(request);
		map.put("changePassword", aes.encrypt(map.get("changePassword").toString()));
		int uidx = sqlSession.update("accountMapper.updateMem", map);
		if(uidx > 0) {
			return "Y";
		} else {
			return "N";
		}
	}
	
	/**
	 * 특정 학년에 대한 학급정보 Ajax ( 선생님관리 )
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "tchClassSet", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String tchClassSet(HttpServletRequest request) throws Exception {
		logger.info("manageController.tchClassSet !!");

		CommonMap map = new CommonMap(request);
		List<CommonMap> list = sqlSession.selectList("classMapper.gradeToClass", map);
		return list.toString();
	}


	/**
	 * 선생님 학급 추가
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "InsertTchClass", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String InsertTchClass(HttpServletRequest request) throws Exception {
		logger.info("manageController.InsertTchClass !!");

		CommonMap map = new CommonMap(request);
		System.out.println(map);
		// 중복 판단
		int didx = Integer.parseInt(sqlSession.selectOne("classMapper.classUsr", map).toString());
		if(didx > 0) {
			return "D";
		} else {
			// 학급인원
			List<CommonMap> list = sqlSession.selectList("classMapper.classTeacherCnt2", map);
			if(list.size() > 2) {
				return "N";
			} else {
				// 담당 번호 매칭
				if(list.size() == 0){
					map.put("tch_num", "1");
				}else if(list.size() == 1){
					int val = Integer.parseInt(list.get(0).get("tch_num").toString());
					if(val == 1){
						map.put("tch_num", "2");
					}else{
						map.put("tch_num", "1");
					}
				}else{
					String vals = list.get(0).get("tch_num").toString() + "," + list.get(1).get("tch_num").toString();
					if(!vals.contains("1")){
						map.put("tch_num", "1");
					}else{
						if(vals.contains("1") && vals.contains("2")){
							map.put("tch_num", "3");
						}else if(vals.contains("1") && vals.contains("3")){
							map.put("tch_num", "2");
						}else if(vals.contains("2") && vals.contains("3")){
							map.put("tch_num", "1");
						}
					}
				}
				map.put("tchseq", map.get("seq_user"));
				int iidx = sqlSession.insert("classMapper.insertClass", map);
				return "Y";
			}
		}
	}

}