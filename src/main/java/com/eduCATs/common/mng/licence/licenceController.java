package com.eduCATs.common.mng.licence;

import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@RequestMapping("/mng/licence")
@Controller
public class licenceController {

	private final Logger logger = LoggerFactory.getLogger(licenceController.class);

	@Autowired
	private SqlSession sqlSession;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 라이센스관리 > 라이센스정보관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "info")
	public String licenceInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("licenceController.licenceInfo !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap maps = new CommonMap(request);
		model.addAttribute("map", maps);
		maps.put("auth_typ_cd", adm.getAuth_typ_cd());
		maps.put("schorg_id", adm.getSchorg_id());
		maps.put("salescp_id", adm.getSalescp_id());
		model.addAttribute("msg", maps.get("msg"));
		// 검사
		maps.put("code_set", "survey_typ");
		model.addAttribute("surveyList", sqlSession.selectList("valueMapper.codeList", maps));
		if(maps.get("currentPage") == null) {
			maps.put("currentPage", "1");
		}
		request.setAttribute("currentPage", maps.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("licMapper.licList3Cnt", maps));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		maps.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> licList = sqlSession.selectList("licMapper.licList3", maps);
		model.addAttribute("licList", licList);

		if(adm.getAuth_typ_cd().equals("06")) {
			if(licList.size() == 1) {
				return "redirect:/mng/licence/infoDetail?seq_lic=" + licList.get(0).get("seq_lic");
			}
		}
		return "common/mng/licence/info";
	}


	/**
	 * 라이센스관리 > 라이센스상세정보
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "infoDetail")
	public String licenceInfoDetail(HttpServletRequest request, ModelMap model) throws Exception {
		logger.info("licenceController.licenceInfoDetail !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap licDetail = (CommonMap)sqlSession.selectOne("licMapper.licDetail", new CommonMap(request));
		model.addAttribute("licDetail", licDetail);
		model.addAttribute("licLoanInfo", sqlSession.selectOne("licMapper.licLoanInfo", licDetail));
		return "common/mng/licence/infoDetail";
	}

	/**
	 * 라이센스관리 > 라이센스상세정보수정
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "UpdateInfoDetail")
	public String UpdateInfoDetail(HttpServletRequest request) throws Exception {
		logger.info("licenceController.UpdateInfoDetail !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap map = new CommonMap(request);
		map.put("auth_typ_cd", adm.getAuth_typ_cd());
		map.put("upt_id", adm.getUser_id());
		int uidx = sqlSession.update("licMapper.updateLic", map);
		int iidx = sqlSession.insert("licMapper.insertHis", map);
		if(adm.getAuth_typ_cd().equals("10")) {
			if(map.get("stat_cd") == null || map.get("stat_cd") == ""){
//				System.out.println("insert tme>>>");
				map.put("reg_id", adm.getUser_id());
				CommonMap survey = (CommonMap)sqlSession.selectOne("valueMapper.selectSurveyVersion", map);
				map.put("version", survey.get("version"));
				int tidx = sqlSession.insert("planMapper.insertTmeListAdm", map);
			}
		}

		return "redirect:/mng/licence/info";
	}


	/**
	 * 라이센스관리 > 라이센스연장신청 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "licenceApplicationAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String licenceApplicationAjax(HttpServletRequest request, ModelMap model) throws Exception {
		logger.info("licenceController.licenceApplicationAjax !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		CommonMap map = new CommonMap(request);
		map.put("user_id", adm.getUser_id());
		if(adm.getAuth_typ_cd().equals("10")){
			map.put("stat_cd", "03");
		} else {
			map.put("stat_cd", "01");
		}
		map.put("survey_typ", map.get("survey_typ_cd"));
		int iidx = sqlSession.insert("licMapper.insertHis2", map);
		//최초 신청
		if(map.get("laonYn").equals("N")){
			int pidx = sqlSession.insert("licMapper.insertApplicationLicence", map);
			if(adm.getAuth_typ_cd().equals("10")){
				CommonMap survey = (CommonMap)sqlSession.selectOne("valueMapper.selectSurveyVersion", map);
				CommonMap tmeMap = (CommonMap)sqlSession.selectOne("licMapper.tmeChk", map);
				tmeMap.put("survey_typ", map.get("survey_typ_cd"));
				tmeMap.put("reg_id", adm.getUser_id());
				tmeMap.put("start_dt", tmeMap.get("start_dt").toString());
				tmeMap.put("version", survey.get("version"));
				int tidx = sqlSession.insert("planMapper.insertTmeListAdm", tmeMap);
			}
			if(pidx > 0) {
				return "succ";
			} else {
				return "fail";
			}
		//재 신청
		} else {
			int uidx = sqlSession.update("licMapper.updateApplicationLicence", map);
			if(adm.getAuth_typ_cd().equals("10")){
				CommonMap survey = (CommonMap)sqlSession.selectOne("valueMapper.selectSurveyVersion", map);
				CommonMap tmeMap = (CommonMap)sqlSession.selectOne("licMapper.tmeChk", map);
				tmeMap.put("survey_typ", map.get("survey_typ_cd"));
				tmeMap.put("reg_id", adm.getUser_id());
				tmeMap.put("start_dt", tmeMap.get("start_dt").toString());
				tmeMap.put("version", survey.get("version"));
				int tidx = sqlSession.insert("planMapper.insertTmeListAdm", tmeMap);
			}
			if(uidx > 0) {
				return "succ";
			} else {
				return "fail";
			}
		}
	}


	/**
	 * 라이센스관리 > 라이센스이력 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "licenceHistoryAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String licenceHistory(HttpServletRequest request, ModelMap model) throws Exception {
		logger.info("licenceController.licenceHistory !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> list = sqlSession.selectList("licMapper.licHistory", map);

		return list.toString();
	}


	/**
	 * 라이센스관리 > 라이센스이력 Ajax 페이징
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "licenceHistoryCntAjax")
	public String licenceHistoryCntAjax(HttpServletRequest request,  ModelMap model) throws Exception {
		logger.info("licenceController.licenceHistoryCntAjax !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("licMapper.licHistoryCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		return "common/mng/paging/licPaging";
	}


	/**
	 * 라이센스관리 > 연장신청관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loan")
	public String licenceLoan(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("licenceController.licenceLoan !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		if(adm.getAuth_typ_cd().equals("06")) {
			// 관리자 아닌경우
			return "redirect:/mng/joins?msg=E";
		}
		CommonMap maps = new CommonMap(request);
		model.addAttribute("map", maps);

		if(maps.get("currentPage") == null){
			maps.put("currentPage", "1");
		}
		maps.put("auth_typ_cd", adm.getAuth_typ_cd());
		maps.put("salescp_id", adm.getSalescp_id());
		request.setAttribute("currentPage", maps.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("licMapper.licLoanListCnt", maps));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		maps.put("recordsPerPage", Integer.parseInt(comPages));
		// 검사
		maps.put("code_set", "survey_typ");
		model.addAttribute("surveyList", sqlSession.selectList("valueMapper.codeList", maps));
		// 상태
		maps.put("code_set", "confm_typ");
		model.addAttribute("confmList", sqlSession.selectList("valueMapper.codeList", maps));
		// 연장신청 리스트
		model.addAttribute("licLoanList", sqlSession.selectList("licMapper.licLoanList", maps));

		return "common/mng/licence/loan";
	}


	/**
	 * 라이센스관리 > 연장신청관리 > 연장승인 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "licStatUpdate", method = RequestMethod.POST)
	public String licenceLoanUpdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("licenceController.licenceLoanUpdate !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		String val = request.getParameter("valList");
		String typ = request.getParameter("typList");
		String[] valList = val.split(",");
		String[] typList = typ.split(",");
		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		try {
			for(int i=0; i<valList.length; i++) {
				CommonMap map = new CommonMap();
				map.put("val", valList[i]);
				map.put("typ", typList[i]);
				list.add(map);
			}
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			if(request.getParameter("loan_type").equals("Y")){
				insMap.put("loan_type", 'Y');
				insMap.put("reg_id", adm.getUser_id());
				List<CommonMap> version = sqlSession.selectList("valueMapper.surveyVersion");

				if(version.get(0).get("survey_typ_cd").equals("SV01")) {
					insMap.put("oneVersion", version.get(0).get("version"));
					insMap.put("twoVersion", version.get(1).get("version"));
				} else {
					insMap.put("oneVersion", version.get(1).get("version"));
					insMap.put("twoVersion", version.get(0).get("version"));
				}
				int tidx = sqlSession.insert("planMapper.insertTmeList", insMap);

			} else {
				insMap.put("loan_type", 'N');
			}
			insMap.put("upt_id", adm.getUser_id());

			int uidx = sqlSession.update("licMapper.licStatUpdate", insMap);

		} catch(Exception e) {
			System.out.println(e);
		}

		return "redirect:/mng/licence/loan";
	}


	/**
	 * 라이센스관리 > 연장신청관리 > 연장승인 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loanConfrm", method = RequestMethod.POST)
	public void licenceLoanConfrm(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("licenceController.licenceLoanConfrm !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		String val = request.getParameter("valList");
		String[] valList = val.split(",");
		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		try {
			for(int i=0; i<valList.length; i++) {
				CommonMap map = new CommonMap();
				map.put("val", valList[i]);
				list.add(map);
			}
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			insMap.put("loan_type", 'Y');
			insMap.put("upt_id", adm.getUser_id());

//			System.out.println("ttt" + insMap);
			int uidx = sqlSession.update("licMapper.licStatUpdate", insMap);

		} catch(Exception e) {
			System.out.println(e);
		}
		model.addAttribute("msg", "1");
	}


	/**
	 * 라이센스관리 > 연장신청관리 > 반려 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loanReturn", method = RequestMethod.POST)
	public void licenceLoanReturn(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("licenceController.licenceLoanReturn !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
		}
		String val = request.getParameter("valList");
		String[] valList = val.split(",");
		ArrayList<CommonMap> list = new ArrayList<CommonMap>();

		try {
			for(int i=0; i<valList.length; i++) {
				CommonMap map = new CommonMap();
				map.put("val", valList[i]);
				list.add(map);
			}
			CommonMap insMap = new CommonMap();
			insMap.put("list", list);
			insMap.put("loan_type", 'N');
			insMap.put("upt_id", adm.getUser_id());

//			System.out.println("ttt" + insMap);
			int uidx = sqlSession.update("licMapper.licStatUpdate", insMap);

		} catch(Exception e) {
			System.out.println(e);
		}
		model.addAttribute("msg", "1");
	}

}