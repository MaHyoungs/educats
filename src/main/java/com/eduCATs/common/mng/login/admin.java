package com.eduCATs.common.mng.login;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class admin extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(admin.class);

	/**
	 * 관리자 로그인 체크
	 * @param request
	 * @param response
	 * @param handler
	 * @return
	 * @throws Exception
	 */
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		logger.info("admin.preHandle !!");

		HttpSession session = request.getSession();
		adminVo vo = (adminVo)session.getAttribute("adm");
		if(vo == null) {
			response.sendRedirect(request.getContextPath() + "/adm/login");
			return false;
		}

		return true;
	}

}