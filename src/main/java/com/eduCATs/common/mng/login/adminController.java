package com.eduCATs.common.mng.login;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RequestMapping("/adm")
@Controller
public class adminController {

	private final Logger logger = LoggerFactory.getLogger(adminController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	/**
	 * 관리자 로그인 페이지
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/login")
	public String mngLoginView(HttpServletRequest request, ModelMap model) throws Exception {
		logger.info("adminController.mngLoginView !!");
		CommonMap map = new CommonMap(request);
		model.addAttribute("msg", map.get("msg"));
		return "common/mng/login/admLogin";
	}


	/**
	 * 관리자 로그인 처리
	 * @param request
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "loginAction", method = RequestMethod.POST)
	public String mngLoginAction(HttpServletRequest request, HttpSession session) throws Exception {
		logger.info("adminController.mngLoginAction !!");

		CommonMap map = new CommonMap(request);
		String pwd = aes.encrypt(map.get("password").toString());
		map.put("pwd", pwd);

		adminVo adm = (adminVo)sqlSession.selectOne("admMapper.admChk", map);

		if(adm != null){
			session.setAttribute("adm", adm);
			session.removeAttribute("user");
			return "redirect:/mng/joins";
		}

		return "redirect:/adm/login?msg=D";
	}


	/**
	 * 관리자 로그아웃
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "logOut")
	public String mngLogOut(HttpSession session) throws Exception {
		logger.info("adminController.mngLogOut !!");

		session.removeAttribute("adm");

		return "redirect:/adm/login";
	}

}