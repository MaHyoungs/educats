package com.eduCATs.common.mng.login;

import java.io.Serializable;

@SuppressWarnings("serial")
public class adminVo implements Serializable {

	/** 관리자SEQ */
	private String seq_adm;
	/** 아이디 */
	private String user_id;
	/** 이름 */
	private String user_nm;
	/** 관리자권한구분 */
	private String auth_typ_cd;
	/** 휴대폰번호 */
	private String hp_num;
	/** 설명 */
	private String description;
	/** 승인여부 */
	private String confm_yn;
	/** 영업사PK */
	private String salescp_id;
	/** 학교/기관PK */
	private String schorg_id;


	/** 마스터(10) & 영업사(08) 권한이 있을 경우 'Y' 아닐 경우 'N' */
	private String auth_typ_cd_adm_grp;


	public String getSeq_adm(){
		return seq_adm;
	}

	public void setSeq_adm(String seq_adm){
		this.seq_adm = seq_adm;
	}

	public String getUser_id(){
		return user_id;
	}

	public void setUser_id(String user_id){
		this.user_id = user_id;
	}

	public String getUser_nm(){
		return user_nm;
	}

	public void setUser_nm(String user_nm){
		this.user_nm = user_nm;
	}

	public String getAuth_typ_cd(){
		return auth_typ_cd;
	}

	public void setAuth_typ_cd(String auth_typ_cd){
		this.auth_typ_cd = auth_typ_cd;
	}

	public String getHp_num(){
		return hp_num;
	}

	public void setHp_num(String hp_num){
		this.hp_num = hp_num;
	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getConfm_yn(){
		return confm_yn;
	}

	public void setConfm_yn(String confm_yn){
		this.confm_yn = confm_yn;
	}

	public String getSalescp_id(){
		return salescp_id;
	}

	public void setSalescp_id(String salescp_id){
		this.salescp_id = salescp_id;
	}

	public String getSchorg_id(){
		return schorg_id;
	}

	public void setSchorg_id(String schorg_id){
		this.schorg_id = schorg_id;
	}

	public String getAuth_typ_cd_adm_grp() {
		return auth_typ_cd_adm_grp;
	}

	public void setAuth_typ_cd_adm_grp(String auth_typ_cd_adm_grp) {
		this.auth_typ_cd_adm_grp = auth_typ_cd_adm_grp;
	}

}