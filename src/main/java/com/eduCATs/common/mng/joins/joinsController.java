package com.eduCATs.common.mng.joins;

import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Calendar;
import java.util.List;

@Controller
@RequestMapping("/mng")
public class joinsController {

	private final Logger logger = LoggerFactory.getLogger(joinsController.class);

	@Autowired
	SqlSession sqlSession;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 참여현황
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joins")
	public String joins(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.joins !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		model.addAttribute("map", map);
		map.put("auth_typ_cd", adm.getAuth_typ_cd());
		if(adm.getAuth_typ_cd().equals("06")) {
			map.put("schorg_id", adm.getSchorg_id());
			List<CommonMap> list = sqlSession.selectList("licMapper.licList", map);
			if(list.size() == 1) {
				Calendar c = Calendar.getInstance();
				String nowYear = String.valueOf(c.get(Calendar.YEAR));
				map.put("seq_lic", list.get(0).get("survey_typ_cd"));
				return "redirect:/mng/joinsDetail?searchOrg=" + list.get(0).get("schorg_id") + "&searchTyp=" + list.get(0).get("survey_typ_cd") + "&searchTme=all" + "&searchYear=" + nowYear;
			}
		} else if(adm.getAuth_typ_cd().equals("08")) {
			map.put("salescp_id", adm.getSalescp_id());
		}
		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("planMapper.joinsListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		// 검사
		map.put("code_set", "survey_typ");
		model.addAttribute("surveyList", sqlSession.selectList("valueMapper.codeList", map));
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 회차
		map.put("code_set", "cm_tme");
		model.addAttribute("tmeList", sqlSession.selectList("valueMapper.codeList", map));

		model.addAttribute("joinsList", sqlSession.selectList("planMapper.joinsList", map));

		return "common/mng/joins/joins";
	}


	/**
	 * 참여현황 상세보기
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "joinsDetail")
	public String joinsDetail(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.joinsDetail !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		// 검색정보
		model.addAttribute("map", map);
		CommonMap organInfo = (CommonMap)sqlSession.selectOne("valueMapper.organInfo", map);
		model.addAttribute("organInfo", organInfo);
		if(organInfo.get("schorg_typ_cd").equals("ORG01")) {
			map.put("org", "Y");
		} else {
			map.put("org", "N");
		}

		if(map.get("currentPage") == null) {
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("planMapper.joinsDetailCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("yearList", sqlSession.selectList("valueMapper.codeList", map));
		// 회차
		map.put("code_set", "cm_tme");
		model.addAttribute("tmeList", sqlSession.selectList("valueMapper.codeList", map));
		if(map.get("org").equals("N")) {
		// 학년
			map.put("nowYear", map.get("searchYear"));
			model.addAttribute("gradeist", sqlSession.selectList("valueMapper.codeList2", organInfo));
		}
		// 반
		model.addAttribute("classList", sqlSession.selectList("classMapper.classList2", map));
		model.addAttribute("joinsDetail", sqlSession.selectList("planMapper.joinsDetail", map));

		return "common/mng/joins/joinsDetail";
	}


	/**
	 * 참여현황 상세 Layer
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classJoin")
	public String classJoin(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.classJoin !!");

		CommonMap map = new CommonMap(request);
		// 년도
		map.put("code_set", "cm_year");
		model.addAttribute("searchYeaList", sqlSession.selectList("valueMapper.codeList", map));
		// 회차
		map.put("code_set", "cm_tme");
		model.addAttribute("searchtmeList", sqlSession.selectList("valueMapper.codeList", map));

		return "common/mng/joins/classJoinLayer";
	}


	/**
	 * 참여현황 상세 Layer 현황 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classJoinInfo", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classJoinInfo(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.classJoinInfo !!");

		CommonMap map = new CommonMap(request);
		List<CommonMap> infs = sqlSession.selectList("planMapper.joinsDetail2", map);

		return infs.toString();
	}


	/**
	 * 참여현황 상세 Layer 리스트 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classJoinList", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String classJoinList(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.classJoinList !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(comPages));
		List<CommonMap> list = sqlSession.selectList("planMapper.classList", map);
		return list.toString();
	}


	/**
	 * 참여현황 상세 Layer 리스트 페이징 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "classJoinListCnt")
	public String classJoinListCnt(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("joinsController.classJoinListCnt !!");

		CommonMap map = new CommonMap(request);
		model.addAttribute("map2", map);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("planMapper.classListCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		map.put("recordsPerPage", Integer.parseInt(comPages));

		return "common/mng/paging/joinsPaging";
	}

}