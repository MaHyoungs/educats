package com.eduCATs.common.mng.account;

import com.eduCATs.common.auth.aes;
import com.eduCATs.common.dto.CommonMap;
import com.eduCATs.common.mng.login.adminVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@RequestMapping("/mng/account")
@Controller
public class accountController {

	private final Logger logger = LoggerFactory.getLogger(accountController.class);

	@Autowired
	private SqlSession sqlSession;

	@Autowired
	private aes aes;

	@Value("#{prop['commonPages']}")
	private String comPages;

	/**
	 * 시스템관리 > 계정관리 > 계정관리
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "list")
	public String accountList(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("accountController.accountList !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		if(!adm.getAuth_typ_cd().equals("10")) {
			// 관리자 아닌경우
			return "redirect:/mng/joins?msg=E";
		}
		CommonMap maps = new CommonMap(request);
		model.addAttribute("map", maps);

		if(maps.get("currentPage") == null) {
			maps.put("currentPage", "1");
		}
		request.setAttribute("currentPage", maps.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("accountMapper.accountCnt"));
		request.setAttribute("recordsPerPage", Integer.parseInt(comPages));
		maps.put("recordsPerPage", Integer.parseInt(comPages));

		// 권한
		maps.put("code_set", "auth_typ");
		model.addAttribute("authList", sqlSession.selectList("valueMapper.codeList", maps));

		// 권한에 따른 소속
		if(maps.get("searchAuth") != null) {
			maps.put("auth_typ_cd", maps.get("searchAuth"));
			model.addAttribute("organList", sqlSession.selectList("valueMapper.authSelect", maps));
		}
		// 영업사
		model.addAttribute("salesList", sqlSession.selectList("valueMapper.salesList", maps));

		// 계정 리스트
		List<CommonMap> accountList = sqlSession.selectList("accountMapper.accountList", maps);
		for(int i=0; i<accountList.size(); i++) {
			accountList.get(i).put("hp_num", aes.decrypt(accountList.get(i).get("hp_num").toString()));
		}
		model.addAttribute("accountList", accountList);

		return "common/mng/account/list";
	}


	/**
	 * 시스템관리 > 계정관리 > 계정등록
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "insert")
	public String accountinsert(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("accountController.accountinsert !!");

		HttpSession session = request.getSession();
		adminVo adm = new adminVo();
		if(session != null){
			adm = (adminVo)session.getAttribute("adm");
			model.addAttribute("adm", adm);
		}
		CommonMap map = new CommonMap(request);
		map.put("password", aes.encrypt(map.get("password").toString()));
		map.put("hp_num", aes.encrypt(map.get("hp_num").toString()));
		map.put("reg_id", adm.getUser_id());
		int taidx = Integer.parseInt(sqlSession.selectOne("accountMapper.adminChk2", map).toString());
		if(taidx > 0) {
			return "redirect:/mng/account/list?msg=N";
		}

		int aidx = sqlSession.insert("accountMapper.insertMember", map);
		int bidx = sqlSession.insert("accountMapper.insertAdmin", map);
		CommonMap info = (CommonMap)sqlSession.selectOne("accountMapper.cmAdminInfo", map);
		info.put("organ_id", map.get("organ_id"));
		int cidx = sqlSession.insert("accountMapper.insertAdminAuth", info);

		return "redirect:/mng/account/list";
	}


	/**
	 * 시스템관리 > 계정관리 > 계정수정
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "update")
	public String accountupdate(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("accountController.accountupdate !!");

		CommonMap map = new CommonMap(request);
		map.put("hp_num", aes.encrypt(map.get("hp_num").toString()));
		// 정보 & 권한
		int iidx = sqlSession.update("accountMapper.updateAccount", map);
		int aidx = sqlSession.update("accountMapper.updateAuth", map);
		if(!map.get("newPassword").toString().trim().equals("")){
			//비밀번호
			map.put("newPassword", aes.encrypt(map.get("newPassword").toString()));
			int uidx = sqlSession.update("accountMapper.updatePasswordAdm", map);
		}
		return "redirect:/mng/account/list?msg=U";
	}


	/**
	 * 시스템관리 > 계정관리 > 계정삭제
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "delete")
	public String accountdelete(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {
		logger.info("accountController.accountdelete !!");

		int idx = sqlSession.update("accountMapper.deleteAccount", new CommonMap(request));

		return "redirect:/mng/account/list?msg=D";
	}


	/**
	 * 시스템관리 > 계정관리 > 계정상세 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "detail", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String accountDetail(HttpServletRequest request) throws Exception {
		logger.info("accountController.accountDetail !!");

		CommonMap map = (CommonMap)sqlSession.selectOne("accountMapper.accountDetail", new CommonMap(request));
		map.put("password", aes.decrypt(map.get("password").toString()));
		map.put("hp_num", aes.decrypt(map.get("hp_num").toString()));

		return map.toString();
	}


	/**
	 * 권한별 소속 Ajax
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/mng/values/authSelectAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String authSelectAjax(HttpServletRequest request) throws Exception {
		logger.info("accountController.authSelectAjax !!");

		List<CommonMap> list = sqlSession.selectList("valueMapper.authSelect", new CommonMap(request));

		return list.toString();
	}


	/**
	 * 회원ID 중복여부 검사 Ajax (계정등록)
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "checkDupUseridAjax", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String checkDupUseridAjax(HttpServletRequest request) throws Exception {
		logger.info("accountController.checkDupUseridAjax !!");

		CommonMap map = new CommonMap(request);
		
		int chk = (Integer.parseInt(sqlSession.selectOne("joinMapper.joinIdChk", map).toString()));
		if(chk == 0){
			return "succ";
		}else{
			return "fail";
		}
	}

}