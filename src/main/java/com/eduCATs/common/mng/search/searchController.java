package com.eduCATs.common.mng.search;

import com.eduCATs.common.dto.CommonMap;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RequestMapping("/mng/search")
public class searchController {

	private final Logger logger = LoggerFactory.getLogger(searchController.class);

	@Autowired
	private SqlSession sqlSession;

	@Value("#{prop['layerPages']}")
	private String layerPages;

	/**
	 * 학교/기관 검색 Ajax ( 검사일정 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchOrgan", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String searchOrgan(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchOrgan !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(layerPages));
		List<CommonMap> list = sqlSession.selectList("searchMapper.searchOrgan", map);

		return list.toString();
	}


	/**
	 * 학교/기관 검색 Ajax ( 검사일정 ) 페이징
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchOrganCnt")
	public String searchOrganCnt(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchOrganCnt !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("searchMapper.searchOrganCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(layerPages));
		map.put("recordsPerPage", Integer.parseInt(layerPages));

		return "common/mng/paging/layerPaging";
	}



	/**
	 * 학교/기관 검색 Ajax ( 학급관리, 선생님관리, 구성원관리 )
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchOrganB", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String searchOrganB(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchOrganB !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(layerPages));
		List<CommonMap> list = sqlSession.selectList("searchMapper.searchOrganB", map);

		return list.toString();
	}


	/**
	 * 학교/기관 검색 Ajax ( 학급관리, 선생님관리, 구성원관리 ) 페이징
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchOrganBCnt")
	public String searchOrganBCnt(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchOrganBCnt !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("searchMapper.searchOrganBCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(layerPages));
		map.put("recordsPerPage", Integer.parseInt(layerPages));

		return "common/mng/paging/layerPaging";
	}


	/**
	 * 선생님 검색
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchTeacher", produces = "text/html; charset=UTF-8")
	@ResponseBody
	public String searchTeacher(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchTeacher !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null || map.get("currentPage") == ""){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		map.put("recordsPerPage", Integer.parseInt(layerPages));
		List<CommonMap> list = sqlSession.selectList("classMapper.teacherSelect", map);
		
		return list.toString();
	}

	/**
	 * 학급관리 선생님 검색 페이징
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "searchTeacherCnt")
	public String searchTeacherCnt(HttpServletRequest request) throws Exception {
		logger.info("searchController.searchTeacherCnt !!");

		CommonMap map = new CommonMap(request);
		if(map.get("currentPage") == null){
			map.put("currentPage", "1");
		}
		request.setAttribute("currentPage", map.get("currentPage"));
		request.setAttribute("totalRecordCount", sqlSession.selectOne("classMapper.teacherSelectCnt", map));
		request.setAttribute("recordsPerPage", Integer.parseInt(layerPages));
		map.put("recordsPerPage", Integer.parseInt(layerPages));

		return "common/mng/paging/layerPaging";
	}

}