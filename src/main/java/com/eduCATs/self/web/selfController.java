package com.eduCATs.self.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/self")
public class selfController{


	/**
	 * 학생 > 검사하기 > 자기주도학습
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "survey")
	public String survey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/survey";
	}


	/**
	 * 학생 > 검사하기 > 자기주도학습 > 검사 시작(자기주도학습)
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "selfSurvey")
	public String selfSurvey(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/selfSurvey";
	}


	/**
	 * 학생 > 검사결과 > 자기주도학습결과
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "surveyResult")
	public String surveyResult(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/surveyResult";
	}


	/**
	 * 학생 > 검사결과 > 자기주도학습결과 > 자기주도학습결과상세 > 학습심리요인
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "psy")
	public String psy(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/psy";
	}


	/**
	 * 학생 > 검사결과 > 자기주도학습결과 > 자기주도학습결과상세 > 학습전략요인
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "str")
	public String str(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/str";
	}


	/**
	 * 학생 > 검사결과 > 자기주도학습결과 > 자기주도학습결과상세 > 다중지능요인
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "intel")
	public String intel(HttpServletRequest request, HttpServletResponse response, ModelMap model) throws Exception {

		return "self/intel";
	}
}
