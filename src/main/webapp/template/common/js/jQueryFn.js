jQuery(function($){
	/**
	 * 메인 컨텐츠 높이조절
	 **/
	$.fn.fnContentSectionSize = function(){
		var winHei = window.innerHeight;

		return this.each(function(i){
			var  _this = $(this)
				,conHei = _this.height();

			fnConResize();

			function fnConResize(){
				if(winHei < 800){
					winHei = 800;
				}

//				_this.css({"margin-top":((winHei - conHei) / 2)+30});
			}

			$(window).resize(function(){
				winHei = window.innerHeight;
				fnConResize();
			});
		});
	}

	/**
	 * 배경이미지 리사이징
	 * $(target).fnSectionSize(); 로 사용
	 * target = 배경이미지를 감싼 태그
	 **/
	$.fn.fnSectionSize = function(){
//		console.log("#container.width >>> " + $("#container").width());
// 		console.log("#container.Height >>> " + $("#container").height());
//		console.log("document.documentElement.clientHeight >>> " + document.documentElement.clientHeight);
//		console.log("document.body.scrollHeight >>> " + document.body.scrollHeight);
//		console.log("window.innerHeight >>> " + window.innerHeight); // html5 표준
//		console.log("window.innerWidth >>> " + window.innerWidth); // html5 표준

		var winWid = window.innerWidth;
		var winHei = window.innerHeight - 60;

		return this.each(function(i){
			var  bgWrap = $(this)
				,bg = $(this).children(".bg");

			fnBgStart();

			function fnBgStart(){
				$(".bx-viewport").css('height','100%');
				var pagingWinWid = window.innerWidth;
/*
				if($(window).width() < 1280){
					pagingWinWid = 1280;
				}
*/
				$(".bx-pager").css('left', ((pagingWinWid - $(".bx-pager").width())/2));
//				$(".bx-controls-direction").css('width', winWid);
				$(".bx-prev").css('left', "40px");
				$(".bx-next").css('right', "40px");

				bgWrap.css({"width":winWid, "height":winHei, "overflow":"hidden"});
//				bg.css({"width":"100%", "height":"auto"});

				if(bg.width() <= winWid){
					fnBgPosition(true);
				}else{
					fnBgPosition(false);
				}
				if(bg.height() <= winHei){
					fnBgPosition(false);
				}else{
					fnBgPosition(true);
				}
			}

			function fnBgPosition(status){
				if(status){
					bg.css({"width":"100%","height":"auto"});
				}else{
					bg.css({"height":"100%","width":"auto"});
				}

/*
				if(bg.attr("src") == "/template/common/images/main/bg_main01_01.jpg"){
					console.clear();
					console.log("bg.width() // " + bg.width());
					console.log("bg.height() // " + bg.height());
				}
*/

				bg.css({
					 "left" : (winWid - bg.width()) / 2
					,"top" : (winHei - bg.height()) / 2
				});
			}

			$(window).resize(function(){
				winWid = window.innerWidth;
				winHei = window.innerHeight - 60;
				fnBgStart();
			});
		});
	}
});