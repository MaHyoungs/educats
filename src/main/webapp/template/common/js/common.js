/**
 * common.js
 */

/** JSON LIST 형태 변환 */
function jsonListSet(data) {
	var value = data.replace(/=/gi, ':"');
	value = value.replace(/}/gi, '"}');
	value = value.replace(/, /gi, '",');
	value = value.replace(/}",{/gi, '},{');
	return eval(value);
}

/** JSON 형태 변환 */
function jsonSet(data) {
	var value = data.replace(/=/gi, ':"');
	value = value.replace(/{/gi, '[{');
	value = value.replace(/}/gi, '"}]');
	value = value.replace(/, /gi, '",');
	value = value.replace(/}",{/gi, '},{');
	return eval(value);
}

function setCookie(cookieName, value, exdays){
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var cookieValue = escape(value) + ((exdays==null) ? "" : "; expires=" + exdate.toGMTString());
	document.cookie = cookieName + "=" + cookieValue;
}

function deleteCookie(cookieName){
	var expireDate = new Date();
	expireDate.setDate(expireDate.getDate() - 1);
	document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString();
}

function getCookie(cookieName){
	cookieName = cookieName + '=';
	var cookieData = document.cookie;
	var start = cookieData.indexOf(cookieName);
	var cookieValue = '';
	if(start != -1){
		start += cookieName.length;
		var end = cookieData.indexOf(';', start);
		if(end == -1)end = cookieData.length;
		cookieValue = cookieData.substring(start, end);
	}
	return unescape(cookieValue);
}

/** 공백사용 막기 */
function noSpace(obj) {
	var str_space = /\s/;
	if(str_space.exec(obj.value)) {
		fnAlertMessage("해당 항목에는 공백을 사용할수 없습니다.");
		obj.focus();
		obj.value = '';
		return false;
	}
	// onkeyup="noSpaceForm(this);" onchange="noSpaceForm(this);"
}

/** 아이디(이메일) 체크 */
function emailcheck(strValue)
{
	var regExp = /[0-9a-zA-Z][_0-9a-zA-Z-]*@[_0-9a-zA-Z-]+(\.[_0-9a-zA-Z-]+){1,2}$/;
	//입력을 안했으면
	if(strValue.length == 0){
		$('#errorEmail').text("* 이메일을 정확히 입력하세요.");
		return false;
	}
	//이메일 형식에 맞지않으면
	if (!strValue.match(regExp)) {
		$('#errorEmail').text("* 이메일을 정확히 입력하세요.");
		return false;
	}
	$('#errorEmail').text("");
	return true;
}

/** 휴대폰번호 체크 */
function phonecheck(strValue) {
	var regExp = /^01([0|1|6|7|8|9]?)([0-9]{3,4})([0-9]{4})$/;
	//입력을 안했거나 휴대폰번호 형식에 맞지 않으면
	if(strValue.length == 0 || strValue.length < 10 || !strValue.match(regExp)){
		return false;
	}
	
	return true;
}

/** 쿠폰번호 체크 */
function couponcheck(strValue) {
	var regExp = /^[A-Z0-9]{4}$/;
	// 입력을 안했거나 쿠폰번호 형식에 만지 않으면
	if(strValue.length != 4 || !strValue.match(regExp)){
		return false;
	}
	
	return true;
}

/** 숫자만 ( keyup용 ) */
function InpuOnlyNumber(obj)
{
	if (event.keyCode >= 48 && event.keyCode <= 57) { //숫자키만 입력
		return true;
	} else {
		fnAlertMessage("숫자만 입력이 가능합니다.");
		obj.value = '';
		return false;
	}
}

/** SMS인증번호 체크 */
function authnumcheck(strValue) {
	var regExp = /^[0-9]+$/;
	//입력을 안했으면
	if(strValue.length != 6 || !strValue.match(regExp)){
		return false;
	}
	
	return true;
}

/** 비밀번호(영문, 숫자, 특수문자 혼합) 체크 */
function chkPwdAll(str){
	var pw = str;
	var num = pw.search(/[0-9]/g);
	var eng = pw.search(/[a-z]/ig);
	var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);

	if(pw.length < 8 || pw.length > 20){
		fnAlertMessage("8자리 ~ 20자리 이내로 입력해주세요.");
		return false;
	}
	if(pw.search(/₩s/) != -1){
		fnAlertMessage("비밀번호는 공백업이 입력해주세요.");
		return false;
	} if(num < 0 || eng < 0 || spe < 0 ){
		fnAlertMessage("영문,숫자, 특수문자를 혼합하여 입력해주세요.");
		return false;
	}
	return true;
}

/** 비밀번호(영문, 숫자) 체크 */
function chkPwd(str){
	var reg_pwd = /^.*(?=.{6,15})(?=.*[0-9])(?=.*[a-zA-Z]).*$/;
	if(!reg_pwd.test(str)){
		return false;
	}
	return true;
}

/** ENTER 안먹게 하는것 */
function captureReturnKey(e) {
	if(e.keyCode==13 && e.srcElement.type != 'textarea')
		return false;
}

// 한글 + 특수문자 체크(일부 허용)
function checkKey(str) {
	var pattern = /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|<|>|(|)|-|&|*|\*]+$/
	if(pattern.test(str)) {
		return true;
	} else {
		return false;
	}
}