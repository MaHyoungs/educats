var sec_v, min_v, McounterHandler;
function MDisplay(min, sec) {
	var disp = "";
	if( min <= 9 ) disp="0";

	disp += min + ":";
	
	if( sec <= 9 ) disp += "0";		
	disp += sec;

	return(disp);
}

function Mcounter(time_gap) {
	sec_v = time_gap % 60;
	min_v = ( time_gap - sec_v ) / 60;
	McounterZero();
}

function McounterZero() {
	sec_v--;

	if(sec_v == -1) {
		sec_v = 59;
		min_v--;
	}
	
	$('#count').html(MDisplay(min_v, sec_v));

	//남은 시간이 0 이 되었을때 처리부분
	if( ( min_v == 0 ) && ( sec_v == 0 ) ) {
		//...
	}
	else{
		McounterHandler = setTimeout( "McounterZero()", 1000 );
	}
}