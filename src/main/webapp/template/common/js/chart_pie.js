/*
jQuery(function($){
	var chartData = [20, 20, 20, 20, 20]
		,colorData = ["#00dab9", "#e44296", "#507ff4", "#ffae00", "#000000"]
		,chartBarData = [95, 75, 46, 66, 11]
		,chartBarColor = ["#3266a5", "#c76161", "#c79b61", "#31a5a4", "#9f5abb"]
		,chartBarSum = [{0:"인문", 1:"사회", 2:"자연", 3:"공학", 4:"예체능"}
					, {0:"97", 1:"80", 2:"62", 3:"42", 4:"2"}
					, {0:"#3266a5", 1:"#c76161", 2:"#c79b61", 3:"#31a5a4", 4:"#9f5abb"}];
	fnChartPie("#chart_pie", chartData, colorData, 110, 105, 29, 80);
	fnChartTriangle("#chartTriangle", "#f01b7f", 208, 60, 60, 60);
	fnChartTriangle("#chartTriangle", "#13bbee", 208, 100, 100, 100);
	fnChartBar(".chartBar", chartBarData, chartBarColor, chartBarSum);
	/!**
	 * 파이 차트 그리기
	 * @param target = 대상 svg 클래스나 아이디
	 * @param DATA = 차트 데이터
	 * @param colorData = 차트 컬러
	 * @param chartWidth = 차트 가로크기
	 * @param chartHeight = 차트 세로크기
	 * @param innerSize = 안쪽 공백 반지름
	 * @param outterSize = 차트 반지름
	 *!/
	function fnChartPie(target, DATA, colorData, chartWidth, chartHeight, innerSize, outterSize){
		var textStartAngle
			,textEndAngle;

		var canvas = d3.select(target);
		var group = canvas.append("g")
			.attr("transform", "translate("+chartWidth+", "+chartHeight+")");

		var arc = d3.svg.arc()
			.innerRadius(innerSize)
			.outerRadius(outterSize);

		var pie = d3.layout.pie()
			.value(function(d) { return d; });

		var arcs = group.selectAll(".arc")
			.data(pie(DATA))
			.enter()
			.append("g")
			.attr("class", "arc");

		arcs.append("path")
			.attr("d", arc)
			.attr("fill", function (d, i) { return colorData[i]; });

		arcs.append("text")
			.attr("transform", function(d) {
				textStartAngle = arc.centroid(d)[0]*2;
				textEndAngle = arc.centroid(d)[1]*2;
				if(textStartAngle > 0){
					textStartAngle = textStartAngle-10;
				}else if(textStartAngle < 0){
					textStartAngle = textStartAngle+10;
				}
				if(textEndAngle > 0){
					textEndAngle = textEndAngle-10;
				}else if(textEndAngle < 0){
					textEndAngle = textEndAngle+10;
				}
				return "translate(" + textStartAngle + ", " + textEndAngle + ")";
			})
			.attr("text-anchor", "middle")
			.text(function(d) { return d.data+"%"});
	}

	function fnChartBar(_target, _data, _colorData, chartBarSum){
		var x = d3.scale.linear()
			.domain([0, 100])
			.range([0, 200]);

		var color = d3.scale.ordinal()
			.range(d3.values(chartBarSum[2]));

		var chart = d3.select(_target);

		var bar = chart.selectAll("g")
			.data(d3.values(chartBarSum[1]))
			.enter().append("g")
			.attr("transform", function(d, i) { return "translate(40, 0)"; });

		bar.append("rect")
			.attr("width", 40)
			.attr("height", x)
			.attr("y", function(d) { return 209 - x(d); })
			.attr("x", function(d,i){ return 40+i*80; })
			.attr("fill", function(d, i) { return color(i)});

		bar.append("text")
			.attr("width", 40)
			.attr("x", function(d, i) { return 45+i*80; })
			.attr("y", 230)
			.text(function(d, i) { return d3.values(chartBarSum[0])[i]; });
	}

	/!**
	 * 삼각형 차트 그리기
	 * @param _target = 대상 svg 클래스나 아이디
	 * @param _color = 차트 색상
	 * @param _width = svg넓이
	 * @param _top = 위쪽 데이터
	 * @param _left = 왼쪽 데이터
	 * @param _right = 오른쪽 데이터
	 *!/
	function fnChartTriangle(_target, _color, _width, _top, _left, _right){
		/!**
		 * (데이터 / 총데이터 * 100) == 총데이터에 대한 현재 데이터의 퍼센트
		 * (데이터 퍼센트 * 데이터범주 길이 / 100) = 데이터 범주에 대한 입력된 데이터의 길이
		 *!/
		var topX = _width / 2 // 위쪽 X = 삼각형 가운데
			,topY = 117 - (_top / 200 * 100 * 117 / 100)
			,leftX = 105 - (_left / 200 * 100 * 105 / 100)
			,leftY = (_left / 200 * 100 * 63 / 100) + 117
			,rightX = (_right / 200 * 100 * 104 / 100) + 105
			,rightY = (_right / 200 * 100 * 63 / 100) + 117;
		d3.select(_target).append("path")
			.attr("d", "M" + topX + " "+topY + " L" + leftX + " " + leftY + " L" + rightX + " " + rightY + " Z")
			.attr("fill", _color)
			.style("stroke", _color)
			.style("stroke-width", 1)
			.style("fill-opacity", "0.3");
	}

});*/
	/**
	 * 파이 차트 그리기
	 * @param target = 대상 svg 클래스나 아이디
	 * @param DATA = 차트 데이터
	 * @param colorData = 차트 컬러
	 * @param chartWidth = 차트 가로크기
	 * @param chartHeight = 차트 세로크기
	 * @param innerSize = 안쪽 공백 반지름
	 * @param outterSize = 차트 반지름
	 */
	function fnChartPie(target, DATA, colorData, chartWidth, chartHeight, innerSize, outterSize){
		var textStartAngle
			,textEndAngle;

		var canvas = d3.select(target);
		var group = canvas.append("g")
			.attr("transform", "translate("+chartWidth+", "+chartHeight+")");

		var arc = d3.svg.arc()
			.innerRadius(innerSize)
			.outerRadius(outterSize);

		var pie = d3.layout.pie()
			.value(function(d) { return d; });

		var arcs = group.selectAll(".arc")
			.data(pie(DATA))
			.enter()
			.append("g")
			.attr("class", "arc");

		arcs.append("path")
			.attr("d", arc)
			.attr("fill", function (d, i) { return colorData[i]; });

		arcs.append("text")
			.attr("transform", function(d) {
				textStartAngle = arc.centroid(d)[0]*2;
				textEndAngle = arc.centroid(d)[1]*2;
				if(textStartAngle > 0){
					textStartAngle = textStartAngle - 0;
				}else if(textStartAngle < 0){
					textStartAngle = textStartAngle + 0;
				}
				if(textEndAngle > 0){
					textEndAngle = textEndAngle - 10;
				}else if(textEndAngle < 0){
					textEndAngle = textEndAngle + 15;
				}
				return "translate(" + textStartAngle + ", " + textEndAngle + ")";
			})
			.attr("text-anchor", "middle")
			.text(function(d) { return d.data+"%"});
	}
	function fnChartBar(_target, _data, _colorData, chartBarSum){
		var _target = $(_target);
		_target.find(".barWrap").each(function(i){
			$(this).children(".bar").css({height:chartBarSum[1][i]+"%", "background-color":chartBarSum[2][i]});
			$(this).children(".text").text(chartBarSum[0][i]);
		});
	}
/*
	function fnChartBar(_target, _data, _colorData, chartBarSum){
		var x = d3.scale.linear()
			.domain([0, 100])
			.range([0, 200]);

		var color = d3.scale.ordinal()
			.range(d3.values(chartBarSum[2]));

		var chart = d3.select(_target);

		var bar = chart.selectAll("g")
			.data(d3.values(chartBarSum[1]))
			.enter().append("g")
			.attr("transform", function(d, i) { return "translate(40, 0)"; });

		bar.append("rect")
			.attr("width", 40)
			.attr("height", x)
			.attr("y", function(d) { return 209 - x(d); })
			.attr("x", function(d,i){ return 40+i*80; })
			.attr("fill", function(d, i) { return color(i)});

		bar.append("text")
			.attr("width", 40)
			.attr("x", function(d, i) { return 45+i*80; })
			.attr("y", 230)
			.text(function(d, i) { return d3.values(chartBarSum[0])[i]; });
	}
*/
	/**
	 * 삼각형 차트 그리기
	 * @param _target = 대상 svg 클래스나 아이디
	 * @param _color = 차트 색상
	 * @param _width = svg넓이
	 * @param _top = 위쪽 데이터
	 * @param _left = 왼쪽 데이터
	 * @param _right = 오른쪽 데이터
	 */
	function fnChartTriangle(_target, _color, _width, _top, _left, _right){
		/**
		 * (데이터 / 총데이터 * 100) == 총데이터에 대한 현재 데이터의 퍼센트
		 * (데이터 퍼센트 * 데이터범주 길이 / 100) = 데이터 범주에 대한 입력된 데이터의 길이
		 */
		var topX = _width / 2 // 위쪽 X = 삼각형 가운데
			,topY = 117 - (_top / 200 * 100 * 117 / 100)
			,leftX = 105 - (_left / 200 * 100 * 105 / 100)
			,leftY = (_left / 200 * 100 * 63 / 100) + 117
			,rightX = (_right / 200 * 100 * 104 / 100) + 105
			,rightY = (_right / 200 * 100 * 63 / 100) + 117;
		d3.select(_target).append("path")
			.attr("d", "M" + topX + " "+topY + " L" + leftX + " " + leftY + " L" + rightX + " " + rightY + " Z")
			.attr("fill", _color)
			.style("stroke", _color)
			.style("stroke-width", 1)
			.style("fill-opacity", "0.3");
	}
