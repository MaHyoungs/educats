function dateOption(){

	/** datepicker(달력) 기본 세팅 **/
	$.datepicker.regional['ko'] = {
		closeText : '닫기',
		prevText : '이전달',
		nextText : '다음달',
		currentText : '오늘',
		monthNames : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ],
		monthNamesShort : [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ],
		dayNames : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesShort : [ '일', '월', '화', '수', '목', '금', '토' ],
		dayNamesMin : [ '일', '월', '화', '수', '목', '금', '토' ],
		dateFormat : 'yy-mm-dd',
		firstDay : 0,
		isRTL : false/*,
		maxDate : "+10y"*/
	};
	$.datepicker.setDefaults($.datepicker.regional['ko']);
	/** datepicker(달력) 기본 세팅 **/
}

$(function(){

	dateOption();

	var Today = new Date();
	var setYear = parseInt(Today.getFullYear()-10)+":"+parseInt(Today.getFullYear()+10);

	$("#lic_start").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#lic_end").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#start_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#end_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#searchSDt").datepicker({dateFormat: "yy-mm-dd", yearRange: setYear, changeYear: true, changeMonth: true});
	$("#searchEDt").datepicker({dateFormat: "yy-mm-dd", yearRange: setYear, changeYear: true, changeMonth: true});
	$("#semiProDt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#validStartDt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
	$("#validEndDt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
});