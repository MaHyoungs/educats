jQuery(function($){
    $(".tScaleSurvey tbody tr td i, .tCkeckSurvey tfoot tr td i").click(function(){
        var _this = $(this);

        _this.parent("td").siblings("td").children("i").removeClass("on");
        _this.addClass("on");
        _this.parent().parent().addClass('checked');
    });
})