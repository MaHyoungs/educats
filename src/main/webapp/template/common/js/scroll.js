var timeFlag = "Y";
var totalSlidTime = 500;
var  sec02Slider
	,sec02Control
	,sec03Slider
	,sec03Control
	,sec04Slider
	,sec04Control
	,thisSec
	,prevSec
	,nextSec
	,slider
	,sliderControl;

$(window).load(function(){
	fnContainerResize();
	var index_check = jQuery(location).attr('pathname');
	if (index_check == "/index") {
		$(".bgWrap").fnSectionSize();
		$(".contentSection").fnContentSectionSize();
	}

	/**
	 * section에 scroll순서 지정
	 **/
	var this_uri = $(location).attr('search');
	this_uri = this_uri.replace("?", "");
	this_uri = this_uri.split("=");
	if(this_uri[0] == "msq"){
		if(this_uri[1] == 50){
			$(".section").each(function(i){
				if(i == 4){
					$(this).addClass("secIdx"+i).css({"display":"block"}).addClass("on");
					fnMainLodingHide();
				}else{
					$(this).addClass("secIdx"+i).css({"display":"none"}).removeClass("on");
				}
			});
			var _menu = $(".menu.menu0" + 5)
			_menu.addClass("on").siblings(".menu").removeClass("on");
		}else{
			$(".section").each(function(i){
				if(i == 0){
					$(this).addClass("secIdx"+i).css({"display":"block"});
				}else{
					$(this).addClass("secIdx"+i).css({"display":"none"});
				}
				if(i == 4){
					fnMainLodingHide();
				}
			});
		}
		if(this_uri[1] && this_uri[1] != 50){
			var  _this    = $(".section.on")
				,_menu    = $(".menu.menu0"+this_uri[1])
				,_thisIdx = _this.index()
				,_nextIdx = _menu.index()
				,delta    = _thisIdx - _nextIdx
				,_next    = $(".section:eq("+_nextIdx+")");

			timeFlag = "N";
			fnMouseWheel(_this, _next, _menu, delta);
			setTimeout(function(){
				timeFlag = "Y";
			}, totalSlidTime);
		}
	}else{
		$(".section").each(function(i){
			if(i == 0){
				$(this).addClass("secIdx"+i).css({"display":"block"});
			}else{
				$(this).addClass("secIdx"+i).css({"display":"none"});
			}
			if(i == 4){
				fnMainLodingHide();
			}
		});
	}

	var startX, startY, endX, endY, startT, endT;

	$(".contentSection").bind('touchstart', function(e) {
		startT = new Date().getTime();
		startY = e.originalEvent.touches[0].screenY;
		startX = e.originalEvent.touches[0].screenX;
		$(".contentSection").bind('touchmove', onConTouchMove);
	});

	var onConTouchMove = function(e){
		endT = new Date().getTime();
		if(startT+100 <= endT){
			endY = e.originalEvent.touches[0].screenY;
			endX = e.originalEvent.touches[0].screenX;
			var totalX = Math.abs(startX - endX)
				, totalY = Math.abs(startY - endY);
			$(".contentSection").unbind('touchmove', onConTouchMove);
			if(totalX < totalY){
				var delta = endY - startY;
				if(delta > 0 || delta < 0){
					if(timeFlag == "Y"){
						var _this = $(".section.on")
							,_next
							,_menu;
						if(delta > 0){
							_next = _this.prev();
						}else{
							_next = _this.next();
						}

						_menu = $("#menu .menu:eq("+_next.index()+")");
						timeFlag = "N";
						fnMouseWheel(_this, _next, _menu, delta);
						setTimeout(function(){
							timeFlag = "Y";
						}, totalSlidTime);
					}
				}
			}
		}
		e.preventDefault();
	};

	$("body").bind("mousewheel", function(event, delta){
		if(timeFlag == "Y"){
			var _this = $(".section.on")
				,_next
				,_menu;

			if(delta > 0){
				_next = _this.prev();
			}else{
				_next = _this.next();
			}

			_menu = $("#menu .menu:eq("+_next.index()+")");
			timeFlag = "N";
			fnMouseWheel(_this, _next, _menu, delta);
			setTimeout(function(){
				timeFlag = "Y";
			}, totalSlidTime);
		}
	});

	$("#menu .menu").click(function(e){
		e.preventDefault();
		var  _this = $(".section.on")
			,_menu = $(this)
			,_thisIdx = _this.index()
			,_nextIdx = _menu.index()
			,delta = _thisIdx - _nextIdx
			,_next = $(".section:eq("+_nextIdx+")");
		if(timeFlag == "Y"){
			fnMouseWheel(_this, _next, _menu, delta);
			setTimeout(function(){
				timeFlag = "Y";
			}, totalSlidTime);
		}

	});
});

function fnContainerResize(){
	$("#container").height(window.innerHeight);
	//fnTestMsg(window.innerHeight);

}

function fnTestMsg(msg){
	$("#header .test").html(msg);
}

function fnMainLodingHide(){
//	setTimeout(function(){$("#mainLoading").hide();}, 2000);
	$("#mainLoading").hide();
}

function fnSliderStart(dept) {
	slider = $(".section0"+dept+"_bxslider").bxSlider({
		easing: "easeInOutQuad",
		speed: totalSlidTime,
		pager: true,
		controls: true
	});
	sliderControl = $(slider[0].parentElement).siblings(".bx-controls");
	sliderControl.appendTo("#section0"+dept).find(".bx-pager").css('left', ((window.innerWidth - $(".bx-pager").width())/2));
}

function fnMouseWheel(_this, _next, _menu, delta){
	fnContainerResize();
	timeFlag = "N";
	var wheelWinHei;
	//fnTestMsg("window.innerHeight = " + window.innerHeight + " // container height = " + $("#container").height());
	if(delta > 0){
		if(_next[0]){
			wheelWinHei = window.innerHeight;
			if(slider){
				setTimeout(function(){
					slider.destroySlider();
				}, totalSlidTime);
			}
			if($(".section0"+(_next.index()+1)+"_bxslider").length){
				setTimeout(function(){
					fnSliderStart(_next.index()+1);
				}, totalSlidTime);
			}
			_this.css({"display":"block"}).animate({top: wheelWinHei+60},{duration:totalSlidTime}).removeClass("on");
			_next.css({"top":-wheelWinHei+120, "display":"block"}).find(".bgWrap").fnSectionSize();
			_next.animate({top: 60},{duration:totalSlidTime}).addClass("on");
			_menu.addClass("on").siblings(".menu").removeClass("on");
			setTimeout(function(){
				_this.css({"display":"none"});
			}, totalSlidTime);
		}
	}else if(delta < 0){
		if(_next[0]){
			wheelWinHei = window.innerHeight;
			if(slider){
				setTimeout(function(){
					slider.destroySlider();
				}, totalSlidTime);
			}
			if($(".section0"+(_next.index()+1)+"_bxslider").length){
				setTimeout(function(){
					fnSliderStart(_next.index()+1);
				}, totalSlidTime);
			}
			_this.css({"display":"block"}).animate({top: -wheelWinHei+120},{duration:totalSlidTime}).removeClass("on");
			_next.css({"top":wheelWinHei, "display":"block"}).find(".bgWrap").fnSectionSize();
			_next.animate({top: 60},{duration:totalSlidTime}).addClass("on");
			_menu.addClass("on").siblings(".menu").removeClass("on");
			setTimeout(function(){
				_this.css({"display":"none"});
			}, totalSlidTime);
		}
	}
}
$(window).resize(function(){
	fnContainerResize();
});