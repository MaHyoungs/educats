(function($) {
	$.fn.jqueryPager = function(options) {
		var defaults = {
			pageSize: 10,
			currentPage: 1,
			pageTotal: 0,
			pageBlock: 10,
			clickEvent: 'goPage'
		};

		var subOption = $.extend(true, detaults, options);

		return this.each(function() {
			var currentPage = subOption.currentPage*1;
			var pageSize = subOption.pageSize*1;
			var pageBlock = subOption.pageBlock*1;
			var pageTotal = subOption.pageTotal*1;
			var clickEvent = subOption.clickEvent;

			if (!pageSize) pageSize = 10;
			if (!pageBlock) pageBlock = 10;

			var pageTotalCnt = Math.ceil(pageTotal/pageSize);
			var pageBlockCnt = Math.ceil(currentPage/pageBlock);
			var sPage, ePage;
			var html = '';

			if (pageBlockCnt > 1) {
				sPage = (pageBlockCnt-1)*pageBlock+1;
			} else {
				sPage = 1;
			}

			if ((pageBlockCnt*pageBlock) >= pageTotalCnt) {
				ePage = pageTotalCnt;
			} else {
				ePage = pageBlockCnt*pageBlock;
			}

			if (sPage > 1) {
				html += '<a href="javascript:'+ clickEvent +'(1);" class="first">처음</a>';
				html += '<a href="javascript:'+ clickEvent +'(' + (sPage-pageBlock) + ');" class="prev">이전</a>';
			}

			for (var i=sPage; i<=ePage; i++) {
				if (currentPage == i) {
					html+= '<a href="javascript:;" class="active">' + i + '</a>';
				} else {
					html+= '<a href="javascript:'+ clickEvent +'(' + i + ');">' + i + '</a>';
				}
			}

			if (ePage < pageTotalCnt) {
				html+= '<a href="javascript:'+ clickEvent +'(' + (ePage+1) + ');" class="next">다음</a>';
				html += '<a href="javascript:'+ clickEvent +'(' + pageTotalCnt + ');" class="last">끝</a>';
			}

			$(this).empty().append(html);
		});
	}
})(jQuery);