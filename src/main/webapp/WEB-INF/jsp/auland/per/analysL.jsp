<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultAnalysL">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/per/analysL?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">하위 인성별 수준분석</span></a>
				<a href="${uri_set}/per/analysM?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>주요 인성군별 수준분석</span></a>
				<a href="${uri_set}/per/trust?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>검사신뢰도</span></a>
			</div>
			<div class="tabInfo">
				<span class="content">
					인성이란 흔히 성품, 기질, 개성, 인격, 성격 등 여러 의미로 일컬어지나, 일반적으로 ‘개인의 독특한 행동과 특성’ 으로 정의됩니다.<br/>
					본 검사가 측정하고 있는 인성은 총 11가지로, 공통 속성에 따라 크게 개인적 인성, 타인적 인성, 환경적 인성 크게 3가지 주요 인성군으로 구분합니다.
				</span>
			</div>
			<div class="section01">
				<span class="title">인성검사 결과</span>
				<table class="tResultAnalysLPersent">
					<caption></caption>
					<colgroup>
						<col width="139" />
						<col width="*" />
						<col width="98" />
						<col width="98" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="35" />
						<col width="40" />
					</colgroup>
					<thead>
						<tr>
							<th colspan="2">인성</th>
							<th>표준점수</th>
							<th>백분위점수</th>
							<th width="35" class="pst">10</th>
							<th width="35" class="pst">20</th>
							<th width="35" class="pst">30</th>
							<th width="35" class="pst">40</th>
							<th width="35" class="pst">50</th>
							<th width="35" class="pst">60</th>
							<th width="35" class="pst">70</th>
							<th width="35" class="pst">80</th>
							<th width="35" class="pst">90</th>
							<th width="35" class="pst">100</th>
							<th>수준</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${perResultDetail}" varStatus="sts">
							<tr>
								<c:if test="${rs.ctgry3_id eq 'PERSUB_01'}" >
									<th rowspan="5">개인적인성</th>
								</c:if>
								<c:if test="${rs.ctgry3_id eq 'PERSUB_06'}" >
									<th rowspan="3">타인적인성</th>
								</c:if>
								<c:if test="${rs.ctgry3_id eq 'PERSUB_09'}" >
									<th rowspan="3">환경적인성</th>
								</c:if>
								<c:if test="${sts.count le 5}">
									<td class="stnScore01">${rs.ctgry3_nm}</td>
								</c:if>
								<c:if test="${sts.count gt 5 and sts.count le 8}">
									<td class="stnScore02">${rs.ctgry3_nm}</td>
								</c:if>
								<c:if test="${sts.count gt 8}">
									<td class="stnScore03">${rs.ctgry3_nm}</td>
								</c:if>
								<td>${rs.std_score}</td>
								<td>${rs.percent_score}</td>
								<c:if test="${rs.grade_cd eq 'A'}">
									<td colspan="10" class="chartBar362"><div class="chartBar blue" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="hei">상</span></td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'B'}">
									<td colspan="10" class="chartBar362"><div class="chartBar green" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="mid">중</span></td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'C'}">
									<td colspan="10" class="chartBar362"><div class="chartBar red" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="low">하</span></td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="section02">
				<div class="title"><span>표준점수</span></div>
				<div class="content">
					<ul>
						<li>타인과의 비교를 위해 피험자가 받은 원점수를 해석하기 편리하게 전환한 검사점수</li>
					</ul>
				</div>
				<div class="title"><span>백분위점수</span></div>
				<div class="content">
					<ul>
						<li>본인이 받은 표준점수보다 낮은 표준 점수를 받은 검사 응시자의 비율</li>
						<li>예) 백분위 점수가 85%라면 해당영역에서 본인보다 낮은 점수를 기록한 검사 응시자가의 비율이 85%라는 것을 의미</li>
					</ul>
				</div>
				<div class="analysLChart">
					<img src="/template/common/images/au/result_analysL_chart.png" alt="" />
				</div>
				<div class="info">* 위의 정규 분포표를 참고하여 본인의 위치를 확인하시기 바랍니다.</div>
			</div>
			<div class="section03">
				<span class="title">인성검사 상세결과</span>
				<span class="saBtn"><a href="#" class="btnAnalysLLayer layerpopupOpen" data-popup-class="analysL">인성이란?</a></span>
				<table class="tResultAnalysLMoreResult" summary="">
					<colgroup>
						<col width="158" />
						<col width="78" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>인성</th>
							<th>수준</th>
							<th>인성 점수 해석</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${perResultDetail}" varStatus="sts">
							<tr>
								<th>${rs.ctgry3_nm}</th>
								<c:if test="${rs.grade_cd eq 'A'}">
									<td class="hei">상</td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'B'}">
									<td class="mid">중</td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'C'}">
									<td class="low">하</td>
								</c:if>
								<td class="desc">${rs.description}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<div class="botBg">
		</div>
	</div>
</div>

<%-- Layer --%>
<div class="layerBg analysL"></div>
<div class="layerPopup analysL">
	<div class="layerHeader">
		<span class="title">인성이란?</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<table class="tLayerAnalysL" summary="">
				<colgroup>
					<col width="118" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>성실성</th>
						<td>성실성이란 맡은 일에 대해 성실하고 부지런한 것을 의미합니다. 성실성 항목에서 높은 점수를 보이는 사람은, 본인이 맡은 일에 최선을 다하며, 매사에 성실하고 부지런합니다. 또한 원칙에 충실하고 행동의 일관성을 기하여 타인으로부터 신뢰가 가는 사람이라는 평가를 받습니다. 약속을 중요하게 생각하며, 항상 미리미리 준비하여 마감기한을 놓치거나 시간을 어기지 않습니다.</td>
					</tr>
					<tr>
						<th>자신감</th>
						<td>자신감이란 스스로에 대한 자부심이 있고 매사에 당당한 것을 의미합니다. 자신감이 높은 사람의 경우, 어려운 문제에 직면해도 굴하지 않고 자신 있게 대처합니다. 또한 근본적으로 자신의 능력과 역량에 대한 자신감을 가지고 있으며, 평소에 “할 수 있다”는 자세를 자주 보여줍니다. 실패하더라도 의기소침하거나 주눅들지 않으며, 다른 사람들 앞에서 자신의 생각을 당당히 주장할 수 있습니다.</td>
					</tr>
					<tr>
						<th>인내지속성</th>
						<td>인내지속성은 사람 및 상황에 대해 인내하며, 중도에 포기하지 않고 지속적으로 일을 추진할 수 있는 능력을 의미합니다. 인내지속성이 높은 사람의 경우, 사람 및 상황에 대해 인내하는 능력이 뛰어나며, 어려운 일이 있더라도 중도에 포기하지 않습니다. 또한 행동이 규칙적이고 일관되며 자신이 맡은 일을 꾸준히 수행해 나갑니다. 주변의 상황이나 환경에 흔들리지 않으며, 안정적입니다.</td>
					</tr>
					<tr>
						<th>책임감</th>
						<td>책임감은 맡은 일은 끝까지 책임지며, 잘못을 부정하거나 감추지 않는 것을 의미합니다. 책임감이 높은 사람의 경우, 자신이 맡은 일의 결과에 대해서 전적으로 책임을 지려고 하며, 잘못이 있더라도 이것을 부정하거나 감추려 하지 않습니다. 자신의 일을 타인에게 미루지 않으며, 맡은 바 임무를 철저히 완수하기 위해 노력합니다.</td>
					</tr>
					<tr>
						<th>추진적극성</th>
						<td>추진적극성은 생각은 행동으로 바로 실천하며, 일을 행동력 있게 밀고 나가는 것을 의미합니다. 추진적극성이 높은 사람의 경우, 보다 높은 성과기준을 달성하기 위해서 끊임없이 노력하며, 매사에 솔선수범하는 모습을 보입니다. 작은 것 하나도 소홀히 하지 않으며, 활발하고 적극적입니다.</td>
					</tr>
					<tr>
						<th>사교성</th>
						<td>사교성은 여러 사람들과 원만한 대인관계를 유지하며, 쉽게 어울릴 수 있는 능력을 의미합니다. 사교성이 높은 사람의 경우, 다양한 스타일이나 배경을 가진 사람들과도 쉽게 공감대를 형성하며, 처음 보는 사람과도 쉽게 친해질 수 있습니다. 여러 사람들과 어울리는 것을 좋아하며, 많은 사람들과 다양한 관계를 형성하는 편입니다.</td>
					</tr>
					<tr>
						<th>타인신뢰</th>
						<td>타인신뢰란 타인의 진심을 있는 그대로 순수하게 받아들이며, 신뢰하는 것을 의미합니다. 타인신뢰가 높은 사람의 경우, 타인의 의도를 의심하지 않으며, 진심을 있는 그대로 순수하게 받아들입니다. 또한 주변사람들을 신뢰하고 의지하며, 자신 또한 다른 사람들에게 신뢰가 가는 사람으로 보여지고자 노력합니다. 타인의 단점보다는 장점을 보려고 노력하며, 안 좋은 점을 들춰내거나 비난하지 않습니다.</td>
					</tr>
					<tr>
						<th>이타심</th>
						<td>이타심이란 타인을 동정하고 배려하는 마음을 의미합니다. 이타심이 높은 사람의 경우, 타인의 다양한 견해와 입장을 중시하며, 고유의 가치관을 인정합니다. 상대방의 입장에서 생각하고 행동하며, 견해가 다른 사람들과도 원만한 관계를 형성할 수 있습니다. 자신의 목적을 위해 타인을 이용하지 않으며, 예의 바른 행동으로 남을 배려하는 모습을 보입니다.</td>
					</tr>
					<tr>
						<th>호기심</th>
						<td>호기심이란 새로운 것에 대한 관심과 흥미를 가지고, 모르는 것을 끝까지 알기 위해 노력하는 것을 의미합니다. 호기심이 높은 사람의 경우, 주변 상황과 사물에 주의를 기울이며, 여러 분야에 걸쳐 다양한 관심과 흥미를 가지고 있습니다. 또한 사소한 것도 그냥 지나치지 않으며, 주의의 폭이 넓어 다른 사람들이 보지 못하는 것을 찾아내기도 합니다. 새로운 지식, 경험에 대한 욕구가 강하며, 모르는 것이 생기면 이를 알아내기 위해 많은 시간과 노력을 투자합니다.</td>
					</tr>
					<tr>
						<th>도전정신</th>
						<td>도전정신이란 새로운 것에 도전하거나, 낯선 상황에 처하는 것을 두려워하지 않으며, 적극적으로 맞서는 능력을 의미합니다. 도전정신이 높은 사람의 경우, 새로운 것에 대한 도전과 다양한 요구를 회피하지 않고 적극적으로 즐깁니다. 또한 새로운 방법을 강구하기 위한 실험정신이 강하며, 낯선 상황에 대한 두려움이 적은 편입니다. 주변의 급격한 환경변화에도 당황하지 않고 융통성 있게 대처하며, 모호한 상황에서도 주저함이 없습니다.</td>
					</tr>
					<tr>
						<th>도덕성</th>
						<td>도덕성이란 사회적 규칙과 절차에 따라 행동하며 매사에 정직한 것을 의미합니다. 도덕성이 높은 사람의 경우, 원리. 원칙을 중요하게 생각하며, 사회적 규칙과 절차를 지키기 위해 노력합니다. 또한 매사에 정직하며 타인을 기만하거나 속이려 하지 않습니다. 타인에게 보여지지 않는 상황에서도 자신을 속이는 행동은 하지 않으며, 사회의 구성원으로서 항상 일관되고 성실한 모습을 보입니다.</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />