<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultAnalysM">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/per/analysL?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>하위 인성별 수준분석</span></a>
				<a href="${uri_set}/per/analysM?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">주요 인성군별 수준분석</span></a>
				<a href="${uri_set}/per/trust?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>검사신뢰도</span></a>
			</div>
			<div class="tabInfo">
				<span class="content">
					다음은 본인의 인성 검사 결과에 따라 산출된 주요 인성군 점수와 각각의 수준을 나타내는 결과 그래프입니다.<br/>
					주요 인성군 점수는 주요 인성군에 속하는 하위 인성들의 결과 점수를 합산한 것입니다.<br/>
					인성 모형은 결과점수를 도식화한 것으로서, 본인이 보유한 인성 중, 어느 인성군이 더 우수하고 부족한지 한눈에 파악할 수 있습니다. 삼각형 모형에서 중앙을 기준으로 바깥 쪽에 위치할수록 해당 인성군이 우수한 것이며, 차지한 면적이 넓을수록 우수한 인성 수준을 가지고 있는 것으로 해석할 수 있습니다.
				</span>
			</div>
			<script>
				jQuery(function($){
					fnChartTriangle("#chartTriangle", "#f01b7f", 208, <c:forEach var="rs" items="${mainperResultDetail}" varStatus="sts"><c:if test="${sts.last ne true}">${rs.std_score},</c:if><c:if test="${sts.last}">${rs.std_score}</c:if></c:forEach>);
					fnChartTriangle("#chartTriangle", "#13bbee", 208, 35.64, 47.88, 54.94);
				});
			</script>
			<div class="section01">
				<span class="title">주요인성군 검사 결과</span>
				<div class="content">
					<svg id="chartTriangle" width="208" height="180">
					</svg>
				</div>
			</div>
			<div class="section02">
				<span class="title">인성검사 상세결과</span>
				<table class="tResultAnalysMMoreResult" summary="">
					<colgroup>
						<col width="112" />
						<col width="88" />
						<col width="88" />
						<col width="88" />
						<col width="88" />
					</colgroup>
					<thead>
						<tr>
							<th>주요인성</th>
							<th>표준점수</th>
							<th>백분위점수</th>
							<th>수준</th>
							<th>평균점수</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${mainperResultDetail}" varStatus="sts">
							<tr>
								<th>${rs.ctgry2_nm}</th>
								<td>${rs.std_score}</td>
								<td>${rs.percent_score}</td>
								<c:choose>
									<c:when test="${rs.grade_cd eq 'A'}">
										<td class="hei">상</td>
									</c:when>
									<c:when test="${rs.grade_cd eq 'B'}">
										<td class="mid">중</td>
									</c:when>
									<c:when test="${rs.grade_cd eq 'C'}">
										<td class="low">하</td>
									</c:when>
									<c:otherwise>
										<td class="low">-</td>
									</c:otherwise>
								</c:choose>
								<td>
									<c:choose>
										<c:when test="${rs.ctgry2_id eq 'PERMAIN_01'}">35.64</c:when>
										<c:when test="${rs.ctgry2_id eq 'PERMAIN_02'}">47.88</c:when>
										<c:when test="${rs.ctgry2_id eq 'PERMAIN_03'}">54.94</c:when>
										<c:otherwise>-</c:otherwise>
									</c:choose>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<table class="tResultAnalysMMoreResultInfo" summary="">
					<colgroup>
						<col width="156" />
						<col width="156" />
						<col width="156" />
					</colgroup>
					<thead>
						<tr>
							<th class="low">하</th>
							<th class="mid">중</th>
							<th class="hei">상</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>0~30%</td>
							<td>30~70%</td>
							<td>70~100%</td>
						</tr>
					</tbody>
				</table>
				<span class="info">* 평균점수는 표준점수를 기준으로 한 평균점수입니다.</span>
			</div>
			<div class="section03">
				<span class="title">주요인성군 검사 결과</span>
				<span class="saBtn"><a href="#" class="btnAnalysMLayer layerpopupOpen" data-popup-class="analysM">주요인성군이란?</a></span>
				<table class="tResultAnalysLMoreResult">
					<colgroup>
						<col width="158" />
						<col width="78" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>인성군</th>
							<th>수준</th>
							<th>인성군 점수 해석</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${mainperResultDetail}" varStatus="sts">
							<tr>
								<th>${rs.ctgry2_nm}</th>
								<c:choose>
									<c:when test="${rs.grade_cd eq 'A'}">
										<td class="hei">상</td>
									</c:when>
									<c:when test="${rs.grade_cd eq 'B'}">
										<td class="mid">중</td>
									</c:when>
									<c:when test="${rs.grade_cd eq 'C'}">
										<td class="low">하</td>
									</c:when>
									<c:otherwise>
										<td class="low">-</td>
									</c:otherwise>
								</c:choose>
								<td class="desc">${rs.description}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		<div class="botBg">
		</div>
	</div>
</div>

<%-- Layer --%>
<div class="layerBg analysM"></div>
<div class="layerPopup analysM">
	<div class="layerHeader">
		<span class="title">주요인성군이란?</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<table class="tLayerAnalysM" summary="">
				<colgroup>
					<col width="118" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>개인적 인성</th>
						<td>개인적 인성이란 학업과 직업 등 개인의 전반적인 삶의 성공을 예측하는 데 있어 중요한 역할을 하는 인성들을 의미하며, 성실성, 자신감, 인내심, 책임감, 추진력 등이 이에 해당합니다. 인적 인성에서 높은 점수를 얻을 경우, 학업과 직업 생활 등 전반적인 분야에서 두각을 나타내며 성공적인 수행을 보일 가능성이 큽니다.</td>
					</tr>
					<tr class="cellspacing"></tr>
					<tr>
						<th>타인적 인성</th>
						<td>타인적 인성이란 자신을 둘러싼 많은 사람들과의 관계 형성 및 유지, 또는 집단에서의 적응에 중요한 영향을 미치는 인성들을 의미하며 사교성, 유머, 이타심 등이 이에 해당합니다. 타인적 인성에서 높은 점수를 얻을 경우, 대인지향적인 성향을 가지고 있는 것으로 설명할 수 있으며, 많은 사람들과 적극적으로 관계를 형성하는 능력이 있음을 의미합니다. 하지만 타인적 인성의 점수가 낮다고 하여 대인관계 능력에 문제가 있는 것은 아니며, 단지 높은 점수의 사람들에 비해 적극적인 면이 다소 부족한 것으로 설명할 수 있습니다.</td>
					</tr>
					<tr class="cellspacing"></tr>
					<tr>
						<th>환경적 인성</th>
						<td>환경적 인성이란 빠르게 변화하며 예측하기 어려운 환경 속에서 역량을 발휘하여 신속히 적응하는 데 중요한 역할을 하며 도전정신, 호기심, 적극성 등을 의미합니다. 또한 사회와 집단의 일부로서 양심에 따라 법과 규칙을 준수하고 자신의 역할과 책임을 다하는 과정에 중요한 영향을 미치는 인성인 정직성과 도덕성 또한 이에 해당합니다. 환경적 인성에서 높은 점수를 얻을 경우, 낯선 환경에서도 빠르게 적응하고, 법과 규칙을 준수하며 집단에 자연스럽게 융화하는 능력이 있음을 의미합니다.</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="conFooter">
		<span class="saBtn">
			<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
		</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />