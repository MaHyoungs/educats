<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultTrust">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/per/analysL?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>하위 인성별 수준분석</span></a>
				<a href="${uri_set}/per/analysM?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>주요 인성군별 수준분석</span></a>
				<a href="${uri_set}/per/trust?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">검사신뢰도</span></a>
			</div>
			<div class="tabInfo">
				<span class="content">
					다음은 인성 검사 결과의 신뢰성을 판단하는 부분으로, 일관성과 사회적 바람직성의 점수가 주의에 위치하고 있을 경우 성실하고 일관되게 답변을 하였는지 의심해 볼 필요가 있습니다.<br/>
					하지만 중학생들의 경우 인성을 형성해 나아가는 시기로서, 아직 자기 자신에 대한 선호가 불명확하여 응답의 일관성이 떨어질 수 있습니다. 때문에 검사 신뢰도에서 "주의"를 받았다면 검사에 다시 응시해 보시거나 선생님과의 면담을 통해 이를 조율해 보시기 바랍니다
				</span>
			</div>
			<div class="section01">
				<table class="tResultCre">
					<caption></caption>
					<colgroup>
						<col width="118" />
						<col width="381" />
						<col width="32" />
						<col width="32" />
						<col width="33" />
						<col width="32" />
						<col width="32" />
						<col width="32" />
						<col width="32" />
						<col width="33" />
						<col width="32" />
						<col width="32" />
						<col width="78" />
					</colgroup>
					<thead>
						<tr>
							<th>신뢰도 척도</th>
							<th>설명</th>
							<th colspan="9">적합</th>
							<th>주의</th>
							<th>수준</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>일관성</th>
							<td class="content">
								검사에 일관되게 응답한 정도<br/>
								<span class="cl_ff8400">(주의에 위치할 경우, 동일한 문항에 일관되지 않게 응답하였을 가능성이 있습니다.)</span>
							</td>
							<c:choose>
								<c:when test="${perConst.const_per ge 90}">
									<td colspan="10" class="chartBar322"><div class="chartBar red" style="width:${perConst.const_per}%;"></div></td>
									<td><span class="cl_c76161">주의</span></td>
								</c:when>
								<c:otherwise>
									<td colspan="10" class="chartBar322"><div class="chartBar blue" style="width:${perConst.const_per}%;"></div></td>
									<td><span class="cl_3266a5">적합</span></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<th>사회적<br/>바람직성</th>
							<td class="content">자신의 성격을 사회적으로 바람직하게 보이려는 정도<br/>
								<span class="cl_ff8400">(주의에 위치할 경우, 타인에게 보기 좋게 보이도록 자신의 성격을 왜곡하려는 시도를 했을 가능성이 있습니다.)</span>
							</td>
							<c:choose>
								<c:when test="${perConst.scpr_per ge 90}">
									<td colspan="10" class="chartBar322"><div class="chartBar red" style="width:${perConst.scpr_per}%;"></div></td>
									<td><span class="cl_c76161">주의</span></td>
								</c:when>
								<c:otherwise>
									<td colspan="10" class="chartBar322"><div class="chartBar blue" style="width:${perConst.scpr_per}%;"></div></td>
									<td><span class="cl_3266a5">적합</span></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		<div class="botBg">
		</div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />