<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultSummary">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">결과요약</span></a>
				<a href="${uri_set}/per/analysL?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>하위 인성별 수준분석</span></a>
				<a href="${uri_set}/per/analysM?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>주요 인성군별 수준분석</span></a>
				<a href="${uri_set}/per/trust?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>검사신뢰도</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">진로탐색검사 - 01. 인성검사 소개</span>
				<span class="content">본 검사는 자신이 어떠한 성향을 가지고 있는지 명확히 알지 못하는 학생들이 사전에 자신의 성격 특성을 종합적으로 파악할 수 있도록 도움을 주기 위한 검사입니다. 본인의 성향을 여러 가지 측면에서 분석한 결과를 바탕으로 나에 대한 객관적인 지식을 얻고 진단해 볼 수 있습니다. </span>
			</div>
			<div class="section01">
				<span class="title">진로탐색검사 - 01. 인성검사 결과지 구성</span>
				<ul>
					<li><span>01. </span>결과요약</li>
					<li><span>02. </span>하위 인성 별 상세 수준 분석</li>
					<li><span>03. </span>주요 인성군 별 상세 수준 분석</li>
					<li><span>04. </span>검사 신뢰도</li>
				</ul>
			</div>
			<div class="section02">
				<table class="tResultIcon" summary="">
					<caption></caption>
					<colgroup>
						<col width="138" />
						<col width="138" />
						<col width="138" />
					</colgroup>
					<thead>
						<tr>
							<td>우수인성</td>
							<td>부족인성</td>
							<td>우수주요인성군</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="per" items="${perResultBrif}" varStatus="sts">
								<c:if test="${sts.first eq true}" >
							<td>
								<i class="${fn:toLowerCase(per.ctgry3_id)} blue"></i>
								<span>${per.ctgry3_nm}</span>
							</td>
								</c:if>
								<c:if test="${sts.last eq true}" >
							<td>
								<i class="${fn:toLowerCase(per.ctgry3_id)} red"></i>
								<span>${per.ctgry3_nm}</span>
							</td>
								</c:if>
							</c:forEach>
							<c:forEach var="rs" items="${mainperResultBrif}" >
								<c:if test="${rs.rank eq '1'}" >
								<td>
									<i class="${fn:toLowerCase(rs.ctgry2_id)} blue"></i>
									<span>${rs.ctgry2_nm}</span>
								</td>
								</c:if>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section03">
				<table class="tResultPersent">
					<caption></caption>
					<colgroup>
						<col width="100" />
						<col width="68" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="28" />
						<col width="50" />
					</colgroup>
					<thead>
						<tr>
							<th>인성</th>
							<th>표준점수</th>
							<th colspan="10">백분위점수</th>
							<th>수준</th>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td width="27">10</td>
							<td width="27">20</td>
							<td width="27">30</td>
							<td width="27">40</td>
							<td width="27">50</td>
							<td width="27">60</td>
							<td width="27">70</td>
							<td width="27">80</td>
							<td width="27">90</td>
							<td width="27">100</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${mainperResultBrif}" varStatus="sts">
							<tr>
								<th>${rs.ctgry2_nm}</th>
								<td>${rs.std_score}</td>
								<c:if test="${rs.grade_cd eq 'A'}">
									<td colspan="10" class="chartBar278"><div class="chartBar blue" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="hei">상</span></td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'B'}">
									<td colspan="10" class="chartBar278"><div class="chartBar green" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="mid">중</span></td>
								</c:if>
								<c:if test="${rs.grade_cd eq 'C'}">
									<td colspan="10" class="chartBar278"><div class="chartBar red" style="width:${rs.percent_score}%;"></div></td>
									<td><span class="low">하</span></td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="section04">
				<table class="tResultCre">
					<caption></caption>
					<colgroup>
						<col width="118" />
						<col width="443" />
						<col width="32" />
						<col width="32" />
						<col width="33" />
						<col width="32" />
						<col width="32" />
						<col width="32" />
						<col width="32" />
						<col width="33" />
						<col width="32" />
						<col width="32" />
						<col width="78" />
					</colgroup>
					<thead>
						<tr>
							<th>신뢰도 척도</th>
							<th>설명</th>
							<th colspan="9">적합</th>
							<th>주의</th>
							<th>수준</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th>일관성</th>
							<td class="content">
								검사에 일관되게 응답한 정도<br/>
								<span class="cl_ff8400">(주의에 위치할 경우, 동일한 문항에 일관되지 않게 응답하였을 가능성이 있습니다.)</span>
							</td>
							<c:choose>
								<c:when test="${perConst.const_per ge 90}">
									<td colspan="10" class="chartBar322"><div class="chartBar red" style="width:${perConst.const_per}%;"></div></td>
									<td><span class="cl_c76161">주의</span></td>
								</c:when>
								<c:otherwise>
									<td colspan="10" class="chartBar322"><div class="chartBar blue" style="width:${perConst.const_per}%;"></div></td>
									<td><span class="cl_3266a5">적합</span></td>
								</c:otherwise>
							</c:choose>
						</tr>
						<tr>
							<th>사회적<br/>바람직성</th>
							<td class="content">자신의 성격을 사회적으로 바람직하게 보이려는 정도<br/>
								<span class="cl_ff8400">(주의에 위치할 경우, 타인에게 보기 좋게 보이도록 자신의 성격을 왜곡하려는 시도를 했을 가능성이 있습니다.)</span>
							</td>
							<c:choose>
								<c:when test="${perConst.scpr_per ge 90}">
									<td colspan="10" class="chartBar322"><div class="chartBar red" style="width:${perConst.scpr_per}%;"></div></td>
									<td><span class="cl_c76161">주의</span></td>
								</c:when>
								<c:otherwise>
									<td colspan="10" class="chartBar322"><div class="chartBar blue" style="width:${perConst.scpr_per}%;"></div></td>
									<td><span class="cl_3266a5">적합</span></td>
								</c:otherwise>
							</c:choose>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="botBg">
		</div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />