<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta http-equiv="Content-Style-Type" content="text/css">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>eduCAT</title>
</head>
<%@ include file="/WEB-INF/jsp/auland/chkAdms.jsp" %>
<frameset cols="*" rows="*,1" border="0">
	<frame name="conFrame" src="${uri_set}/surveyResult?seq_tme=${param.seq_tme}&who=${auth_id}${uri_param}" scrolling="yes" frameborder="0" title="검사결과" />
	<frame name="pdfFrame" src="${uri_set}/pdfDownload?seq_tme=${param.seq_tme}&who=${auth_id}${uri_param}" scrolling="yes" frameborder="0" title="PDF다운로드" />
</frameset>
<noframes>
	<body>
		<p>Sorry, your browser does not handle frames!</p>
	</body>
</noframes>
</html>