<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/header" charEncoding="utf-8" />

<script type="text/javascript" src="/template/common/js/survey.js"></script>
<script type="text/javascript" src="/template/common/js/time.js"></script>

<div id="container" class="studentContainer">
	<div id="content" class="surveyCheck">
		<div class="section_top">
			<img src="/template/common/images/au/bg_top.png" alt="" />
		</div>
		<div class="section">
			<div class="info">
				<div class="title">
					<span>진로적성검사</span>
				</div>
				<div class="infoText">
					<span>다음 검사 문항들을 읽고 더 선호하는 쪽에 표시해 주십시오.</span>
				</div>
			</div>
			<div class="clock">
				<span>권장시간 :&nbsp;</span>
				<p id="count"></p>
			</div>
			<div class="checkSurvey">
				<c:forEach var="rs" items="${affSurvey}" varStatus="sts" >
					<table class="tCkeckSurvey mB20" summary="">
						<caption></caption>
						<colgroup>
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
							<col width="103" />
						</colgroup>
						<thead>
							<tr>
								<td colspan="4">${rs.left_qestion}</td>
								<th>VS</th>
								<td colspan="4">${rs.right_qestion}</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th colspan="2">왼쪽이 아주 좋다</th>
								<td colspan="2">왼쪽이 좋다</td>
								<td class="vs_bc">비슷하다</td>
								<td colspan="2">오른쪽이 좋다</td>
								<th colspan="2">오른쪽이 아주 좋다</th>
							</tr>
							<tr>
								<th>4</th>
								<th>3</th>
								<td>2</td>
								<td>1</td>
								<td class="vs_bc">0</td>
								<td>1</td>
								<td>2</td>
								<th>3</th>
								<th>4</th>
							</tr>
						</tbody>
						<tfoot>
							<tr <c:if test="${not empty rs.reply_num}">class="checked"</c:if> >
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '01'}">class="on"</c:if> value="01"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '02'}">class="on"</c:if> value="02"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '03'}">class="on"</c:if> value="03"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '04'}">class="on"</c:if> value="04"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '05'}">class="on"</c:if> value="05"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '06'}">class="on"</c:if> value="06"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '07'}">class="on"</c:if> value="07"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '08'}">class="on"</c:if> value="08"></i></td>
								<td><i name="affCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '09'}">class="on"</c:if> value="09"></i></td>
							</tr>
						</tfoot>
					</table>
				</c:forEach>
			</div>
			<div class="scalePaging">
				<%@ include file="/WEB-INF/jsp/common/cmm/paging/scalePaging.jsp" %>
			</div>
		</div>
		<div class="section_bot">
			<span class="saBtn poR">
				<span class="poA ts_bubble"><img src="/template/common/images/au/temp_save_bubble.png" /></span>
				<a href="javascript:void(0)" onclick="goSaveTemp('temp')" onmouseover="$('.ts_bubble').show();" onmouseout="$('.ts_bubble').hide();" class="btnScaleTmpSave">임시저장</a>
			</span>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/svPaging.jsp" %>
			<div class="taC mT20"><img src="/template/common/images/au/copy_Info.gif" /></div>
		</div>
	</div>
</div>

<script>

	var click2=0;

	$(document).ready(function(){
		clearTimeout(McounterHandler);
		Mcounter('${info.rest_time}');
	});

	function goSaveTemp(msg) {
		var valList = '';
		var seqList = '';

		$('tfoot tr.checked').each(function() {
			seqList += ',' + $(this).find('i.on').attr('id');
			valList += ',' + $(this).find('i.on').attr('value');
		});
		if(seqList == '' || valList =='') {
			fnAlertMessage('저장할 문항을 선택하여 주십시요.');
			return false;
		}

		seqList = seqList.substr(1);
		valList = valList.substr(1);

		var min = ((($('#count').text().substr(0,2)) * 60) + parseInt($('#count').text().substr(3,5)));

		$.ajax({
			url : "/au/doTempSaveAjax",
			type : "POST",
			data : { seqList:seqList, valList:valList, seqAnswerMaster:${info.seq_answer_master}, ctgryId:'${info.ctgry3_id}', restTime:min},
			dataType : "text",
			success:function(args) {
				if(args == "succ"){
					if(msg == "temp"){
						fnAlertUrlMessage("임시저장 되었습니다.", "/au/survey");
					}else{
						document.location.href = msg;
					}
				}
			},
			error:function(e) {
				fnAlertMessage("임시저장에 실패하였습니다. \n 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function goNext(url) {
		var nonCheckedRow = $('tfoot tr:not(.checked)');
		var count = nonCheckedRow.length;
		if(click2==0){
			if(count){
				fnAlertMessage("검사하지 않은 문항이 있습니다.");
				return false;
			}else{
				++click2;
				goSaveTemp(url)
			}
		} else {
			alert("임시 저장중입니다.");
		}
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />