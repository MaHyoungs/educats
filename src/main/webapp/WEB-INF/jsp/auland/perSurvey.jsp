<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<script type="text/javascript" src="/template/common/js/survey.js"></script>
<script type="text/javascript" src="/template/common/js/time.js"></script>

<div id="container" class="studentContainer">
	<div id="content" class="surveyCheck">
		<div class="section_top">
			<img src="/template/common/images/au/bg_top.png" alt="" />
		</div>
		<div class="section">
			<div class="info">
				<div class="title">
					<span>인성검사</span>
				</div>
				<div class="infoText">
					<span>다음 질문을 읽고 각 물음에 솔직히 응답하여 주십시오.</span>
				</div>
			</div>
			<div class="clock">
				<span>권장시간 :&nbsp;</span>
				<span></span>
				<p id="count"></p>
			</div>
			<table class="tScaleSurvey" summary="">
				<caption></caption>
				<colgroup>
					<col width="58" />
					<col width="*" />
					<col width="60" />
					<col width="60" />
					<col width="60" />
					<col width="60" />
					<col width="60" />
				</colgroup>
				<thead>
					<tr>
						<th>No.</th>
						<th>질문을 잘 읽고 본인과 일치하는 정도를 체크해 주세요.</th>
						<th>전혀<br/>그렇지<br/>않다</th>
						<th>그렇지<br/>않다</th>
						<th>보통이다</th>
						<th>그렇다</th>
						<th>매우<br/>그렇다</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${perSurvey}" varStatus="sts">
						<tr <c:if test="${not empty rs.reply_num}">class="checked"</c:if> >
							<th>${rs.sort_num}</th>
							<td id="qes">${rs.question}</td>
							<td><i name="perCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '01'}">class="on"</c:if> value="01"></i></td>
							<td><i name="perCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '02'}">class="on"</c:if> value="02"></i></td>
							<td><i name="perCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '03'}">class="on"</c:if> value="03"></i></td>
							<td><i name="perCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '04'}">class="on"</c:if> value="04"></i></td>
							<td><i name="perCheck" id="${rs.seq_qesitm}" <c:if test="${rs.reply_num eq '05'}">class="on"</c:if> value="05"></i></td>
							<c:if test="${cnt eq rs.sort_num}" >
								<c:set var="lastChk" value="Y" />
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<div class="scalePaging">
				<%@ include file="/WEB-INF/jsp/common/cmm/paging/scalePaging.jsp" %>
			</div>
		</div>
		<div class="section_bot">
			<span class="saBtn poR">
				<span class="poA ts_bubble"><img src="/template/common/images/au/temp_save_bubble.png" /></span>
				<a href="javascript:void(0)" onclick="goSaveTemp('temp')" onmouseover="$('.ts_bubble').show();" onmouseout="$('.ts_bubble').hide();" class="btnScaleTmpSave">임시저장</a>
			</span>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/svPaging.jsp" %>
			<c:if test="${lastChk eq 'Y'}" >
				<span class="saBtn"><a href="javascript:void(0)" onclick="goComplete()" class="btnScaleSucces">검사완료</a></span>
			</c:if>
			<div class="taC mT20"><img src="/template/common/images/au/copy_Info.gif" /></div>
		</div>
	</div>
</div>

<c:if test="${lastChk eq 'Y'}" >
	<form id="surveyForm" name="surveyForm" action="/au/doComplete" method="post">
		<input type="hidden" id="seq_answer_master" name="seq_answer_master" value="${info.seq_answer_master}"/>
		<input type="hidden" id="ctgry3_id" name="ctgry3_id" value="${info.ctgry3_id}"/>
	</form>
</c:if>

<script>

	var click=0;
	var click2=0;

	$(document).ready(function(){
		clearTimeout(McounterHandler);
		Mcounter('${info.rest_time}');
	});

	function goSaveTemp(msg) {

		var valList = '';
		var seqList = '';

		$('tbody tr.checked').each(function() {
			seqList += ',' + $(this).find('i.on').attr('id');
			valList += ',' + $(this).find('i.on').attr('value');
		});
		if(seqList == '' || valList ==''){
			fnAlertMessage('저장할 문항을 선택하여 주십시요.');
			return false;
		}
		seqList = seqList.substr(1);
		valList = valList.substr(1);

		var min = ((($('#count').text().substr(0,2)) * 60) + parseInt($('#count').text().substr(3,5)));

		$.ajax({
			url : "/au/doTempSaveAjax",
			type : "POST",
			data : {seqList:seqList, valList:valList, seqAnswerMaster:${info.seq_answer_master}, ctgryId:'${info.ctgry3_id}', restTime:min},
			dataType : "text",
			success:function(args) {
				if(args == "succ"){
					if(msg == "complete") {
						if(confirm("모든 문항에 응답하였습니다. \n 검사를 완료하시겠습니까?") == true){
							document.surveyForm.submit();
						}else{
							return false;
						}
					}else if(msg == "temp"){
						fnAlertUrlMessage("임시저장 되었습니다.", "/au/survey");
					}else{
						document.location.href = msg;
					}
				}
			},
			error:function(e) {
				fnAlertMessage("임시저장에 실패하였습니다. \n 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function goNext(url) {
		var nonCheckedRow = $('tbody tr:not(.checked)');
		var count = nonCheckedRow.length;
		if(click2==0){
			if(count){
				fnAlertMessage("검사하지 않은 문항이 있습니다.");
				return false;
			}else{
				++click2;
				goSaveTemp(url);
			}
		} else {
			alert("임시 저장중입니다.");
		}
	}

	function goComplete() {
		var nonCheckedRow = $('tbody tr:not(.checked)');
		var count = nonCheckedRow.length;
		if(click==0){
			if(count){
				fnAlertMessage("검사하지 않은 문항이 있습니다.");
				return false;
			}else{
				++click;
				goSaveTemp('complete');
			}
		} else {
			alert("내용을 저장중입니다.");
		}
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />