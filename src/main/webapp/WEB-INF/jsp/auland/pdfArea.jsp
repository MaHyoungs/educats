<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<link charset="utf-8" href="/template/common/css/pdfArea.css" type="text/css" rel="stylesheet" />

<div id="pdfArea">
	<div class="pdfWrap surveyresult"></div>
	<div class="pdfWrap persummary"></div>
	<div class="pdfWrap peranalysL1"></div>
	<div class="pdfWrap peranalysL2"></div>
	<div class="pdfWrap peranalysL3"></div>
	<div class="pdfWrap peranalysM"></div>
	<div class="pdfWrap pertrust"></div>
	<div class="pdfWrap affsummary"></div>
	<div class="pdfWrap affanalys"></div>
	<div class="pdfWrap affmajAnalys1"></div>
	<div class="pdfWrap affmajAnalys2"></div>
	<div class="pdfWrap affauGuide"></div>
	<div class="pdfWrap cloneresultInfo"></div>
	<div class="pdfWrap cloneunivInfo"></div>
	<div class="pdfWrap clonejobInfo"></div>
</div>
<div id="iframeArea">
<%--
	<iframe id="iFrameSurveyresult" class="disNon" src="/au/surveyResult?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFramePerSummary" class="disNon" src="/au/per/summary?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFramePeranalysL" class="disNon" src="/au/per/analysL?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFramePeranalysM" class="disNon" src="/au/per/analysM?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFramePertrust" class="disNon" src="/au/per/trust?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameAffsummary" class="disNon" src="/au/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameAffanalys" class="disNon" src="/au/aff/analys?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameAffmajAnalys" class="disNon" src="/au/aff/majAnalys?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameAffauGuide" class="disNon" src="/au/aff/auGuide?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameCloneresultInfo" class="disNon" src="/au/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameCloneunivInfo" class="disNon" src="/au/clone/univInfo?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
	<iframe id="iFrameClonejobInfo" class="disNon" src="/au/clone/jobInfo?seq_tme=${usrInfo.seq_tme}&who=${user.user_id}"></iframe>
--%>
</div>
<div id="pdfLoading"><img src="/template/common/images/loading_pdf_download.gif" alt="" /></div>
<script>
	/**
	 * PDF 다운로드 준비
	 */
	var  iframeLoad = 0
		,pdfAreaLoad = 0
		,status = "iframe create"
		,fnRunTime;
	function fnCopy(){
		fnRunTime = setInterval(function(){
			var  _dd = new Date().getSeconds()
				,_mm = new Date().getMinutes();
			console.clear();
			console.log("status = " + status);
			console.log("_time_ = " + ((_mm*60+_dd)-(_m*60+_d)));
		}, 1000)
		$("body").css("overflow","hidden");
		$("#pdfLoading").css("display","block");

		$("#iframeArea iframe").each(function(){
			$(this).load(function(){
				iframeLoad++;
				if(iframeLoad == 12){
					fnCopy2();
				}
			});
		});
	}

	/**
	 * PDF 다운로드
	 * 화면캡쳐
	 */
	function fnCopy2() {
		status = "pdf area append";
		$("#pdfArea").css("display", "block");
		$("iframe").contents().find("script").remove();
		$("iframe").contents().find("#pdfLoading").remove();
		$("iframe").contents().find("#pdfArea").remove();
		$("iframe").contents().find("#iframeArea").remove();
		$("iframe").contents().find("#footer").remove();

		$("#pdfArea .surveyresult").append($("#iFrameSurveyresult").contents().find("#container").html()); // 02 - 인성검사 결과
		//$("#pdfArea .surveyresult").append($("#container").html()); // 01 - 검사결과
		$("#pdfArea .surveyresult table.tResultPersent thead td[width=27]").attr("width", "").addClass("wid59");
		$("#pdfArea .surveyresult table.tResultPersent colgroup").remove();
		$("#pdfArea .persummary").append($("#iFramePerSummary").contents().find("#container").html()).find(".header").html("01. 인성검사 결과"); // 02 - 인성검사 결과
		$("#pdfArea .persummary table.tResultPersent thead td[width=27]").attr("width", "").addClass("wid59");
		$("#pdfArea .persummary table.tResultPersent colgroup").remove();
		$("#pdfArea .peranalysL1").append($("#iFramePeranalysL").contents().find("#container").html()).find(".header").html("1) 하위 인성별 수준분석"); // 03 - 하위 인성별 수준분석
		$("#pdfArea .peranalysL2").append($("#iFramePeranalysL").contents().find("#container").html()).find(".header").html("인성검사 상세결과"); // 04 - 인성검사 상세결과
		$("#pdfArea .peranalysL3").append($("#iFramePeranalysL").contents().find(".layerPopup.analysL .conContainer").html()).find("table").before("<div class='header'>인성이란</div>"); // 05 - 인성이란
		$("#pdfArea .peranalysM").append($("#iFramePeranalysM").contents().find("#container").html()).find(".header").html("2) 주요인성군별 수준분석"); // 06 - 주요인성군별 수준분석
		$("#pdfArea .pertrust").append($("#iFramePeranalysM").contents().find(".layerPopup.analysM .conContainer").html()).find("table").before("<div class='header'>주요인성군이란?</div>"); // 07 - 검사신뢰도
		$("#pdfArea .pertrust .cellspacing").remove();
		$("#pdfArea .pertrust").append($("#iFramePertrust").contents().find("#container").html()).find("#content .header").html("3) 검사신뢰도"); // 07 - 검사신뢰도
		$("#pdfArea .affsummary").append($("#iFrameAffsummary").contents().find("#container").html()).find("#content .header").html("02. 진로적성검사결과"); // 08 - 진로적성검사결과
		$("#pdfArea .affanalys").append($("#iFrameAffanalys").contents().find("#container").html()).find("#content .header").html("1) 학업성향 분석"); // 09 - 학업성향 분석
		$("#pdfArea .affmajAnalys1").append($("#iFrameAffanalys").contents().find("#container").html()); // 10 - 학과 선택 가이드 1
		$("#pdfArea .affmajAnalys1").append($("#iFrameAffmajAnalys").contents().find("#container").html()).find("#content .header").html("2) 학과 선택 가이드"); // 10 - 학과 선택 가이드 1
		$("#pdfArea .affmajAnalys2").append($("#iFrameAffmajAnalys").contents().find("#container").html()); // 11 - 학과 선택 가이드 2
		$("#pdfArea .affauGuide").append($("#iFrameAffauGuide").contents().find("#container").html()).find("#content .header").html("3) 진로&middot;직업 선택 가이드"); // 12 - 진로.직업 선택 가이드
		$("#pdfArea .cloneresultInfo").append($("#iFrameCloneresultInfo").contents().find("#container").html()).find("#content.cloneResultInfo .header").html("03. 클론 매칭"); // 13 - 클론매칭
		$("#pdfArea .cloneresultInfo").append($("#iFrameCloneunivInfo").contents().find("#container").html()).find("#content.cloneUnivInfo .header").html("1) 대학생활 정보"); // 13 - 클론매칭
		$("#pdfArea .cloneunivInfo").append($("#iFrameCloneunivInfo").contents().find("#container").html()); // 14 - 취업정보 1
		$("#pdfArea .cloneunivInfo").append($("#iFrameClonejobInfo").contents().find("#container").html()).find("#content .header").html("2) 취업정보"); // 14 - 취업정보 1
		$("#pdfArea .clonejobInfo").append($("#iFrameClonejobInfo").contents().find("#container").html()); // 15 - 취업정보 2
		html2canvas($("#pdfArea"), {
			//logging: true,
			profile: true,
			useCORS: true,
			allowTaint: true,
			proxy: '/etc/proxy_image',
			onrendered: function (canvas) {
				var options = {pagesplit: true, width: 1002, height: (canvas.height)};
				var doc = new jsPDF('p', 'pt', [1000, 1415]);
				status = "pdf create";
				doc.addHTML($("#pdfArea"), 0, 0, options, function () {
					/*
					 * 파일명생성기준 -> 학년도 + 학교/기관명 + 라이선스연차 + 검사종류 + 검사회차 + (학년) + 반 + (번호) + 이름 + 아이디
					 * 저장파일명예시 -> 2016년_한국중학교_1학년_1반_홍길동_1회차_진로탐색검사결과.pdf
					 */
					$("#pdfLoading").css("display","none");
					<c:if test="${userPdfInfo.schorg_typ_cd ne 'ORG01'}" >
						doc.save('${userPdfInfo.st_year_cd}년_${userPdfInfo.schorg_nm}_${userPdfInfo.st_grade_num}학년_${userPdfInfo.st_class}반_${usrInfo.who_nm}_${userPdfInfo.tme_num}회차_${userPdfInfo.survey_typ_nm}결과.pdf');
					</c:if>
					<c:if test="${userPdfInfo.schorg_typ_cd eq 'ORG01'}" >
						doc.save('${userPdfInfo.st_year_cd}년_${userPdfInfo.schorg_nm}_${userPdfInfo.st_class}_${usrInfo.who_nm}_${userPdfInfo.tme_num}회차_${userPdfInfo.survey_typ_nm}결과.pdf');
					</c:if>
					status = "pdf save";
					clearInterval(fnRunTime);
					$("#pdfArea").css("display", "none");
					$("body").css("overflow", "auto");
				});
			}
		});

		/*
		// 이미지 형식으로 저장
		html2canvas($("#pdfArea"), {
			useCORS: true,
			proxy: '/etc/proxy_image',
			onrendered: function(canvas) {
				var imgData = canvas.toDataURL('image/jpeg', 0.5);
				var _height = canvas.height*59.5/100;
				// 페이지 크기로 이미지 크롭
				var image = new Image();
				image = Canvas2Image.convertToJPEG(canvas);
				var doc = new jsPDF('p', 'pt', 'a4');
				doc.addImage(imgData, 'jpeg', 15, 0, 700, _height);
				var croppingYPosition = 841;
				var count = (_height) / 841;
				for (var i =2; i < count; i++) {
					doc.addPage();
					doc.addImage(imgData, 'jpeg', 15, -croppingYPosition, 700, _height);
					console.log(i);
					croppingYPosition += 841;
				}
			}
		});
		*/
	}
</script>