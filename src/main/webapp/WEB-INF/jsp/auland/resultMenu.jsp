<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="pdfLoading"><img src="/template/common/images/loading_pdf_download.gif" alt="" /></div>

<c:choose>
	<c:when test="${param.adms eq 'Y'}">
		<c:set var="uri_set" value="/mng" />
		<c:set var="uri_param" value="&adms=Y" />
	</c:when>
	<c:otherwise>
		<c:set var="uri_set" value="/au" />
		<c:if test="${user.user_typ_cd eq '04'}">
			<c:set var="uri_param" value="&tchs=Y" />
		</c:if>
	</c:otherwise>
</c:choose>

<c:if test="${param.adms eq 'Y' or param.tchs eq 'Y'}">
	<script>
		$(window).load(function() {
			$("#container").css({'padding-top': '0'});
		});
	</script>
</c:if>

<div class="header">
	<span class="title">검사결과<c:if test="${menuType ne '0'}"> 상세</c:if></span>
	<div class="btnArea">
		<c:if test="${menuType ne '0'}">
			<a href="${uri_set}/surveyResult?seq_tme=${param.seq_tme}&who=${param.who}${uri_param}&goRsMain=ok"><span class="resultHome">검사결과<br/>Home</span></a>
		</c:if>
		<a href="#" onclick="top.pdfFrame.fnCopy();"><span class="download">FULL REPORT<br/>다운로드</span></a>
	</div>
</div>
<div class="info">
	<span class="header">이름</span>
	<span class="content">${usrInfo.who_nm}</span>
	<span class="header">검사명</span>
	<span class="content">진로탐색검사</span>
	<span class="header">검사일자</span>
	<span class="content">${usrInfo.reg_dt}</span>
</div>