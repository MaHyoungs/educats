<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult cloneResultInfo">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">결과지 소개</span></a>
				<a href="${uri_set}/clone/univInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>대학생활 정보</span></a>
				<a href="${uri_set}/clone/jobInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>취업정보</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">진로탐색검사 – 03. 클론매칭 결과지 소개</span>
				<span class="content">
					클론매칭시스템은 검사자의 인성검사, 진로적성검사 결과를 자체 개발한 매칭 알고리즘에 적용한 뒤, 검사자와 유사한 성향을 가진 대학생 클론을 찾아 매칭해 주는 방법입니다.<br/>
					검사자와 유사한 조건을 가진 클론들의 대학생활과 취업 정보를 참고하여 진로선택의 가이드로 활용할 수 있습니다.<br/>
					결과지 내용은 클론들의 전반적인 수준을 반영한 결과를 정리한 것으로서, 검사자가 반드시 그러한 대학생활을 하고 해당 기업과 직무에 합격을 할 것임을 예측하는 것은 아닙니다. 클론매칭 결과지의 내용은 단지 참고사항으로만 활용해 주시기 바라며, 해당 내용은 표본집단 및 응시시기에 따라서 일부 내용이 변동될 수 있습니다.
				</span>
			</div>
			<div class="section01">
				<span class="title">진로탐색검사 – 03. 클론매칭 결과지 구성</span>
				<ul>
					<li><span>01. </span>대학생활 정보</li>
					<li><span>02. </span>취업정보</li>
				</ul>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />