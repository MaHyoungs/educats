<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult cloneJoinInfo">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과지 소개</span></a>
				<a href="${uri_set}/clone/univInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>대학생활 정보</span></a>
				<a href="${uri_set}/clone/jobInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">취업정보</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">클론들의 취업정보란?</span>
				<span class="content">
					${usrInfo.who_nm}님과 유사한 조건을 가진 대학생 클론들이 졸업 후 최종 합격한 기업과 직무(입사 후 하는 일)를 정리한 결과 입니다. <br/>
					나와 유사한 조건을 가진 대학생 클론들이 어느 기업, 직무에 취업하였는지를 참고로, 내가 평소 관심을 가지고 있었던 기업과 직무(입사 후 하는 일)가 포함되어
					있는지 살펴보세요.<br/>
					전체 합격 기업, 직무 중 1순위~5순위까지 정보만을 선별하였으니 참고 바랍니다.

				</span>
			</div>
			<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
				<c:if test="${sts.first}">
					<c:set var="maxPassbiz" value="${rs.max_passbiz}" />
					<c:set var="maxPasscomp" value="${rs.max_passcomp}" />
				</c:if>
				<c:if test="${sts.last ne true}">
					<c:set var="chartData" value="${chartData}${rs.passbiz_dist}," />
				</c:if>
				<c:if test="${sts.last}">
					<c:set var="chartData" value="${chartData}${rs.passbiz_dist}" />
				</c:if>
			</c:forEach>
			<script>
				jQuery(function($){
					var  chartData = [${chartData}]
						,colorData = ["#00dab9", "#e44296", "#507ff4", "#ffae00", "#5C1B1B"]
					fnChartPie("#chart_pie2", chartData, colorData, 130, 105, 29, 80)
				});
			</script>
			<div class="section01">
				<span class="title">클론들이 최종 합격한 기업정보</span>
				<div class="tableSection">
					<table class="tJoinInfoChart" summary="">
						<colgroup>
							<col width="127" />
							<col width="350" />
							<col width="127" />
							<col width="350" />
						</colgroup>
						<thead>
							<tr>
								<th>최다 합격 업종</th>
								<td>${maxPassbiz}</td>
								<th>최다 합격 기업</th>
								<td>${maxPasscomp}</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="4">
									<span class="chart_pie_title">최종 합격 업종</span>
									<svg id="chart_pie2" width="230" height="200"></svg>
									<ul>
										<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
											<c:if test="${rs.order_num eq '1'}"><li><i class="color_tag_00dab9"></i> <span>${rs.passbiz}</span></li></c:if>
											<c:if test="${rs.order_num eq '2'}"><li><i class="color_tag_e44296"></i> <span>${rs.passbiz}</span></li></c:if>
											<c:if test="${rs.order_num eq '3'}"><li><i class="color_tag_507ff4"></i> <span>${rs.passbiz}</span></li></c:if>
											<c:if test="${rs.order_num eq '4'}"><li><i class="color_tag_ffae00"></i> <span>${rs.passbiz}</span></li></c:if>
											<c:if test="${rs.order_num eq '5'}"><li><i class="color_tag_5C1B1B"></i> <span>${rs.passbiz}</span></li></c:if>
										</c:forEach>
									</ul>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="section02">
				<span class="title">최종 합격 기업</span>
				<div class="tableSection">
					<table class="tJoinInfoPersent" summary="">
						<colgroup>
							<col width="85" />
							<col width="245" />
							<col width="125" />
						</colgroup>
						<thead>
							<tr>
								<th>순위</th>
								<th>기업</th>
								<th>분포</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
								<tr>
									<th>${rs.order_num}</th>
									<td>${rs.passcomp}</td>
									<td>${rs.passcomp_dist}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
			<div class="section02 mL40 mB30">
				<span class="title">최종 합격 직무</span>
				<div class="tableSection">
					<table class="tJoinInfoPersent" summary="">
						<colgroup>
							<col width="85" />
							<col width="245" />
							<col width="125" />
						</colgroup>
						<thead>
						<tr>
							<th>순위</th>
							<th>직무</th>
							<th>분포</th>
						</tr>
						</thead>
						<tbody>
							<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
								<tr>
									<th>${rs.order_num}</th>
									<td>${rs.duty}</td>
									<td>${rs.duty_dist}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<div class="section03">
				<span class="title">직무설명</span>
				<div class="tableSection">
					<table class="tUnivInfoTerm" summary="">
						<colgroup>
							<col width="250" />
							<col width="*" />
						</colgroup>
						<tbody>
							<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
								<c:if test="${not empty rs.duty_prt}">
									<tr>
										<th>${rs.duty_prt}</th>
										<td>
											${rs.duty_desc_prt}
										</td>
									</tr>
								</c:if>
							</c:forEach>
							<%--
							<tr>
								<th>학점</th>
								<td>
									시장환경 및 급변하는 소비자 동향을 연구하여 브랜드 가치 제고 및 경영목표 달성을 위한 다양한 마케팅 활동을 주도하는 역할을 하는
									직무입니다. 광고나 프로모션, 소비자 조사 등 마케팅, 홍보 기본 업무 이외에도 고객 만족도 제고를 위한 신규 서비스 기획 등의 업무
									활동 등도 수반합니다.
								</td>
							</tr>
							<tr>
								<th>일반사무</th>
								<td>
									기업이 생산 활동을 원활히 할 수 있도록 지원하는 직무로서 인사, 총무, 재무, 홍보부 등이 대표적이며, 주로 사무직에 종사하는 사람들
									에게 적합합니다.
								</td>
							</tr>
							<tr>
								<th>항공승무원</th>
								<td>
									호텔, 기업 내 고객 서비스 팀 등, 다양한 사람들과 접촉하여 관계를 유지하는 일, 서비스 향상을 위한 서비스 운영 기획, 개선 등의 업무
									수행 시 요구되는 직무입니다. 타인을 이해하는 능력 및 커뮤니케이션 능력이 요구됩니다.
								</td>
							</tr>
							<tr>
								<th>인사, 경영지원</th>
								<td>
									기업이 생산 활동을 원활히 할 수 있도록 지원하는 직무로서 인사, 총무, 재무, 홍보부 등이 대표적이며, 주로 사무직에 종사하는 사람들
									에게 적합합니다.
								</td>
							</tr>
							--%>
						</tbody>
					</table>
				</div>
				<span class="info">※ 해당 직무가 포함되어 있는 상위 직무의 공통적인 특성에 대한 설명입니다.</span>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />