<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult cloneUnivInfo">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과지 소개</span></a>
				<a href="${uri_set}/clone/univInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">대학생활 정보</span></a>
				<a href="${uri_set}/clone/jobInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>취업정보</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">클론매칭이란?</span>
				<span class="content">
					${usrInfo.who_nm} 님의 인성검사, 진로적성 검사 결과를 바탕으로 ${usrInfo.who_nm}님과 유사한 조건과 성향을 가진 대학생 클론들을 찾아 매칭하는 방법입니다.<br/>
					나와 유사한 조건을 가진 대학생 클론들이 어떻게 대학생활을 했는지 참고하여 나의 대학생활을 예측해 보세요.<br/>
					단, 검사자가 반드시 이러한 활동을 해야 함을 의미하는 것은 아니니 참고사항으로만 활용하시기 바랍니다.<br/>
				</span>
			</div>
			<div class="section01">
				<div class="tableSection">
					<table class="tUnivInfoClone" summary="">
						<colgroup>
							<col width="178" />
							<col width="158" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th class="info" rowspan="8">나의 클론들은<br/>어떤 대학생활을<br/>했을까?</th>
								<th>학점</th>
								<td><span class="cl_ff8400">${cloneProfile.credit}</span> 점 이상 관리</td>
							</tr>
							<tr>
								<th>토익</th>
								<td><span class="cl_ff8400">${cloneProfile.toeic}</span> 점 이상 관리</td>
							</tr>
							<tr>
								<th>자격증</th>
								<td><span class="cl_ff8400">${cloneProfile.cert}</span> 자격증 취득</td>
							</tr>
							<tr>
								<th>봉사활동</th>
								<td><span class="cl_ff8400">${cloneProfile.volunteer}</span> 봉사활동 수행</td>
							</tr>
							<tr>
								<th>인턴</th>
								<td><span class="cl_ff8400">${cloneProfile.intern}</span> 관련 인턴활동 수행</td>
							</tr>
							<tr>
								<th>공모전</th>
								<td><span class="cl_ff8400">${cloneProfile.contest}</span> 관련 공모전</td>
							</tr>
							<tr>
								<th>해외연수</th>
								<td><span class="cl_ff8400">${cloneProfile.overseastrn}</span> 해외연수</td>
							</tr>
							<tr>
								<th>동아리</th>
								<td><span class="cl_ff8400">${cloneProfile.club}</span> 활동</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="section02">
				<span class="title">용어설명</span>
				<div class="tableSection">
					<table class="tUnivInfoTerm" summary="">
						<colgroup>
							<col width="158" />
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<th>학점</th>
								<td>대학생들의 경우 수업을 듣고 시험을 본 뒤에 평가를 받는 데, 이때의 성적을 학점이라고 하며 일반적으로 4.5점 만점입니다.</td>
							</tr>
							<tr>
								<th>토익</th>
								<td>
									대표공인영어시험으로, 대부분의 기업에서 점수를 요구하는 경우가 많아 대학생들에게 필수로 여겨지는 시험입니다.<br/>
									최고점은 990점이며 취업 시 800점~900점 대의 성적이 필요합니다.
								</td>
							</tr>
							<tr>
								<th>자격증</th>
								<td>워드프로세스, 컴퓨터활용능력처럼 어떤 능력을 가지고 있음을 인증하는 지표로 활용됩니다.</td>
							</tr>
							<tr>
								<th>봉사활동</th>
								<td>타인을 돕기 위해 자의적으로 하는 활동들을 의미합니다. </td>
							</tr>
							<tr>
								<th>인턴</th>
								<td>취업 전 기업에서 실제로 일을 해보는 것이며 실무경험을 쌓을 수 있다는 점에서 권장 됩니다.</td>
							</tr>
							<tr>
								<th>공모전</th>
								<td>특정 분야, 주제를 놓고 자신의 능력을 경쟁하는 경진대회라고 볼 수 있습니다.</td>
							</tr>
							<tr>
								<th>해외연수</th>
								<td>외국어능력을 향상시키고 다른 나라의 문화를 체험하기 위해 타국에서 일정 기간 동안 공무를 하는 것입니다. </td>
							</tr>
							<tr>
								<th>동아리</th>
								<td>
									서로의 관심사나 취미를 함께 공유하고 체험하는 단체를 의미하는 것으로서, 다양한 사람들과 인간관계를 맺고 다양한 경험을 쌓을 수 있다는 점에서 권장 됩니다.
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />