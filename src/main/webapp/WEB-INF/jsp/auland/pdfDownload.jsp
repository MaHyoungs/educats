<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="ko">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="Content-Script-Type" content="text/javascript">
		<meta http-equiv="Content-Style-Type" content="text/css">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><title>eduCAT</title></title>
		<link charset="utf-8" href="/template/common/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/common.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/style.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/pdfArea.css" type="text/css" rel="stylesheet" />
		<script src="/template/common/js/jquery-1.9.1.min.js"></script>
		<script src="/template/common/jquery-ui/jquery-ui.js"></script>
		<script src="/template/common/js/jsPDF/html2canvas.js"></script>
		<script src="/template/common/js/jsPDF/jsPDF.js"></script>
		<script src="/template/common/js/common.js"></script>
		<script src="/template/common/js/d3.v3.min.js"></script>
		<script src="/template/common/js/layerpopup.js"></script>
		<script src="/template/common/js/chart_pie.js"></script>
		<%--<script src="/template/common/js/jsPDF/canvas2image.js"></script> jsPDF 이미지컷팅할때 사용--%>
		<%--<script src="/template/common/js/jsPDF/addimage.js"></script> jsPDF 이미지형식으로 추가할때 사용--%>
	</head>
	<body>
		<div id="pdfArea"><div class="pdfWrap surveyresult"></div></div>
		<div id="pdfArea2"><div class="pdfWrap persummary"></div></div>
		<div id="pdfArea3"><div class="pdfWrap peranalysL1"></div></div>
		<div id="pdfArea4"><div class="pdfWrap peranalysL2"></div></div>
		<div id="pdfArea5"><div class="pdfWrap peranalysL3"></div></div>
		<div id="pdfArea6"><div class="pdfWrap peranalysM"></div></div>
		<div id="pdfArea7"><div class="pdfWrap pertrust"></div></div>
		<div id="pdfArea8"><div class="pdfWrap affsummary"></div></div>
		<div id="pdfArea9"><div class="pdfWrap affanalys"></div></div>
		<div id="pdfArea10"><div class="pdfWrap affmajAnalys1"></div></div>
		<div id="pdfArea11"><div class="pdfWrap affmajAnalys2"></div></div>
		<div id="pdfArea12"><div class="pdfWrap affauGuide"></div></div>
		<div id="pdfArea13"><div class="pdfWrap cloneresultInfo"></div></div>
		<div id="pdfArea14"><div class="pdfWrap cloneunivInfo"></div></div>
		<div id="pdfArea15"><div class="pdfWrap clonejobInfo"></div></div>
<%--
		<div class="pdfWrap persummary"></div>
		<div class="pdfWrap peranalysL1"></div>
		<div class="pdfWrap peranalysL2"></div>
		<div class="pdfWrap peranalysL3"></div>
		<div class="pdfWrap peranalysM"></div>
		<div class="pdfWrap pertrust"></div>
		<div class="pdfWrap affsummary"></div>
		<div class="pdfWrap affanalys"></div>
		<div class="pdfWrap affmajAnalys1"></div>
		<div class="pdfWrap affmajAnalys2"></div>
		<div class="pdfWrap affauGuide"></div>
		<div class="pdfWrap cloneresultInfo"></div>
		<div class="pdfWrap cloneunivInfo"></div>
		<div class="pdfWrap clonejobInfo"></div>
--%>
		<%@ include file="/WEB-INF/jsp/auland/chkAdms.jsp" %>
		<div id="iframeArea">
			<iframe width="300" height="200" frameborder="0" id="iFrameSurveyresult"    src="${uri_set}/surveyResult?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFramePerSummary"      src="${uri_set}/per/summary?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFramePeranalysL"      src="${uri_set}/per/analysL?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFramePeranalysM"      src="${uri_set}/per/analysM?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFramePertrust"        src="${uri_set}/per/trust?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameAffsummary"      src="${uri_set}/aff/summary?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameAffanalys"       src="${uri_set}/aff/analys?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameAffmajAnalys"    src="${uri_set}/aff/majAnalys?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameAffauGuide"      src="${uri_set}/aff/auGuide?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameCloneresultInfo" src="${uri_set}/clone/resultInfo?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameCloneunivInfo"   src="${uri_set}/clone/univInfo?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
			<iframe width="300" height="200" frameborder="0" id="iFrameClonejobInfo"    src="${uri_set}/clone/jobInfo?seq_tme=${param.seq_tme}&who=${auth_id}"></iframe>
		</div>
		<script>
			var img = new Array(15);
			/**
			 * PDF 다운로드
			 * 화면캡쳐
			 */
			function fnCopy(){
				$(document).find("#pdfArea").css("display","block");
				$(window.parent.frames["conFrame"].document).contents().find("#pdfLoading").css("display","block");

				$("iframe").contents().find("#pdfLoading").remove();
				$("iframe").contents().find("#pdfArea").remove();
				$("iframe").contents().find("#pdfArea2").remove();
				$("iframe").contents().find("#pdfArea3").remove();
				$("iframe").contents().find("#pdfArea4").remove();
				$("iframe").contents().find("#pdfArea5").remove();
				$("iframe").contents().find("#pdfArea6").remove();
				$("iframe").contents().find("#pdfArea7").remove();
				$("iframe").contents().find("#pdfArea8").remove();
				$("iframe").contents().find("#pdfArea9").remove();
				$("iframe").contents().find("#pdfArea10").remove();
				$("iframe").contents().find("#pdfArea11").remove();
				$("iframe").contents().find("#pdfArea12").remove();
				$("iframe").contents().find("#pdfArea13").remove();
				$("iframe").contents().find("#pdfArea14").remove();
				$("iframe").contents().find("#pdfArea15").remove();
				$("iframe").contents().find("#iframeArea").remove();

				$("#pdfArea .surveyresult").append($("#iFrameSurveyresult").contents().find("#container").html()); // 01page : 검사결과
				$("#pdfArea .surveyresult table.tResultPersent thead td[width=27]").attr("width", "").addClass("wid59");
				$("#pdfArea .surveyresult table.tResultPersent colgroup").remove();

				html2canvas($("#pdfArea"), {
					onrendered: function (canvas) {
						img[0] = canvas.toDataURL("image/jpeg");
						fnCopy2();
					}
				});

				console.log("01: html");
			}

			function fnCopy2() {
				$(document).find("#pdfArea").css("display","none");
				$(document).find("#pdfArea2").css("display","block");
				$("#pdfArea2 .persummary").append($("#iFramePerSummary").contents().find("#container").html()).find(".header").html("01. 인성검사 결과"); // 02page : 01.인성검사 결과
				$("#pdfArea2 .persummary table.tResultPersent thead td[width=27]").attr("width", "").addClass("wid59");
				$("#pdfArea2 .persummary table.tResultPersent colgroup").remove();

				html2canvas($("#pdfArea2"), {
					onrendered: function (canvas) {
						img[1] = canvas.toDataURL("image/jpeg");
						fnCopy3();
					}
				});
			}

			function fnCopy3() {
				$(document).find("#pdfArea2").css("display","none");
				$(document).find("#pdfArea3").css("display","block");
				$("#pdfArea3 .peranalysL1").append($("#iFramePeranalysL").contents().find("#container").html()).find(".header").html("1) 하위 인성별 수준분석"); // 03page : 1)하위 인성별 수준분석

				html2canvas($("#pdfArea3"), {
					onrendered: function (canvas) {
						img[2] = canvas.toDataURL("image/jpeg");
						fnCopy4();
					}
				});
			}

			function fnCopy4() {
				$(document).find("#pdfArea3").css("display","none");
				$(document).find("#pdfArea4").css("display","block");
				$("#pdfArea4 .peranalysL2").append($("#iFramePeranalysL").contents().find("#container").html()).find(".header").html("인성검사 상세결과"); // 04page : 인성검사 상세결과

				html2canvas($("#pdfArea4"), {
					onrendered: function (canvas) {
						img[3] = canvas.toDataURL("image/jpeg");
						fnCopy5();
					}
				});
			}

			function fnCopy5() {
				$(document).find("#pdfArea4").css("display","none");
				$(document).find("#pdfArea5").css("display","block");
				$("#pdfArea5 .peranalysL3").append($("#iFramePeranalysL").contents().find(".layerPopup.analysL .conContainer").html()).find("table").before("<div class='header'>인성이란</div>"); // 05page : 인성이란

				html2canvas($("#pdfArea5"), {
					onrendered: function (canvas) {
						img[4] = canvas.toDataURL("image/jpeg");
						fnCopy6();
					}
				});
			}

			function fnCopy6() {
				$(document).find("#pdfArea5").css("display","none");
				$(document).find("#pdfArea6").css("display","block");
				$("#pdfArea6 .peranalysM").append($("#iFramePeranalysM").contents().find("#container").html()).find(".header").html("2) 주요인성군별 수준분석"); // 06page : 2)주요인성군별 수준분석

				html2canvas($("#pdfArea6"), {
					onrendered: function (canvas) {
						img[5] = canvas.toDataURL("image/jpeg");
						fnCopy7();
					}
				});
			}

			function fnCopy7() {
				$(document).find("#pdfArea6").css("display","none");
				$(document).find("#pdfArea7").css("display","block");
				$("#pdfArea7 .pertrust").append($("#iFramePeranalysM").contents().find(".layerPopup.analysM .conContainer").html()).find("table").before("<div class='header'>주요인성군이란?</div>"); // 07page : 주요인성군이란?
				$("#pdfArea7 .pertrust .cellspacing").remove();
				$("#pdfArea7 .pertrust").append($("#iFramePertrust").contents().find("#container").html()).find("#content .header").html("3) 검사신뢰도"); // 07page : 3)검사신뢰도

				html2canvas($("#pdfArea7"), {
					onrendered: function (canvas) {
						img[6] = canvas.toDataURL("image/jpeg");
						fnCopy8();
					}
				});
			}

			function fnCopy8() {
				$(document).find("#pdfArea7").css("display","none");
				$(document).find("#pdfArea8").css("display","block");
				$("#pdfArea8 .affsummary").append($("#iFrameAffsummary").contents().find("#container").html()).find("#content .header").html("02. 진로적성검사결과"); // 08page :02. 진로적성검사결과

				html2canvas($("#pdfArea8"), {
					onrendered: function (canvas) {
						img[7] = canvas.toDataURL("image/jpeg");
						fnCopy9();
					}
				});
			}

			function fnCopy9() {
				$(document).find("#pdfArea8").css("display","none");
				$(document).find("#pdfArea9").css("display","block");
				$("#pdfArea9 .affanalys").append($("#iFrameAffanalys").contents().find("#container").html()).find("#content .header").html("1) 학업성향 분석"); // 09page : 1)학업성향 분석

				html2canvas($("#pdfArea9"), {
					onrendered: function (canvas) {
						img[8] = canvas.toDataURL("image/jpeg");
						fnCopy10();
					}
				});
			}

			function fnCopy10() {
				$(document).find("#pdfArea9").css("display","none");
				$(document).find("#pdfArea10").css("display","block");
				$("#pdfArea10 .affmajAnalys1").append($("#iFrameAffanalys").contents().find("#container").html()); // 10 - 학과 선택 가이드 1
				$("#pdfArea10 .affmajAnalys1").append($("#iFrameAffmajAnalys").contents().find("#container").html()).find("#content .header").html("2) 학과 선택 가이드"); // 10page : 2)학과 선택 가이드 1

				html2canvas($("#pdfArea10"), {
					onrendered: function (canvas) {
						img[9] = canvas.toDataURL("image/jpeg");
						fnCopy11();
					}
				});
			}

			function fnCopy11() {
				$(document).find("#pdfArea10").css("display","none");
				$(document).find("#pdfArea11").css("display","block");
				$("#pdfArea11 .affmajAnalys2").append($("#iFrameAffmajAnalys").contents().find("#container").html()); // 11page : 2)학과 선택 가이드 2

				html2canvas($("#pdfArea11"), {
					onrendered: function (canvas) {
						img[10] = canvas.toDataURL("image/jpeg");
						fnCopy12();
					}
				});
			}

			function fnCopy12() {
				$(document).find("#pdfArea11").css("display","none");
				$(document).find("#pdfArea12").css("display","block");
				$("#pdfArea12 .affauGuide").append($("#iFrameAffauGuide").contents().find("#container").html()).find("#content .header").html("3) 진로&middot;직업 선택 가이드"); // 12page : 진로.직업 선택 가이드

				html2canvas($("#pdfArea12"), {
					onrendered: function (canvas) {
						img[11] = canvas.toDataURL("image/jpeg");
						fnCopy13();
					}
				});
			}

			function fnCopy13() {
				$(document).find("#pdfArea12").css("display","none");
				$(document).find("#pdfArea13").css("display","block");
				$("#pdfArea13 .cloneresultInfo").append($("#iFrameCloneresultInfo").contents().find("#container").html()).find("#content.cloneResultInfo .header").html("03. 클론 매칭"); // 13page : 03.클론매칭
				$("#pdfArea13 .cloneresultInfo").append($("#iFrameCloneunivInfo").contents().find("#container").html()).find("#content.cloneUnivInfo .header").html("1) 대학생활 정보"); // 13page : 1)대학생활정보

				html2canvas($("#pdfArea13"), {
					onrendered: function (canvas) {
						img[12] = canvas.toDataURL("image/jpeg");
						fnCopy14();
					}
				});
			}

			function fnCopy14() {
				$(document).find("#pdfArea13").css("display","none");
				$(document).find("#pdfArea14").css("display","block");
				$("#pdfArea14 .cloneunivInfo").append($("#iFrameCloneunivInfo").contents().find("#container").html()); // 14page : 용어설명
				$("#pdfArea14 .cloneunivInfo").append($("#iFrameClonejobInfo").contents().find("#container").html()).find("#content .header").html("2) 취업정보"); // 14page : 2)취업정보 1

				html2canvas($("#pdfArea14"), {
					onrendered: function (canvas) {
						img[13] = canvas.toDataURL("image/jpeg");
						fnCopy15();
					}
				});
			}

			function fnCopy15() {
				$(document).find("#pdfArea14").css("display","none");
				$(document).find("#pdfArea15").css("display","block");
				$("#pdfArea15 .clonejobInfo").append($("#iFrameClonejobInfo").contents().find("#container").html()); // 15page : 2)취업정보 2

				html2canvas($("#pdfArea15"), {
					onrendered: function (canvas) {
						img[14] = canvas.toDataURL("image/jpeg");

						var doc = new jsPDF('p', 'pt', [1000, 1415]);
						doc.addImage(img[0],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[1],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[2],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[3],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[4],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[5],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[6],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[7],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[8],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[9],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[10],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[11],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[12],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[13],0,0,canvas.width,canvas.height);
						doc.addPage();
						doc.addImage(img[14],0,0,canvas.width,canvas.height);

						console.log("03: addHTML");
						$(window.parent.frames["conFrame"].document).contents().find("#pdfLoading").css("display","none");
						/*
						 * 파일명생성기준 -> 학년도 + 학교/기관명 + 라이선스연차 + 검사종류 + 검사회차 + (학년) + 반 + (번호) + 이름 + 아이디
						 * 저장파일명예시 -> 2016년_한국중학교_1학년_1반_홍길동_1회차_진로탐색검사결과.pdf
						 *
						 * [검사일자_검사종류_학교/기관명_학년반(학급)_이름]
						 * [20160401_진로탐색검사결과_한국중학교_1학년1반_홍길동]
						 */
						<c:if test="${userPdfInfo.schorg_typ_cd ne 'ORG01'}">
//							doc.save('${userPdfInfo.st_year_cd}년_${userPdfInfo.lic_max}년차_${userPdfInfo.tme_num}회차_${userPdfInfo.schorg_nm}_${userPdfInfo.st_grade_num}학년_${userPdfInfo.st_class}반_${usrInfo.who_nm}_${userPdfInfo.survey_typ_nm}결과.pdf');
							doc.save('${userPdfInfo.reg_dt}_${userPdfInfo.survey_typ_nm}결과_${userPdfInfo.schorg_nm}_${userPdfInfo.st_grade_num}학년${userPdfInfo.st_class}반_${usrInfo.who_nm}.pdf');
						</c:if>
						<c:if test="${userPdfInfo.schorg_typ_cd eq 'ORG01'}">
//							doc.save('${userPdfInfo.st_year_cd}년_${userPdfInfo.lic_max}년차_${userPdfInfo.tme_num}회차_${userPdfInfo.schorg_nm}_${userPdfInfo.st_class}_${usrInfo.who_nm}_${userPdfInfo.survey_typ_nm}결과.pdf');
							doc.save('${userPdfInfo.reg_dt}_${userPdfInfo.survey_typ_nm}결과_${userPdfInfo.schorg_nm}_${userPdfInfo.st_class}_${usrInfo.who_nm}.pdf');
						</c:if>
					}
				});
			}

			// TODO : 12개 iframe 모두 로딩 후 처리 방안 찾기
			// 임시로 첫번째 iframe 로딩 후 pdf버튼 활성화 처리
			$("#iFrameSurveyresult").load(function(){
				$(window.parent.frames["conFrame"].document).contents().find(".btnArea").css("display","block");
//				$(this).contents().find("#footer").addClass("footer");
			});
		</script>
	</body>
</html>