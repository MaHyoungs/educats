<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:choose>
	<c:when test="${param.adms eq 'Y'}">
		<c:set var="uri_set" value="/mng" />
		<c:set var="uri_param" value="&adms=Y" />
	</c:when>
	<c:otherwise>
		<c:if test="${user.user_typ_cd eq '04'}">
			<c:set var="uri_param" value="&tchs=Y" />
		</c:if>
		<c:set var="uri_set" value="/au" />
	</c:otherwise>
</c:choose>
<c:set var="auth_id" value="${param.who}" />