<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultMajAnalys">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/aff/analys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학업성향분석</span></a>
				<a href="${uri_set}/aff/majAnalys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학과 선택 가이드</span></a>
				<a href="${uri_set}/aff/auGuide?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">진로&middot;직업 선택 가이드</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">진로&middot;직업 선택</span>
				<span class="content">
					다음은 ${usrInfo.who_nm}님이 높은 적성을 가지고 있는 것으로 나타난 계열과 전공으로 추천되는 직업입니다. 직업 별 설명과 추천되는 자료 및 경험 등을 참고로 어떠한 준비를 하면 좋은지 살펴보시기 바랍니다.
				</span>
			</div>
			<c:forEach var="rs" items="${majResultBrif}" varStatus="sts">
				<c:if test="${rs.rank eq '1'}">
					<c:set var="majbrancharea" value="${rs.brancharea_txt}" />
					<c:set var="majbranchjob" value="${rs.branchjob_txt}" />
					<c:set var="majprofjob" value="${rs.profjob_txt}" />
					<c:set var="majrelcert" value="${rs.relcert_txt}" />
				</c:if>
			</c:forEach>

			<%-- TODO : 하드코딩된 관련 자격증.면허 뿌려주는 조건 DB 추가 설계 필요  --%>
			<c:set var="majrelcertA_idx" value="${fn:indexOf(majrelcert, '* 국가자격')}" />
			<c:set var="majrelcertB_idx" value="${fn:indexOf(majrelcert, '* 민간자격')}" />
			<c:set var="majrelcertC_idx" value="${fn:indexOf(majrelcert, '* 해외자격')}" />

			<c:if test="${majrelcertA_idx ge 0}">
				<c:set var="majrelcertA" value="${fn:substringAfter(majrelcert, '* 국가자격')}" />
				<c:if test="${majrelcertB_idx ge 0}">
					<c:set var="majrelcertA" value="${fn:substringBefore(majrelcertA, '* 민간자격')}" />
				</c:if>
				<c:if test="${majrelcertC_idx ge 0}">
					<c:set var="majrelcertA" value="${fn:substringBefore(majrelcertA, '* 해외자격')}" />
				</c:if>
			</c:if>

			<c:if test="${majrelcertB_idx ge 0}">
				<c:set var="majrelcertB" value="${fn:substringAfter(majrelcert, '* 민간자격')}" />
				<c:if test="${majrelcertC_idx ge 0}">
					<c:set var="majrelcertB" value="${fn:substringBefore(majrelcertB, '* 해외자격')}" />
				</c:if>
			</c:if>

			<c:if test="${majrelcertC_idx ge 0}">
				<c:set var="majrelcertC" value="${fn:substringAfter(majrelcert, '* 해외자격')}" />
			</c:if>

			<c:if test="${majrelcertA_idx lt 0 and majrelcertB_idx lt 0 and majrelcertC_idx lt 0}">
				<c:set var="majrelcertETC" value="${majrelcert}" />
			</c:if>

			<div class="section01">
				<span class="title">${usrInfo.who_nm} 님의 적성에 잘 맞는 진출분야 및 직업은?</span>
				<div class="tableSection">
					<table class="tResultAnalysTend" summary="">
						<caption></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<div class="title wid100">진출분야</div>
									<div class="content">${majbrancharea}</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="title wid100">진출 직업</div>
									<div class="content">${majbranchjob}</div>
								</td>
							</tr>
							<tr>
								<td>
									<div class="title wid320">일정 이상의 학력 및 전문성을 갖춰야 유리한 직업</div>
									<div class="content">${majprofjob}</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="section01">
				<span class="title">유용한 정보</span>
				<div class="tableSection">
					<table class="tAuGuide" summary="">
						<caption></caption>
						<colgroup>
							<col width="*" />
						</colgroup>
						<tbody>
							<tr>
								<td>
									<span class="title">관련 자격증.면허</span>
									<c:if test="${not empty majrelcertA}">
										<span class="subTitle">국가자격</span>
										<span class="content pT5">
											${majrelcertA}
										</span>
									</c:if>
									<c:if test="${not empty majrelcertB}">
										<span class="subTitle">민간자격</span>
										<span class="content pT5">
											${majrelcertB}
										</span>
									</c:if>
									<c:if test="${not empty majrelcertC}">
										<span class="subTitle">해외자격</span>
										<span class="content pT5">
											${majrelcertC}
										</span>
									</c:if>
									<c:if test="${not empty majrelcertETC}">
										<span class="content pT5 lhei25">
											${majrelcertETC}
										</span>
									</c:if>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />