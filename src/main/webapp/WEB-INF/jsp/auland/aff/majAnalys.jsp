<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultMajAnalys">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/aff/analys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학업성향분석</span></a>
				<a href="${uri_set}/aff/majAnalys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">학과 선택 가이드</span></a>
				<a href="${uri_set}/aff/auGuide?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>진로&middot;직업 선택 가이드</span></a>
			</div>
		<div class="tabInfo">
			<span class="title">대학 전공&middot;학과 선택</span>
			<span class="content">
				대학 진학 시 학생들은 어떤 학문을 전공할 것인지 선택을 하게 되는데, 전공학과는 직업선택과 밀접한 관계가 있기 때문에 매우 중요합니다. 만약 자신의 적성에 맞지 않는 전공을 선택할 시 대학생활의 부적응을 초래할 수 있으며 심할 경우 전공을 바꾸어야 하는 경우도 있습니다. 때문에 무엇보다도 자신의 흥미와 적성에 부합하는 전공이 무엇인지 아는 것이 중요하며, 이에 맞추어 준비를 할 필요가 있습니다. 다음은 ${usrInfo.who_nm}님의 응답 결과를 분석하였을 때 ${usrInfo.who_nm}님에게 추천되는 학과입니다.
			</span>
		</div>
		<c:forEach var="rs" items="${majResultBrif}" varStatus="sts">
			<c:if test="${rs.rank eq '1'}">
				<c:set var="majId1" value="${rs.ctgrydp3_id}" />
				<c:set var="majnm1" value="${rs.ctgry3_nm}" />
				<c:set var="majintro1" value="${rs.intro_txt}" />
				<c:set var="majinterest1" value="${rs.interest_txt}" />
			</c:if>
			<c:if test="${rs.rank eq '2'}">
				<c:set var="majId2" value="${rs.ctgrydp3_id}" />
				<c:set var="majnm2" value="${rs.ctgry3_nm}" />
				<c:set var="majintro2" value="${rs.intro_txt}" />
				<c:set var="majinterest2" value="${rs.interest_txt}" />
			</c:if>
			<c:if test="${rs.rank eq '3'}">
				<c:set var="majId3" value="${rs.ctgrydp3_id}" />
				<c:set var="majnm3" value="${rs.ctgry3_nm}" />
				<c:set var="majintro3" value="${rs.intro_txt}" />
				<c:set var="majinterest3" value="${rs.interest_txt}" />
			</c:if>
		</c:forEach>
		<div class="section01">
			<span class="title">${usrInfo.who_nm} 님에게 추천되는 학과는?</span>
			<div class="tableSection">
				<table class="tResultAnalysTend" summary="">
					<caption></caption>
					<colgroup>
						<col width="178" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th rowspan="4">
								<span class="iconTitle">1순위</span>
								<span class="iconWrap">
									<i class="${fn:toLowerCase(majId1)} sky"></i>
									<span class="iconSubject">${majnm1}</span>
								</span>
							</th>
							<td class="titleSec">
								<p><span class="cl_339cde">${usrInfo.who_nm}</span> 님의 적성에 가장 잘 맞는 학과는 <span class="cl_ff8400">[${majnm1}]</span> 입니다.</p>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">학과소개</span>
								<span class="content">${majintro1}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">적성과 흥미</span>
								<span class="content">${majinterest1}</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tableSection">
				<table class="tResultAnalysTend" summary="">
					<caption></caption>
					<colgroup>
						<col width="178" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th rowspan="4">
								<span class="iconTitle">2순위</span>
								<span class="iconWrap">
									<i class="${fn:toLowerCase(majId2)} sky"></i>
									<span class="iconSubject">${majnm2}</span>
								</span>
							</th>
							<td class="titleSec">
								<p><span class="cl_339cde">${usrInfo.who_nm}</span> 님의 적성에 두 번째로 잘 맞는 학과는 <span class="cl_ff8400">[${majnm2}]</span> 입니다.</p>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">학과소개</span>
								<span class="content">${majintro2}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">적성과 흥미</span>
								<span class="content">${majinterest2}</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tableSection">
				<table class="tResultAnalysTend" summary="">
					<caption></caption>
					<colgroup>
						<col width="178" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th rowspan="4">
								<span class="iconTitle">3순위</span>
								<span class="iconWrap">
									<i class="${fn:toLowerCase(majId3)} sky"></i>
									<span class="iconSubject">${majnm3}</span>
								</span>
							</th>
							<td class="titleSec">
								<p><span class="cl_339cde">${usrInfo.who_nm}</span> 님의 적성에 세 번째로 잘 맞는 학과는 <span class="cl_ff8400">[${majnm3}]</span> 입니다.</p>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">학과소개</span>
								<span class="content">${majintro3}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">적성과 흥미</span>
								<span class="content">${majinterest3}</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />