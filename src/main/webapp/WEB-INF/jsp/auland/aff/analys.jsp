<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultAnalys">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>결과요약</span></a>
				<a href="${uri_set}/aff/analys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">학업성향분석</span></a>
				<a href="${uri_set}/aff/majAnalys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학과 선택 가이드</span></a>
				<a href="${uri_set}/aff/auGuide?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>진로&middot;직업 선택 가이드</span></a>
			</div>
			<div class="tabInfo">
				<span class="content">
					고등학교에 진학하게 되면 학생들은 언어, 수학, 과학, 음악, 체육 등 다양한 과목에 대한 학습을 하게 됩니다. 이때 어떤 학생은 언어에 더 흥미를 느낄 것이며, 또 어떤 학생은 수학에 더 흥미를 느낄 것입니다.  이렇듯 검사자가 어떠한 과목과 학업 분야에 관심을 가지고 있는지는 이후 대학진학 시 계열과 전공 선택의 중요한 키가 됩니다. 때문에 자신이 어떠한 학업 분야에 특히 더 관심을 가지고 있는지를 아는 것이 중요하며, 이에 따라 다른 학습 방법을 사용해야 합니다.<br/>
					또한 자신의 흥미나 적성은 고려하지 않은 채 다른 누군가의 의견에 따라 혹은 성적 등과 같은 조건에 의한 선택은 학업생활의 혼란과 부적응이라는 결과로 이어질 수 있으므로 주의하여야 합니다.  아래 결과는 ${usrInfo.who_nm}님의 응답 결과를 분석하였을 때 ${usrInfo.who_nm}님에게 좀 더 적합한 것으로 나타난 학업 분야입니다. 향후 학과 선택의 가이드로서 본 검사 결과 내용을 참고하시기 바랍니다.
				</span>
			</div>
			<c:forEach var="rs" items="${majResultBrif}" begin="0" end="0">
				<c:set var="ppsid" value="${rs.ctgry_id}" />
				<c:set var="ppsnm" value="${rs.ctgry2_nm}" />
				<c:set var="relmaj_txt" value="${rs.relmaj_txt}" />
			</c:forEach>
			<c:forEach var="rs" items="${ppsResultDetail}" >
				<c:if test="${rs.ctgry2_id eq ppsid}">
					<c:set var="intro_txt" value="${rs.intro_txt}" />
					<c:set var="content_txt" value="${rs.content_txt}" />
					<c:set var="career_txt" value="${rs.career_txt}" />
				</c:if>
				<c:if test="${rs.ctgry2_id ne ppsid}">
				</c:if>
			</c:forEach>
			<div class="section01">
				<span class="title">${usrInfo.who_nm} 님의 학업 성향은?</span>
				<table class="tResultAnalysTend" summary="">
					<caption></caption>
					<colgroup>
						<col width="178" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th rowspan="4">
								<span class="iconTitle">최적학업성향</span>
								<span class="iconWrap">
									<i class="${fn:toLowerCase(ppsid)} sky"></i>
									<span class="iconSubject">${ppsnm}계열</span>
								</span>
							</th>
							<td class="titleSec">
								<p><span class="cl_339cde">${usrInfo.who_nm}</span> 님은 <span class="cl_ff8400">[${ppsnm}계열]</span> 과 관련된 과목을 공부할 때 흥미를 느낄 가능성이 높습니다.</p>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">소개</span>
								<span class="content">${intro_txt}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">학습내용</span>
								<span class="content">${content_txt}</span>
							</td>
						</tr>
						<tr>
							<td>
								<span class="title">진로</span>
								<span class="content">${career_txt}</span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section02">
				<span class="title">${usrInfo.who_nm} 님의 학업성향과 관련이 있는 전공들로,<br/>대학 진학 시 해당 전공을 선택할 경우 좀 더 흥미를 가지고 학습을 할 수 있습니다.</span>
					<c:forTokens var="item" items="${relmaj_txt}" delims=",">
					   <span class="content">${item}</span>
					</c:forTokens>
				<span class="info">※대표적인 전공들을 간추린 것이니 참고하시기 바랍니다.</span>
			</div>
			<div class="section03">
				<span class="title">학업성향 분포</span>
				<div class="subSection01"><span class="cl_339cde">${usrInfo.who_nm}</span> 님의 <span class="cl_ff8400">학업성향</span> 분석 결과</div>
				<div class="subSection02">
					<div class="tableSection">
						<span class="title">학업성향 선호 순위</span>
						<table class="tResultAnalysLMoreResult" summary="">
							<caption></caption>
							<colgroup>
								<col width="112" />
								<col width="245" />
							</colgroup>
							<thead>
								<tr>
									<th>순위</th>
									<th>학업계열</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>1위</th>
									<td>${ppsnm}</td>
								</tr>
								<c:set var="rank" value="2" />
								<c:set var="cnts" value="1" />
								<c:forEach var="rs" items="${ppsResultDetail}" varStatus="sts">
									<c:if test="${ppsid ne rs.ctgry2_id}" >
										<c:if test="${sts.last ne true}">
											<c:set var="grpas" value="${grpas}${cnts}:'${rs.ctgry2_nm}',"/>
										</c:if>
										<c:if test="${sts.last}">
											<c:set var="grpas" value="${grpas}${cnts}:'${rs.ctgry2_nm}'"/>
										</c:if>
										<tr>
											<th>${rank}위</th>
											<td>${rs.ctgry2_nm}</td>
										</tr>
										<c:set var="rank" value="${rank+1}" />
										<c:set var="cnts" value="${cnts+1}" />
									</c:if>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<c:set var="grap" value="0:'${ppsnm}',${grpas}" />
					<script>
						jQuery(function($){
							var  chartBarData = [100, 80, 60, 40, 20]
								,chartBarColor = ["#3266a5", "#c76161", "#c79b61", "#31a5a4", "#9f5abb"]
								,chartBarSum = [{${grap}}
									, {0:"100", 1:"80", 2:"60", 3:"40", 4:"20"}
									, {0:"#3266a5", 1:"#c76161", 2:"#c79b61", 3:"#31a5a4", 4:"#9f5abb"}];

							fnChartBar(".chartBar2", chartBarData, chartBarColor, chartBarSum);
							});
					</script>
					<div class="chartSection">
						<div class="chartBar2">
							<div class="barWrap barWrap0">
								<div class="bar"></div>
								<div class="text"><span></span></div>
							</div>
							<div class="barWrap barWrap1">
								<div class="bar"></div>
								<div class="text"><span></span></div>
							</div>
							<div class="barWrap barWrap2">
								<div class="bar"></div>
								<div class="text"><span></span></div>
							</div>
							<div class="barWrap barWrap3">
								<div class="bar"></div>
								<div class="text"><span></span></div>
							</div>
							<div class="barWrap barWrap4">
								<div class="bar"></div>
								<div class="text"><span></span></div>
							</div>
						</div>
						<%--<svg class="chartBar2"></svg>--%>
					</div>
				</div>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />