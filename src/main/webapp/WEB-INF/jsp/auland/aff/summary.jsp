<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyMoreResult surveyMoreResultTrust">
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="tabSection01">
			<a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>01. 인성검사결과</span></a>
			<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">02. 진로적성검사결과</span></a>
			<a href="${uri_set}/clone/resultInfo?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>03. 클론매칭</span></a>
		</div>
		<div class="sectionWrap">
			<div class="tabSection02">
				<a href="${uri_set}/aff/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span class="on">결과요약</span></a>
				<a href="${uri_set}/aff/analys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학업성향분석</span></a>
				<a href="${uri_set}/aff/majAnalys?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>학과 선택 가이드</span></a>
				<a href="${uri_set}/aff/auGuide?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}"><span>진로&middot;직업 선택 가이드</span></a>
			</div>
			<div class="tabInfo">
				<span class="title">진로탐색검사 - 02. 진로적성검사 소개</span>
				<span class="content">
					중고등학생들의 경우 아직 자기자신이 무엇을 좋아하고 잘 하는지 불분명하여 진로를 선택하는데 어려움을 겪습니다.<br/>
					자신의 흥미와 적성을 인식하고 확립해 가는 시기에 올바른 결정을 내리지 못할 경우, 이후 대학 학과 선택 뿐만 아니라 직업 선택에까지 영향을 미칠 수 있습니다.<br/>
					때문에 이 시기에 자신의 흥미와 적성을 파악하는 과정은 필수이며 매우 중요합니다.<br/>
					본 검사는 진로에 대한 고민을 겪는 학생들로 하여금 자신의 흥미와 적성을 파악하고 종합적으로 진단해 볼 수 있도록 고안된 검사입니다. 검사를 통해 스스로를 진단해 보고 향후 진로 및 학과 선택의 가이드로 활용하시기 바랍니다.
				</span>
			</div>
			<div class="section01">
				<span class="title">진로탐색검사 - 02. 진로적성검사 결과지 구성</span>
				<ul>
					<li><span>01. </span>결과요약</li>
					<li><span>02. </span>하위 성향 분석</li>
					<li><span>03. </span>학과전공선택가이드</li>
					<li><span>04. </span>진로직업선택가이드</li>
				</ul>
			</div>
			<div class="section02">
				<table class="tResultIcon" summary="">
					<colgroup>
						<col width="*" />
						<col width="*" />
						<col width="*" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>최적학업성향</th>
							<th colspan="3">추천학과</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="rs" items="${majResultBrif}" varStatus="st">
								<c:if test="${st.first}" >
									<c:set var="brancharea_txt" value="${rs.brancharea_txt}" />
									<c:set var="branchjob_txt" value="${rs.branchjob_txt}" />
									<td class="fwB">
										<i class="${fn:toLowerCase(rs.ctgry_id)} sky"></i>
										<span>${rs.ctgry2_nm}계열</span>
									</td>
								</c:if>
								<td class="fwB">
									<i class="${fn:toLowerCase(rs.ctgrydp3_id)} blue"></i>
									<span>${rs.ctgry3_nm}</span>
								</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="section03">
				<table class="tResultTrust" summary="">
					<colgroup>
						<col width="118" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th>진출분야</th>
							<%--<td>${fn:substring(brancharea_txt, 0, 50)}..</td>--%>
							<td>${brancharea_txt}</td>
						</tr>
						<tr>
							<th>진출직업</th>
							<td>${branchjob_txt}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="botBg"></div>
	</div>
</div>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />