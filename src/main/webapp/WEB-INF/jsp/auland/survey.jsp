<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="survey auSurvey">
		<div class="section_top">
			<div class="title">진로탐색검사</div>
			<div class="info">
				중고등학교 시절은 진로를 탐색하는 시기로서 이 시기의 대다수 학생들은 자신의 진로에<br/>
				대한 인식이 불분명하여 선택의 어려움을 겪습니다.<br/>
				이 시기의 선택이 향후 대학의 전공 선택으로까지 이어지는 경우가 많기 때문에, 만약<br/>
				이 시기에 올바른 결정을 내리지 못할 경우, 향후 진로와<br/>
				적성의 불일치로 인해 혼란을 겪을 수 있습니다. 때문에 자신의 성향과 적성을 조기에<br/>
				파악하는 과정은 필수이며 매우 중요합니다.<br/>
				진로탐색검사는 아직 자신에 대한 객관적인 지식이 부족한 중고등학생이 자신의 인성<br/>
				수준과 적성을 객관적으로 진단해 볼 수 있도록 고안된 종합 인성, 적성 검사입니다.<br/>
				검사를 통해 스스로를 진단해 보고 자아탐색과 진로선택의 가이드로 활용하시기 바랍니다.
			</div>
		</div>
		<div class="section_bot">
			<table class="tSurvey" summary="">
				<caption></caption>
				<colgroup>
					<col width="50" />
					<col width="370" />
					<col width="250" />
					<col width="200" />
					<col width="20" />
				</colgroup>
				<thead>
					<tr>
						<td colspan="3">검사 시작하기</td>
						<c:if test="${tmeChk eq 'Y'}">
							<c:if test="${perChk.anst_typ_cd eq '02' and affChk.anst_typ_cd eq '02' and majChk.anst_typ_cd eq '02'}">
								<td class="taC poR">
									<%--<span class="saBtn"><a href="/au/surveyResult?seq_tme=${majChk.seq_tme}&who=${user.user_id}" class="poA btnSurveyResult">검사결과 확인</a></span>--%>
									<span class="saBtn"><a href="/au/aulandResult?seq_tme=${majChk.seq_tme}&who=${user.user_id}" class="poA btnSurveyResult">검사결과 확인</a></span>
									<%--<span class="saBtn"><a href="/au/surveyResult?seq_tme=${auRecent.seq_tme}&who=${user.user_id}" class="poA btnSurveyResult">검사결과 확인</a></span>--%>
								</th>
							</c:if>
						</c:if>
						<td></td>
					</tr>
					<tr>
						<th>No.</th>
						<th>검사명</th>
						<th>문항/시간</th>
						<th>진행상태</th>
						<td></td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>1</td>
						<td>인성검사</td>
						<td>100문항 / 30분</td>
						<td>
							<span class="saBtn">
								<c:choose>
									<c:when test="${tmeChk eq 'Y'}">
										<c:if test="${empty perChk or perChk.anst_typ_cd eq '01'}">
											<a href="" class="btnSurveyStart layerpopupOpen" data-popup-class="perSurvey">검사하기</a>
										</c:if>
										<c:if test="${perChk.anst_typ_cd eq '02'}">
											<%--<a href="javascript:void(0)" onclick="survey_message('1')" class="btnSurveyEnd">검사완료</a>--%>
											<a href="javascript:void(0)" class="btnSurveyEnd">검사완료</a>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${empty perChk}">
											<a href="javascript:void(0)" onclick="survey_message('3')" class="btnSurveyStart">검사하기</a>
										</c:if>
										<c:if test="${not empty perChk}">
											<a href="javascript:void(0)" onclick="survey_message('2')" class="btnSurveyEnd">검사완료</a>
										</c:if>
									</c:otherwise>
								</c:choose>
							</span>
						</td>
						<td></td>
					</tr>
					<tr>
						<td>2</td>
						<td>진로적성검사</td>
						<td>268문항 / 50분</td>
						<td>
							<span class="saBtn">
								<c:choose>
									<c:when test="${tmeChk eq 'Y'}">
										<c:if test="${empty perChk or perChk.anst_typ_cd eq '01'}">
											<%--<a href="javascript:void(0)" onclick="survey_message('2')" class="btnSurveyReady">검사대기</a>--%>
											<a href="javascript:void(0)" class="btnSurveyReady csD">검사대기</a>
										</c:if>
										<c:if test="${perChk.anst_typ_cd eq '02'}">
											<c:choose>
												<c:when test="${affChk.anst_typ_cd eq '02' and majChk.anst_typ_cd eq '02'}">
													<%--<a href="javascript:void(0)" onclick="survey_message('1')" class="btnSurveyEnd">검사완료</a>--%>
													<a href="javascript:void(0)" class="btnSurveyEnd">검사완료</a>
												</c:when>
												<c:otherwise>
													<a href="" class="btnSurveyStart layerpopupOpen" data-popup-class="affSurvey">검사하기</a>
												</c:otherwise>
											</c:choose>
										</c:if>
									</c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${empty affChk and empty majChk}">
												<a href="javascript:void(0)" onclick="survey_message('3')" class="btnSurveyReady csD">검사대기</a>
											</c:when>
											<c:otherwise>
												<a href="javascript:void(0)" onclick="survey_message('2')" class="btnSurveyEnd">검사완료</a>
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</span>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>

<%-- 인성검사 Layer --%>
<div class="layerBg perSurvey"></div>
<div class="layerPopup perSurvey">
	<div class="layerHeader">
		<span class="title">검사시 유의 사항</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conHeader"><span>진로탐색검사 - 01. 인성검사</span></div>
		<div class="conContainer">
			<div class="secTittle"><p>진로탐색검사 – 01. 인성검사는 <span class="cl_ff8400">총 100문항</span>으로 구성되어 있습니다.</p></div>
			<div class="secContent">
				<dl>
					<dt>01.</dt>
					<dd>본 검사는 정답이 정해져 있지 않으므로, 편한 마음으로 자신과 좀 더 가까운 것을 선택해 주시면 됩니다.</dd>
					<dt>02.</dt>
					<dd>정확히 자신과 일치하는 답이 없을 경우, 조금이라도 더 가까운 쪽을 선택해 주세요.</dd>
					<dt>03.</dt>
					<dd>검사응시시간은 검사를 완료하는데 권장되는 시간으로, 되도록이면 제한된 시간 내에 모든 문항에 응답하시는 것이 좋습니다.</dd>
					<dt>04.</dt>
					<dd>검사에 응시하는 도중, 중단하게 되더라도 다음 단계에서부터 다시 검사에 응시하실 수 있습니다.</dd>
				</dl>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="/au/perSurvey" class="btnSurveyLayerStart">검사시작</a>
			</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<%-- 진로적성검사 Layer --%>
<div class="layerBg affSurvey"></div>
<div class="layerPopup affSurvey">
	<div class="layerHeader">
		<span class="title">검사시 유의 사항</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conHeader"><span>진로탐색검사 - 02. 진로적성검사</span></div>
		<div class="conContainer">
			<div class="secTittle"><p>진로탐색검사 – 02. 진로적성검사는 <span class="cl_ff8400">총 268문항</span>으로 구성되어 있습니다.</p></div>
			<div class="secContent">
				<dl>
					<dt>01.</dt>
					<dd>본 검사는 정답이 정해져 있지 않으므로, 편한 마음으로 자신과 좀 더 가까운 것을 선택해 주시면 됩니다.</dd>
					<dt>02.</dt>
					<dd>정확히 자신과 일치하는 답이 없을 경우, 조금이라도 더 가까운 쪽을 선택해 주세요.</dd>
					<dt>03.</dt>
					<dd>검사응시시간은 검사를 완료하는데 권장되는 시간으로, 되도록이면 제한된 시간 내에 모든 문항에 응답하시는 것이 좋습니다.</dd>
					<dt>04.</dt>
					<dd>검사에 응시하는 도중, 중단하게 되더라도 다음 단계에서부터 다시 검사에 응시하실 수 있습니다.</dd>
				</dl>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<c:choose>
					<c:when test="${empty affChk}">
						<a href="/au/affSurvey" class="btnSurveyLayerStart">검사시작</a>
					</c:when>
					<c:when test="${not empty affChk and cnt ne affInfo.totalCnt}">
						<a href="/au/affSurvey" class="btnSurveyLayerStart">검사시작</a>
					</c:when>
					<c:when test="${not empty affChk and cnt eq affInfo.totalCnt}">
						<a href="/au/majSurvey" class="btnSurveyLayerStart">검사시작</a>
					</c:when>
					<c:otherwise>
						<a href="/au/affSurvey" class="btnSurveyLayerStart">검사시작</a>
					</c:otherwise>
				</c:choose>
			</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:if test="${msg eq 'Y'}">
				fnAlertMessage("검사 완료되었습니다.");
			</c:if>
			<c:if test="${msg eq 'C'}">
				fnAlertMessage("설문조사를 완료한 상태입니다.");
			</c:if>
			<c:if test="${msg eq 'N'}">
				fnAlertMessage("잘못된 경로로 접속하셨습니다.");
			</c:if>
			<c:if test="${msg eq 'T'}">
				fnAlertMessage("예기치 못한 문제가 발생하였습니다.</br>관리자에게 문의하십시요.");
			</c:if>
			<c:if test="${msg eq 'D'}">
				fnAlertMessage("검사가 완료되지 않았습니다.</br>검사완료 후 결과확인이 가능합니다.");
			</c:if>
		</c:if>
	});

	function survey_message(flag) {
		if (flag == '1') {
			fnAlertMessage('설문조사 완료한 항목입니다.');
			return false;
		} else if(flag == '2') {
//			fnAlertMessage('인성검사 완료 후, 설문조사가 가능합니다.');
			fnAlertMessage('검사 기간이 아닙니다.');
		} else if(flag == '3') {
//			fnAlertMessage('해당 검사를 진행할 수 없습니다.\n' + '라이센스를 확인하여 주십시요.');
			fnAlertMessage('검사 기간이 아닙니다.');
		}

		return false;
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />