<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="surveyResult">
		<c:set var="menuType" value="0" />
		<%@ include file="/WEB-INF/jsp/auland/resultMenu.jsp" %>
		<div class="section section01">
			<span class="title">인성검사 결과</span>
			<div>
				<table class="tResultIcon" summary="">
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>우수인성</th>
							<th>부족인성</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="per" items="${perResultBrif}" varStatus="sts">
								<c:if test="${sts.first eq true}">
									<td class="fwB">
										<i class="${fn:toLowerCase(per.ctgry3_id)} blue"></i>
										<span>${per.ctgry3_nm}</span>
									</td>
								</c:if>
								<c:if test="${sts.last eq true}">
									<td class="fwB">
										<i class="${fn:toLowerCase(per.ctgry3_id)} red"></i>
										<span>${per.ctgry3_nm}</span>
									</td>
								</c:if>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="section section02">
			<span class="title">진로적성검사 결과</span>
			<div>
				<table class="tResultIcon" summary="">
					<caption></caption>
					<colgroup>
						<col width="*" />
						<col width="*" />
						<col width="*" />
						<col width="*" />
					</colgroup>
					<thead>
						<tr>
							<th>최적학업성향</th>
							<th colspan="3">추천학과</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<c:forEach var="rs" items="${majResultBrif}" varStatus="st">
								<c:if test="${st.first}" >
									<c:set var="brancharea_txt" value="${rs.brancharea_txt}" />
									<c:set var="branchjob_txt" value="${rs.branchjob_txt}" />
									<td class="fwB">
										<i class="${fn:toLowerCase(rs.ctgry_id)} sky"></i>
										<span>${rs.ctgry2_nm}계열</span>
									</td>
								</c:if>
								<td class="fwB">
									<i class="${fn:toLowerCase(rs.ctgrydp3_id)} blue"></i>
									<span>${rs.ctgry3_nm}</span>
								</td>
							</c:forEach>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="section section03">
			<table class="tResultPersent">
				<caption></caption>
				<colgroup>
					<col width="100" />
					<col width="70" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="28" />
					<col width="50" />
				</colgroup>
				<thead>
					<tr>
						<th>인성</th>
						<th>표준점수</th>
						<th colspan="10">백분위점수</th>
						<th>수준</th>
					</tr>
					<tr>
						<td><br /></td>
						<td><br /></td>
						<td width="27">10</td>
						<td width="27">20</td>
						<td width="27">30</td>
						<td width="27">40</td>
						<td width="27">50</td>
						<td width="27">60</td>
						<td width="27">70</td>
						<td width="27">80</td>
						<td width="27">90</td>
						<td width="27">100</td>
						<td><br /></td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${mainperResultBrif}" varStatus="sts">
						<tr>
							<th>${rs.ctgry2_nm}</th>
							<td>${rs.std_score}</td>
							<c:if test="${rs.grade_cd eq 'A'}">
								<td colspan="10" class="chartBar278"><div class="chartBar blue" style="width:${rs.percent_score}%;"></div></td>
								<td><span class="hei">상</span></td>
							</c:if>
							<c:if test="${rs.grade_cd eq 'B'}">
								<td colspan="10" class="chartBar278"><div class="chartBar green" style="width:${rs.percent_score}%;"></div></td>
								<td><span class="mid">중</span></td>
							</c:if>
							<c:if test="${rs.grade_cd eq 'C'}">
								<td colspan="10" class="chartBar278"><div class="chartBar red" style="width:${rs.percent_score}%;"></div></td>
								<td><span class="low">하</span></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<div class="section section04">
			<table class="tResultClsJob">
				<caption></caption>
				<colgroup>
					<col width="118" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>진출분야</th>
						<td>${fn:substring(brancharea_txt, 0, 50)}..</td>
					</tr>
					<tr>
						<th>진출직업</th>
						<td>${fn:substring(branchjob_txt, 0, 50)}..</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="section section05">
			<span class="title">클론 매칭</span>
			<table class="tResultCloneMatch">
				<caption></caption>
				<colgroup>
					<col width="98" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>학점</th>
						<td><span>${cloneProfile.credit}</span>점 이상 관리</td>
					</tr>
					<tr>
						<th>토익</th>
						<td><span>${cloneProfile.toeic}</span>점 이상 관리</td>
					</tr>
					<tr>
						<th>자격증</th>
						<td><span>${cloneProfile.cert}</span> 자격증 취득</td>
					</tr>
					<tr>
						<th>봉사활동</th>
						<td><span>${cloneProfile.volunteer}</span> 봉사활동 수행</td>
					</tr>
					<tr>
						<th>인턴</th>
						<td><span>${cloneProfile.intern}</span> 관련 인턴활동 수행</td>
					</tr>
					<tr>
						<th>공모전</th>
						<td><span>${cloneProfile.contest}</span> 관련 공모전</td>
					</tr>
					<tr>
						<th>해외연수</th>
						<td><span>${cloneProfile.overseastrn}</span> 해외연수</td>
					</tr>
					<tr>
						<th>동아리</th>
						<td><span>${cloneProfile.club}</span> 활동</td>
					</tr>
				</tbody>
			</table>
		</div>
		<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
			<c:if test="${sts.first}">
				<c:set var="maxPassbiz" value="${rs.max_passbiz}" />
				<c:set var="maxPasscomp" value="${rs.max_passcomp}" />
			</c:if>
			<c:if test="${sts.last ne true}">
				<c:set var="chartData" value="${chartData}${rs.passbiz_dist}," />
			</c:if>
			<c:if test="${sts.last}">
				<c:set var="chartData" value="${chartData}${rs.passbiz_dist}" />
			</c:if>
		</c:forEach>
		<script>
			jQuery(function($){
				var  chartData = [${chartData}]
					,colorData = ["#00dab9", "#e44296", "#507ff4", "#ffae00", "#5C1B1B"]
				fnChartPie("#chart_pie", chartData, colorData, 130, 125, 29, 80)
			});
		</script>
		<div class="section section06">
			<span class="title">클론이 최종 합격한 기업정보</span>
			<table class="tResultCloneResult">
				<caption></caption>
				<colgroup>
					<col width="127" />
					<col width="*" />
				</colgroup>
				<thead>
					<tr>
						<th>최대 합격 업종</th>
						<td>${maxPassbiz}</td>
					</tr>
					<tr>
						<th>최다 합격 기업</th>
						<td>${maxPasscomp}</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="2" class="poR">
							<span class="chart_pie_title">최종 합격 업종</span>
							<svg id="chart_pie" width="230" height="243"></svg>
							<ul>
								<c:forEach var="rs" items="${clonePassbiz}" varStatus="sts">
									<c:if test="${rs.order_num eq '1'}"><li><i class="color_tag_00dab9"></i> <span>${rs.passbiz}</span></li></c:if>
									<c:if test="${rs.order_num eq '2'}"><li><i class="color_tag_e44296"></i> <span>${rs.passbiz}</span></li></c:if>
									<c:if test="${rs.order_num eq '3'}"><li><i class="color_tag_507ff4"></i> <span>${rs.passbiz}</span></li></c:if>
									<c:if test="${rs.order_num eq '4'}"><li><i class="color_tag_ffae00"></i> <span>${rs.passbiz}</span></li></c:if>
									<c:if test="${rs.order_num eq '5'}"><li><i class="color_tag_5C1B1B"></i> <span>${rs.passbiz}</span></li></c:if>
								</c:forEach>
							</ul>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="section section07">
			<span>* 검사결과는 일정기간의 표본추출 후, 업데이트되는 내용에 따라 일부 변경될 수 있습니다.</span>
		</div>
		<div class="section section08">
			<span class="saBtn"><a href="${uri_set}/per/summary?seq_tme=${usrInfo.seq_tme}&who=${usrInfo.who}${uri_param}" class="btnMoreResult">상세결과 확인</a></span>
		</div>
		<div class="botBg">
		</div>
	</div>
</div>
${param.goRsMain}
<c:if test="${param.goRsMain eq 'ok'}">
	<script>
		$(window).load(function(){
/*
			if($(window.parent.frames["pdfFrame"].document).contents().find("#iFrameSurveyresult").contents().find("#footer").hasClass("footer")){
				$(".btnArea").css("display","block");
			}
*/
			$(".btnArea").css("display","block");
		});
	</script>
</c:if>

<%--<c:import url="/pdfArea" charEncoding="utf-8" />--%>
<c:import url="/footer" charEncoding="utf-8" />