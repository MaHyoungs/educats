<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fm" uri="/WEB-INF/tid/fn.tld" %>
<jsp:include page="/WEB-INF/jsp/self/web/common/incSmartiTop.jsp"/>
<jsp:include page="/WEB-INF/jsp/self/web/common/incTop.jsp"/>
<div class="self_cont">
	<div class="error_w">
		<p class="bu_img"><img src="${pageContext.request.contextPath}/common/img/er_2.png" alt=""></p>
		<p class="e_txt0">일시적인 오류가 발생하였습니다.</p>
		<p class="e_txt">
			<span class="fc1">문제가 지속되면,</span><br>
			<span class="fc2">세종시교육청 smart-아이</span>
			<span class="fc3">콜센터 1644-9423</span>으로 연락주세요.
		</p>
	</div>
</div>
<jsp:include page="/WEB-INF/jsp/self/web/common/incBottom.jsp"/>