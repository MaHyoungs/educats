<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="survey selfSurvey">
		<div class="section_top">
			<div class="title">자기주도학습역량진단검사</div>
			<div class="info">
				자기주도학습역량진단검사는 나의 자기주도학습 상태를 통합적으로 진단하고,<br/>
				학생의 학업성취에 긍정적인 영향을 미치도록, 자기주도학습력을 강화하기 위한<br/>
				방법을 제공합니다.<br/>
				학습 역량은 어느 한가지 측면만을 개선하여 나아질 수 있는 것이 아닙니다.<br/>
				내가 처해있는 심리적인 상황, 평상시의 학습 태도와 방법, 선·후천적으로 타고난<br/>
				강점 지능이 종합적으로 반영된 결과를 확인하여, 학생 스스로가 지식을 재구성할<br/>
				수 있도록 자기주도학습역량을 강화하여, 능동적인 학습의 주인공이 되기 바랍니다.
			</div>
		</div>
		<div class="section_bot">
			<table class="tSurvey" summary="">
				<caption></caption>
				<colgroup>
					<col width="160" />
					<col width="370" />
					<col width="250" />
					<col width="200" />
					<col width="20" />
				</colgroup>
				<thead>
				<tr>
					<td colspan="3">검사 시작하기</td>
					<td class="bC_ec7063 vaB"><span class="saBtn"><a href="#" class="btnSurveyResult">검사결과 확인</a></span></th>
					<td></td>
				</tr>
				<tr>
					<th>No.</th>
					<th>검사명</th>
					<th>문항/시간</th>
					<th colspan="2">진행상태</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<td>1</td>
					<td>인성검사</td>
					<td>100문항 / 50분</td>
					<td colspan="2"><span class="saBtn"><a href="#" class="btnSurveyStart layerpopupOpen" data-popup-class="selfSurvey">검사하기</a></span></td>
				</tr>
				<tr>
					<td>2</td>
					<td>진로적성검사</td>
					<td>268문항 / 50분</td>
					<td colspan="2"><span class="saBtn"><a href="#" class="btnSurveyEnd">검사완료</a></span></td>
				</tr>
				<tr>
					<td>2</td>
					<td>진로적성검사</td>
					<td>268문항 / 50분</td>
					<td colspan="2"><span class="saBtn"><a href="#" class="btnSurveyReady">검사대기</a></span></td>
				</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<div class="layerBg selfSurvey"></div>
<div class="layerPopup selfSurvey">
	<div class="layerHeader">
		<span class="title">검사시 유의 사항</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conHeader"><span>자기주도학습역량진단검사</span></div>
		<div class="conContainer">
			<div class="secTittle"><p>자기주도학습역량진단검사는 <span class="cl_ff8400">총 156문항</span>으로 구성됩니다.<br/>본 검사를 통해 자신의 학습유형을 평가할 수 있는 기회를 얻을 수 있기를 바랍니다.</p></div>
			<div class="secContent">
				<dl>
					<dt>01.</dt>
					<dd>마음을 편안하게 하고 나를 돌아보는 마음으로 검사에 임해 주시기 바랍니다.</dd>
					<dt>02.</dt>
					<dd>평소에 생각하거나 느낀 점 가운데 하나를 선택하세요.</dd>
					<dt>03.</dt>
					<dd>본 검사는 제약시간이 없습니다. 이해가 되지 않는 내용이 있으면, 선생님 또는 부모님께 문하여 내용을 충분히 이해한 후에 응답하도록 합니다.</dd>
					<dt>04.</dt>
					<dd>검사에 응시하는 도중, 중단하게 되더라도 다음 단계에서부터 다시 검사에 응시하실 수 있습니다</dd>
				</dl>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn"><a href="#" class="btnSurveyLayerStart">검사시작</a></span>
		</div>
	</div>
</div>

<c:import url="/footer" charEncoding="utf-8" />