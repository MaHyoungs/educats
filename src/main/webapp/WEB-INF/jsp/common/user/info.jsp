<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="userInsert">
		<h1 class="conTitle">내 정보관리</h1>
		<div class="tabSection01">
			<a href="/usr/info"><span class="on">내 정보관리</span></a>
			<c:if test="${user.confm_yn ne 'Y'}">
				<a href="javascript:void(0);" onclick="noGo()"><span>내 검사 보관함</span></a>
			</c:if>
			<c:if test="${user.confm_yn eq 'Y'}">
				<a href="/usr/surveyHistory"><span>내 검사 보관함</span></a>
			</c:if>
		</div>
		<div class="section section01 mT20 mB50">
			<form id="userInfo" name="userInfo" action="/usr/infoUpdate" method="post">
				<input type="hidden" name=seq_user value="${user.seq_user}"/>
				<table class="memberStyle01">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="350" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon icon_stuname"></i>이름</th>
							<td colspan="2">
								${user.user_nm}
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_id"></i>ID(이메일)</th>
							<td>
								${user.user_id}
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_repass"></i>현재 비밀번호</th>
							<td>
								<input type="password" id="password" name="password" placeholder="현재 비밀번호" class="wid320" />
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_pass"></i>비밀번호변경</th>
							<td>
								<input type="password" id="newPassword" name="newPassword" placeholder="비밀번호 변경" class="wid320" />
							</td>
							<td><span class="info cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상</span></td>
						</tr>
						<tr>
							<th><i class="icon icon_passcheck"></i>비밀번호변경 확인</th>
							<td>
								<input type="password" id="newPasswordChk" name="newPasswordChk" placeholder="비밀번호 확인" class="wid320" />
							</td>
							<td><span id="errorTxt" class="cl_ec7063"></span></td>
						</tr>
						<tr>
							<th><i class="icon icon_school"></i><c:if test="${user.schorg_typ_cd ne 'ORG01'}">학교</c:if><c:if test="${user.schorg_typ_cd eq 'ORG01'}">기관</c:if></span></th>
							<td colspan="2">
								${schorgDetail.schorg_nm}
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_classinfo"></i>현재 학급정보</th>
							<td colspan="2">
								<c:choose>
									<c:when test="${not empty userCurrent}">
										<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
											${userCurrent.st_year_cd}학년도 &nbsp; ${fn:replace(userCurrent.st_grade_cd,'0', '')} 학년 &nbsp; ${userCurrent.st_class} 반 &nbsp; ${userCurrent.st_number_Cd} 번&nbsp;
										</c:if>
										<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
											${userCurrent.st_year_cd}학년도 &nbsp; ${userCurrent.st_class} 반&nbsp;
										</c:if>
									</c:when>
									<c:otherwise>
										<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
											${user.st_year_cd}학년도 &nbsp; ${fn:replace(user.st_grade_cd,'0', '')} 학년 &nbsp; ${user.st_class} 반 &nbsp; ${user.st_number_Cd} 번&nbsp;
										</c:if>
										<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
											${user.st_year_cd}학년도 &nbsp; ${user.st_class} 반&nbsp;
										</c:if>
									</c:otherwise>
								</c:choose>
								<c:if test="${user.confm_yn eq null}">
									<span class="info cl_7f7f7f">(대기)</span>&nbsp;
									<span class="saBtn"><a href="#" class="btnMemGreenSmall layerpopupOpen" onclick="modify('${user.seq_user}', '${user.st_year_cd}', '${user.st_grade_cd}', '${user.st_class}', 'cu')" data-popup-class="userApplyClass">수정요청</a></span>
								</c:if>
								<c:if test="${user.confm_yn eq 'N'}">
									<span class="info cl_ff0000">(반려)</span>&nbsp;&nbsp;
									<span class="saBtn"><a href="#" class="btnMemGreenSmall layerpopupOpen" onclick="modify('${user.seq_user}', '${user.st_year_cd}', '${user.st_grade_cd}', '${user.st_class}', 'cu')" data-popup-class="userApplyClass">수정요청</a></span>
								</c:if>
								<c:if test="${user.confm_yn eq 'Y'}">
									<span class="info cl_009900">(승인완료)</span>&nbsp;
								</c:if>
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_classinfonext"></i>다음 학년도 학급정보</th>
							<td colspan="2">
								<c:if test="${user.confm_yn eq 'Y'}">
									<c:if test="${empty userHistory}">
										<span class="saBtn"><a href="#" class="btnMemGreenSmall layerpopupOpen" onclick="modify('${user.seq_user}', '${user.st_year_cd + 1}', '${user.st_grade_cd}', '${user.st_class}', 'ne')" data-popup-class="userApplyClassNext">다음 학년도 학급 신청</a></span>
									</c:if>
									<c:if test="${not empty userHistory}">
										<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
											${userHistory.st_year_cd}학년도 &nbsp; ${userHistory.st_grade}학년 &nbsp; ${userHistory.st_class}반 &nbsp; ${userHistory.st_number_cd}번&nbsp;
										</c:if>
										<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
											${userHistory.st_year_cd}학년도 &nbsp; ${userHistory.st_class} 반&nbsp;
										</c:if>
										<c:if test="${userHistory.stat_cd eq '01'}">
											<span class="info cl_7f7f7f">(대기)</span>&nbsp;
											<span class="saBtn"><a href="#" class="btnMemGreenSmall layerpopupOpen" onclick="modify('${userHistory.seq_user}', '${userHistory.st_year_cd}', '${userHistory.st_grade_cd}', '${userHistory.st_class}', 're')" data-popup-class="nextClassReApp">재신청</a></span>
										</c:if>
										<c:if test="${userHistory.stat_cd eq '02'}">
											<span class="info cl_ff0000">(반려)</span>&nbsp;&nbsp;
											<span class="saBtn"><a href="#" class="btnMemGreenSmall layerpopupOpen" onclick="modify('${userHistory.seq_user}', '${userHistory.st_year_cd}', '${userHistory.st_grade_cd}', '${userHistory.st_class}', 're')" data-popup-class="nextClassReApp">재신청</a></span>
										</c:if>
										<c:if test="${userHistory.stat_cd eq '03'}">
											<span class="info cl_009900">(승인완료)</span>&nbsp;
										</c:if>
									</c:if>
								</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="btnArea mT30">
			<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdate()" class="btnLayerGreen">수정</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="cancel()" class="btnLayerGray">취소</a></span>
		</div>
	</div>
</div>

<%-- Layer --%>
<div class="layerBg userApplyClass"></div>
<div class="layerPopup userApplyClass userInfoLayer">
	<div class="layerHeader">
		<span class="title">현재 학년도 학급정보 수정요청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			학급정보가 일치하지 않으면<br/>
			서비스 이용 승인이 되지 않습니다.<br/>
			수정하여 다시 요청하시겠습니까?
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				<label for="modifyGra" class="mR5">학년</label>
				<span class="select wid85 mR12">
					<select id="modifyGra" name="modifyGra" onchange="doGraChange('C', this.value, '${user.st_year_cd}')" class="wid100p">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
						</c:if>
					</select>
				</span>
				<label for="modifyCla" class="mR5">반</label>
				<span class="select wid85 mR12">
					<select id="modifyCla" name="modifyCla" class="wid100p">
						<option>선택</option>
					</select>
				</span>
				<label for="modifyNum" class="mR5">번호</label>
				<span class="select wid85">
					<select id="modifyNu" name="modifyNu" class="wid100p">
						<option value="99">선택</option>
						<c:forEach var="rs" items="${numList}">
							<option value="${rs.code}">${rs.code_nm}</option>
						</c:forEach>
					</select>
				</span>
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				<span class="select wid120">
					<select id="modifyCla" name="modifyCla" class="wid100p">
						<option value="99">선택</option>
						<c:forEach var="rs" items="${classList}">
							<option value="${rs.st_class}">${rs.st_class}</option>
						</c:forEach>
					</select>
				</span>
				<label for="modifyCla" class="mR5">반</label>
			</c:if>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('C')" class="btnUsrManageLayerModify">수정요청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg userApplyClassNext"></div>
<div class="layerPopup userApplyClassNext userInfoLayer">
	<div class="layerHeader">
		<span class="title">다음 학년도 학급 신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			다음 학년도 학급을 선택하세요.<br/>
			담임선생님의 승인 후 확정됩니다.
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				<label for="modifyNr" class="mR5">학년</label>
				<span class="select wid85 mR12">
					<select id="modifyNr" name="modifyNr" onchange="doGraChange('T', this.value, (${user.st_year_cd} + 1))" class="wid100p">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
						</c:if>
					</select>
				</span>
				<label for="modifyTr" class="mR5">반</label>
				<span class="select wid85 mR12">
					<select id="modifyTr" name="modifyTr" class="wid100p">
						<option>선택</option>
					</select>
				</span>
				<label for="modifyTNum" class="mR5">번호</label>
				<span class="select wid85">
					<select id="modifyTNum" name="modifyTNum" class="wid100p">
						<option value="99">선택</option>
						<c:forEach var="rs" items="${numList}">
							<option value="${rs.code}">${rs.code_nm}</option>
						</c:forEach>
					</select>
				</span>
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				<label for="modifyTr" class="mR5">학급</label>
				<span class="select wid85">
					<select id="modifyTr" name="modifyTr" class="wid100p">
						<option>선택</option>
					</select>
				</span>
			</c:if>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('T')" class="btnUsrManageLayerSumbit">신청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg nextClassReApp"></div>
<div class="layerPopup nextClassReApp userInfoLayer">
	<div class="layerHeader">
		<span class="title">다음 학년도 학급 재신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			학급정보가 일치하지 않으면<br/>
			서비스 이용 승인이 되지 않습니다.<br/>
			수정하여 다시 요청하시겠습니까?
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				<label for="modifyGr" class="mR5">학년</label>
				<span class="select wid85 mR12">
					<select id="modifyGr" onchange="doGraChange('N', this.value, (${user.st_year_cd} + 1))" class="wid100p">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
						<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
							<option value="04">4</option>
							<option value="05">5</option>
							<option value="06">6</option>
						</c:if>
					</select>
				</span>
				<label for="modifyCl" class="mR5">반</label>
				<span class="select wid85 mR12">
					<select id="modifyCl" class="wid100p">
						<option>선택</option>
					</select>
				</span>
				<label for="modifyNum" class="mR5">번호</label>
				<span class="select wid85">
					<select id="modifyNum" name="modifyNum" class="wid100p">
						<option value="99">선택</option>
						<c:forEach var="rs" items="${numList}">
							<option value="${rs.code}">${rs.code_nm}</option>
						</c:forEach>
					</select>
				</span>
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				<label for="modifyCl" class="mR5">학급</label>
				<span class="select wid120">
					<select id="modifyCl" class="wid120">
						<option>선택</option>
					</select>
				</span>
			</c:if>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('N')" class="btnUsrManageLayerSumbit">재신청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<form id="modifyForm" name="modifyForm" action="/usr/usrUpdateAjax" method="post">
	<input type="hidden" id="userSeq" name="userSeq" />
	<input type="hidden" id="userYea" name="userYea" />
	<input type="hidden" id="userGra" name="userGra" />
	<input type="hidden" id="userCla" name="userCla" />
	<input type="hidden" id="newGra" name="newGra" />
	<input type="hidden" id="newCla" name="newCla" />
	<input type="hidden" id="newNum" name="newNum" />
	<input type="hidden" id="subTyp" name="subTyp" />
	<input type="hidden" id="userOrg" name="userOrg" value="${user.schorg_id}" />
	<input type="hidden" id="userOrgTyp" name="userOrgTyp" value="${user.schorg_typ_cd}" />
</form>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:choose>
				<c:when test="${msg eq 'R'}">
					fnAlertUrlMessage("가입신청이 반려되었습니다. <br/> 내정보관리에서 다시 신청하세요.", "/usr/info");
				</c:when>
				<c:when test="${msg eq 'B'}">
					fnAlertUrlMessage("가입신청 승인 이전에는 <br/> 일부 메뉴 이용이 제한됩니다.", "/usr/info");
				</c:when>
				<c:when test="${msg eq 'H'}">
					fnAlertMessage("검사 서비스 라이선스 기간이 만료되어 <br/> 검사 외 서비스만 이용 가능합니다.");
				</c:when>
				<c:when test="${msg eq 'Y'}">
					fnAlertMessage("수정되었습니다.");
				</c:when>
				<c:otherwise>
					fnAlertMessage('${msg}');
				</c:otherwise>
			</c:choose>
		</c:if>
		$('#newPasswordChk').keyup(function() {
			if($('#newPassword').val() != $('#newPasswordChk').val()) {
				document.getElementById("errorTxt").className= "cl_ec7063";
				$('#errorTxt').text("*비밀번호와 일치하지 않습니다.");
			} else {
				$('#errorTxt').text("*비밀번호와 일치합니다.");
				document.getElementById("errorTxt").className= "cl_839ab5";
			}
		});
	});
	function cancel() {
		location.href = "/usr/info";
	}

	function doUpdate() {
		var chkPassword = $('#password').val();
		$.ajax({
			url    : "/usr/passwordChk",
			type   : "post",
			data   : {chkId:'${user.user_id}', chkSeqUser:'${user.seq_user}', chkPassword:chkPassword},
			success: function(data){
				if(data == 'N') {
					fnAlertMessage('현재 비밀번호가 일치하지 않습니다.');
					return false;
				} else {
					if($('#newPassword').val() != $('#newPasswordChk').val()){
						fnAlertMessage('비밀번호와 비밀번호 확인이 일치하지 않습니다.');
						return false;
					}
					if(!chkPwdAll($.trim($('#newPassword').val()))){
						$('#newPassword').val('');
						$('#newPasswordChk').val('');
						$('#newPassword').focus();
						return false;
					}
					document.userInfo.submit();
				}
			},
			error:function(e) {
				return false;
			}
		});

	}

	function modify(seq, yea, gra, cla, types) {
		$('#userSeq').val(seq);
		$('#userYea').val(yea);
		$('#userGra').val(gra);
		$('#userCla').val(cla);

		<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
		$.ajax({
			url    : "/usr/classListAjax",
			type   : "post",
			data   : {st_year_cd : yea, schorg_id : '${user.schorg_id}'},
			success: function(data){
				var json = jsonListSet(data);
				if(types == 'cu'){
					$('#modifyCla option').remove();
					$('#modifyCla').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCla').append(cpt);
					}
				} else if(types == 'ne'){
					$('#modifyTr option').remove();
					$('#modifyTr').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyTr').append(cpt);
					}
				} else {
					$('#modifyCl option').remove();
					$('#modifyCl').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCl').append(cpt);
					}
				}
			},
			error:function(e) {
				return false;
			}
		});
		</c:if>
	}

	function doGraChange(tp, gra, yea) {
		$.ajax({
			url    : "/usr/classListAjax",
			type   : "post",
			data   : {st_year_cd : yea, st_grade_cd : gra, schorg_id : '${user.schorg_id}'},
			success: function(data){
				var json = jsonListSet(data);

				if(tp == 'C'){
					$('#modifyCla option').remove();
					$('#modifyCla').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCla').append(cpt);
					}
				}else if(tp == 'N'){
					$('#modifyCl option').remove();
					$('#modifyCl').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCl').append(cpt);
					}
				}else{
					$('#modifyTr option').remove();
					$('#modifyTr').append('<option value="99">선택</option>');
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyTr').append(cpt);
					}
				}
							},
			error:function(e) {
				return false;
			}
		});
	}

	function doNowClass(typs) {
		$('#subTyp').val(typs);
		if(typs == 'C') {
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				if($('#modifyGra').val() == '99') {
					fnAlertMessage('학년을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyCla').val() == '99') {
					fnAlertMessage('반을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyNu').val() == '99') {
					fnAlertMessage('번호를 선택하여 주십시요.');
					return false;
				}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				if($('#modifyCla').val() == '99') {
					fnAlertMessage('학급을 선택하여 주십시요.');
					return false;
				}
			</c:if>
			$('#newGra').val($('#modifyGra').val());
			$('#newCla').val($('#modifyCla').val());
			$('#newNum').val($('#modifyNu').val());
		} else if(typs == 'N') {
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				if($('#modifyGr').val() == '99') {
					fnAlertMessage('학년을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyCl').val() == '99') {
					fnAlertMessage('반을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyNum').val() == '99') {
					fnAlertMessage('번호를 선택하여 주십시요.');
					return false;
				}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				if($('#modifyCl').val() == '99') {
					fnAlertMessage('학급을 선택하여 주십시요.');
					return false;
				}
			</c:if>
			$('#newGra').val($('#modifyGr').val());
			$('#newCla').val($('#modifyCl').val());
			$('#newNum').val($('#modifyNum').val());
		} else {
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				if($('#modifyNr').val() == '99') {
					fnAlertMessage('학년을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyTr').val() == '99') {
					fnAlertMessage('반을 선택하여 주십시요.');
					return false;
				}
				if($('#modifyTNum').val() == '99') {
					fnAlertMessage('학년을 선택하여 주십시요.');
					return false;
				}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
				if($('#modifyTr').val() == '99') {
					fnAlertMessage('학급을 선택하여 주십시요.');
					return false;
				}
			</c:if>
			$('#newGra').val($('#modifyNr').val());
			$('#newCla').val($('#modifyTr').val());
			$('#newNum').val($('#modifyTNum').val());
		}

		var frm = $('#modifyForm').serialize();

		$.ajax({
			url    : "/usr/usrUpdateAjax",
			type   : "post",
			data   : frm,
			success: function(data){
				if(data =="up") {
					popupMessageDown('userApplyClass.userInfoLayer', 'userApplyClass');
					fnAlertUrlMessage("신청하였습니다.", "/usr/info");
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function noGo() {
		fnAlertMessage('승인 후, 확인 가능합니다.');
		return false;
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />