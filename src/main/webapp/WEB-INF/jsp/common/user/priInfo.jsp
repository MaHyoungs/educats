<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="userInsert">
		<h1 class="conTitle">내 정보관리</h1>
		<div class="tabSection01">
			<a href="/usr/info"><span class="on">내 정보관리</span></a>
			<a href="/usr/surveyHistory"><span>내 검사 보관함</span></a>
		</div>
		<div class="section section01 mT20 mB50">
			<form id="userInfo" name="userInfo" action="/usr/infoUpdate" method="post">
				<input type="hidden" name=seq_user value="${user.seq_user}"/>
				<table class="memberStyle01">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="350" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon icon_stuname"></i>이름</th>
							<td colspan="2">
								${user.user_nm}
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_id"></i>ID(이메일)</th>
							<td>
								${user.user_id}
							</td>
						</tr>
						<tr>
						  <th><i class="icon icon_repass"></i>현재 비밀번호</th>
						  <td>
							<input type="password" id="password" name="password" value="${pas.password}" class="wid320" />
						  </td>
						</tr>
						<tr>
							<th><i class="icon icon_pass"></i>비밀번호변경</th>
							<td>
								<input type="password" id="newPassword" name="newPassword" class="wid320" />
							</td>
							<td><span class="info cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상</span></td>
						</tr>
						<tr>
							<th><i class="icon icon_passcheck"></i>비밀번호변경 확인</th>
							<td>
								<input type="password" id="newPasswordChk" name="newPasswordChk" class="wid320" />
							</td>
							<td><span id="errorTxt" class="cl_ec7063"></span></td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="btnArea mT30">
			<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdate()" class="btnLayerGreen">수정</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="cancel()" class="btnLayerGray">취소</a></span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:if test="${msg eq 'Y'}" >
				fnAlertMessage("수정되었습니다.");
			</c:if>
			<c:if test="${msg eq 'V'}" >
				fnAlertMessage("쿠폰 유효기간이 종료되었습니다.");
			</c:if>
		</c:if>
		$('#newPasswordChk').keyup(function() {
			if($('#newPassword').val() != $('#newPasswordChk').val()) {
				document.getElementById("errorTxt").className= "cl_ec7063";
				$('#errorTxt').text("*비밀번호와 일치하지 않습니다.");
			} else {
				$('#errorTxt').text("*비밀번호와 일치합니다.");
				document.getElementById("errorTxt").className= "cl_839ab5";
			}
		});
	});
	function cancel() {
		location.href = "/usr/info";
	}

	function doUpdate() {
		if($('#newPassword').val() != $('#newPasswordChk').val()) {
			fnAlertMessage('비밀번호와 비밀번호 확인이 일치하지 않습니다.');
			return false;
		}
		if(!chkPwdAll($.trim($('#newPassword').val()))) {
			$('#newPassword').val('');
			$('#newPasswordChk').val('');
			$('#newPassword').focus();
			return false;
		}
		document.userInfo.submit();
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />