<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="studentContainer">
	<div id="content" class="choice">
		<c:choose>
			<c:when test="${type eq 's'}">
				<div class="section">
					<img src="/template/common/images/user/choice_au.jpg" alt="" class="choiceImg" />
					<span class="choiceText">자신의 인성 수준과 적성을 객관적으로<br />진단하여 진로를 탐색해 보세요.</span>
					<span class="choiceLink"><a href="/au/survey">탐색 시작하기</a></span>
				</div>
				<div class="section">
					<img src="/template/common/images/user/choice_self.jpg" alt="" class="choiceImg" />
					<span class="choiceText">나의 자기주도학습 상태를 통합적으로 진단하여<br />자기주도학습력을 올려보세요.</span>
					<%--<span class="choiceLink"><a href="/self/survey">탐색 시작하기</a></span>--%>
					<span class="choiceLink"><a href="javascript:void(0)" onclick="noReady()">탐색 시작하기</a></span>
				</div>
			</c:when>
			<c:otherwise>
				<div class="section">
					<img src="/template/common/images/user/choice_au.png" alt="" class="choiceImg" />
					<span class="choiceText">자신의 인성 수준과 적성에 대한 객관적 진단을 통해,<br />후 진로 및 학과 선택에 가이드로 활용하세요.</span>
						<%--<span class="choiceLink"><a href="/au/surveyResult">탐색 검사결과</a></span>--%>
					<span class="choiceLink"><a href="javascript:void(0)" onclick="chkYn('${auRecent.seq_tme}')">검사결과보기</a></span>
				</div>
				<div class="section">
					<img src="/template/common/images/user/choice_self.png" alt="" class="choiceImg" />
					<span class="choiceText">나의 자기주도학습역량에 대한 진단을 통해<br />나는 어떤 학습자인지 확인해 보세요.</span>
						<%--<span class="choiceLink"><a href="/self/surveyResult">검사결과보기</a></span>--%>
					<span class="choiceLink"><a href="javascript:void(0)" onclick="noReady()">검사결과보기</a></span>
				</div>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<script>
	function chkYn(val) {
		if(val == null || val == '') {
			fnAlertMessage("검사가 완료되지 않았습니다. 검사완료 후 결과확인이 가능합니다.");
		} else {
//			document.location.href = "/au/surveyResult?seq_tme=" + val + "&who=${user.user_id}";
			document.location.href = "/au/aulandResult?seq_tme=" + val + "&who=${user.user_id}";
		}
	}

	function noReady() {
		fnAlertMessage("준비중입니다.");
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />