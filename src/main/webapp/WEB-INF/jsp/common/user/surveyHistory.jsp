<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="userInsert">
		<h1 class="conTitle">내 정보관리</h1>
		<div class="tabSection01">
			<a href="/usr/info"><span>내 정보관리</span></a>
			<a href="/usr/surveyHistory"><span class="on">내 검사 보관함</span></a>
		</div>
		<div class="section section01 mT20 mB50">
			<div class="listWrap">
				<table class="tUsrManageList" summary="">
					<caption></caption>
					<colgroup>
						<col width="60" />
						<col width="150" />
						<col width="300" />
						<col width="100" />
					</colgroup>
					<thead>
						<tr>
							<th>No.</th>
							<th>검사일시</th>
							<th>검사명</th>
							<th>검사결과</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${userSurveys}">
							<tr>
								<td>${rs.num}</td>
								<td>${rs.reg_dt}</td>
								<td>${rs.survey_typ_nm}</td>
								<%--<td><span class="saBtn"><a href="/au/surveyResult?seq_tme=${rs.seq_tme}&who=${rs.user_id}" class="btnMngJoinsResult">검사결과</a></span></td>--%>
								<td><span class="saBtn"><a href="/au/aulandResult?seq_tme=${rs.seq_tme}&who=${rs.user_id}" class="btnMngJoinsResult">검사결과</a></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
			</div>
		</div>
	</div>
</div>