<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="/header" charEncoding="utf-8" />

<link charset="utf-8" href="/template/common/js/jquery.bxslider/jquery.bxslider.css" type="text/css" rel="stylesheet" />
<link charset="utf-8" href="/template/common/css/main.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="/template/common/js/jquery.bxslider/jquery.bxslider.js"></script>
<script type="text/javascript" src="/template/common/js/jquery.mousewheel.js"></script>

<div id="container">
	<div id="content">
		<div id="section01" class="section on">
			<div class="contentWrap">
				<ul>
					<li class="bgWrap">
						<img src="/template/common/images/main/bg_main01_01.jpg" alt="" class="bg" />
						<div id="main01_01" class="contentSection">
							<div class="contentFlex">
								<div class="logo">
									<img src="/template/common/images/main/main01_01_01.png" alt="eduCAT" />
								</div>
								<div class="mainTitle">
									<span>초&nbsp;&middot;&nbsp;중&nbsp;&middot;&nbsp;고를 위한 자기주도학습진단 및 진로적성검사 서비스</span>
								</div>
								<div class="subSectionWrap">
									<div class="subSection">
										<img src="/template/common/images/main/main01_01_02.png" alt="" />
										<span class="title">진로탐색검사</span>
										<span class="infoBlue">
											&quot;내 적성과 역량에 맞는 길은 무엇일까?&quot;
											&quot;나와 같은 특성을 가진 대학생들은 어떤 길을 가고 있을까?&quot;
										</span>
										<span class="infoGray">
											고등학교 계열 – 대학교 학과&middot;전공 – 직업&middot;직무 선택까지!<br/>
											선택의 기로에서 옭은 길로 안내하는 길잡이!
										</span>
									</div>
									<div class="subSection">
										<img src="/template/common/images/main/main01_01_03.png" alt="" />
										<span class="title">자기주도학습역량 진단검사</span>
										<span class="infoBlue">
											&quot;나는 어떤 유형의 학습자일까?&quot;<br/>
											&quot;내 자기주도학습역량은 어느정도일까?&quot;
										</span>
										<span class="infoGray">
											학습심리요인, 학습전략요인, 다중지능요인을 종합적으로 분석!<br/>
											부족한 부분을 인지하고 그에 대한 보완을 위한 지침 제공!
										</span>
									</div>
									<div class="subSection mR0">
										<img src="/template/common/images/main/main01_01_04.png" alt="" />
										<span class="title">학부모를 위한 자기주도학습 세미나</span>
										<span class="infoBlue">
											&quot;자기주도학습이 왜 중요한가?&quot;<br/>
											&quot;어떻게 해야 우리아이의 자기주도학습역량을 높일 수 있을까?&quot;
										</span>
										<span class="infoGray">
											전문가가 들려주는 자기주도학습역량을<br/>
											높이는 방법!
										</span>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="section02" class="section">
			<div class="contentWrap">
				<ul class="section02_bxslider">
					<li class="bgWrap">
						<div id="main02_01" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사</h1>
								<div class="subSectionWrap">
									<h2 class="info">
										고등학교 학습성향-대학교 학과 선택- 직업 선택까지, 진로선택 단계별 가이드 제공!<br/>
										클론 매칭을 통해 본인과 유사한 성향을 가진 대학생들의 데이터 제공!
									</h2>
									<div class="subSection01">
										<div class="subSecCon subSecCon01">
											<img src="/template/common/images/main/main02_01_01.png" alt="" />
											<div>
												<span class="name">개인성향분석</span>
											</div>
										</div>
										<div class="subSecCon subSecCon02">
											<img src="/template/common/images/main/main02_01_02.png" alt="" />
											<div>
												<span class="name">고등학교<br/>계열 추천</span>
											</div>
										</div>
										<div class="subSecCon subSecCon03">
											<img src="/template/common/images/main/main02_01_03.png" alt="" />
											<div>
												<span class="name">대학교<br/>전공&middot;학과 추천</span>
											</div>
										</div>
										<div class="subSecCon subSecCon04">
											<img src="/template/common/images/main/main02_01_04.png" alt="" />
											<div>
												<span class="name">진로&middot;직업 추천</span>
											</div>
										</div>
										<div class="subSecCon subSecCon05">
											<img src="/template/common/images/main/main02_01_05.png" alt="" />
											<div>
												<span class="name">대학생클론 매칭</span>
											</div>
										</div>
									</div>
									<div class="subSection02">
										<span class="subTitle">진로탐색검사란?</span>
										<span class="content">
											대기업 온라인 직무적성검사를 국내 최초로 개발하고, 취업진로진단검사 AULAND로 국내 최초 특허를 획득한 ㈜에듀스가 중학생을 대상으로 진로선택을 위한 도움을 주기
											위해 고안된 검사입니다. 대부분의 진로는 고등학교 계열 선택을 시작으로 정해지기에 중학생 시기에 자신의 적성 및 성향을 파악하는 과정은 필수이며 매우 중요합니다.
											아우란트 학과진로선정검사는 진로 결정, 직업선택, 취업 등 자신의 미래 선택에 어려움을 겪고 있는 다양한 대상들이 자기자신을 종합적으로 진단해 볼 수 있도록 합니다.
										</span>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<div id="main02_02" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 특징</h1>
								<div class="subSectionWrap">
									<div class="subSection subSection01">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">현재 국내 실정에 맞는 적합도</h1>
												<ul>
													<li>외국에서 오래전 개발된 기존 인성, 적성 검사에 비해 국내 실정에 적합</li>
													<li>현재 국내 여러 기업들이 채용 평가 기준으로 사용하고 있는 인성, 역량 항목 조사를 통해<br />공통적으로 포함하는 요인 선정 및 하위 검사 문항 개발</li>
													<li>여러 기업들의 다양한 직무 종류에 대한 조사를 통해 공통적으로 수렴되는<br />직무군을 분석하여 19가지 직무군 선정</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="subSection subSection02">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">다측면적, 심층적 내면 탐색</h1>
												<ul>
													<li>인성, 역량, 진로적성 등 여러 검사를 통해 한 개인의 특징을 다 측면적으로 파악</li>
													<li>&quot;클론 매칭&quot; 방식을 도입하여 자신과 동일한 특성을 가진 사람들의 심층정보 제공<br />&lt;특허 제 10-1278693호&gt;</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="subSection subSection03">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">높은 신뢰도, 타당도 확보</h1>
												<ul>
													<li>1년 6개월동안 3차례 예비검사 시행 (규준 샘플 : 약 20,000명)</li>
													<li>문항 신뢰도 검사 및 타당도 분석 과정을 통한 문항 최적화</li>
													<li>매년 3회 모집단 업데이트 실시</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<div id="main02_03" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 핵심 로직</h1>
								<div class="subSectionWrap">
									<div class="subSection subSection01"></div>
									<div class="subSection subSection02">
										<h1 class="subTitle">CLONE MATCHING<span>(대학생 클론 매칭)</span></h1>
										<div class="thdSection01">
											<h1 class="thdTitle">클론 매칭이란?</h1>
										<span class="thdContent">
											검사자와 유사한 환경의 모집단 데이터에서 나와 동일한 인성, 진로적성을 가진 클론을 찾아
											해당 클론의 상세정보를 제공해주는 방법입니다. 나와 같은 조건을 가지고 나와 유사한 길을
											걸었던 클론의 현재 위치를 통해 나의 미래를 예측해 볼 수 있습니다.
										</span>
										</div>
										<div class="thdSection02">
											<h1 class="thdTitle">
												나의 대학생클론들은 어떤 대학생활을 했는지?<br/>
												나의 대학생클론들이 어떤 기업, 어떤 직무에 취업했는지?
											</h1>
											<ul>
												<li>나와 같은 인성, 진로적성을 가진 대학생클론의 정보를 통해 자신의 진로 및 전공, 직무 선택의 가이드로 활용</li>
												<li>검사결과를 단순히 나열하는 것과 달리, 검사응시자와 유사한 성향(인성, 진로정성 등)을 가진 대학생 클론을 찾아 해당 클론들의 상세 정보를 제공</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<div id="main02_04" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 신뢰도</h1>
								<div class="subSection">
									<div class="thdSection">
										<h1 class="thdTitle">
											국내&middot;외 연구논문 및<br/>
											국내 대기업 평가 요인 조사
										</h1>
										<ul>
											<li>국내·외 연구논문 조사</li>
											<li>국내 대기업 인재상 및 인적성검사와 지원서, 면접 평가 시트 조사를 통한 검사 요인 선정 및 문항 개발</li>
										</ul>
									</div>
									<div class="thdSection">
										<h1 class="thdTitle taR">
											1~3차 예비검사 통한<br/>
											신뢰도&amp;타당도 검사
										</h1>
										<ul>
											<li>1년 6개월 동안 전국 대학생 2만명 임의 표집. 일관성 등급 점수를 기준으로 10% 내외의 극단치 샘플 제거, 1만명 대학생 샘플 표준화</li>
											<li>
												내적 일관성 신뢰도 검사를 통해 높은 수준의 신뢰도를 가지고 있음을 검증<br/>
												(1에 가까울 수록 높은 신뢰도, 최대 0.914)
											</li>
										</ul>
									</div>
									<div class="thdSection">
										<h1 class="thdTitle">
											검사표준화<br/>
											&amp;특허 등록
										</h1>
										<ul>
											<li>최종 규준집단 2만명 대상 검사표준화</li>
											<li>매년 2회 클론매칭 모집단 업데이트</li>
											<li>
												&quot;합격률 예측 방법 및 그 장치&quot; 특허 등록<br/>
												&lt;특허 제 10-1278693호&gt;
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<div id="main02_05" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 구성</h1>
								<div class="subSection">
									<div class="thdSection thdSection01">
										<h1 class="thdTitle">인성검사</h1>
										<p class="thdContent">
											인성검사는 본인의 인성 수준을 확인, 진단하는데 도움을 주기 위한 검사입니다.<br/>
											본 검사를 통해 본인의 성격 특성을 파악하고, 나에 대한 객관적인 지식을 가질 수 있습니다.
										</p>
										<p class="thdInfo">
											11가지 인성 - 성실성, 자신감, 인내·지속성, 책임감, 추진·적극성, 사교성, 이타심, 타인신뢰, 호기심, 도전정신, 도덕성 수준
										</p>
									</div>
									<div class="thdSection thdSection02">
										<h1 class="thdTitle">진로적성검사</h1>
										<p class="thdContent">
											진로적성검사는 본인의 직업 적성을 파악하고 향후 진로에 대한 준비도를 높이는데 도움을 주는 검사입니다.<br/>
											본 검사를 통해 19가지 직무에 대한 선호도 및 역량 수준을 파악하고, 자신에게 가장 적합한 직무가 무엇인지 확인할 수 있습니다.
										</p>
										<p class="thdInfo">
											19가지 직무 –  연구개발·R&map;D(과학,기술), 경영·경영지원, 생산기술, 생산시공, 영업, 무역, 회계, 금융, 전산, 마케팅·홍보, 서비스 등
										</p>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<div id="main02_06" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 결과지 구성</h1>
								<div class="subSectionWrap">
									<div class="subSection subSection01">
										<div class="thdSection thdSection01">
											<h1 class="thdTitle">검사자 개인성향 분석</h1>
											<ul>
												<li>주요인성군 및 인성별 수준</li>
												<li>우수인성 및 부족인성</li>
											</ul>
										</div>
										<div class="thdSection thdSection02">
											<h1 class="thdTitle">대학 전공, 학과 추천</h1>
											<ul>
												<li>대학 전공, 학과 추천</li>
												<li>해당 학과에서 학습하게 되는<br />내용 및 진로 방향 등</li>
											</ul>
										</div>
									</div>
									<div class="subSection subSection02">
										<div class="thdSection thdSection03">
											<h1 class="thdTitle">고등학교 학업 성향 파악</h1>
											<ul>
												<li>인문, 사회, 자연, 공학, 예체능</li>
												<li>학업성향에 따른 학습 내용 및 진로</li>
											</ul>
										</div>
										<div class="thdSection thdSection04">
											<h1 class="thdTitle">진로와 직업 추천</h1>
											<ul>
												<li>추천되는 진로와 직업</li>
												<li>추천 진로와 직업에 유용한 정보</li>
											</ul>
										</div>
										<div class="thdSection thdSection05">
											<h1 class="thdTitle">대학생 클론 정보</h1>
											<ul>
												<li>나의 같은 클론들의 대학생활 정보</li>
												<li>나와 유사한 조건을 가진 대학생 클론들의 취업 정보</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="section03" class="section">
			<div class="contentWrap">
				<ul class="section03_bxslider">
					<li class="bgWrap">
						<img src="/template/common/images/main/bg_main03_01.jpg" alt="" class="bg" />
						<div id="main03_01" class="contentSection">
							<div class="contentFlex">
								<div class="subSectionWrap">
									<div class="subSection01">
										<img src="/template/common/images/main/main03_01_01.png" alt="" />
									</div>
									<div class="subSection02">
										<h1 class="title">자기주도학습역량진단검사</h1>
										<h2 class="info">
											학습 심리적 요인, 전략적 요인, 다중지능 요인 분석을 통한<br/>
											자기주도학습역량 통합 진단!<br/>
											자기주도학습력 강화를 위한 방법 제공!
										</h2>
										<div class="iconWrap">
											<p class="icon_title">진단결과에 따른 전문가의 진단 결과 리포트</p>
											<p class="icon_info">94종의 진단 결과에 따른 진단 결과 제공</p>
										</div>
										<div class="pgb">
											<span class="title">자기주도학습역량진단검사란?</span>
											<span class="content">
												학습문제 유형을 진단하고, 진단에 따른 솔루션을 제공하는 형식의<br/>
												자기주도 학습 역량을 진단하는 검사도구입니다.<br/>
												학습에 영향을 미치는 학습심리 요인, 학습기술 및 방법 등<br/>
												학습 전략적 요인, 개인의 강점을 파악하는 다중지능 요인을<br/>
												통합적으로 분석하여, 학업 성취에 도움이 되는<br/>
												자기주도학습령 강화를 위한 방법을 제공합니다.
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<img src="/template/common/images/main/bg_main03_01.jpg" alt="" class="bg" />
						<div id="main03_02" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">진로탐색검사 특징</h1>
								<div class="subSectionWrap">
									<div class="subSection subSection01">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">22개 핵심요인을 통한 자기주도학습역량 진단</h1>
												<ul>
													<li>학습심리요인 6가지, 학습전략요인 8가지, 다중지능요인 8가지 각각의 진단 결과 제공</li>
													<li>결과에 따른 94종의 전문가 레포트 제공</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="subSection subSection02">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">다각적 요인을 통합적으로 분석</h1>
												<ul>
													<li>성격적, 정서적, 동기적 특성을 나타내는 요인을 통한 학습심리와 학습태도, 학습습관, 학습방법, 학습기술 관련 학습전략과
														대표 강점지능인 다중지능 요인을 통해 다각적인 진단과 솔루션 제공</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="subSection subSection03">
										<div class="head"></div>
										<div class="content">
											<div>
												<h1 class="subTitle">검증된 신뢰도&amp;타당도</h1>
												<ul>
													<li>문헌조사 및 학계 전문가 참여를 통한 문항 선정</li>
													<li>초&middot;중&middot;고등학생 대상 파일럿 테스트를 통한 검증</li>
													<li>루브링 분석 기반의 진단 결과 분류</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<img src="/template/common/images/main/bg_main03_01.jpg" alt="" class="bg" />
						<div id="main03_03" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">자기주도학습역량진단검사의 신뢰도</h1>
								<div class="subSection">
									<div class="thdSection">
										<h1 class="thdTitle">
											문헌조사 및<br/>
											학계 전문가 참여
										</h1>
										<ul>
											<li>국내 교육학 및 교육심리학 학계 전문가 참여</li>
											<li>1500여건의 연구논문 및 문헌자료, 타 진단검사 도구 검토를 통해 500여개 문항 초기 설계</li>
											<li>학계전문가 및 일선 교사로 구성된 문항 검토 위원회의 타당도 검토를 통한 270여개 문항 선정</li>
										</ul>
									</div>
									<div class="thdSection">
										<h1 class="thdTitle">
											초&middot;중&middot;고등학생 대상<br/>
											파일럿 테스트
										</h1>
										<ul>
											<li>초&middot;중&middot;고등학생 표집단 750여명 대상 파일럿 테스트 실시</li>
											<li>문항 타당도 및 신뢰도가 높은 최종 156문항 선정</li>
										</ul>
									</div>
									<div class="thdSection">
										<h1 class="thdTitle">
											루브릭 분석 기반의<br/>
											진단 결과 분류
										</h1>
										<ul>
											<li>학습 심리 및 학습전략요소는 5단계 (지지적개입 1&middot;2, 교육적개입, 협력적개입, 적극협력적개입)</li>
											<li>다중지능 요소는 3단계(대표강점지능, 근접발달지능, 다표약점지능)로 분류</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</li>
					<li class="bgWrap">
						<img src="/template/common/images/main/bg_main03_01.jpg" alt="" class="bg" />
						<div id="main03_04" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">자기주도학습역량진단검사 결과지 구성</h1>
								<div class="subSectionWrap">
									<div class="subSection subSection01">
										<ul>
											<li>학습심리요인, 학습전략요인 - 요인별 A~E 등급</li>
											<li>다중지능요인 – 상/중/하 등급</li>
										</ul>
										<div class="roundWrap"><i class="smallRound">나의 등급</i></div>
									</div>
									<div class="subSection subSection02">
										<ul>
											<li>각 요인별 현재 상태에 대한 상세 진단 결과 내용 제공</li>
										</ul>
										<div class="roundWrap"><i class="smallRound">현재 상태</i></div>
									</div>
									<div class="subSection subSection03">
										<div class="roundWrap"><i class="smallRound">나의<br/>키워드</i></div>
										<ul>
											<li>각 요인별 진단 결과에 따른 한마디 함축 키워드</li>
										</ul>
									</div>
									<div class="subSection subSection04">
										<div class="roundWrap"><i class="smallRound">강화전략</i></div>
										<ul>
											<li>해당 요인의 진단 결과에 따른 학습자를 위한 강화 전략 제공</li>
										</ul>
									</div>
									<div class="subSection subSection05">
										<i>종합 진단 결과</i>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="section04" class="section">
			<div class="contentWrap">
				<%--<ul class="section04_bxslider">--%>
				<ul>
					<li class="bgWrap">
						<div id="main04_01" class="contentSection">
							<div class="subSection subSection01">
								<div class="thdSection thdSection01"><span>아이의<br/>자기주도력을<br/>더하다</span></div>
								<div class="thdSection thdSection02"></div>
								<div class="thdSection thdSection03"></div>
								<div class="thdSection thdSection04"><span>학부모가 바뀌면<br/>아이도 달라집니다</span></div>
							</div>
							<div class="subSection subSection02">
								<div class="inner">
									<h1 class="title">자기주도학습 세미나</h1>
									<p class="thdSection thdSection01">
										전문가들에게 듣는 우리 아이의<br/>
										자기주도학습 역량 높이는 방법!<br/>
										학부모가 바뀌면 아이도 달라집니다!
									</p>
									<p class="thdSection thdSection02">
										자기주도학습 전문가 박상학 교수의 명품 강의!<br/>
										학부모에게 꼭 필요한 자기주도학습 지도 방법!
									</p>
									<div class="thdSection thdSection03 layerpopupOpen" data-popup-class="mainTeacherAdd">
										<span class="small">학부모를 위한<br/>자기주도학습 세미나</span>
										<span class="big">신청하기</span>
									</div>
								</div>
							</div>
						</div>
					</li>
<%--
					<li class="bgWrap">
						<div id="main04_02" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">자기주도학습 세미나 사례</h1>
								<div class="photoSection">
									<ul>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
										<li>
											<img src="/template/common/images/main/main04_02_img.jpg" alt="" />
											<p>ㅇㅇ중학교</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</li>
--%>
				</ul>
			</div>
		</div>
		<div id="section05" class="section">
			<div class="contentWrap">
				<ul class="">
					<li class="bgWrap bgc_main05_01">
						<div id="main05_01" class="contentSection">
							<div class="contentFlex">
								<h1 class="title">문의하기</h1>
								<div class="subSectionWrap">
									<div class="subSection01">
										<div class="thdSection">
											<i class="icon icon_main_tel"></i>
											<%--<span><a href="tel:02-3454-2058" style="text-decoration:none; color:#878787">02-3454-2058</a></span>--%>
											<span>02-3454-2058</span>
											<i class="icon icon_main_email mL60"></i>
											<span><a href="mailto:sales@squarenet.co.kr" style="text-decoration:none; color:#878787">sales@squarenet.co.kr</a></span>
										</div>
										<div class="thdSectionSub1">
											[입금계좌]<br/>
											국민은행 781437-04-003980 (예금주: 스퀘어네트(주))<br/>
											우리은행 1005-401-493510 (예금주: 스퀘어네트(주))<br/>
											기업은행 065-072518-01-010 (예금주: 스퀘어네트(주))
										</div>
										<div class="thdSectionSub2 layerpopupOpen" data-popup-class="cashReceiptAdd">
											현금영수증 신청하기>
										</div>
									</div>
									<div class="subSection02">
										<div class="thdSection01">
											<a href="http://www.squarenet.co.kr/" target="_blank">회사소개</a>
											<span>|</span>
											<a href="/etc/introduce">이용약관</a>
											<span>|</span>
											<a href="/etc/privatePolicy">개인정보취급방침</a>
											<span>|</span>
											<a href="/etc/emailPolicy">이메일집단수집거부</a>
											<span>|</span>
											<a href="/etc/lawPolicy">법적고지</a>
										</div>
										<div class="thdSection02">
											서울특별시 강남구 도곡동 553-3번지 지호빌딩 4층 스퀘어네트㈜   대표: 장형문  사업자등록번호: 215-86-53175<br/>
											통신판매업신고번호: 2016-서울강남-01645   TEL: 02-3454-2058  FAX: 02-3454-2057  E-mail: help@squarenet.co.kr<br/>
											COPYRIGHT© 2015 SQUARENET CORP. ALL RIGHT RESERVED.
										</div>
										<div class="thdSection03">
											<img src="/template/common/images/main/main05_01_03.png" alt="" />
										</div>
									</div>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>

<%-- Layer --%>
<div class="layerBg mainTeacherAdd"></div>
<div class="layerPopup mainTeacherAdd style01">
	<div class="layerHeader">
		<span class="title">세미나 신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="mainTableWrap">
				<form id="semiForm" name="semiForm" action="cmm/semi/semiInsert" method="post">
					<table class="style03" summary="">
						<caption></caption>
						<colgroup>
							<col width="36%" />
							<col width="64%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_date">신청일자</th>
								<td colspan="4">
									<div class="select calendar">
										<input type="text" id="semiProDt" name="semiProDt" readonly="readonly" />
									</div>
								</td>
							</tr>
							<tr>
								<th class="icon_clock">희망시간</th>
								<td colspan="4">
									<div class="select wid40p">
										<select id="semiAmPm" name="semiAmPm" class="wid100p">
											<c:forEach var="rs" items="${ampmList}">
												<option value="${rs.code}">${rs.code_nm}</option>
											</c:forEach>
										</select>
									</div>
									<div class="select wid40p">
										<select id="semiDayTme" name="semiDayTme" class="wid100p">
											<c:forEach var="rs" items="${dayTmeList}">
												<option value="${rs.code}">${rs.code_nm}</option>
											</c:forEach>
										</select>
									</div>
									<label for="semiDayTme" class="wid7p disIB">시</label>
								</td>
							</tr>
							<tr>
								<th class="icon_school">학교/기관명</th>
								<td colspan="4">
									<input type="text" id="semiOrgan" name="semiOrgan" />
								</td>
							</tr>
							<tr>
								<th class="icon_name">담당자명</th>
								<td colspan="4">
									<input type="text" id="semiRes" name="semiRes" />
								</td>
							</tr>
							<tr>
								<th class="icon_tel">연락처</th>
								<td>
									<input type="text" id="semiPhone" name="semiPhone" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')"/>
								</td>
							</tr>
							<tr>
								<th class="icon_comment">설명</th>
								<td>
									<textarea id="semiDescription" name="semiDescription"></textarea>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="semiChk()" class="btnLayerGreen">신청</a>
				<a href="javascript:void(0)" onclick="deleteSet()" class="btnLayerGray layerClose">취소</a>
			</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg mainTeacherAddConfirm zi2"></div>
<div class="layerPopup mainTeacherAddConfirm style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="submitSemi(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg cashReceiptAdd"></div>
<div class="layerPopup cashReceiptAdd style99">
	<div class="layerHeader">
		<span class="title">현금영수증 신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="cashTableWrap">
				<form id="cashForm" name="cashForm" action="cmm/cash/cashInsert" method="post">
					<input type="hidden" id="cashReceiptTypeTxt" name="cashReceiptTypeTxt" />
					<input type="hidden" id="cashTypeTxt" name="cashTypeTxt" />
					<table class="style99" summary="">
						<caption></caption>
						<colgroup>
							<col width="35%" />
							<col width="65%" />
						</colgroup>
						<tbody>
							<tr>
								<th>ID</th>
								<td colspan="4">
									<input type="text" id="cashId" name="cashId" />
								</td>
							</tr>
							<tr>
								<th>입금자명</th>
								<td colspan="4">
									<input type="text" id="cashNm" name="cashNm" />
								</td>
							</tr>
							<tr>
								<th>거래자구분</th>
								<td colspan="4">
									<select id="cashReceiptType" name="cashReceiptType" class="wid64p" onchange="doTypeChange(this.value)">
										<option value="01">소비자소득공제용</option>
										<option value="02">사업자지출증빙용</option>
									</select>
								</td>
							</tr>
							<tr>
								<th rowspan="2">현금영수증 신청정보</th>
								<td colspan="4">
									<select id="cashType" name="cashType" class="wid64p">
										<option value="01">휴대폰</option>
										<option value="02">주민등록번호</option>
									</select>
								</td>
							</tr>
							<tr>
								<td colspan="4">
									<input type="text" id="cashNumber" name="cashNumber" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" /> <span>  '-'제외, 숫자만 입력</span>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conContents">
			* 현금영수증 발급은 신청일로부터 7일 이내 처리되며, <br/>
			&nbsp;&nbsp;&nbsp;&nbsp;발급영수증은 국세청 홈텍스(<a href="http://www.hometax.go.kr">www.hometax.go.kr</a>)에서 확인 가능합니다.
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="cashChk()" class="btnLayerGreen">신청</a>
				<a href="javascript:void(0)" onclick="deleteCash()" class="btnLayerGray layerClose">취소</a>
			</span>
		</div>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg cashReceiptAddConfirm zi2"></div>
<div class="layerPopup cashReceiptAddConfirm style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="submitCash()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<c:import url="/WEB-INF/jsp/common/mng/layout/layerMessage.jsp" charEncoding="utf-8" />

<div id="mainLoading"><img src="/template/common/images/loading.gif" alt="" /></div>

<script src="/template/common/js/jQueryFn.js"></script>
<script src="/template/common/js/scroll.js"></script>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:choose>
				<c:when test="${msg eq 'RT'}">
					fnAlertUrlMessage("가입신청이 반려되었습니다. <br/> 내정보관리에서 다시 신청하세요.", "/tch/info");
				</c:when>
				<c:when test="${msg eq 'BT'}">
					fnAlertUrlMessage("가입신청 승인 이전에는 <br/> 일부 메뉴 이용이 제한됩니다.", "/tch/info");
				</c:when>
				<c:when test="${msg eq 'R'}">
					fnAlertUrlMessage("가입신청이 반려되었습니다. <br/> 내정보관리에서 다시 신청하세요.", "/usr/info");
				</c:when>
				<c:when test="${msg eq 'B'}">
					fnAlertUrlMessage("가입신청 승인 이전에는 <br/> 일부 메뉴 이용이 제한됩니다.", "/usr/info");
				</c:when>
				<c:when test="${msg eq 'H'}">
					fnAlertMessage("검사 서비스 라이선스 기간이 만료되어 <br/> 검사 외 서비스만 이용 가능합니다.");
				</c:when>
				<c:when test="${msg eq 'S'}">
					fnAlertMessage("세미나가 신청되었습니다.");
				</c:when>
				<c:when test="${msg eq 'O'}">
					fnAlertMessage("현금영수증이 신청되었습니다.");
				</c:when>
				<c:otherwise>
//					fnAlertMessage('${msg}');
				</c:otherwise>
			</c:choose>
		</c:if>
	});

	function semiChk() {
		if($('#semiProDt').val() == "") {
			fnAlertMessage("신청일자를 입력하여 주십시요.");
			return false;
		}
		if($('#semiOrgan').val().replace(/ /g, '') == "") {
			$('#semiOrgan').val("");
			fnAlertMessage("학교기관명을 입력하여 주십시요.");
			return false;
		}
		if($('#semiRes').val().replace(/ /g, '') == "") {
			$('#semiRes').val("");
			fnAlertMessage("담당자명을 입력하여 주십시요.");
			return false;
		}
		if($('#semiPhone').val() == "" || $('#semiPhone').val().length < 9) {
			fnAlertMessage("연락처를 입력하여 주십시요.");
			return false;
		}

		fnPopupConfirmOpen('mainTeacherAddConfirm');
	}

	function submitSemi(e) {
		popupMessageDown('mainTeacherAddConfirm.style02.zi3', 'mainTeacherAddConfirm.zi2');
		document.semiForm.submit();
	}

	function deleteSet() {
		document.semiForm.reset();
	}

	function doTypeChange(val) {

		$('#cashType option').remove();

		if(val == '01') {
			$('#cashType').append($('<option value="01">휴대폰</option>'));
			$('#cashType').append($('<option value="02">주민등록번호</option>'));
		} else if(val == '02') {
			$('#cashType').append($('<option value="03">사업자등록번호</option>'));
		}
	}

	function cashChk() {
		if($('#cashId').val().replace(/ /g, '') == "") {
			$('#cashId').val("");
			fnAlertMessage("ID를 입력하세요.");
			return false;
		}
		if($('#cashNm').val().replace(/ /g, '') == "") {
			$('#cashNm').val("");
			fnAlertMessage("입금자명을 입력하세요.");
			return false;
		}
		if($('#cashNumber').val() == "") {
			fnAlertMessage("현금영수증 신청정보를 입력하세요.");
			return false;
		}

		fnPopupConfirmOpen('cashReceiptAddConfirm');
	}

	function deleteCash() {
		document.cashForm.reset();
		$('#cashType option').remove();
		$('#cashType').append($('<option value="01">휴대폰</option>'));
		$('#cashType').append($('<option value="02">주민등록번호</option>'));
	}

	function submitCash() {
		popupMessageDown('cashReceiptAddConfirm.style02.zi3', 'cashReceiptAddConfirm.zi2');
		var target = document.getElementById("cashReceiptType");
		var target2 = document.getElementById("cashType");
		$('#cashReceiptTypeTxt').val(target.options[target.selectedIndex].text);
		$('#cashTypeTxt').val(target2.options[target2.selectedIndex].text);

		document.cashForm.submit();
	}
</script>

<c:if test="${not empty param.msq}">
	<script>
		$(window).load(function(){
		});
	</script>
</c:if>