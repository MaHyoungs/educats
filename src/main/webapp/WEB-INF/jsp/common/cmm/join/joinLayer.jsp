<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="layerBg userInsertSchool"></div>
<div class="layerPopup userInsertSchool style01">
	<div class="layerHeader">
		<span class="title">학교/기관 선택</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<form id="organInfo" name="organInfo" action="/join/searchOrgan" method="post" onkeydown="return captureReturnKey(event)" onsubmit="return false;">
				<div class="searchWrap mB20">
					<div class="section">
						<label for="search_are">지역</label>
						<div class="select ">
							<select id="search_are" name="search_are">
								<option value="all">시/도</option>
								<c:forEach var="rs" items="${areaList}" >
									<option value="${rs.code}">${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="search_typ">구분</label>
						<div class="select ">
							<select id="search_typ" name="search_typ">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${schList}" >
									<option value="${rs.code}">${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="search_nm">학교/기관명</label>
						<div class="select wid85">
							<input type="text" id="search_nm" name="search_nm" class="wid85" value="" />
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="doSearch()">검색</a></span>
					</div>
				</div>
			</form>
			<div class="tableWrap">
				<div>
					<table id="organList" class="style01" summary="">
						<caption></caption>
						<colgroup>
							<col width="50px" />
							<col width="*" />
							<col width="*" />
							<col width="*" />
						</colgroup>
						<thead>
							<tr>
								<th></th>
								<th>지역</th>
								<th>구분</th>
								<th>학교/기관명</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="list" items="${schorgList}">
								<tr>
									<td>
										<div class="radioSection">
											<input type="radio" value="${list.schorg_id}" name='choiceSchool' onclick="doSelect(this)" />
										</div>
									</td>
									<td>${list.are_nm}</td>
									<td>${list.schorg_typ_nm}</td>
									<td>${list.schorg_nm}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<%--<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>--%>
				</div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	function doSearch() {
		$('#organList > tbody').children().remove();
		var frmData = $("#organInfo").serialize();
		$.ajax({
			url : "/join/searchOrgan",
			type : "post",
			data : frmData,
			success:function(data) {
				var json = jsonListSet(data);

				for(i=0; i<json.length; i++) {
					$('#organList > tbody').append(
							"<tr>" +
							"   <td>" +
							"       <div class='radioSection'>" +
							"           <input type='radio'' value='" + json[i].schorg_id + "' name='choiceSchool' onclick='doSelect(this)' />" +
							"       </div>" +
							"   </td>" +
							"   <td>" + json[i].are_nm + "</td>" +
							"   <td>" + json[i].schorg_typ_nm + "</td>" +
							"   <td>" + json[i].schorg_nm + "</td>" +
							"</tr>"
					)
				}
				fnLayerResize('mngJoins');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doSelect(e) {
		$("#schorg_id").val(e.value);

		$.ajax({
			url : "/join/organDetail",
			type : "post",
			data : {schorg_id : e.value},
			success:function(data){
				var json = jsonSet(data);
				$('#schorg_nm').val(json[0].schorg_nm);
				if(json[0].schorg_typ_cd == 'ORG01'){
					doClassSet(json[0].schorg_id, json[0].schorg_typ_cd, 'Y');
				}else{
					doClassSet(json[0].schorg_id, json[0].schorg_typ_cd, 'N');
				}
			},
			error:function(err) {
				return false;
			}
		});
		$(".layerPopup.userInsertSchool.style01").css({"display":"none"});
		$(".layerBg.userInsertSchool").css({"display":"none"});
	}

	function doClassSet(val, typ, ov) {
		$.ajax({
			url : "/join/classSet",
			type : "post",
			data : {schorg_id : val, typ : typ, yn : ov},
			success:function(data) {
					var json = jsonListSet(data);
					$('#insertClass01 option').remove();
					$('#insertClass02 option').remove();
					$('#organClass option').remove();
				if($('#memClass01').is(':checked')){
					$('#orgTypId').val(typ);
					$('#classSetChk').val("Y");

					if(typ != 'ORG01'){
						// Step1 선택 후 Step2 노출
						$('#step02Txt').attr('hidden', false);
						$('#memberInfo').attr('hidden', false);
					} else {
						// 기관 Step2 숨김
						$('#step02Txt').attr('hidden', true);
						$('#memberInfo').attr('hidden', true);
					}
					// Step3는 Step2 세부 조건에 따라서 노출해야 하므로 숨김처리
					$('#step03Txt').attr('hidden', true);
					$('#parentSms').attr('hidden', true);

					if(ov == 'Y'){ // 기관
						$('#organClass').attr('hidden', false);
						
						$('#classTr').attr('hidden', true); // 학급정보
						$('#studentTr').attr('hidden', false); // 학생정보

						var cpt = '<option value="">선택</option>';
						for(var i = 0; i < json.length; i++){
							cpt += '<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>';
						}
						$('#organClass').append(cpt);
					}else{ // 학교
						if($('#insertMemClass01').is(':checked')){
							$('#clasnum').attr('hidden', false);
							$('#insertClass03').attr('hidden', false);
							if(typ != 'SCH03' && typ != 'ORG01'){
								$('#parentSms').attr('hidden', false);
								$('#step03Txt').attr('hidden', false);
							}
						}
						$('#organClass').attr('hidden', true);
						$('#studentTr').attr('hidden', true);
						$('#classTr').attr('hidden', false);
						$('#insertClass01').css({"display": "inline-block"});

						if(typ == 'SCH01'){
							$('#insertClass01').append(
									"<option value='N'>선택</option>" +
									"<option value='01'>1</option>" +
									"<option value='02'>2</option>" +
									"<option value='03'>3</option>" +
									"<option value='04'>4</option>" +
									"<option value='05'>5</option>" +
									"<option value='06'>6</option>"
							);
						}else{
							$('#insertClass01').append(
									"<option value='N'>선택</option>" +
									"<option value='01'>1</option>" +
									"<option value='02'>2</option>" +
									"<option value='03'>3</option>"
							);
						}
						var cpt = $('<option value="N">선택</option>');
						$('#insertClass02').append(cpt);
					}
				}
			},
			error:function(err) {
				return false;
			}
		});
	}
</script>