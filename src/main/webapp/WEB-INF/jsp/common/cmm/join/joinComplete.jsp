<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="joinComplete">
		<h1 class="conTitle">회원가입</h1>
		<div class="tabSection01">
			<span>약관동의</span>
			<span>회원정보입력</span>
			<span class="on">가입신청완료</span>
		</div>
		<div class="section">
			<div class="logo">
				<img src="/template/common/images/member/logo_joinComplete.jpg" alt="" />
			</div>
			<div class="content">
				<p class="tit">회원 가입신청이 완료되었습니다.</p>
				<c:if test="${success eq 'S'}" >
					<p class="con"><span>담당선생님</span>의 승인 후 이용할 수 있습니다.</p>
				</c:if>
				<c:if test="${success eq 'T'}" >
					<p class="con"><span>관리자</span>의 승인 후 이용할 수 있습니다.</p>
				</c:if>
				<c:if test="${success eq 'P'}" >
					<p class="con"><span>로그인</span>후 이용할 수 있습니다.</p>
				</c:if>
			</div>
		</div>
		<div class="btnArea mT30 clB">
			<span class="saBtn"><a href="/index" class="btnMemGreenBig">메인페이지로 이동</a></span>
		</div>
	</div>
</div>

<c:import url="/footer" charEncoding="utf-8" />