<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="userInsert">
		<h1 class="conTitle">회원가입</h1>
		<div class="tabSection01">
			<span>약관동의</span>
			<span class="on">회원정보입력</span>
			<span>가입신청완료</span>
		</div>
		<h2 class="step"><span>Step 01.</span>서비스에 가입된 학교 또는 기관을 선택하세요. 개인회원 가입 시, 개인회원을 선택하세요.</h2>
		<form id="userInfo" name="userInfo" action="/join/joinInsert" method="post">
			<div class="section section01 mB50">
				<table class="memberStyle01">
					<caption></caption>
					<colgroup>
						<col width="210" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon icon_id"></i>회원 구분</th>
							<td>
								<div class="radioSection mR40">
									<input type="radio" id="memClass01" value="orgMem" name="memClass" />
									<label for="memClass01">학교/기관 회원</label>
								</div>
								<div class="radioSection">
									<input type="radio" id="memClass02" value="priMem" name="memClass" />
									<label for="memClass02">개인회원</label>
								</div>
							</td>
						</tr>
						<tr id="organTr">
							<th><i class="icon icon_building"></i>학교/기관 선택</th>
							<td>
								<input type="hidden" id="schorg_id" name="schorg_id" />
								<input type="text" id="schorg_nm" name="schorg_nm" readonly class="wid198" />
								<span class="saBtn mR40"><a href="#" class="btnMemGreenSmall layerpopupOpen" data-popup-class="userInsertSchool">선택</a></span>
								<select name="organClass" id="organClass" class="wid200" onchange="selectClass()">
								</select>
							</td>
						</tr>
						<tr id="couponTr">
							<th><i class="icon icon_building"></i>쿠폰번호</th>
							<td>
								<input type="text" id="coupon1" name="coupon1" maxlength="4" class="wid98" />
								<span class="pA5">-</span>
								<input type="text" id="coupon2" name="coupon2" maxlength="4" class="wid98" />
								<span class="pA5">-</span>
								<input type="text" id="coupon3" name="coupon3" maxlength="4" class="wid98" />
								<span class="pA5">-</span>
								<input type="text" id="coupon4" name="coupon4" maxlength="4" class="wid98" />
								<span class="saBtn mL20"><a href="javascript:void(0)" onclick="doCoupon()" class="btnMemGreenSmall">확인</a></span>
								<input type="hidden" id="couponNumber" name="couponNumber" />
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h2 id="step02Txt" class="step"><span>Step 02.</span>회원정보를 입력하세요.</h2>
			<div id="memberInfo" class="section section01 mB50">
				<table class="memberStyle01">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="420" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon icon_stuclass"></i>회원 구분</th>
							<td colspan="2">
								<div class="radioSection mR110">
									<input type="radio" id="insertMemClass01" name="insertMemClass" value="02"/>
									<label for="insertMemClass01">학생</label>
								</div>
								<div class="radioSection">
									<input type="radio" id="insertMemClass02" name="insertMemClass" value="04"/>
									<label for="insertMemClass02">선생님</label>
								</div>
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_stuname"></i>이름</th>
							<td colspan="2">
								<input type="text" name="user_nm" class="wid398" />
							</td>
						</tr>
						<tr>
							<th><i class="icon icon_id"></i>ID(이메일)</th>
							<td>
								<input type="text" name="user_id" class="wid398" />
							</td>
							<td><span id="errorEmail" class="info cl_ec7063"></span></td>
						</tr>
						<tr>
							<th><i class="icon icon_pass"></i>비밀번호</th>
							<td>
								<input type="password" name="password" class="wid398" />
							</td>
							<td><span class="info cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상</span></td>
						</tr>
						<tr>
							<th><i class="icon icon_passcheck"></i>비밀번호 확인</th>
							<td>
								<input type="password" name="passwordChk" class="wid398" />
							</td>
							<td><span id="errorTxt" class="info cl_ec7063"></span></td>
						</tr>
						<tr id="classTr">
							<th><i class="icon icon_classinfo"></i>학급정보</th>
							<td colspan="2">
								<select id="insertClass01" class="wid85 mR10" name="st_grade_cd">
									<option value="">학년</option>
								</select>
								<label for="insertClass01" class="mR20">학년</label>
								<select id="insertClass02" class="wid85 mR10" name="st_class">
									<option value="">반</option>
								</select>
								<label for="insertClass02" class="mR20">반</label>
								<select id="insertClass03" class="wid85 mR10" name="st_number_cd">
									<option value="N">선택</option>
									<c:forEach var="rs" items="${numList}">
										<option value="${rs.code}">${rs.code_nm}</option>
									</c:forEach>
								</select>
								<label id="clasnum" for="insertClass03">번호</label>
							</td>
						</tr>
						<tr id="studentTr">
							<th><i class="icon icon_stuinfo"></i>학생정보</th>
							<td colspan="2">
								<div class="radioSection mR6">
									<input type="radio" id="insertSchool01" value="01" name="org_typ" />
									<label for="insertSchool01">초등학교</label>
								</div>
								<div class="radioSection mR6">
									<input type="radio" id="insertSchool02" value="02" name="org_typ" />
									<label for="insertSchool02">중학교</label>
								</div>
								<div class="radioSection mR6">
									<input type="radio" id="insertSchool03" value="03" name="org_typ" />
									<label for="insertSchool03">고등학교</label>
								</div>
								<select id="insertSchool04" class="wid85 mR10" name="priGrade">
									<option value="">학년</option>
								</select>
								<label for="insertSchool04">학년</label>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<h2 id="step03Txt" class="step"><span>Step 03.</span>개인정보취급 및 서비스 이용에 대한 부모님 SMS인증이 필요합니다.</h2>
			<div id="parentSms" class="section section01 mB50">
				<table class="memberStyle01">
					<caption></caption>
					<colgroup>
						<col width="200" />
						<col width="420" />
						<col width="*" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon icon_parentname"></i>부모님 성함</th>
							<td>
								<input type="text" id="parentName" name="parentName" class="wid398" />
							</td>
							<td><span id="errorParentName" class="info cl_ec7063"></span></td>
						</tr>
						<tr>
							<th><i class="icon icon_parenthp"></i>휴대폰번호</th>
							<td>
								<input type="text" id="phoneNumber" name="phoneNumber" maxlength="11" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" class="wid278 mR9" placeholder="- 없이 숫자만 입력" />
								<span class="saBtn"><a href="javascript:void(0)" onclick="doSms()" class="btnMemGreenSmall">인증번호발송</a></span>
								<input type="hidden" id="confrmYn" name="confrmYn" />
							</td>
							<td><span class="info cl_ec7063">* 최대 3분 이내에 SMS전송</span></td>
						</tr>
						<tr>
							<th><i class="icon icon_sms"></i>SMS인증번호</th>
							<td>
								<input type="text" id="confrmNum" name="confrmNum" class="wid398" />
							</td>
							<td><span id="restAuthTime" class="info cl_839ab5"></span></td>
						</tr>
					</tbody>
				</table>
			</div>
			<input type="hidden" id="classSetChk" name="classSetChk" value="N" />
			<input type="hidden" id="couponChk" name="couponChk" value="N" />
			<input type="hidden" id="orgTypId" name="orgTypId" value="" />
		</form>
		<div class="btnArea mT30">
			<span class="saBtn"><a href="javascript:void(0)" onclick="doInsert()" class="btnLayerGreen">가입완료</a></span>
			<span class="saBtn"><a href="/index" class="btnLayerGray">취소</a></span>
		</div>
	</div>
</div>

<c:import url="/join/schoolSelect" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<script>
	$(document).ready(function(){
		// 초기
		$('#memClass01').attr('checked', true);
		$('#insertMemClass01').attr('checked', true);
		$('#organClass').attr('hidden', true);
		$('#couponTr').attr('hidden', true);
		$('#step02Txt').attr('hidden', true);
		$('#memberInfo').attr('hidden', true);
		$('#step03Txt').attr('hidden', true);
		$('#parentSms').attr('hidden', true);

		// 개인회원
		$('#memClass02').click(function() {
			$('#memClass01').attr('checked', false);
			$('#memClass02').attr('checked', true);
			$('#organTr').attr('hidden', true);
			$('#classTr').attr('hidden', true);
			$('#step03Txt').attr('hidden', true);
			$('#parentSms').attr('hidden', true);

			$('#insertMemClass02').attr('disabled', true);
			if($('#insertMemClass02').is(':checked')){
				$('#studentTr').attr('hidden', true);
			}else{
				$('#studentTr').attr('hidden', false);
			}
			$('#couponTr').attr('hidden', false);
		});

		// 학교/기관
		$('#memClass01').click(function() {
			$('#memClass01').attr('checked', true);
			$('#memClass02').attr('checked', false);
			$('#classTr').attr('hidden', false);
			$('#organTr').attr('hidden', false);
			$('#organClass').attr('hidden', true);
			$('#couponTr').attr('hidden', true);
			$('#insertMemClass02').attr('disabled', false);
		});

		// 선생님
		$('#insertMemClass02').click(function() {
			$('#insertMemClass01').attr('checked', false);
			$('#insertMemClass02').attr('checked', true);
			$('#studentTr').attr('hidden', true);
			$('#step03Txt').attr('hidden', true);
			$('#parentSms').attr('hidden', true);
			if($('#memClass02').is(':checked')){
				$('#studentTr').attr('hidden', true);
				$('#classTr').attr('hidden', true);
			}
			if($('#memClass01').is(':checked')){
				$('#insertClass03').attr('hidden', true);
				$('#clasnum').attr('hidden', true);
			}
		});

		// 학생
		$('#insertMemClass01').click(function() {
			$('#insertMemClass01').attr('checked', true);
			$('#insertMemClass02').attr('checked', false);
			if($('#orgTypId').val() == "ORG01" || $('#memClass02').is(':checked')){ // 기관 또는 개인인 경우
				if($('#memClass01').is(':checked')){ // 선생님 클릭 후 학생 클릭 시 학생정보, Step3 이 안보이는 경우에 대한 처리
					$('#classTr').attr('hidden', true);
					$('#studentTr').attr('hidden', false);
					if ($('#insertSchool01').is(':checked') || $('#insertSchool02').is(':checked')){
						$('#step03Txt').attr('hidden', false);
						$('#parentSms').attr('hidden', false);
					}
				}
			}else if($('#orgTypId').val() != null && $('#orgTypId').val() != "ORG01"){
				$('#classTr').attr('hidden', false);
				$('#studentTr').attr('hidden', true);
				$('#clasnum').attr('hidden', false);
				$('#insertClass03').attr('hidden', false);

				if ($('#orgTypId').val() == 'SCH01' || $('#orgTypId').val() == 'SCH02') { // 선생님 클릭 후 학생 클릭 시 학교급정보에 따라 Step3 이 보이도록 처리
					$('#step03Txt').attr('hidden', false);
					$('#parentSms').attr('hidden', false);
				}
			}else{
				$('#classTr').attr('hidden', false);
				$('#studentTr').attr('hidden', false);
				$('#insertClass03').attr('hidden', false);
				$('#clasnum').attr('hidden', false);
				if($('#memClass02').is(':checked')){
					$('#classTr').attr('hidden', true);
					$('#studentTr').attr('hidden', false);
				}
			}
		});

		// 초등 선택
		$('#insertSchool01').click(function() {
			$('#step03Txt').attr('hidden', false);
			$('#parentSms').attr('hidden', false);
		});
		// 중등 선택
		$('#insertSchool02').click(function() {
			$('#step03Txt').attr('hidden', false);
			$('#parentSms').attr('hidden', false);
		})
		// 고등 선택
		$('#insertSchool03').click(function() {
			$('#step03Txt').attr('hidden', true);
			$('#parentSms').attr('hidden', true);
		});

		$('input[name=org_typ]').click(function() {
			var chk = this.value;
			$('#insertSchool04 option').remove();
			if(chk == '01'){
				$('#insertSchool04').append(
						"<option value='N'>선택</option>" +
						"<option value='01'>1</option>" +
						"<option value='02'>2</option>" +
						"<option value='03'>3</option>" +
						"<option value='04'>4</option>" +
						"<option value='05'>5</option>" +
						"<option value='06'>6</option>"
				)
			}else if(chk == '02' || chk == '03'){
				$('#insertSchool04').append(
						"<option value='N'>선택</option>" +
						"<option value='01'>1</option>" +
						"<option value='02'>2</option>" +
						"<option value='03'>3</option>"
				)
			}
		});

		$('input[name=user_id]').keyup(function(){
			if (!emailcheck($('input[name=user_id]').val())) {
				$('#errorEmail').text("* 이메일을 정확히 입력하세요.");
			} else {
				$('#errorEmail').text("");
			}
		});

		$('input[name=passwordChk]').keyup(function() {
			if($('input[name=password]').val() != $('input[name=passwordChk]').val()){
				document.getElementById("errorTxt").className= "cl_ec7063";
				$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			}else{
				document.getElementById("errorTxt").className= "cl_839ab5";
				$('#errorTxt').text("");
			}
		});

		$('input[name=parentName]').keyup(function() {
			$('#errorParentName').text("");
		});

		$('#insertClass01').change(function(){
			var st_grade_cd = $(this).val();
			var schorg_id = $('#schorg_id').val();
			var st_year_cd = (new Date()).getFullYear();

			if (st_grade_cd != null) {
				$.ajax({
					url : "/join/classSet",
					type : "POST",
					data : {st_grade_cd : st_grade_cd, schorg_id : schorg_id, st_year_cd : st_year_cd},
					success:function(data) {
						var json = jsonListSet(data);

						$('#insertClass02 option').remove();
						var cpt = $('<option value="N">선택</option>');
						$('#insertClass02').append(cpt);
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#insertClass02').append(cpt);
						}
					},
					error:function(e) {
						// 오류 메시지 출력
						fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
						return false;
					}
				});
			}
		});

	});

	var csec1j, cmin1j, TcounterHandler;
	function Display(min,sec) {
		var disp;
		if( min <= 9 ) disp=" 0";
		else disp=" ";

		if( min >= 60 ){
			hour = parseInt( min / 60 );
			if( hour >=24 ){
				day = parseInt( hour / 24 );
				disp += day + "일 " + ( hour % 24 ) + "시간 " + parseInt( min % 60 ) + "분 ";
			}
			else{
				disp +=  hour + "시간 " + parseInt( min % 60 ) + "분 ";
			}
		}
		else{
			disp += min + "분 ";
		}

		if( sec <= 9 ) disp += "0" + sec + "초";
		else disp += sec + "초";

		return(disp);
	}
	function Tcounter(timegap) {
		csec1j = timegap % 60;
		cmin1j = ( timegap - csec1j ) / 60;
		TcounterZero();
	}
	function TcounterZero() {
		csec1j--;

		if(csec1j == -1){
			csec1j = 59;
			cmin1j--;
		}

		$('#restAuthTime').text("* 남은시간 : " + Display( cmin1j, csec1j ));

		//남은 시간이 0 이 되었을때 처리부분
		if( ( cmin1j == 0 ) && ( csec1j == 0 ) ){
			document.getElementById("restAuthTime").className= "cl_ec7063";
			$('#restAuthTime').text("* 시간이 만료되었습니다.");
			$('#confrmYn').val('N');
		}
		else{
			TcounterHandler = setTimeout( "TcounterZero()", 1000 );
		}
	}

	function doSms() {
		if($.trim($('#parentName').val()).length < 1){
			$('#errorParentName').text("* 부모님 성함을 입력하세요.");
			return false;
		}

		if($('#phoneNumber').val().length < 7) {
			fnAlertMessage('휴대폰번호를 정확히 입력하세요.');
			return false;
		}

		if(!phonecheck($('#phoneNumber').val())){
			fnAlertMessage('- 없이 숫자만 입력하세요.');
			return false;
		}

		$.ajax({
			url : "/join/sendSmsAjax",
			type : "POST",
			data : {phoneNumber : $('#phoneNumber').val()},
			success:function(res) {
				if(res == 'succ'){
					fnAlertMessage('인증번호가 발송되었습니다.');
					// 남은시간 감소 처리
					document.getElementById("restAuthTime").className= "cl_839ab5";
					clearTimeout(TcounterHandler);
					Tcounter(180); // 3분으로 설정
				}else{
					// 발송 실패 시 메시지 출력
					fnAlertMessage('다시 시도하여 주세요.');
				}
			},
			error:function(e) {
				// 오류 메시지 출력
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});

		$('#confrmYn').val('Y');
	}

	function doCoupon() {
		var coupon1 = $.trim($('#coupon1').val());
		var coupon2 = $.trim($('#coupon2').val());
		var coupon3 = $.trim($('#coupon3').val());
		var coupon4 = $.trim($('#coupon4').val());

		if(coupon1.length < 4 || coupon2.length < 4 || coupon3.length < 4 || coupon4.length < 4){
			fnAlertMessage('쿠폰번호를 입력하세요.');
			return false;
		}

		if(!couponcheck(coupon1) || !couponcheck(coupon2) || !couponcheck(coupon3) || !couponcheck(coupon4)){
			fnAlertMessage('쿠폰번호가 일치하지 않습니다.');
			return false;
		}

		var couponNumber = coupon1 + '-' + coupon2 + '-' + coupon3 + '-' + coupon4;

		$('#couponChk').val('N');

		$.ajax({
			url : "/join/checkCouponAjax",
			type : "POST",
			data : {couponNumber : couponNumber},
			success:function(res) {
				if(res == 'succ'){
					$('#couponChk').val('Y');
					$('#couponNumber').val(couponNumber);

					$('#step02Txt').attr('hidden', false);
					$('#memberInfo').attr('hidden', false);

					$('#coupon1').attr("disabled",true);
					$('#coupon2').attr("disabled",true);
					$('#coupon3').attr("disabled",true);
					$('#coupon4').attr("disabled",true);
				}else{
					// 사용불가능한 쿠폰인 경우 메시지 출력
					fnAlertMessage('쿠폰번호가 일치하지 않습니다.');
				}
			},
			error:function(e) {
				// 오류 메시지 출력
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}

	function doInsert() {
		if($('#memClass01').is(':checked')){
			if($('#classSetChk').val() != "Y"){
				fnAlertMessage("학교/기관을 선택하세요.");
				return false;
			}
			if($('#orgTypId').val() == 'ORG01' && ($('#organClass').val() == null || $('#organClass').val() == "")){
				fnAlertMessage("학급을 선택하세요.");
				return false;
			}
			$('#couponChk').val('N');
		}
		var isNeedStuinfo = false;
		if($('#memClass02').is(':checked')){
			isNeedStuinfo = true;
			if($('#couponChk').val() != "Y"){
				fnAlertMessage("쿠폰번호를 입력해 주세요.");
				return false;
			}
		}
		if($('#insertMemClass01').is(':checked') && $('#orgTypId').val() == 'ORG01'){
			isNeedStuinfo = true;
		}

		if($('input[name=user_nm]').val().length < 1) {
			fnAlertMessage("이름을 입력하세요.");
			return false;
		}

		if($('input[name=user_id]').val().length < 1) {
			fnAlertMessage("아이디를 입력하세요.");
			return false;
		}

		if($('input[name=password]').val().length < 1) {
			fnAlertMessage("비밀번호를 입력하세요.");
			return false;
		}

		if($('input[name=passwordChk]').val().length < 1) {
			document.getElementById("errorTxt").className= "cl_ec7063";
			$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			$('input[name=passwordChk]').focus();
			return false;
		}

		if(!chkPwdAll($.trim($('input[name=password]').val()))){
			$('input[name=password]').val('');
			$('input[name=passwordChk]').val('');
			$('input[name=password]').focus();
			return false;
		}

		if($('input[name=password]').val() != $('input[name=passwordChk]').val()){
			$('input[name=passwordChk]').focus();
			return false;
		}

		if (isNeedStuinfo) { // 기관이나 개인인 경우를 선택하여 학생정보 선택이 필요한 경우
			if(!$('input:radio[name=org_typ]').is(':checked')){
				fnAlertMessage("학교급을 선택하세요.");
				return false;
			}
		
			if($('#insertSchool04').val() == 'N'){
				fnAlertMessage("학년을 선택하세요.");
				return false;
			}
		}

		if($('#orgTypId').val() != 'ORG01' && !$('#memClass02').is(':checked')) { // 기관이나 개인이 아닌 경우
			if($('#insertClass01').val() == 'N') {
				fnAlertMessage('학년을 선택하세요.');
				return false;
			}
			if($('#insertClass02').val() == 'N') {
				fnAlertMessage('반을 선택하세요.');
				return false;
			}
			
			if ($('#insertMemClass01').is(':checked')) { // 학생인 경우에만 번호를 입력받도록 처리
				if($('#insertClass03').val() == 'N') {
					fnAlertMessage('번호를 선택하세요.');
					return false;
				}
			}
		}

		if(!$('#step03Txt').attr('hidden')){
			if($('#confrmYn').val() != 'Y'){
				fnAlertMessage('부모님 SMS인증이 필요합니다.');
				return false;
			}
			if(!authnumcheck($('input[name=confrmNum]').val())){
				fnAlertMessage('휴대폰으로 전송된 6자리 인증번호를 입력하세요.');
				return false;
			}
		}
		if($('#step03Txt').attr('hidden')){
			$('#confrmYn').val('N');
		}

		if(!emailcheck($('input[name=user_id]').val())){
			$('input[name=user_id]').focus();
			return false;
		} else {
			$.ajax({
				url : "/join/checkDupUseridAjax",
				type : "POST",
				data : {user_id : $('input[name=user_id]').val()},
				success:function(res) {
					if(res == 'fail'){
						fnAlertMessage('동일한 아이디가 있습니다.');
						return false;
					} else if($('#confrmYn').val() == 'Y'){
						$.ajax({
							url : "/join/checkSmsAuthnumberAjax",
							type : "POST",
							data : {authNumber : $('input[name=confrmNum]').val()},
							success:function(res) {
								if(res == 'fail'){
									fnAlertMessage('SMS 인증번호가 일치하지 않습니다.');
									return false;
								} else {
									document.userInfo.submit();
								}
							},
							error:function(e) {
								// 오류 메시지 출력
								fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
								return false;
							}
						});
					} else {
						document.userInfo.submit();
					}
				},
				error:function(e) {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			});
		}
	}

	function selectClass() {
		// Step1 선택 후 Step2 노출
		$('#step02Txt').attr('hidden', false);
		$('#memberInfo').attr('hidden', false);

		// Step3는 Step2 세부 조건에 따라서 노출해야 하므로 숨김처리
		$('#step03Txt').attr('hidden', true);
		$('#parentSms').attr('hidden', true);
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />