<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginCT pA0">
	<div id="content" class="login">
		<div class="logo">
			<img src="/template/common/images/login/logo_usr_login.png" alt="eduCAT Education Career Aptitude Test" />
		</div>
		<div class="loginFormWrap">
			<div class="loginFormTop">
				<img src="/template/common/images/login/bg_usr_login.png" />
			</div>
			<div class="loginFormSection">
				<form name="loginForm" method="post" action="loginAction">
					<div id="loginForm">
						<div id="loginInputWrap">
							<div class="loginInputSection">
								<label for="user_id">아이디</label>
								<input type="text" name="user_id" id="user_id">
							</div>
							<div class="loginInputSection">
								<label for="password">비밀번호</label>
								<input type="password" name="password" id="password">
							</div>
							<div class="idSaveSection">
								<input type="checkbox" id="id_save" />
								<label for="id_save">아이디 저장</label>
							</div>
						</div>
						<div id="loginBtnWrap">
							<input type="submit" class="btnLogin csP" value="LOGIN">
						</div>
					</div>
				</form>
			</div>
			<div class="idpassSch">
				<a href="/searchInfo">아이디/비밀번호찾기</a>
			</div>
		</div>

		<!-- footer start -->
		<div id="footer">
			<div class="fcont">
				<p>COPYRIGHTⓒ 2016 BY SQUARENET , ALL RIGHTS RESERVED. </p>
			</div>
		</div>
		<!-- footer End -->
	</div>
</div>
<c:import url="/WEB-INF/jsp/common/mng/layout/layerMessage.jsp" charEncoding="utf-8" />
<!-- wrap End -->
<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:if test="${msg eq 'D'}" >
				fnAlertMessage("아이디 또는 비밀번호가 다릅니다.<br/>(아이디/비밀번호를 입력하세요)");
			</c:if>
			<c:if test="${msg eq 'S'}" >
				if(confirm("동일한 아이디가 있습니다.<br/>아이디/비밀번호 찾기 페이지로 가시겠습니까?")) {
					document.location.href = "/searchInfo";
				}
			</c:if>
		</c:if>

		var saveId = getCookie("saveId");
		$("#user_id").val(saveId);

		if($("#user_id").val() != ""){
			$("#id_save").attr("checked", true);
		}
		$("#id_save").change(function(){
			if($("#id_save").is(":checked")){
				var saveId = $("#user_id").val();
				setCookie("saveId", saveId, 90);
			}else{ // ID 저장하기 체크 해제 시,
				deleteCookie("saveId");
			}
		});
		$("#user_id").keyup(function(){
			if($("#id_save").is(":checked")){
				var saveId = $("#user_id").val();
				setCookie("saveId", saveId, 90);
			}
		});
	});
</script>

			</div>
		</div>
</body>
</html>