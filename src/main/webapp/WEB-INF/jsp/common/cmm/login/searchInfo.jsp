<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="searchInfo">
		<h1 class="conTitle">아이디/비밀번호 찾기</h1>
		<h2 class="step"><span>Step 01.</span>회원 구분을 선택하세요.</h2>
		<div class="section section01 mB50">
			<table class="memberStyle01">
				<caption></caption>
				<colgroup>
					<col width="210" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th><i class="icon icon_id"></i>회원 구분</th>
						<td>
							<div class="radioSection mR40">
								<input type="radio" id="memClass01" name="memClass" />
								<label for="memClass01">학교/기관 회원</label>
							</div>
							<div class="radioSection">
								<input type="radio" id="memClass02" name="memClass" />
								<label for="memClass02">개인회원</label>
							</div>
						</td>
					</tr>
					<tr id="organTr">
						<th><i class="icon icon_building"></i>학교/기관 선택</th>
						<td>
							<input type="text" id="schorg_nm" name="schorg_nm" readonly class="wid198" />
							<span class="saBtn mR40"><a href="#" class="btnMemGreenSmall layerpopupOpen" data-popup-class="searchInfoSchool">선택</a></span>
							<select name="organClass" id="organClass" class="wid200" onchange="selectClass()">
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h2 id="step02Txt" class="step"><span>Step 02.</span>가입된 정보를 입력하여 아이디 또는 비밀번호를 확인하세요.</h2>
		<div class="floatSection mR12">
			<h3 class="secTitle"><i class="icon icon_id"></i>아이디 찾기</h3>
			<div class="section section02 mB50">
				<form id="idSearchForm" name="idSearchForm" action="/searchIdAjax" method="post">
					<table class="memberStyle02">
						<caption></caption>
						<colgroup>
							<col width="100" />
							<col width="284" />
						</colgroup>
						<tbody>
							<tr>
								<th>구분</th>
								<td>
									<div class="radioSection mR40">
										<input type="radio" id="memIdClass01" name="memIdClass" />
										<label for="memIdClass01">학생</label>
									</div>
									<div class="radioSection">
										<input type="radio" id="memIdClass02" name="memIdClass" />
										<label for="memIdClass02">선생님</label>
									</div>
								</td>
							</tr>
							<tr>
								<th>이름</th>
								<td>
									<input type="text" id="memIdName" name="memIdName" class="wid279" />
								</td>
							</tr>
							<tr id="memIdClaTr">
								<th>학급정보</th>
								<td>
									<select id="memIdCla00" class="wid85 mR10" name="memIdCla00">
										<option value="N">학년도</option>
										<c:forEach var="rs" items="${yearList}">
											<option value="${rs.code}" <c:if test="${nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
									<br/>
									<select id="memIdCla01" class="wid85 mR10" name="memIdCla01">
										<option value="N">학년</option>
									</select>
									<select id="memIdCla02" class="wid85 mR10" name="memIdCla02">
										<option value="N">반</option>
									</select>
									<select id="memIdCla03" class="wid85" name="memIdCla03">
										<option value="N">번호</option>
										<c:forEach var="rs" items="${numList}">
											<option value="${rs.code}">${rs.code_nm}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr id="memIdStuTr">
								<th>학생정보</th>
								<td>
									<select id="memIdStuYear" class="wid85 mR10" name="memIdStuYear">
										<option value="N">학년도</option>
										<c:forEach var="rs" items="${yearList}">
											<option value="${rs.code}" <c:if test="${nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
									<br/>
									<div class="radioSection mR18">
										<input type="radio" id="memIdStu01" name="memIdStu" value="01" />
										<label for="memIdStu01">초</label>
									</div>
									<div class="radioSection mR18">
										<input type="radio" id="memIdStu02" name="memIdStu" value="02" />
										<label for="memIdStu02">중</label>
									</div>
									<div class="radioSection mR17">
										<input type="radio" id="memIdStu03" name="memIdStu" value="03" />
										<label for="memIdStu03">고</label>
									</div>
									<select id="memIdStuNum" class="wid85" name="memIdStuNum">
										<option value="N">학년</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" id="memIdStdCla" name="memCla" />
					<input type="hidden" id="schorg_id" name="schorg_id" />
					<input type="hidden" id="classSetChk" name="classSetChk" value="N" />
					<input type="hidden" id="orgTypId" name="orgTypId" value="" />
					<input type="hidden" id="user_type" name="user_type" />
				</form>
				<div class="btnArea mT30">
					<span class="saBtn"><a href="javascript:void(0)" class="btnMemGreenLong wid284" onclick="doIdSearch()">아이디 찾기</a></span>
				</div>
			</div>
		</div>
		<div class="floatSection">
			<h3 class="secTitle"><i class="icon icon_id"></i>비밀번호 찾기</h3>
			<div class="section section02 mB50">
				<form id="pwSearchForm" name="pwSearchForm" action="/sendPasswordEmailAjax" method="post">
					<table class="memberStyle02">
						<caption></caption>
						<colgroup>
							<col width="100" />
							<col width="284" />
						</colgroup>
						<tbody>
							<tr>
								<th>구분</th>
								<td>
									<div class="radioSection mR40">
										<input type="radio" id="memPwClass01" name="memPwClass" />
										<label for="memPwClass01">학생</label>
									</div>
									<div class="radioSection">
										<input type="radio" id="memPwClass02" name="memPwClass" />
										<label for="memPwClass02">선생님</label>
									</div>
								</td>
							</tr>
							<tr>
								<th>ID</th>
								<td>
									<input type="text" id="memPwId" name="memPwId" class="wid279" />
								</td>
							</tr>
							<tr id="memPwClaTr">
								<th>학급정보</th>
								<td>
									<select id="memPwCla00" class="wid85 mR10" name="memPwCla00">
										<option value="N">학년도</option>
										<c:forEach var="rs" items="${yearList}">
											<option value="${rs.code}" <c:if test="${nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
									<br/>
									<select id="memPwCla01" class="wid85 mR10" name="memPwCla01">
										<option value="N">학년</option>
									</select>
									<select id="memPwCla02" class="wid85 mR10" name="memPwCla02">
										<option value="N">반</option>
									</select>
									<select id="memPwCla03" class="wid85" name="memPwCla03">
										<option value="N">번호</option>
										<c:forEach var="rs" items="${numList}">
											<option value="${rs.code}">${rs.code_nm}</option>
										</c:forEach>
									</select>
								</td>
							</tr>
							<tr id="memPwStuTr">
								<th>학생정보</th>
								<td>
									<select id="memPwStuYear" class="wid85 mR10" name="memPwStuYear">
										<option value="N">학년도</option>
										<c:forEach var="rs" items="${yearList}">
											<option value="${rs.code}" <c:if test="${nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
									<br/>
									<div class="radioSection mR18">
										<input type="radio" id="memPwStu01" name="memPwStu" value="01" />
										<label for="memPwStu01">초</label>
									</div>
									<div class="radioSection mR18">
										<input type="radio" id="memPwStu02" name="memPwStu" value="02" />
										<label for="memPwStu02">중</label>
									</div>
									<div class="radioSection mR17">
										<input type="radio" id="memPwStu03" name="memPwStu" value="03" />
										<label for="memPwStu03">고</label>
									</div>
									<select id="memPwStuNum" class="wid85" name="memPwStuNum">
										<option value="N">학년</option>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" id="memPwStdCla" name="memCla" />
					<input type="hidden" id="schorg_id2" name="schorg_id" />
					<input type="hidden" id="classSetChk2" name="classSetChk" value="N" />
					<input type="hidden" id="orgTypId2" name="orgTypId" value="" />
					<input type="hidden" id="user_type2" name="user_type" />
				</form>
				<div class="btnArea mT30">
					<span class="saBtn"><a href="javascript:void(0)" class="btnMemGreenLong wid284" onclick="doPasswordEmail()">임시 비밀번호 발송</a></span>
				</div>
			</div>
		</div>
	</div>
</div>

<c:import url="/schoolSelects" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<script>
	$(document).ready(function(){
		// 초기
		$('#memClass01').attr('checked', true);
		$('#memIdClass01').attr('checked', true);
		$('#memPwClass01').attr('checked', true);
		$('#organClass').attr('hidden', true);
		$('#step02Txt').attr('hidden', true);
		$('.floatSection').hide();
	});

	// 학교/기관
	$('#memClass01').click(function() {
		$('#memClass01').attr('checked', true);
		$('#memIdClass01').attr('checked', true);
		$('#memPwClass01').attr('checked', true);
		$('#organTr').attr('hidden', false);
		
		if ($('#schorg_nm').val().length > 0) {
			if ($('#orgTypId').val() == 'ORG01') { // 기관을 선택한 경우
				$('#organClass').attr('hidden', false);
			} else { // 학교를 선택한 경우
				$('#organClass').attr('hidden', true);
				$('#memIdClaTr').attr('hidden', false);
				$('#memPwClaTr').attr('hidden', false);
				$('#memIdStuTr').attr('hidden', true);
				$('#memPwStuTr').attr('hidden', true);
			}	
		}
		
		if ($('#schorg_nm').val().length < 1) {
			$('#step02Txt').attr('hidden', true);
			$('.floatSection').hide();
		}
		
		$('#memIdClass02').attr('disabled', false);
		$('#memPwClass02').attr('disabled', false);
	});

	// 개인회원
	$('#memClass02').click(function() {
		$('#organTr').attr('hidden', true);
		$('#organClass').attr('hidden', true);
		$('#memIdClaTr').attr('hidden', true);
		$('#memPwClaTr').attr('hidden', true);
		$('#memIdStuTr').attr('hidden', false);
		$('#memPwStuTr').attr('hidden', false);
		$('#memIdClass01').attr('checked', true);		
		$('#memIdClass02').attr('disabled', true);
		$('#memPwClass01').attr('checked', true);
		$('#memPwClass02').attr('disabled', true);
		
		$('#step02Txt').attr('hidden', false);
		$('.floatSection').show();
	});

	// 아이디 찾기 > 학생
	$('#memIdClass01').click(function() {
		if($('#orgTypId').val() != null && $('#orgTypId').val() == "ORG01") {
			$('#memIdStuTr').attr('hidden', false);
		} else {
			$('#memIdCla03').attr('hidden', false);
		}
	});
	
	// 아이디 찾기 > 선생님
	$('#memIdClass02').click(function() {
		$('#memIdCla03').attr('hidden', true);
		$('#memIdStuTr').attr('hidden', true);
	});
	
	// 아이디 찾기 > 학교 일때 학년 선택에 따른 반 설정
	$('#memIdCla01').change(function(){
		var st_grade_cd = $(this).val();
		var schorg_id = $('#schorg_id').val();
		if ($('#memIdCla00').val() == 'N') {
			fnAlertMessage('학년도를 선택하세요.');
			return false;
		}
		var st_year_cd = $('#memIdCla00').val();

		if (st_grade_cd != null) {
			$.ajax({
				url : "/join/classSet",
				type : "POST",
				data : {st_grade_cd : st_grade_cd, schorg_id : schorg_id, st_year_cd : st_year_cd},
				success:function(data) {
					var json = jsonListSet(data);

					$('#memIdCla02 option').remove();
					var cpt = $('<option value="N">반</option>');
					$('#memIdCla02').append(cpt);
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#memIdCla02').append(cpt);
					}
				},
				error:function(e) {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			});
		}
	});

	// 아이디 찾기 > 기관 일때 학교급 선택에 따른 학년 설정
	$('input[name=memIdStu]').click(function() {
		var chk = this.value;
		$('#memIdStuNum option').remove();		
		if(chk == '01') {
			var spt =   "<option value='N'>학년</option>" +
						"<option value='01'>1</option>" +
						"<option value='02'>2</option>" +
						"<option value='03'>3</option>" +
						"<option value='04'>4</option>" +
						"<option value='05'>5</option>" +
						"<option value='06'>6</option>";
			$('#memIdStuNum').append(spt);
		} else if(chk == '02' || chk == '03') {
			var spt = "<option value='N'>학년</option>" +
					  "<option value='01'>1</option>" +
					  "<option value='02'>2</option>" +
					  "<option value='03'>3</option>";
			$('#memIdStuNum').append(spt);
		}
	});
	
	// 비밀번호 찾기 > 학생
	$('#memPwClass01').click(function() {
		if($('#orgTypId').val() != null && $('#orgTypId').val() == "ORG01") {
			$('#memPwStuTr').attr('hidden', false);
		} else {
			$('#memPwCla03').attr('hidden', false);
		}
	});
	
	// 비밀번호 찾기 > 선생님
	$('#memPwClass02').click(function() {
		$('#memPwCla03').attr('hidden', true);
		$('#memPwStuTr').attr('hidden', true);
	});
	
	// 비밀번호 찾기 > 학교 일때 학년 선택에 따른 반 설정
	$('#memPwCla01').change(function(){
		var st_grade_cd = $(this).val();
		var schorg_id = $('#schorg_id').val();
		if ($('#memPwCla00').val() == 'N') {
			fnAlertMessage('학년도를 선택하세요.');
			return false;
		}
		var st_year_cd = $('#memPwCla00').val();

		if (st_grade_cd != null) {
			$.ajax({
				url : "/join/classSet",
				type : "POST",
				data : {st_grade_cd : st_grade_cd, schorg_id : schorg_id, st_year_cd : st_year_cd},
				success:function(data) {
					var json = jsonListSet(data);

					$('#memPwCla02 option').remove();
					var cpt = $('<option value="N">반</option>');
					$('#memPwCla02').append(cpt);
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#memPwCla02').append(cpt);
					}
				},
				error:function(e) {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			});
		}
	});
	
	// 비밀번호 찾기 > 기관 일때 학교급 선택에 따른 학년 설정
	$('input[name=memPwStu]').click(function() {
		var chk = this.value;		
		$('#memPwStuNum option').remove();
		if(chk == '01') {
			var spt =   "<option value='N'>학년</option>" +
						"<option value='01'>1</option>" +
						"<option value='02'>2</option>" +
						"<option value='03'>3</option>" +
						"<option value='04'>4</option>" +
						"<option value='05'>5</option>" +
						"<option value='06'>6</option>";
			$('#memPwStuNum').append(spt);
		} else if(chk == '02' || chk == '03') {
			var spt = "<option value='N'>학년</option>" +
					  "<option value='01'>1</option>" +
					  "<option value='02'>2</option>" +
					  "<option value='03'>3</option>";
			$('#memPwStuNum').append(spt);
		}
	});
	
	function selectClass() {
		// Step1 의 학급 선택 후 Step2 노출
		if ($('#organClass').val().length > 0) {
			$('#step02Txt').attr('hidden', false);
			$('.floatSection').show();
		} else {
			$('#step02Txt').attr('hidden', true);
			$('.floatSection').hide();
		}
	}

	// 아이디 찾기
	function doIdSearch() {
		// 학교/기관 회원
		if($('#memClass01').is(':checked')){
			if($('#classSetChk').val() == 'N'){
				fnAlertMessage('학교/기관을 선택하세요.');
				return false;
			}

			if ($('#orgTypId').val() == 'ORG01') { // 기관을 선택한 경우에 대한 처리
				if($('#organClass').val() == null || $('#organClass').val() == ""){
					fnAlertMessage("학급을 선택하세요.");
					return false;
				}

				if($('#memIdName').val() == null || $('#memIdName').val() == "") {
					fnAlertMessage('이름을 입력하세요.');
					return false;
				}

				// 학생
				if($('#memIdClass01').is(':checked')){					
					if($('#memIdStuYear').val() == 'N'){
						fnAlertMessage('학년도를 선택하세요.');
						return false;
					}
					if(!($('#memIdStu01').is(':checked') || $('#memIdStu02').is(':checked') || $('#memIdStu03').is(':checked'))){
						fnAlertMessage('학교급을 선택하세요.');
						return false;
					}
					if($('#memIdStuNum').val() == 'N'){
						fnAlertMessage('학년을 선택하세요.');
						return false;
					}

					$('#user_type').val('S');
				}
				// 선생님
				if($('#memIdClass02').is(':checked')){
					$('#user_type').val('T');
				}
				
				$('#memIdStdCla').val($('#organClass').val());	
			} else { // 학교를 선택한 경우에 대한 처리
				if($('#memIdName').val() == null || $('#memIdName').val() == "") {
					fnAlertMessage('이름을 입력하세요.');
					return false;
				}			
				if($('#memIdCla00').val() == 'N'){
					fnAlertMessage('학년도를 선택하세요.');
					return false;
				}				
				if($('#memIdCla01').val() == 'N'){
					fnAlertMessage('학년을 선택하세요.');
					return false;
				}
				if($('#memIdCla02').val() == 'N'){
					fnAlertMessage('반을 선택하세요.');
					return false;
				}
				
				// 학생
				if($('#memIdClass01').is(':checked')){
					if($('#memIdCla03').val() == 'N'){
						fnAlertMessage('번호를 선택하세요.');
						return false;
					}
					$('#user_type').val('S');
				}
				// 선생님
				if($('#memIdClass02').is(':checked')){
					$('#user_type').val('T');
				}
			}
		}
		// 개인회원
		else
		{
			if($('#memIdName').val() == null || $('#memIdName').val() == "") {
				fnAlertMessage('이름을 입력하세요.');
				return false;
			}
			if($('#memIdStuYear').val() == 'N') {
				fnAlertMessage('학년도를 선택하세요.');
				return false;
			}
			if(!($('#memIdStu01').is(':checked') || $('#memIdStu02').is(':checked') || $('#memIdStu03').is(':checked'))) {
				fnAlertMessage('학교급을 선택하세요.');
				return false;
			}
			if($('#memIdStuNum').val() == 'N') {
				fnAlertMessage('학년을 선택하세요.');
				return false;
			}
			$('#user_type').val('P');
		}
		
		var frm = $('#idSearchForm').serialize();
		$.ajax({
			url : "/searchIdAjax",
			type : "post",
			data : frm,
			success:function(data){
				if(data == 'N') {
					//fnAlertMessage('(정보와 일치하는 아이디가 없습니다. <br/> (학교/학년/반/번호이름/아이디 을/를 입력하세요.');
					fnAlertMessage('정보와 일치하는 아이디가 없습니다.');
				} else {
					fnAlertMessage('회원님의 아이디는 <span>' + data + '</span>입니다.');
				}
			},
			error:function(e) {
				return false;
			}
		});
	}
	
	function doPasswordEmail() {
		// 학교/기관 회원
		if($('#memClass01').is(':checked')){
			if($('#classSetChk').val() == 'N'){
				fnAlertMessage('학교/기관을 선택하세요.');
				return false;
			}
			
			if($('#orgTypId').val() == "ORG01") { // 기관을 선택한 경우에 대한 처리
				if($('#organClass').val() == null || $('#organClass').val() == ""){
					fnAlertMessage("학급을 선택하세요.");
					return false;
				}
			
				if($('#memPwId').val() == null || $('#memPwId').val() == "") {
					fnAlertMessage('아이디를 입력하세요.');
					return false;
				}
				
				// 학생
				if($('#memPwClass01').is(':checked')){
					if($('#memPwStuYear').val() == 'N') {
						fnAlertMessage('학년도를 선택하세요.');
						return false;
					}
					if(!($('#memPwStu01').is(':checked') || $('#memPwStu02').is(':checked') || $('#memPwStu03').is(':checked'))){
						fnAlertMessage('학교급을 선택하세요.');
						return false;
					}
					if($('#memPwStuNum').val() == 'N'){
						fnAlertMessage('학년을 선택하세요.');
						return false;
					}
					
					$('#user_type2').val('S');
				}
				// 선생님
				if($('#memPwClass02').is(':checked')){
					$('#user_type2').val('T');
				}
				
				$('#memPwStdCla').val($('#organClass').val());
			} else { // 학교를 선택한 경우에 대한 처리
				if($('#memPwId').val() == null || $('#memPwId').val() == "") {
					fnAlertMessage('아이디를 입력하세요.');
					return false;
				}
				if($('#memPwCla00').val() == 'N') {
					fnAlertMessage('학년도를 선택하세요.');
					return false;
				}
				if($('#memPwCla01').val() == 'N'){
					fnAlertMessage('학년을 선택하세요.');
					return false;
				}
				if($('#memPwCla02').val() == 'N'){
					fnAlertMessage('반을 선택하세요.');
					return false;
				}
				
				// 학생
				if($('#memPwClass01').is(':checked')){
					if($('#memPwCla03').val() == 'N'){
						fnAlertMessage('번호를 선택하세요.');
						return false;
					}
					$('#user_type2').val('S');
				}
				// 선생님
				if($('#memPwClass02').is(':checked')){
					$('#user_type2').val('T');
				}
			}
		}
		// 개인회원
		else
		{
			if($('#memPwId').val() == null || $('#memPwId').val() == "") {
				fnAlertMessage('아이디를 입력하세요.');
				return false;
			}
			if($('#memPwStuYear').val() == 'N') {
				fnAlertMessage('학년도를 선택하세요.');
				return false;
			}
			if(!($('#memPwStu01').is(':checked') || $('#memPwStu02').is(':checked') || $('#memPwStu03').is(':checked'))){
				fnAlertMessage('학교급을 선택하세요.');
				return false;
			}
			if($('#memPwStuNum').val() == 'N') {
				fnAlertMessage('학년을 선택하세요.');
				return false;
			}
			$('#user_type2').val('P');
		}
		var forms = $('#pwSearchForm').serialize();
		$.ajax({
			url : "/sendPasswordEmailAjax",
			type : "post",
			data : forms,
			success:function(res) {
				if (res == 'succ') {
					fnAlertMessage('<span>' + $('#memPwId').val() + '</span>로 임시 비밀번호가 발송되었습니다.');
				} else if(res == 'N') {
					//fnAlertMessage('(정보와 일치하는 아이디가 없습니다.) <br/> (학교/학년/반/번호이름/아이디 을/를 입력하세요.)');
					fnAlertMessage('정보와 일치하는 아이디가 없습니다.');
				} else {
					fnAlertMessage('다시 시도하여 주세요.');
				}
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />