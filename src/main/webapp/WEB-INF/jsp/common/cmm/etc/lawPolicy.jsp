<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="lawPolicy">
		<h1 class="title">법적고지</h1>
		<p class="pbg">사이트 이용에 대한 법적인 제한과 금지사항을 알려드립니다.</p>
		<h2 class="top">저작권</h2>
		<h4 class="bot">
			에듀캣 사이트는 스퀘어네트㈜의 소중한 자산이며, 에듀캣 사이트에서 제공중인 모든 사항에 대한 저작권 또한 스퀘어네트㈜에 귀속됩니다.<br/>
			에듀캣 사이트에서 제공하는 모든 서비스 및 자료, 정보 등은 비상업적인 용도의 사용을 위하여만 제공되므로 그림, 파일다운로드, 링크 등 사이트에서 제공되는
			모든 사항은 상업적인 목적과 공공의 목적으로 사용하는 어떠한 방식도 허가하지 않습니다.
		</h4>
		<h2 class="top">법적부인</h2>
		<h4 class="bot">
			에듀캣 사이트는 본 홈페이지에서 제공하는 서비스와 정보에 대한 정확도, 신뢰도, 완성도에 대하여 보장하지 않으며 웹사이트에서 제공하는 자료나 표현에
			대하여는 오류가 있을 수 있음을 알려드립니다.<br/>
			스퀘어네트에서 서면으로 보장해 드리지 않는 정보나 자료에 대하여 전적으로 의지하지 마시기 바랍니다.<br/>
			에듀캣 사이트의 서비스를 활용해서 개인이나 기업의 이익에 관여되는 판매, 무역, 구매 등의 거래에 직접적인 활용은 삼가해 주시기 바랍니다. 어떠한 경우에서도
			회사에서 제공하는 정보 및 자료로 인한 직접, 간접, 부수적, 특별 또는 확대 손해에 대해 책임지지 않습니다.
		</h4>
		<h2 class="top">브랜드 도용금지</h2>
		<h4 class="bot">
			스퀘어네트의 CI 및 모든 상표, 로고는 관련 법령에 의해 보호를 받는 스퀘어네트의 자산입니다. 따라서 스퀘어네트의 허가 없이 사용하는 경우 관련법에 의하여
			제재를 할 수 있으며 ,어떠한 경우에서라도 스퀘어네트 CI 및 상표의 무분별한 도용을 엄격하게 규제합니다.
		</h4>
		<h2 class="top">링크에 대한 책임</h2>
		<h4 class="bot">
			에듀캣 사이트는 자체적인 기준에 의거하여 제공하고 있습니다. 에듀캣 사이트에서 링크된 타사 혹은 개인의 사이트는 이용자의 편의를 위하여 제공되는 것이므로
			어떠한 법적인 책임도 스퀘어네트에 있지 않으며 웹사이트에 대한 신뢰성에 대한 보장을 하지 않습니다.<br/>
			내용이 고지된 후에 에듀캣 사이트에서 제공하는 서비스를 이용하시는 모든 이용자는 본 고지내용에 동의하는 것으로 간주합니다.<br/>
			에듀캣 사이트는 본 고지사항에 명시된 세부항목과 사용조건의 변경이유를 적절한 시간 여유를 가지고 고지하거나, 인터넷 산업의 상관례에 맞추어 적절한 방법으로
			사전 통지 없이 수시로 변경할 권리와 권한을 가지고 있습니다.<br/>
			에듀캣 사이트를 이용 중이신 모든 이용자께서는 수시로 변경되는 내용을 열람하시고 확인하여 주시기 바랍니다.
		</h4>
		<h2 class="end">이상 법적고지는 2016년 3월 1일 부터 시행합니다.</h2>
		<div class="btnArea mT30 clB">
			<span class="saBtn"><a href="/index?msq=50" class="btnMemGreenBig">이전</a></span>
		</div>
	</div>
</div>

<c:import url="/footer" charEncoding="utf-8" />