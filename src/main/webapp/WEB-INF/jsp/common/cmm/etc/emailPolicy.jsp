<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="loginContainer">
	<div id="content" class="emailPolicy">
		<h1 class="title">이메일 무단 수집 거부</h1>
		<p class="pbg">에듀캣 사이트 내 이메일 주소가 무단으로 수집되는 것을 거부합니다.</p>
		<p class="imgArea">
			<img src="/template/common/images/etc/etc_email.png" alt="" />
		</p>
		<h2 class="top">
			에듀캣은 게시된 이메일 주소가 전자우편 수집 프로그램이나<br/>
			그 밖의 기술적인 장치를 이용하여 무단으로 수집되는 것을 거부하여, 이에 반하여 수집할 시<br/>
			<span class="cl_ec7063">정보통신망법에 의해 형사 처벌됨</span>을 유념하시기 바랍니다.
		</h2>
		<h4 class="bot">
			게시일 : 2016년 3월 1일
		</h4>
		<div class="btnArea mT30 clB">
			<span class="saBtn"><a href="/index?msq=50" class="btnMemGreenBig">이전</a></span>
		</div>
	</div>
</div>

<c:import url="/footer" charEncoding="utf-8" />