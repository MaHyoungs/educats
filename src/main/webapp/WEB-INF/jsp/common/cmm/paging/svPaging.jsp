<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="custom" uri="tid/custom-taglib"%>

<c:set var="page" value="1" />
<c:if test="${!empty param.currentPage}"><c:set var="page" value="${param.currentPage}" /></c:if>
<c:if test="${!empty currentPage}"><c:set var="page" value="${currentPage}" /></c:if>
<c:set var="indexPerPage" value="10" />
<c:if test="${!empty param.indexPerPage}"><c:set var="indexPerPage" value="${param.indexPerPage}" /></c:if>
    <custom:paging name="paging" recordsPerPage="${recordsPerPage}" totalRecordCount="${totalRecordCount}" currentPage="${page}" indexPerPage="${indexPerPage}">

        <c:choose>
            <%--인성검사--%>
            <c:when test="${not empty perSurvey}" >
                <%-- Prev --%>
                <c:if test="${!empty paging.previousPage.href and page gt 1}">
                    <span class="saBtn"><a href="${paging.previousPage.href}" class="btnScalePrev">이전</a></span>
                </c:if>
                <%-- Next --%>
                <c:if test="${!empty paging.nextPage.href}">
                    <c:if test="${lastChk ne 'Y'}" >
                        <span class="saBtn"><a href="javascript:void(0)" onclick="goNext('${paging.nextPage.href}')" class="btnScaleNext">다음</a></span>
                    </c:if>
                    <%--<span class="saBtn"><a href="${paging.nextPage.href}" class="btnScaleNext">다음</a></span>--%>
                </c:if>
            </c:when>
            <%--계열검사--%>
            <c:when test="${not empty affSurvey}" >
                <fmt:parseNumber var="lastPaging" value="${(cnt / paging.recordsPerPage)}" integerOnly="true"/>
                    <%-- Prev --%>
                    <c:if test="${!empty paging.previousPage.href and page gt 1}">
                        <span class="saBtn"><a href="${paging.previousPage.href}" class="btnScalePrev">이전</a></span>
                    </c:if>
                    <%-- Next --%>
                    <c:if test="${paging.currentPage eq lastPaging}" >
                        <c:if test="${!empty paging.nextPage.href}">
                            <span class="saBtn"><a href="javascript:void(0)" onclick="goNext('/au/majSurvey')" class="btnScaleNext">다음</a></span>
                        </c:if>
                    </c:if>
                    <c:if test="${paging.currentPage ne lastPaging}" >
                        <c:if test="${!empty paging.nextPage.href}">
                            <span class="saBtn"><a href="javascript:void(0)" onclick="goNext('${paging.nextPage.href}')" class="btnScaleNext">다음</a></span>
                        </c:if>
                    </c:if>
            </c:when>
            <%--학과검사--%>
            <c:when test="${not empty majSurvey}" >
                <%-- Prev --%>
                <c:if test="${paging.currentPage eq '1'}" >
                    <c:if test="${!empty paging.previousPage.href}">
                        <span class="saBtn"><a href="/au/affSurvey" class="btnScalePrev">이전</a></span>
                    </c:if>
                </c:if>
                <c:if test="${paging.currentPage ne '1'}" >
                    <c:if test="${!empty paging.previousPage.href}">
                        <span class="saBtn"><a href="${paging.previousPage.href}" class="btnScalePrev">이전</a></span>
                    </c:if>
                </c:if>
                <%-- Next --%>
                <c:if test="${!empty paging.nextPage.href}">
                    <c:if test="${lastChk ne 'Y'}" >
                        <span class="saBtn"><a href="javascript:void(0)" onclick="goNext('${paging.nextPage.href}')" class="btnScaleNext">다음</a></span>
                    </c:if>
                    <%--<span class="saBtn"><a href="${paging.nextPage.href}" class="btnScaleNext">다음</a></span>--%>
                </c:if>
            </c:when>
            <c:otherwise>
                <%-- Prev --%>
                <c:if test="${!empty paging.previousPage.href and page gt 1}">
                    <span class="saBtn"><a href="${paging.previousPage.href}" class="btnScalePrev">이전</a></span>
                </c:if>

                <%-- Next --%>
                <c:if test="${!empty paging.nextPage.href}">
                    <span class="saBtn"><a href="javascript:void(0)" onclick="goNext('${paging.nextPage.href}')" class="btnScaleNext">다음</a></span>
                    <%--<span class="saBtn"><a href="${paging.nextPage.href}" class="btnScaleNext">다음</a></span>--%>
                </c:if>
            </c:otherwise>
        </c:choose>

    </custom:paging>