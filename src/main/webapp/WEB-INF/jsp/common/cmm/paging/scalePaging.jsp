<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="custom" uri="tid/custom-taglib"%>

<c:set var="page" value="1" />
<c:if test="${!empty param.currentPage}"><c:set var="page" value="${param.currentPage}" /></c:if>
<c:if test="${!empty currentPage}"><c:set var="page" value="${currentPage}" /></c:if>
<c:set var="indexPerPage" value="5" />
<c:if test="${!empty param.indexPerPage}"><c:set var="indexPerPage" value="${param.indexPerPage}" /></c:if>
    <custom:paging name="paging" recordsPerPage="${recordsPerPage}" totalRecordCount="${totalRecordCount}" currentPage="${page}" indexPerPage="${indexPerPage}">
        <c:if test="${empty majSurvey}">
            <ul>
            <c:forEach var="numbering" items="${paging.pages}" varStatus="status">
                <c:choose>
                    <c:when test="${numbering.index eq paging.currentPage}">
                        <li class="on"><span>${numbering.index}</span></li>
                    </c:when>
                    <c:otherwise>
                        <li><span>${numbering.index}</span></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            </ul>
        </c:if>
        <c:if test="${not empty majSurvey}">
            <ul>
            <c:forEach var="numbering" items="${paging.pages}" varStatus="status">
                <c:choose>
                    <c:when test="${(affPages+numbering.index) eq (affPages+paging.currentPage)}">
                        <li class="on"><span>${(affPages+numbering.index)}</span></li>
                    </c:when>
                    <c:otherwise>
                        <li><span>${(affPages+numbering.index)}</span></li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            </ul>
        </c:if>
        <span class="next"><img src="/template/common/images/au/paging_next.png" alt="" /></span>
    </custom:paging>