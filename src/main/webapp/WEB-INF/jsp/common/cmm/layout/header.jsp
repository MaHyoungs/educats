<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!doctype html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=1000" />
		<title>에듀캣</title>
		<link rel="shortcut icon" href="/favicon.ico" />
		<meta name="naver-site-verification" content="fe48f89e6944f2cb1f90629f06031ea0025e56d5"/>
		<meta name="description" content="eduCAT 초/중/고 자기주도학습진단 및 진로탐색검사(인성검사, 진로적성검사) 서비스">
		<meta property="og:type" content="website">
		<meta property="og:title" content="에듀캣">
		<meta property="og:description" content="eduCAT 초/중/고 자기주도학습진단 및 진로탐색검사(인성검사, 진로적성검사) 서비스">
		<meta property="og:image" content="http://educats.co.kr/educat_og.png">
		<meta property="og:url" content="http://educats.co.kr">
		<link charset="utf-8" href="/template/common/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/common.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/style.css" type="text/css" rel="stylesheet" />
		<script src="/template/common/js/jquery-1.9.1.min.js"></script>
		<script src="/template/common/jquery-ui/jquery-ui.js"></script>
		<%--<script src="/template/common/jquery-ui/jquery-ui.min.js"></script>--%>
		<script src="/template/common/js/datepicker.js"></script>
		<script src="/template/common/js/common.js"></script>
		<script src="/template/common/js/d3.v3.min.js"></script>
		<script src="/template/common/js/layerpopup.js"></script>
		<script src="/template/common/js/chart_pie.js"></script>
	</head>
	<body>
		<div id="wrap">
			<c:if test="${param.adms ne 'Y' and param.tchs ne 'Y'}">
				<div id="header">
					<h1 id="logo">
						<a href="/index?gh=y" target="_top"><img src="/template/common/images/logo.png" alt="eduCAT logo"></a>
					</h1>
					<div id="lnb">
						<h2 class="hdn">주메뉴</h2>
						<c:choose>
							<c:when test="${not empty user}">
								<div class="lnbTab">
									<a href="/index?gh=y" class="ghom <c:if test="${param.gh eq 'y'}">on</c:if>" target="_top">Home</a>
									<c:choose>
										<%-- 02: 학생권한 --%>
										<c:when test="${user.user_typ_cd eq '02'}">
											<a href="/usr/choice?type=s" class="gstu <c:if test="${empty param.gh}">on</c:if>" target="_top">학생</a>
										</c:when>
										<%-- 04: 선생님권한 --%>
										<c:when test="${user.user_typ_cd eq '04'}">
											<a href="/tch/joinState" class="gtea <c:if test="${empty param.gh}">on</c:if>" target="_top">선생님</a>
										</c:when>
										<c:otherwise>
											<%-- 권한 업는 사용자 진입 --%>
										</c:otherwise>
									</c:choose>
								</div>
								<c:choose>
									<%-- 02: 학생권한 --%>
									<c:when test="${user.user_typ_cd eq '02'}">
										<c:choose>
											<c:when test="${param.gh eq 'y'}">
												<ul id="menu" class="menu">
													<li class="menu menu01 on"><a href="/index?msq=1" target="_top">About<br/>eduCAT</a></li>
													<li class="menu menu02"><a href="/index?msq=2" target="_top">진로탐색검사</a></li>
													<li class="menu menu03"><a href="/index?msq=3" target="_top">자기주도학습<br/>역량진단검사</a></li>
													<li class="menu menu04"><a href="/index?msq=4" target="_top">자기주도학습<br/>세미나</a></li>
													<li class="menu menu05"><a href="/index?msq=5" target="_top">문의하기</a></li>
												</ul>
											</c:when>
											<c:otherwise>
												<ul class="menu">
													<c:choose>
														<c:when test="${not empty user.confm_yn}">
															<c:choose>
																<c:when test="${user.confm_yn eq 'Y'}">
																	<c:if test="${empty user.pri_tme}">
																		<li class="m1"><a href="/usr/choice?type=s" target="_top">검사하기</a></li>
																	</c:if>
																	<c:if test="${not empty user.pri_tme}">
																		<li class="m1"><a href="javascript:void(0)" onclick="chkValid('${user.seq_user}', '${user.pri_tme}')" target="_top">검사하기</a></li>
																	</c:if>
																	<%--<li class="m2"><a href="/usr/choice?type=r" target="_top">검사결과</a></li>--%>
																	<li class="m2"><a href="javascript:void(0)" onclick="chkResult('${user.user_id}')">검사결과</a></li>
																</c:when>
																<c:otherwise>
																	<li class="m1"><a href="/usr/info?msg=R" target="_top">검사하기</a></li>
																	<li class="m2"><a href="/usr/info?msg=R" target="_top">검사결과</a></li>
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															<li class="m1"><a href="/usr/info?msg=B" target="_top">검사하기</a></li>
															<li class="m2"><a href="/usr/info?msg=B" target="_top">검사결과</a></li>
														</c:otherwise>
													</c:choose>
													<li class="m3"><a href="/usr/info" target="_top">마이페이지</a></li>
												</ul>
											</c:otherwise>
										</c:choose>
									</c:when>
									<%-- 04: 선생님권한 --%>
									<c:when test="${user.user_typ_cd eq '04'}">
										<c:choose>
											<c:when test="${param.gh eq 'y'}">
												<ul id="menu" class="menu">
													<li class="menu menu01 on"><a href="/index?msq=1" target="_top">About<br/>eduCAT</a></li>
													<li class="menu menu02"><a href="/index?msq=2" target="_top">진로탐색검사</a></li>
													<li class="menu menu03"><a href="/index?msq=3" target="_top">자기주도학습<br/>역량진단검사</a></li>
													<li class="menu menu04"><a href="/index?msq=4" target="_top">자기주도학습<br/>세미나</a></li>
													<li class="menu menu05"><a href="/index?msq=5" target="_top">문의하기</a></li>
												</ul>
											</c:when>
											<c:otherwise>
												<ul class="menu">
													<c:choose>
														<c:when test="${user.confm_yn eq 'N'}">
															<li class="m1"><a href="/tch/info?msg=RT" target="_top">참여현황</a></li>
															<li class="m2"><a href="/tch/info?msg=RT" target="_top">학생관리</a></li>
														</c:when>
														<c:when test="${empty user.confm_yn}">
															<li class="m1"><a href="/tch/info?msg=BT" target="_top">참여현황</a></li>
															<li class="m2"><a href="/tch/info?msg=BT" target="_top">학생관리</a></li>
														</c:when>
														<c:otherwise>
															<li class="m1"><a href="/tch/joinState" target="_top">참여현황</a></li>
															<li class="m2"><a href="/tch/usrManage" target="_top">학생관리</a></li>
														</c:otherwise>
													</c:choose>
													<li class="m3"><a href="/tch/info" target="_top">내정보관리</a></li>
												</ul>
											</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										<%-- 권한 업는 사용자 진입 --%>
									</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<ul id="menu" class="menu">
									<li class="menu menu01"><a href="/index?msq=1">About<br/>eduCAT</a></li>
									<li class="menu menu02"><a href="/index?msq=2">진로탐색검사</a></li>
									<li class="menu menu03"><a href="/index?msq=3">자기주도학습<br/>역량진단검사</a></li>
									<li class="menu menu04"><a href="/index?msq=4">자기주도학습<br/>세미나</a></li>
									<li class="menu menu05"><a href="/index?msq=5">문의하기</a></li>
								</ul>
							</c:otherwise>
						</c:choose>
					</div>
					<div id="gnb">
						<c:choose>
							<c:when test="${not empty user}">
								<c:set var="session_nm" value="${user.user_nm}"/>
								<c:choose>
									<c:when test="${user.user_typ_cd eq '02'}">
										<c:set var="info_uri" value="/usr"/>
									</c:when>
									<c:when test="${user.user_typ_cd eq '04'}">
										<c:set var="info_uri" value="/tch"/>
									</c:when>
									<c:otherwise></c:otherwise>
								</c:choose>
							</c:when>
							<c:when test="${not empty adm}">
								<c:set var="session_nm" value="${adm.user_nm}"/>
							</c:when>
							<c:otherwise>
								<c:set var="session_nm" value=""/>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${not empty session_nm}">
								<a href="${info_uri}/info" class="uicon" target="_top">${session_nm} 님</a>
								<a href="/logOut" target="_top">로그아웃</a>
							</c:when>
							<c:otherwise>
								<a href="/login">로그인</a>
								<a href="/join/lawConfirm">회원가입</a>
							</c:otherwise>
						</c:choose>
					</div>
				</div>
				<c:if test="${user.confm_yn eq 'Y'}">
					<script>
						/**
						 * 사용자 결과 체크
						 * @param id
						 */
						function chkResult(id) {
							$.ajax({
								url : "/usr/chkResultAjax",
								type : "POST",
								data : {user_id:id},
								success:function(data) {
									if(data == 'Y'){
										top.document.location.href = "/usr/choice?type=r";
									}else{
										fnAlertMessage('검사가 완료되지 않았습니다. <br/> 검사완료 후 결과확인이 가능합니다.');
										return false;
									}
								},
								error:function(e) {
									// 오류 메시지 출력
									fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
									return false;
								}
							});
						}
						<c:if test="${not empty user.schlvl_cd}">

						/**
						 * 쿠폰사용자 유효성 체크
						 * @param id
						 */
						function chkValid(seq, tme) {
							$.ajax({
								url : "/usr/chkValidAjax",
								type : "POST",
								data : {seq_user:seq, pri_tme:tme},
								success:function(data) {
									if(data == 'Y'){
										top.document.location.href = "/usr/choice?type=s";
									}else{
										fnAlertMessage("쿠폰 유효기간이 종료되었습니다.");
										return false;
									}
								},
								error:function(e) {
									// 오류 메시지 출력
									fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
									return false;
								}
							});
						}
						</c:if>
					</script>
				</c:if>
			</c:if>
