<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

			<div id="footer">
				<div class="fcont">
					<p>COPYRIGHTⓒ 2016 BY SQUARENET , ALL RIGHTS RESERVED. </p>
				</div>
			</div>
		</div>
		<c:import url="/WEB-INF/jsp/common/mng/layout/layerMessage.jsp" charEncoding="utf-8" />
		<%-- Google Analytics --%>
		<script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-75657673-1', 'auto');ga('send', 'pageview');</script>
		<%-- Google Analytics --%>
	</body>
</html>