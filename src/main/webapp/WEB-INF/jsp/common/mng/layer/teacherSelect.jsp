<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script src="/template/common/js/paging.js"></script>

<div class="layerBg mngManageClassTeacher"></div>
<div class="layerPopup mngManageClassTeacher style01">
	<div class="layerHeader">
		<span class="title">선생님 선택</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<form id="searchForm" name="searchForm" action="/mng/layer/teacherSelect" method="post">
				<input type="hidden" name="schorg_id" value="${map.schorg_id}" />
				<input type="hidden" id="currentPagess" name="currentPage" value="${map.currentPage}" />
				<div class="searchWrap mB20">
					<div class="section">
						<label for="searchName">이름</label>
						<div class="select wid85">
							<input type="text" id="searchName" name="searchName" class="wid85"/>
						</div>
					</div>
					<div class="section">
						<%--<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="fnLayerResize('mngJoins')">검색</a></span>--%>
						<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="doSearch()">검색</a></span>
					</div>
					<div class="section">
						<div class="select">
							<select id="searchOrder" name="searchOrder">
								<option value="DESC">이름 오름차순</option>
								<option value="ASC">이름 내림차순</option>
							</select>
						</div>
					</div>
				</div>
			</form>
			<div class="tableWrap">
				<table id="teachersLst" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="35%" />
						<col width="65%" />
					</colgroup>
					<thead>
						<tr>
							<th>이름</th>
							<th>이메일</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="rs" items="${teacherSelect}">
							<tr>
								<td><a href="javascript:void(0)" onclick="setName('${rs.seq_user}','${rs.user_id}', '${rs.user_nm}')">${rs.user_nm}</a></td>
								<td>${rs.user_id}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div id="pagingss" class="paging"></div>
<%--
				<div class="paging">
					<span class="start"><a href="#">&lt;&lt;</a></span>
					<span class="prev"><a href="#">&lt;</a></span>
					<span class="on">1</span>
					<span><a href="#">2</a></span>
					<span><a href="#">3</a></span>
					<span><a href="#">4</a></span>
					<span><a href="#">5</a></span>
					<span class="next"><a href="#">&gt;</a></span>
					<span class="end"><a href="#">&gt;&gt;</a></span>
				</div>
--%>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		doSearch();
	});

	function doSearch(currentPage) {
		var frm = $('#searchForm').serialize();
		$('#currentPagess').val(currentPage);
		$('#teachersLst > tbody').children().remove();
		$.ajax({
			url    : "/mng/search/searchTeacher",
			type   : "post",
			data   : frm,
			success: function(data){
				var json = jsonListSet(data);
				for(i=0; i<json.length; i++) {
					$('#teachersLst > tbody').append(
							'<tr>' +
							'   <td><a href="javascript:void(0)" onclick=setName("' + json[i].seq_user + '","' + json[i].user_id + '","' + json[i].user_nm + '")>' + json[i].user_nm + '</a></td>' +
							'   <td>' + json[i].user_id + '</td>' +
							'</tr>'
					)
				}
				doPagings()
				fnLayerResize('mngManageClassTeacher');
			},
			error: function(e) {
				return false;
			}
		});
	}

	function doPagings() {
		var frm = $('#searchForm').serialize();
		$.ajax({
			url : "/mng/search/searchTeacherCnt",
			type : "post",
			data : frm,
			dataType: "html",
			success:function(data) {
				$('#pagingss').html(data);
				fnLayerResize('mngManageClassTeacher');
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>