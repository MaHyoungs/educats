<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="layerBg mngManageTeacherInfoSchool"></div>
<div class="layerPopup mngManageTeacherInfoSchool style01">
	<div class="layerHeader">
		<span class="title">선생님 상세정보</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<table class="style02" summary="">
					<caption></caption>
					<colgroup>
						<col width="10%" />
						<col width="18%" />
						<col width="10%" />
						<col width="31%" />
						<col width="13%" />
						<col width="18%" />
					</colgroup>
					<tbody>
						<tr>
							<th>이름</th>
							<td id="tchName"></td>
							<th>이메일</th>
							<td id="tchId"></td>
							<th>학교/기관명</th>
							<td id="tchOrgNm">${schInfo.schorg_nm}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="searchWrap mB20">
				<div class="section">
					<label for="search2Year">학년도</label>
					<div class="select ">
						<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
							<select id="search2Year" name="search2Year" onChange="yearToClass2(this.value)">
								<c:forEach var="yl" items="${yearList}">
									<option value="${yl.code}">${yl.code_nm}</option>
								</c:forEach>
							</select>
						</c:if>
						<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
							<select id="search2Year" name="search2Year" onChange="yearToClass(this.value)">
								<c:forEach var="yl" items="${yearList}">
									<option value="${yl.code}">${yl.code_nm}</option>
								</c:forEach>
							</select>
						</c:if>
					</div>
				</div>
				<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
					<div class="section">
						<label for="search2Cla">담당</label>
						<div class="select ">
							<select id="search2Cla" class="" name="search2Cla">
								<option value="N">선택</option>
							</select>
						</div>
					</div>
				</c:if>
				<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
					<div class="section">
						<label for="search2Gra">담당학년</label>
						<div class="select ">
							<select id="search2Gra" name="search2Gra" onChange="gradeToClass(this.value)">
								<option value="N">선택</option>
								<c:forEach var="gl" items="${gradeist}">
									<option value="${gl.code}">${gl.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="search2Cla">담당반</label>
						<div class="select ">
							<select id="search2Cla" name="search2Cla">
								<option value="N">선택</option>
							</select>
						</div>
					</div>
				</c:if>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doAdd()" class="btnMngJoinsSearch">추가</a></span>
				</div>
			</div>
			<div class="tableWrap">
				<table id="detailClass" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="19%" />
						<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
							<col width="19%" />
							<col width="19%" />
						</c:if>
						<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
							<col width="38%" />
						</c:if>
						<col width="19%" />
						<col width="24%" />
					</colgroup>
					<thead>
						<tr>
							<th>학년도</th>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<th>담당학년</th>
								<th>담당반</th>
							</c:if>
							<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<th>담당학급</th>
							</c:if>
							<th>상태</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
<%--						<tr>
							<td>2015</td>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<td>1학년</td>
							</c:if>
								<td>3반</td>
							<td>대기</td>
							<td>
								<span class="saBtn"><a href="#" class="btnMngTeacherApprove layerpopupOpen" data-popup-class="mngManageTeacherApprove">승인</a></span>
								<span class="saBtn"><a href="#" class="btnMngTeacherReturn layerpopupOpen" data-popup-class="mngManageTeacherReturn">반려</a></span>
							</td>
						</tr>--%>
					</tbody>
				</table>
				<div id="pagings2" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="fnPop()" class="btnLayerGreen">삭제</a>
				<a href="javascript:void(0)" onclick="location.href='/mng/manage/teacher?seq_lic=${schInfo.seq_lic}&org_id=${schInfo.schorg_id}'; return false;" class="btnLayerGray layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<input type="hidden" id="detailSeq" name="detailSeq" />
<input type="hidden" id="detailOrg" name="detailOrg" />
<input type="hidden" id="detailTyp" name="detailTyp" />
<input type="hidden" id="detailConf" name="detailConf" />

<%-- Layer --%>
<div class="layerBg mngManageTeacherApprove2 zi2"></div>
<div class="layerPopup mngManageTeacherApprove2 style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청을 승인 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdatees2(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg mngManageTeacherReturn2 zi2"></div>
<div class="layerPopup mngManageTeacherReturn2 style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청을 반려 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdatees2(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg mngManageTeacherDelete zi2"></div>
<div class="layerPopup mngManageTeacherDelete style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			선생님을 삭제 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doDelete(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<form id="tchForm2" name="tchForm2" action="/mng/manage/tchUpdateAjax2" method="post">
	<input type="hidden" id="tchTyp2" name="tchTyp" />
	<input type="hidden" id="tchCon2" name="tchCon" />
	<input type="hidden" id="tchSeq2" name="tchSeq" />
	<input type="hidden" id="tchYea2" name="tchYea" />
	<input type="hidden" id="tchGra2" name="tchGra" />
	<input type="hidden" id="tchCla2" name="tchCla" />
	<input type="hidden" id="tchOrg2" name="tchOrg" value="${schInfo.schorg_id}" />
	<input type="hidden" id="orgTyp2" name="orgTyp" value="${schInfo.schorg_typ_cd}" />
</form>

<script>
	/**
	 * 팝업열기
	 * @param popupId
	 */
	function fnPopupOpen(popupId, zIndex){
		var  winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	function tchDetail(seq, id, nm, conf, yea, org, orgTyp) {
		$('#tchId').text(id);
		$('#tchName').text(nm);
		$('#detailSeq').val(seq);
		$('#detailOrg').val(org);
		$('#detailTyp').val(orgTyp);
		$('#detailConf').val(conf);

		<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
			yearToClass(yea);
		</c:if>

		doDetailAjax(seq, org, orgTyp);
	}

	function yearToClass(yea) {
		$('#search2Cla option').remove();
		var orgs = $('#detailOrg').val();
		$.ajax({
			url    : "/mng/manage/tchClassSet",
			type   : "post",
			data   : {st_year_cd : yea, schorg_id : orgs},
			success: function(data){
				var json = jsonListSet(data);
					$('#search2Cla').append($('<option value="N">선택</option>'));
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
						$('#search2Cla').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function yearToClass2(yea) {
		$('#search2Cla option').remove();
		var orgs = $('#detailOrg').val();
		var gras = $('#search2Gra').val();
		$.ajax({
			url    : "/mng/manage/tchClassSet",
			type   : "post",
			data   : {st_year_cd : yea, st_grade_cd : gras, schorg_id : orgs},
			success: function(data){
				var json = jsonListSet(data);
				$('#search2Cla').append($('<option value="N">선택</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
					$('#search2Cla').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function gradeToClass(gra) {
		$('#search2Cla option').remove();
		var orgs = $('#detailOrg').val();
		var yeas = $('#search2Year').val();
		$.ajax({
			url    : "/mng/manage/tchClassSet",
			type   : "post",
			data   : {st_year_cd : yeas, st_grade_cd : gra, schorg_id : orgs},
			success: function(data){
				var json = jsonListSet(data);
				$('#search2Cla').append($('<option value="N">선택</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
					$('#search2Cla').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doAdd() {
		if($('#detailConf').val() == null || $('#detailConf').val() == '' || $('#detailConf').val() == 'N') {
			fnAlertMessage("승인상태인 선생님만 학급을 추가할 수 있습니다.");
			return false;
		}
		if($('#search2Cla').val() == 'N') {
			<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
				fnAlertMessage("학급을 선택하여 주십시요.");
				return false;
			</c:if>
			<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
				fnAlertMessage("학년/반을 선택하여 주십시요.");
				return false;
			</c:if>
		}
		$.ajax({
			url    : "/mng/manage/InsertTchClass",
			type   : "post",
			data   : {seq_user:$('#detailSeq').val(), st_year_cd:$('#detailSeq').val(), seq_class:$('#search2Cla').val(), schorg_typ_cd:$('#detailTyp').val()},
			success: function(data){
				if(data == "N") {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					fnAlertMessage("신청 학급에 선생님들이 모두 정해져있습니다.");
					return false;
				} else if(data == "D") {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					fnAlertMessage("신청 학급에 이미 신청되어있습니다.");
				} else {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					doDetailAjax($('#detailSeq').val(), $('#detailOrg').val(), $('#detailTyp').val());
					fnAlertMessage("신청 학급에 추가 되었습니다.");
				}
			},
			error: function(e) {
				return false;
			}
		});
	}

	function doTchSet2(typ, con, seq, yea, gra, cla) {
		$('#tchTyp2').val(typ);
		$('#tchCon2').val(con);
		$('#tchSeq2').val(seq);
		$('#tchYea2').val(yea);
		$('#tchGra2').val(gra);
		$('#tchCla2').val(cla);

		if(con == 'Y') {
			fnPopupOpen('mngManageTeacherApprove2');
		} else {
			fnPopupOpen('mngManageTeacherReturn2');
		}
	}

	function doUpdatees2(e) {
		var seq = $('#detailSeq').val();
		var org = $('#detailOrg').val();
		var typ = $('#detailtyp').val();
		var frmData2 = $('#tchForm2').serialize();

		$.ajax({
			url    : "/mng/manage/tchUpdateAjax2",
			type   : "post",
			data   : frmData2,
			success: function(data){
				if(data == "N") {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					fnAlertMessage("신청 학급에 선생님들이 모두 정해져있습니다.");
					return false;
				} else if(data == "deny") {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					doDetailAjax(seq,org,typ);
					fnAlertMessage("반려 처리 되었습니다.");
				} else {
					popupMessageDown('mngManageTeacherApprove2.style02.zi3', 'mngManageTeacherApprove2.zi2');
					popupMessageDown('mngManageTeacherReturn2.style02.zi3', 'mngManageTeacherReturn2.zi2');
					doDetailAjax(seq,org,typ);
					fnAlertMessage("승인 처리 되었습니다.");
				}
			},
			error: function(e) {
				return false;
			}
		});
	}

	function fnPop() {
		fnPopupOpen('mngManageTeacherDelete');
	}

	function doDelete() {
		location.href='/mng/manage/tchDelete?seq_lic=${schInfo.seq_lic}&org_id=${schInfo.schorg_id}&user_seq=' + $('#detailSeq').val();
	}

	function doDetailAjax(seq, org, orgTyp, currentPage) {
		$('#detailClass > tbody').children().remove();
		$.ajax({
			url    : "/mng/manage/teacherDetailAjax",
			type   : "post",
			data   : {seq_user:seq, schorg_id:org, schorg_typ_cd:orgTyp, currentPage:currentPage},
			success: function(data){
				var json = jsonListSet(data);
				for(var i=0; i<json.length; i++) {
					var states = '';
					if(json[i].user_type == 'R'){
						states = '<td>승인</td><td>&nbsp;</td>';
					}
					if(json[i].user_type == 'N'){
						if(json[i].confm_yn == null || json[i].confm_yn == '') {
							states = '<td>대기</td> +' +
									'<td>' +
									'<span class="saBtn"><a href="javascript:void(0)" onclick=doTchSet2("N","Y","' + json[i].seq_user + '","' + json[i].st_year_cd +'","' + json[i].st_grade_cd + '","' + json[i].st_class + '") class="btnMngTeacherApprove">승인</a></span>' +
									'<span class="saBtn"><a href="javascript:void(0)" onclick=doTchSet2("N","N","' + json[i].seq_user + '","' + json[i].st_year_cd +'","' + json[i].st_grade_cd + '","' + json[i].st_class + '") class="btnMngTeacherReturn">반려</a></span>' +
									'</td>';
						}
						if(json[i].confm_yn == 'N') {
							states = '<td>반려</td><td>&nbsp;</td>';
						}
						if(json[i].confm_yn == 'Y') {
							states = '<td>승인</td><td>&nbsp;</td>';
						}
					}
					if(json[i].user_type == 'H'){
						if(json[i].stat_cd == '01') {
							states = '<td>대기</td> +' +
									'<td>' +
									'<span class="saBtn"><a href="javascript:void(0)" onclick=doTchSet2("H","Y","' + json[i].seq_user + '","' + json[i].st_year_cd +'","' + json[i].st_grade_cd + '","' + json[i].st_class + '") class="btnMngTeacherApprove">승인</a></span>' +
									'<span class="saBtn"><a href="javascript:void(0)" onclick=doTchSet2("H","N","' + json[i].seq_user + '","' + json[i].st_year_cd +'","' + json[i].st_grade_cd + '","' + json[i].st_class + '") class="btnMngTeacherReturn">반려</a></span>' +
									'</td>';
						}
						if(json[i].stat_cd == '02') {
							states = '<td>반려</td><td>&nbsp;</td>';
						}
					}
					<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
					$('#detailClass > tbody').append(
							"<tr>" +
							"   <td>" + json[i].st_year_cd + "</td>" +
							"   <td>" + json[i].st_grade + "학년</td>" +
							"   <td>" + json[i].st_class + "반</td>" +
							states +
							"</tr>"
					)
					</c:if>
					<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
					$('#detailClass > tbody').append(
							"<tr>" +
							"   <td>" + json[i].st_year_cd + "</td>" +
							"   <td>" + json[i].st_class + "반</td>" +
							states +
							"</tr>"
					)
					</c:if>
				}
				doPaging2(seq, org, orgTyp, currentPage);
				fnLayerResize('mngManageTeacherInfoSchool');
			},
			error  : function(e){
				return false;
			}
		});
	}

	function doPaging2(seq, org, orgTyp, currentPage) {
		$.ajax({
			url : "/mng/manage/teacherDetailAjaxCnt",
			type : "post",
			data : {seq_user:seq, schorg_id:org, schorg_typ_cd:orgTyp, currentPage:currentPage},
			dataType: "html",
			success:function(data) {
				$('#pagings2').html(data);
				fnLayerResize('mngManageTeacherInfoSchool');
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>