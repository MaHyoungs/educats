<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="layerBg mngChoiceSales"></div>
<div class="layerPopup mngChoiceSales style01">
	<div class="layerHeader">
		<span class="title">발행대상 선택</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<form id="searchForm" name="searchForm" action="/mng/layer/teacherSelect" method="post" onsubmit="return false;">
				<input type="hidden" name="schorg_id" value="${map.schorg_id}" />
				<input type="hidden" id="currentPagess" name="currentPage" value="${map.currentPage}" />
				<div class="searchWrap mB20">
					<div class="section">
						<label for="searchName">회사명</label>
						<div class="select wid180">
							<input type="text" id="searchName" name="searchName" class="wid180"/>
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="doSearch2()">검색</a></span>
					</div>
				</div>
			</form>
			<div class="tableWrap">
				<table id="salesList" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="20%" />
						<col width="30%" />
						<col width="50%" />
					</colgroup>
					<thead>
						<tr>
							<th></th>
							<th>회사코드</th>
							<th>회사명</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagings2" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	function initSales(name, page) {
		$('#salesList > tbody').children().remove();
		$.ajax({
			url : "/mng/coupon/publisherListAjax",
			type : "post",
			data : {currentPage:page, searchName:name},
			success:function(data) {
				var json2 = jsonListSet(data);
				var lists = '';
				for(j=0; j<json2.length; j++) {
					var company = json2[j].company_nm.replace(/ /gi, "&nbsp;");
					lists += '<tr>' +
								'<td>' +
									'<div class="radioSection">' +
										'<input type="radio" name="choiceCompany" onclick=doSelect("'+json2[j].salescp_id.toUpperCase()+'","'+company+'") />' +
									'</div>' +
								'</td>' +
								'<td>' + json2[j].salescp_id + '</td>' +
								'<td><a href="javascript:void(0);" onclick=doSelect("'+json2[j].salescp_id.toUpperCase()+'","'+company+'")>' + company + '</a></td>' +
							'</tr>'
				}
				$('#salesList > tbody').append(lists);
				doPaging2(name, page);
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doPaging2(name, page) {
		$.ajax({
			url : "/mng/coupon/publisherListCntAjax",
			type : "post",
			data : {currentPage:page, searchName:name},
			dataType: "html",
			success:function(data) {
				$('#pagings2').html(data);
				fnLayerResize('mngChoiceSales');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doSearch2() {
		var nam = $('#searchName').val();
		initSales(nam, '1');
	}

	function doSelect(id, nm) {
		$('#salescpId').val(id);
		var company = nm.replace(/&nbsp;/gi, " ");
		$('#companyNm').val(company);
		$(".layerPopup.mngChoiceSales.style01").css({"display":"none"});
		$(".layerBg.mngChoiceSales").css({"display":"none"});
	}
</script>