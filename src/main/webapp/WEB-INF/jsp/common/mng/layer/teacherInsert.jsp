<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="layerBg mngManageTeacherAdd"></div>
<div class="layerPopup mngManageTeacherAdd style01">
	<div class="layerHeader">
		<span class="title">선생님 등록</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="tchInsert" name="tchInsert" action="/mng/manage/tchInsert" method="post">
					<input type="hidden" name="seq_lic" value="${schInfo.seq_lic}" />
					<input type="hidden" name="org_id" value="${schInfo.schorg_id}" />
					<table class="style03 wid495 mA20" summary="">
						<caption></caption>
						<colgroup>
							<col width="33%" />
							<col width="67%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_tchName">이름</th>
								<td>
									<input type="text" name="tchName" onkeyup="noSpace(this);" />
								</td>
							</tr>
							<tr>
								<th class="icon_tchId">ID(이메일)</th>
								<td>
									<input type="text" name="tchId" />
									<span id="errorEmail" class="info"></span>
								</td>
							</tr>
							<tr>
								<th class="icon_tchPass">비밀번호</th>
								<td>
									<input type="password" name="tchPassword" />
									<span class="info">* 영문소문자, 영문대문자 또는 특수문자, 숫자 포함 8자리 이상</span>
								</td>
							</tr>
							<tr>
								<th class="icon_tchRepassCheck">비밀번호 확인</th>
								<td>
									<input type="password" name="tchPasswordChk" />
									<span id="errorTxt" class="info"></span>
								</td>
							</tr>
							<tr>
								<th class="icon_tchNextClass">담당학급</th>
								<c:choose>
									<c:when test="${schInfo.schorg_typ_cd eq 'ORG01'}">
										<td>
											<div class="select wid80p">
												<select id="tchClass" name="tchClass" class="wid100p">
													<c:forEach var="cl" items="${classList}">
														<option value="${cl.st_class}">${cl.st_class}</option>
													</c:forEach>
												</select>
											</div>&nbsp;
											<label for="tchClass">반</label>
										</td>
									</c:when>
									<c:otherwise>
										<td>
											<div class="select">
												<select id="tchGrade" name="tchGrade">
													<option value="N">선택</option>
													<c:forEach var="gl" items="${gradeist}">
														<option value="${gl.code}">${gl.code_nm}</option>
													</c:forEach>
												</select>
											</div>&nbsp;
											<label for="tchGrade">학년</label>
											&nbsp;&nbsp;
											<div class="select wid50p">
												<select id="tchClass" name="tchClass" class="wid100p">
													<option value="N">선택</option>
												</select>
											</div>&nbsp;
											<label for="tchClass">반</label>
										</td>
									</c:otherwise>
								</c:choose>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="insertChk()" class="btnLayerGreen">등록</a>
				<a href="javascript:void(0)" onclick="deleteSet()" class="btnLayerGray layerClose">취소</a>
			</span>
		</div>
	</div>
</div>

<div class="layerBg mngManageTeacherAddSubmit zi2"></div>
<div class="layerPopup mngManageTeacherAddSubmit style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			등록하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doInsert()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('input[name=tchId]').keyup(function() {
			emailcheck($('input[name=tchId]').val());
		});
		
		$('input[name=tchPasswordChk]').keyup(function() {
			if($('input[name=tchPassword]').val() != $('input[name=tchPasswordChk]').val()) {
				document.getElementById("errorTxt").className= "info";
				$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			} else {
				document.getElementById("errorTxt").className= "info_ok";
				$('#errorTxt').text("");
			}
		});
	
	<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		$('#tchGrade').change(function(){
			var st_grade_cd = $(this).val();
			var schorg_id = $('input[name=org_id]').val();
			var st_year_cd = (new Date()).getFullYear();
			
			if (st_grade_cd != null) {
				$.ajax({
					url : "/mng/manage/tchClassSet",
					type : "POST",
					data : {st_grade_cd : st_grade_cd, schorg_id : schorg_id, st_year_cd : st_year_cd},
					success:function(data) {
						var json = jsonListSet(data);
						
						$('#tchClass option').remove();
						var cpt = $('<option value="N">선택</option>');
						$('#tchClass').append(cpt);
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#tchClass').append(cpt);
						}
					},
					error:function(e) {
						// 오류 메시지 출력
						fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
						return false;
					}
				});
			}
		});
	</c:if>
		
	});

	function insertChk(){
		if ($('input[name=tchName]').val().length < 1) {
			fnAlertMessage("이름을 입력하세요.");
			return false;
		}
		
		if ($('input[name=tchId]').val().length < 1) {
			fnAlertMessage("아이디를 입력하세요.");
			return false;
		}
		
		if ($('input[name=tchPassword]').val().length < 1) {
			fnAlertMessage("비밀번호를 입력하세요.");
			return false;
		}
		
		if ($('input[name=tchPasswordChk]').val().length < 1) {
			document.getElementById("errorTxt").className= "info";
			$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			$('input[name=tchPasswordChk]').focus();
			return false;
		}
		
		if (!chkPwdAll($.trim($('input[name=tchPassword]').val()))) {
			$('input[name=tchPassword]').val();
			$('input[name=tchPasswordChk]').val('');
			$('input[name=tchPassword]').focus();
			return false;
		}
		
		if ($('input[name=tchPassword]').val() != $('input[name=tchPasswordChk]').val()) {
			$('input[name=tchPasswordChk]').focus();
			return false;
		}
		
		if ($('#tchGrade').val() == 'N') {
			fnAlertMessage("학년을 선택하세요.");
			return false;
		}
		
		if ($('#tchClass').val() == 'N') {
			fnAlertMessage("반을 선택하세요.");
			return false;
		}
		
		if (!emailcheck($('input[name=tchId]').val())) {	
			return false;
		} else {
			$.ajax({
				url : "/mng/account/checkDupUseridAjax",
				type : "POST",
				data : {user_id : $('input[name=tchId]').val()},
				success:function(res) {
					if(res == 'fail'){
						fnAlertMessage('동일한 아이디가 있습니다.');
						return false;
					} else {
						$.ajax({
							url : "/mng/manage/teacherClassMaxChkAjax",
							type : "POST",
							data : {tchOrg : $('input[name=org_id]').val(), thisYea : (new Date()).getFullYear(), tchGra : $('#tchGrade').val(), tchCla : $('#tchClass').val()},
							success:function(res) {
								if (res == 'fail') {
									fnAlertMessage("선택된 학급에 배정할 수 있는 선생님은 최대 3명입니다.");
								} else {
									fnPopupConfirmOpen('mngManageTeacherAddSubmit');
								}
							},
							error:function(e) {
								// 오류 메시지 출력
								fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
								return false;
							}
						});
					}
				},
				error:function(e) {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			});
		}
	}

	function doInsert(){
		popupMessageDown('mngManageTeacherAddSubmit.style02.zi3', 'mngManageTeacherAddSubmit.zi2');

		document.tchInsert.submit();
	}
	
	function deleteSet(){
		$('input[name=tchName]').val("");
		$('input[name=tchId]').val("");
		$('#errorEmail').text("");
		$('input[name=tchPassword]').val("");
		$('input[name=tchPasswordChk]').val("");
		$('#errorTxt').text("");
	}
</script>