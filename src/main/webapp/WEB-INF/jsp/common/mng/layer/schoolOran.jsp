<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="layerBg mngManageClassClass"></div>
<div class="layerPopup mngManageClassClass style01">
	<div class="layerHeader">
		<span class="title">학교/기관 선택</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<h1 class="info">* 관리할 학교 또는 기관을 선택하세요.</h1>
		<div class="conContainer">
			<form id="searchOrgan" name="searchOrgan" action="/mng/search/searchOrgan" method="post" onsubmit="return false;">
				<input type="hidden" name="adm_typ_cd" value="${adm.auth_typ_cd}" />
				<input type="hidden" id="currentPages" name="currentPage" />
				<input type="hidden" name="adm_salescp_id" value="${adm.salescp_id}" />
				<input type="hidden" name="adm_schorg_id" value="${adm.schorg_id}" />
				<div class="searchWrap mB20">
					<div class="section">
						<label for="nowYear1">학년도</label>
						<div class="select ">
							<select id="nowYear1" name="nowYear">
								<c:forEach var="rs" items="${yearList}">
									<option value="${rs.code}" <c:if test="${map.nowYear eq rs.code}">selected="selected" </c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="schorg">구분</label>
						<div class="select ">
							<select id="schorg" name="schorg">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${schList}">
									<option value="${rs.code}">${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="organ_nm">학교/기관명</label>
						<div class="select wid150">
							<input type="text" id="organ_nm" name="organ_nm" class="wid100p" value="" />
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="doSearchOrgan();">검색</a></span>
					</div>
				</div>
			</form>
			<div class="tableWrap">
				<table id="organList" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="8%" />
						<col width="30%" />
						<col width="26%" />
						<col width="16%" />
						<col width="20%" />
					</colgroup>
					<thead>
						<tr>
							<th>No</th>
							<th>학교/기관명</th>
							<th>검사명</th>
							<th>연차</th>
							<th>학년도</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagings" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<form id="setData" name="setData" action="${param.returnUrl}">
	<input type="hidden" id="seq_lic" name="seq_lic" />
	<input type="hidden" id="org_id" name="org_id" />
	<input type="hidden" id="nowYear" name="nowYear" value="${map.nowYear}"/>
</form>

<script>
	$(document).ready(function(){
		doSearchOrgan();
	});

	function doSearchOrgan(currentPage) {
		$('#organList > tbody').children().remove();
		$('#currentPages').val(currentPage);
		var frmData = $("#searchOrgan").serialize();
		$.ajax({
			url : "/mng/search/searchOrgan",
			type : "post",
			data : frmData,
			success:function(data) {
				var json = jsonListSet(data);
				for(i=0; i<json.length; i++) {
					var schorgNm = json[i].schorg_nm.replace(/ /gi, "&nbsp;");
					$('#organList > tbody').append(
							"<tr>" +
							"   <td>" + json[i].num + "</td>" +
							"   <td><a href=# onclick=setSubmit('" + json[i].seq_lic + "','" + json[i].schorg_id + "')>" + schorgNm + "</a></td>" +
							"   <td>" + json[i].survey_typ_nm + "</td>" +
							"   <td>" + json[i].lic_max + "</td>" +
							"   <td>" + json[i].st_year_cd + "</td>" +
							"</tr>"
					)
				}
				doPaging();
				fnLayerResize('mngManageClassClass');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doPaging() {
		var frmData = $("#searchOrgan").serialize();
		$.ajax({
			url : "/mng/search/searchOrganCnt",
			type : "post",
			data : frmData,
			dataType: "html",
			success:function(data) {
				$('#pagings').html(data);
				fnLayerResize('mngManageClassClass');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function setSubmit(lic, org) {
		$("#seq_lic").val(lic);
		$("#org_id").val(org);
		document.setData.submit();
	}
</script>