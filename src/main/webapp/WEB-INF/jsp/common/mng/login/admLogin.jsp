<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<title>로그인 </title>
		<link charset="utf-8" href="/template/common/css/common.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/mng.css" type="text/css" rel="stylesheet" />
		<script src="/template/common/js/jquery-1.9.1.min.js"></script>
		<script src="/template/common/js/common.js"></script>
		<script src="/template/common/js/layerpopup.js"></script>
	</head>
	<body>
		<div id="wrap">
			<div id="admLoginContainer">
				<div id="admLoginContent">
					<h2 class="title">
						eduCAT 관리자 사이트에 오신것을 환영합니다.<br/>
						로그인을 하시면 다양한 정보와 서비스를 자유롭게 이용할 수 있습니다.
					</h2>
					<form name="loginForm" method="post" action="/adm/loginAction">
						<div id="loginForm">
							<div id="loginInputWrap">
								<div class="loginInputSection">
									<label for="user_id">아이디</label>
									<input type="text" name="user_id" id="user_id">
								</div>
								<div class="loginInputSection">
									<label for="password">비밀번호</label>
									<input type="password" name="password" id="password">
								</div>
								<div class="idSaveSection">
									<input type="checkbox" id="id_save" />
									<label for="id_save">아이디 저장</label>
								</div>
							</div>
							<div id="loginBtnWrap">
								<input type="submit" class="btnLogin csP" value="LOGIN">
							</div>
						</div>
					</form>
					<div class="fcont">
						<p>COPYRIGHTⓒ 2016 BY SQUARENET , ALL RIGHTS RESERVED. </p>
					</div>
				</div>
			</div>
		</div>

		<c:import url="/WEB-INF/jsp/common/mng/layout/layerMessage.jsp" charEncoding="utf-8" />
		<script>
			$(document).ready(function(){
				<c:if test="${not empty msg}">
					<c:if test="${msg eq 'D'}" >
						fnAlertMessage("아이디 또는 비밀번호가 다릅니다.<br/>(아이디/비밀번호를 입력하세요)");
					</c:if>
				</c:if>

				var saveAdmin = getCookie("saveAdmin");
				$("#user_id").val(saveAdmin);

				if($("#user_id").val() != ""){
					$("#id_save").attr("checked", true);
				}
				$("#id_save").change(function(){
					if($("#id_save").is(":checked")){
						var saveAdmin = $("#user_id").val();
						setCookie("saveAdmin", saveAdmin, 90);
					}else{ // ID 저장하기 체크 해제 시,
						deleteCookie("saveAdmin");
					}
				});
				$("#user_id").keyup(function(){
					if($("#id_save").is(":checked")){
						var saveAdmin = $("#user_id").val();
						setCookie("saveAdmin", saveAdmin, 90);
					}
				});
			});
		</script>
	</body>
</html>