<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="leftMenu">
	<dl class="leftMenu01">
		<dt <c:if test="${param.menu1Num eq '1'}">class="on"</c:if>>
			<a href="/mng/joins">
				<span>참여현황</span>
				<i></i>
			</a>
		</dt>
		<dd <c:if test="${param.menu1Num eq '1' and param.menu2Num eq '1'}">class="on"</c:if>>
			<a href="/mng/joins">
				<span>참여현황</span>
			</a>
		</dd>
	</dl>
	<dl class="leftMenu02">
		<dt <c:if test="${param.menu1Num eq '2'}">class="on"</c:if>>
			<c:if test="${empty map.seq_lic}" >
				<a href="/mng/manage/class<c:if test="${not empty schInfo}">?org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
			<c:if test="${not empty map.seq_lic}" >
				<a href="/mng/manage/class<c:if test="${not empty schInfo}">?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
				<span>학교/기관관리</span>
				<i></i>
			</a>
		</dt>
		<dd <c:if test="${param.menu1Num eq '2' and param.menu2Num eq '1'}">class="on"</c:if>>
			<c:if test="${empty map.seq_lic}" >
				<a href="/mng/manage/class<c:if test="${not empty schInfo}">?org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
			<c:if test="${not empty map.seq_lic}" >
				<a href="/mng/manage/class<c:if test="${not empty schInfo}">?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
				<span>학급관리</span>
			</a>
		</dd>
		<dd <c:if test="${param.menu1Num eq '2' and param.menu2Num eq '2'}">class="on"</c:if>>
			<c:if test="${empty map.seq_lic}" >
				<a href="/mng/manage/teacher<c:if test="${not empty schInfo}">?org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
			<c:if test="${not empty map.seq_lic}" >
				<a href="/mng/manage/teacher<c:if test="${not empty schInfo}">?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
				<span>선생님관리</span>
			</a>
		</dd>
		<dd <c:if test="${param.menu1Num eq '2' and param.menu2Num eq '3'}">class="on"</c:if>>
			<c:if test="${empty map.seq_lic}" >
				<a href="/mng/manage/plan<c:if test="${not empty schInfo}">?org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
			<c:if test="${not empty map.seq_lic}" >
				<a href="/mng/manage/plan<c:if test="${not empty schInfo}">?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
				<span>검사일정관리</span>
			</a>
		</dd>
		<dd <c:if test="${param.menu1Num eq '2' and param.menu2Num eq '4'}">class="on"</c:if>>
			<c:if test="${empty map.seq_lic}" >
				<a href="/mng/manage/member<c:if test="${not empty schInfo}">?org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
			<c:if test="${not empty map.seq_lic}" >
				<a href="/mng/manage/member<c:if test="${not empty schInfo}">?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}</c:if>">
			</c:if>
				<span>구성원관리</span>
			</a>
		</dd>
	</dl>
	<dl class="leftMenu03">
		<dt <c:if test="${param.menu1Num eq '3'}">class="on"</c:if>>
			<a href="/mng/licence/info">
				<span>라이선스관리</span>
				<i></i>
			</a>
		</dt>
		<dd <c:if test="${param.menu1Num eq '3' and param.menu2Num eq '1'}">class="on"</c:if>>
			<a href="/mng/licence/info">
				<span>라이선스정보관리</span>
			</a>
		</dd>
		<c:if test="${adm.auth_typ_cd ne '06'}">
			<dd <c:if test="${param.menu1Num eq '3' and param.menu2Num eq '2'}">class="on"</c:if>>
				<a href="/mng/licence/loan">
					<span>연장신청관리</span>
				</a>
			</dd>
		</c:if>
	</dl>
	<c:if test="${adm.auth_typ_cd eq '10'}">
		<dl class="leftMenu05">
			<dt <c:if test="${param.menu1Num eq '5'}">class="on"</c:if>>
				<a href="/mng/cash/receipt">
					<span>쿠폰회원관리</span>
					<i></i>
				</a>
			</dt>
			<dd <c:if test="${param.menu1Num eq '5' and param.menu2Num eq '1'}">class="on"</c:if>>
				<a href="/mng/coupon/joins">
					<span>쿠폰회원 참여현황</span>
				</a>
			</dd>
			<dd <c:if test="${param.menu1Num eq '5' and param.menu2Num eq '2'}">class="on"</c:if>>
				<a href="/mng/coupon/list">
					<span>쿠폰관리</span>
				</a>
			</dd>
		</dl>
		<dl class="leftMenu04">
			<dt <c:if test="${param.menu1Num eq '4'}">class="on"</c:if>>
				<a href="/mng/cash/receipt">
					<span>시스템관리</span>
					<i></i>
				</a>
			</dt>
			<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '1'}">class="on"</c:if>>
				<a href="/mng/cash/receipt">
					<span>현금영수증신청관리</span>
				</a>
			</dd>
			<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '2'}">class="on"</c:if>>
				<a href="/mng/semi/apply">
					<span>세미나신청관리</span>
				</a>
			</dd>
			<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '3'}">class="on"</c:if>>
				<a href="/mng/code/organ">
					<span>코드관리</span><i></i>
				</a>
				<dl>
					<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '3' and param.menu3Num eq '1'}">class="on"</c:if>>
						<a href="/mng/code/organ">
							<span>학교기관코드관리</span>
						</a>
					</dd>
					<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '3' and param.menu3Num eq '2'}">class="on"</c:if>>
						<a href="/mng/code/business">
							<span>영업사코드관리</span>
						</a>
					</dd>
				</dl>
			</dd>
			<dd <c:if test="${param.menu1Num eq '4' and param.menu2Num eq '4'}">class="on"</c:if>>
				<a href="/mng/account/list">
					<span>계정관리</span>
				</a>
			</dd>
		</dl>
	</c:if>
</div>