<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
		<title>eduCAT</title>
		<link charset="utf-8" href="/template/common/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />
		<%--<link href="http://code.jquery.com/ui/1.10.3/themes/redmond/jquery-ui.css" rel="stylesheet">--%>
		<link charset="utf-8" href="/template/common/css/common.css" type="text/css" rel="stylesheet" />
		<link charset="utf-8" href="/template/common/css/mng.css" type="text/css" rel="stylesheet" />
		<script src="/template/common/js/jquery-1.9.1.min.js"></script>
		<script src="/template/common/jquery-ui/jquery-ui.js"></script>
		<script src="/template/common/jquery-ui/jquery-ui.min.js"></script>
		<script src="/template/common/js/datepicker.js"></script>
		<script src="/template/common/js/common.js"></script>
		<script src="/template/common/js/layerpopup.js"></script>
	</head>
	<body>
		<!-- wrap Start -->
		<div id="wrap">
			<c:import url="/mng/message" charEncoding="utf-8" />

			<!-- head Start -->
			<div id="header">
				<div class="header">
					<h1 id="logo">
						<a href="/mng/joins"><img src="/template/common/images/logo.png" alt="eduCAT logo"></a>
					</h1>
					<div id="gnb">
						<c:if test="${not empty adm}" >
							<a href="/login">${adm.user_nm} 님</a>
							<a href="/adm/logOut">로그아웃</a>
						</c:if>
					</div>
					<div id="lnb">
						<h2 class="hdn">주메뉴</h2>
						<ul class="menuTypeSub">
							<li class="m1 <c:if test="${param.menu1Num eq '1'}">on</c:if>">
								<a href="/mng/joins" class="on">참여현황</a>
							</li>
							<li class="m2 <c:if test="${param.menu1Num eq '2'}">on</c:if>">
								<a href="/mng/manage/class">학교/기관관리</a>
							</li>
							<li class="m3 <c:if test="${param.menu1Num eq '3'}">on</c:if>">
								<a href="/mng/licence/info">라이선스관리</a>
							</li>
							<c:if test="${adm.auth_typ_cd eq '10'}">
								<li class="m5 <c:if test="${param.menu1Num eq '5'}">on</c:if>">
									<a href="/mng/coupon/joins" class="on">쿠폰회원관리</a>
								</li>
								<li class="m4 <c:if test="${param.menu1Num eq '4'}">on</c:if>">
									<a href="/mng/cash/receipt">시스템관리</a>
								</li>
							</c:if>
						</ul>
					</div>
				</div>
			</div>
			<!-- head End -->