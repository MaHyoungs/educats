<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="messageLayerWrap messagePopupClone">
	<div class="messageBg zi10"></div>
	<div class="messageLayer zi11">
		<div class="layerHeader">
			<span class="title">알림</span>
			<a href="#" class="messageClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
		</div>
		<div class="layerContainer">
			<div class="message"></div>
		</div>
		<div class="layerFooter">
			<span class="saBtn"><a href="#" class="btnLayerGreen messageClose">확인</a></span>
		</div>
	</div>
</div>