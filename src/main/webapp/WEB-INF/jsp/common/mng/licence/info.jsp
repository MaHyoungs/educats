<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="3" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="3" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngLicenceInfo">
		<h1 class="title">라이선스 정보관리</h1>
		<c:if test="${adm.auth_typ_cd ne '06'}">
			<form id="searchForm" name="searchForm" action="/mng/licence/info" method="post">
				<div class="searchWrap">
					<div class="section">
						<label for="searchTyp">검사명</label>
						<div class="select">
							<select id="searchTyp" name="searchTyp">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${surveyList}">
									<option value="${rs.code}" <c:if test="${rs.code eq map.searchTyp}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section">
						<label for="searchSDt">라이선스 기간</label>
						<div class="select">
							<input type="text" id="searchSDt" name="searchSDt" value="${map.searchSDt}" readonly="readonly" class="calendar" />
							<span>~</span>
							<input type="text" id="searchEDt" name="searchEDt" value="${map.searchEDt}" readonly="readonly" class="calendar" />
						</div>
					</div>
					<div class="section">
						<label for="searchName">학교/기관명</label>
						<div class="select wid180">
							<input type="text" id="searchName" name="searchName" value="${map.searchName}" class="wid100p" />
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" onclick="searchLic()" class="btnMngJoinsSearch">검색</a></span>
					</div>
				</div>
			</form>
		</c:if>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="6%" />
					<col width="26%" />
					<col width="24%" />
					<col width="20%" />
					<col width="24%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>학교/기관명</th>
						<th>검사명</th>
						<th>라이선스 연차</th>
						<th>현재 라이선스 기간</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${licList}" varStatus="sts">
					<tr>
						<td>${rs.num}</td>
						<td><a href="/mng/licence/infoDetail?seq_lic=${rs.seq_lic}">${rs.schorg_nm}</a></td>
						<td>${rs.survey_typ_nm}</td>
						<c:if test="${empty rs.end_dt}">
							<td>미등록</td>
							<td> - </td>
						</c:if>
						<c:if test="${not empty rs.end_dt}">
							<td>${rs.lic_max}</td>
							<td>${rs.start_dt}&nbsp;&nbsp;&nbsp; - &nbsp;&nbsp;&nbsp;${rs.end_dt}</td>
						</c:if>
					</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<script>
	function searchLic() {
		document.searchForm.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />