<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="3" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="3" />
		<c:param name="menu2Num" value="2" />
	</c:import>

	<div id="content" class="mngLicenceLoan">
		<h1 class="title">연장신청관리</h1>
		<div class="">
			<form id="searchForm" name="searchForm" action="/mng/licence/loan" method="post">
				<div class="searchWrap">
					<div class="searchBlock searchBlockLeft">
						<div class="section">
							<label for="searchTyp">검사명</label>
							<div class="select">
								<select id="searchTyp" name="searchTyp">
									<option value="all">전체</option>
									<c:forEach var="rs" items="${surveyList}">
										<option value="${rs.code}" <c:if test="${map.searchTyp eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="section">
							<label for="searchStat">상태</label>
							<div class="select">
								<select id="searchStat" name="searchStat">
									<option value="all">전체</option>
									<c:forEach var="rs" items="${confmList}">
										<option value="${rs.code}" <c:if test="${map.searchStat eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="section">
							<label for="searchName">학교/기관명</label>
							<div class="select wid180">
								<input type="text" id="searchName" name="searchName" value="${map.searchName}" class="wid100p" />
							</div>
						</div>
						<div class="section">
							<label for="searchSDt">연장신청일</label>
							<div class="select">
								<input type="text" id="searchSDt" name="searchSDt" readonly="readonly" class="calendar" value="${map.searchSDt}"/>
								<span>~</span>
								<input type="text" id="searchEDt" name="searchEDt" readonly="readonly" class="calendar" value="${map.searchEDt}"/>
							</div>
						</div>
					</div>
					<div class="searchBlock searchBlockRight">
						<div class="section">
							<span class="saBtn"><a href="javascript:void(0)" onclick="searchLic()" class="btnMngJoinsSearch">검색</a></span>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="tabSection">
			<span class="saBtn"><a href="javascript:void(0)" class="btnMngClassTabGreen layerpopupOpen" data-popup-class="mngLicenceLoanTrue">연장승인</a></span>
			<span class="saBtn"><a href="javascript:void(0)" class="btnMngClassTabGray layerpopupOpen" data-popup-class="mngLicenceLoanFalse">반려</a></span>
		</div>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="6%" />
					<col width="13%" />
					<col width="13%" />
					<col width="24%" />
					<col width="12%" />
					<col width="22%" />
					<col width="10%" />
				</colgroup>
				<thead>
				<tr>
					<th>
						<div class="select checkbox">
							<input type="checkbox" name="checkAll"  id="checkAll" value="" />
						</div>
					</th>
					<th>연장신청일</th>
					<th>학교명</th>
					<th>검사명</th>
					<th>라이선스 연차</th>
					<th>연장신청라이선스 기간</th>
					<th>상태</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${licLoanList}" varStatus="sts">
						<tr>
							<td>
								<div class="select checkbox">
									<c:if test="${rs.stat_cd eq '03'}" >
										<input type="checkbox" id="${rs.survey_typ_cd}" name="uncheckItem" value="${rs.seq_lic}" disabled="true"/>
									</c:if>
									<c:if test="${rs.stat_cd ne '03'}" >
										<input type="checkbox" id="${rs.survey_typ_cd}" name="checkItem" value="${rs.seq_lic}"/>
									</c:if>
								</div>
							</td>
							<td>${rs.relay_dt}</td>
							<td>${rs.schorg_nm}</td>
							<td>${rs.survey_typ_nm}</td>
							<td>${rs.lic_max}</td>
							<td>${rs.start_dt} ~ ${rs.end_dt}</td>
							<c:if test="${rs.stat_cd eq '03'}" >
								<td>승인</td>
							</c:if>
							<c:if test="${rs.stat_cd eq '02'}" >
								<td>반려</td>
							</c:if>
							<c:if test="${rs.stat_cd eq '01'}" >
								<td>대기</td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>
<form id="statCdUpdate" name="statCdUpdate" action="/mng/licence/licStatUpdate" method="post">
	<input type="hidden" id="valList" name="valList" value=""/>
	<input type="hidden" id="typList" name="typList" value=""/>
	<input type="hidden" id="loan_type" name="loan_type" value=""/>
</form>

<div class="layerBg mngLicenceLoanTrue zi2"></div>
<div class="layerPopup mngLicenceLoanTrue style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			라이선스 연장신청을 승인하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doLoan(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngLicenceLoanFalse zi2"></div>
<div class="layerPopup mngLicenceLoanFalse style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			라이선스 연장신청을 반려하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doReturn(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<script>
	$("#checkAll").click(function(){
		if($("#checkAll").prop("checked")){
			$("input:checkbox[name='checkItem']").prop("checked", true);
		}else{
			$("input:checkbox[name='checkItem']").prop("checked", false);
		}
	});

	function doLoan(e) {
		var _target = $(e).parents(".layerPopup").attr("data-popup-this");
		$(e).parents(".layerPopup." + _target).css({"display":"none"});
		$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});
		var valList = '';
		var typList = '';
		$("input:checkbox[name='checkItem']:checked").each(function() {
			typList += ',' + $(this).attr('id');
			valList += ',' + $(this).val();
		});
		if(valList == '') {
			fnAlertMessage("연장승인할 라이선스를 클릭하여 주십시요.");
			return false;
		}
		valList = valList.substr(1);
		typList = typList.substr(1);

		$("#valList").val(valList);
		$("#typList").val(typList);
		$("#loan_type").val("Y");

		document.statCdUpdate.submit();
	}

	function searchLic() {
		document.searchForm.submit();
	}

	function doReturn(e) {
		var _target = $(e).parents(".layerPopup").attr("data-popup-this");
		$(e).parents(".layerPopup." + _target).css({"display":"none"});
		$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});
		var valList = '';
		$("input:checkbox[name='checkItem']:checked").each(function() {
			valList += ',' + $(this).val();
		});
		if(valList == ''){
			fnAlertMessage("반려할 라이선스를 클릭하여 주십시요.");
			return false;
		}
		valList = valList.substr(1);

		$("#valList").val(valList);
		$("#loan_type").val("N");

		document.statCdUpdate.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />