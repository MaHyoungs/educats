<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="3" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="3" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngLicenceInfo">
		<h1 class="title">라이선스정보</h1>
		<div class="tabSection">
			<span class="saBtn"><a href="javascript:void(0)" onclick="licHistory('${licDetail.schorg_id}', '${licDetail.survey_typ_cd}', '${licDetail.lic_max}')" class="btnMngClassTabGray layerpopupOpen" data-popup-class="mngLicenceHistory">이력</a></span>
		</div>
		<div class="tableWrap mB30">
			<form id="updateLic" name="updateLic" action="/mng/licence/UpdateInfoDetail" method="post">
				<input type="hidden" name="seq_lic" value="${licDetail.seq_lic}" />
				<input type="hidden" name="stat_cd" value="${licDetail.stat_cd}" />
				<input type="hidden" name="schorg_id" value="${licDetail.schorg_id}" />
				<input type="hidden" name="survey_typ" value="${licDetail.survey_typ_cd}" />
				<input type="hidden" name="lic_max" value="${licDetail.lic_max}" />
				<table class="style02" summary="">
					<caption></caption>
					<colgroup>
						<col width="22%" />
						<col width="28%" />
						<col width="22%" />
						<col width="28%" />
					</colgroup>
					<tbody>
						<tr>
							<th>학교/기관명</th>
							<td>${licDetail.schorg_nm}</td>
							<th>검사명</th>
							<td>${licDetail.survey_typ_nm}</td>
						</tr>
						<tr>
							<th>현재 라이선스 연차</th>
							<td colspan="3" class="taL pL100">${licDetail.lic_max} 차</td>
						</tr>
						<tr>
							<th>현재 라이선스 학생수</th>
							<td colspan="3" class="taL pL20">
								<div class="section">
									<div class="select input">
										<c:if test="${adm.auth_typ_cd ne '06'}">
											<input type="text" id="std_num" name="std_num" value="${licDetail.std_num}" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" class="wid170" /> 명
										</c:if>
										<c:if test="${adm.auth_typ_cd eq '06'}">
											<c:if test="${not empty licDetail.std_num}">
												<span>${licDetail.std_num} 명</span>
											</c:if>
											<c:if test="${empty licDetail.std_num}">
												<span>미등록</span>
											</c:if>
										</c:if>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>현재 라이선스 기간</th>
							<td colspan="3" class="taL pL20">
								<div class="section">
									<div class="select">
										<c:if test="${adm.auth_typ_cd eq '10'}" >
											<input type="text" id="startS_dt" name="start_dt" value="${licDetail.start_dt}" class="calendar" />
											<span>~</span>
											<input type="text" id="endE_dt" name="end_dt" value="${licDetail.end_dt}" class="calendar" />
										</c:if>
										<c:if test="${adm.auth_typ_cd ne '10'}" >
											<input type="hidden" name="start_dt" value="${licDetail.start_dt}" />
											<input type="hidden" name="end_dt" value="${licDetail.end_dt}" />

											<c:if test="${not empty licDetail.start_dt}">
												<span>${licDetail.start_dt}&nbsp;&nbsp;~&nbsp;&nbsp;${licDetail.end_dt}</span>
											</c:if>
											<c:if test="${empty licDetail.start_dt}">
												<span>미등록</span>
											</c:if>
										</c:if>
										<c:if test="${not empty licDetail.remainDay}">
											<c:if test="${licDetail.remainDay gt 0}">
												<span class="info cl_ec7063">*남은기간 ${licDetail.remainDay}일</span>
											</c:if>
											<c:if test="${licDetail.remainDay le 0}">
												<span class="info cl_ec7063">*남은기간 0일</span>
											</c:if>
										</c:if>
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<th>다음 라이선스 신청</th>
							<td colspan="3" class="taL pL20">
								<c:if test="${licDetail.stat_cd eq '03'}" >
									<c:if test="${empty licLoanInfo}">
										<span class="saBtn"><a href="#" class="btnMngJoinsSearch layerpopupOpen" data-popup-class="mngLicenceExtensionCofirmNew">라이선스 연장신청</a></span>
									</c:if>
									<c:if test="${not empty licLoanInfo}">
										<c:if test="${licLoanInfo.stat_cd ne '03'}">
											<span class="saBtn"><a href="#" class="btnMngJoinsSearch layerpopupOpen" data-popup-class="mngLicenceExtensionCofirmOverlap">라이선스 연장신청</a></span>
										</c:if>
										&nbsp;&nbsp; ${licLoanInfo.std_num}명 / ${licLoanInfo.start_dt} ~ ${licLoanInfo.end_dt}
										<c:if test="${licLoanInfo.stat_cd eq '01'}">
											(신청중)
										</c:if>
										<c:if test="${licLoanInfo.stat_cd eq '02'}">
											(반려)
										</c:if>
										<c:if test="${licLoanInfo.stat_cd eq '03'}">
											(승인완료)
										</c:if>
									</c:if>
								</c:if>
								<c:if test="${licDetail.stat_cd ne '03'}" >
									<span class="cl_ec7063"> 다음 라이선스를 신청할 수 없습니다. </span>
								</c:if>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="btnArea">
			<c:if test="${adm.auth_typ_cd ne '06'}">
				<span class="saBtn"><a href="javascript:void(0)" onclick="updateChk()" class="btnLayerGreen layerpopupOpen">수정</a></span>
			</c:if>
			<span class="saBtn"><a href="/mng/licence/info" class="btnLayerGray">취소</a></span>
		</div>
	</div>
</div>

<div class="layerBg mngLicenceSave zi2"></div>
<div class="layerPopup mngLicenceSave style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			수정하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="updateLicence(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngLicenceExtensionCofirmNew zi2"></div>
<div class="layerPopup mngLicenceExtensionCofirmNew style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			라이선스 연장을 신청하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGreen layerpopupOpen layerClose" data-popup-class="mngLicenceExtensionApp">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngLicenceExtensionCofirmOverlap zi2"></div>
<div class="layerPopup mngLicenceExtensionCofirmOverlap style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			라이선스 연장 신청중입니다.<br/>취소 후 다시 신청하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="#" class="btnLayerGreen layerpopupOpen layerClose" data-popup-class="mngLicenceExtensionApp">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngLicenceExtensionApp zi4"></div>
<div class="layerPopup mngLicenceExtensionApp style01 zi5">
	<div class="layerHeader">
		<span class="title">라이선스 연장신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="applicationForm" name="applicationForm" action="/mng/licence/licenceApplicationAjax" method="post">
					<c:if test="${not empty licLoanInfo}">
						<input type="hidden" name="laonYn" value="Y" />
						<input type="hidden" name="seq_lic" value="${licLoanInfo.seq_lic}" />
						<input type="hidden" name="schorg_id" value="${licLoanInfo.schorg_id}" />
						<input type="hidden" name="survey_typ_cd" value="${licLoanInfo.survey_typ_cd}" />
						<input type="hidden" name="lic_max" value="${licLoanInfo.lic_max}" />
					</c:if>
					<c:if test="${empty licLoanInfo}">
						<input type="hidden" name="laonYn" value="N" />
						<input type="hidden" name="seq_lic" value="${licDetail.seq_lic}" />
						<input type="hidden" name="schorg_id" value="${licDetail.schorg_id}" />
						<input type="hidden" name="survey_typ_cd" value="${licDetail.survey_typ_cd}" />
						<input type="hidden" name="lic_max" value="${licDetail.lic_max + 1}" />
					</c:if>
					<table class="style02" summary="">
						<caption></caption>
						<colgroup>
							<col width="30%" />
							<col width="70%" />
						</colgroup>
						<tbody>
							<tr>
								<th>라이선스 연차</th>
								<c:if test="${not empty licLoanInfo}">
									<td class="taL pL20">${licLoanInfo.lic_max} 차</td>
								</c:if>
								<c:if test="${empty licLoanInfo}">
									<td class="taL pL20">${licDetail.lic_max + 1} 차</td>
								</c:if>
							</tr>
							<tr>
								<th>라이선스 학생수</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select input">
											<input type="text" id="loanStd" name="loanStd" value="${licLoanInfo.std_num}" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" class="wid170" /> 명
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>라이선스 기간</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select calendar">
											<c:if test="${not empty licLoanInfo}">
												<input type="text" id="loanStartDt" readonly="readonly" name="loanStartDt" value="${licLoanInfo.start_dt}" class="calendar"/>
												<span>~</span>
												<input type="text" id="loanEndDt" readonly="readonly" name="loanEndDt" value="${licLoanInfo.end_dt}" class="calendar"/>
											</c:if>
											<c:if test="${empty licLoanInfo}">
												<input type="text" id="loanStartDt" readonly="readonly" name="loanStartDt" class="calendar" />
												<span>~</span>
												<input type="text" id="loanEndDt" readonly="readonly" name="loanEndDt" class="calendar" />
											</c:if>
										</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn"><a href="javascript:void(0)" onclick="loanApp()" class="btnLayerGreen">신청</a></span>
			<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
		</div>
	</div>
</div>

<div class="layerBg mngLicenceHistory"></div>
<div class="layerPopup mngLicenceHistory style01">
	<div class="layerHeader">
		<span class="title">라이선스정보 변경 이력</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<table class="tMngClassTopInfo mB20" summary="">
					<caption></caption>
					<colgroup>
						<col width="18%" />
						<col width="32%" />
						<col width="18%" />
						<col width="32%" />
					</colgroup>
					<tbody>
						<tr>
							<th>학교/기관명</th>
							<td>${licDetail.schorg_nm}</td>
							<td class="sky">검사명</td>
							<td>${licDetail.survey_typ_nm}</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="tableWrap">
				<table id="licHistory" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="8%" />
						<col width="18%" />
						<col width="27%" />
						<col width="20%" />
						<col width="27%" />
					</colgroup>
					<thead>
						<tr>
							<th>No.</th>
							<th>연차</th>
							<th>라이선스 기간</th>
							<th>학생수</th>
							<th>수정 일시</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagings" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
			<a href="#" class="btnLayerGreen layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		<c:if test="${not empty licLoanInfo}">
			$("#startS_dt").datepicker({dateFormat: "yy-mm-dd", minDate: new Date(), maxDate: new Date('${licLoanInfo.limit_dt}'), changeYear: true, changeMonth: true});
			$("#endE_dt").datepicker({dateFormat: "yy-mm-dd", minDate: new Date(), maxDate: new Date('${licLoanInfo.limit_dt}'), changeYear: true, changeMonth: true});
		</c:if>
		<c:if test="${empty licLoanInfo}">
			$("#startS_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
			$("#endE_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true, minDate: new Date()});
		</c:if>
		$("#loanStartDt").datepicker({dateFormat: "yy-mm-dd", minDate: new Date('${licDetail.end_next_dt}'), changeYear: true, changeMonth: true});
		$("#loanEndDt").datepicker({dateFormat: "yy-mm-dd", minDate: new Date('${licDetail.end_next_dt}'), changeYear: true, changeMonth: true});
	});
	function licHistory(schorg, type, licmax, currentPage) {
		$.ajax({
			url    : "/mng/licence/licenceHistoryAjax",
			type   : "post",
			data   : {schorg_id : schorg, survey_typ_cd : type, lic_max:licmax, currentPage:currentPage},
			success: function(data){
				var json = jsonListSet(data);
				$('#licHistory > tbody').children().remove();
				for(var i=0; i<json.length; i++) {
					if(json[i].start_dt == null){
						$('#licHistory > tbody').append('' +
								'<tr>' +
								'   <td>' + json[i].num + '</td>' +
								'   <td>' + json[i].lic_max + ' 차</td>' +
								'   <td> - </td>' +
								'   <td> - </td>' +
								'   <td> - </td>' +
								'</tr>'
						)
					}else{
						$('#licHistory > tbody').append('' +
								'<tr>' +
								'   <td>' + json[i].num + '</td>' +
								'   <td>' + json[i].lic_max + ' 차</td>' +
								'   <td>' + json[i].start_dt + ' ~ ' + json[i].end_dt + '</td>' +
								'   <td>' + json[i].std_num + ' 명</td>' +
								'   <td>' + json[i].upt_dt + '</td>' +
								'</tr>'
						)
					}
				}
				doPaging(schorg, type, licmax, currentPage);
				fnLayerResize('mngManageClassClass');
			},
			error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});
	}

	function doPaging(schorg, type, licmax, currentPage) {
		$.ajax({
			url : "/mng/licence/licenceHistoryCntAjax",
			type : "post",
			data   : {schorg_id : schorg, survey_typ_cd : type, lic_max:licmax, currentPage:currentPage},
			dataType: "html",
			success:function(data) {
				$('#pagings').html(data);
				fnLayerResize('mngManageClassClass');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function updateChk() {
		if($('#std_num').val() == null || $('#std_num').val() == '') {
			fnAlertMessage('학생수를 입력하여주십시요.');
			$('#std_num').focus();
			return false;
		}
	<c:if test="${adm.auth_typ_cd eq '10'}">
		if($('#startS_dt').val() == null || $('#startS_dt').val() == '') {
			fnAlertMessage('시작일을 입력하세요.');
			return false;
		}
		if($('#endE_dt').val() == null || $('#endE_dt').val() == '') {
			fnAlertMessage('종료일을 입력하세요.');
			return false;
		}
		if($('#startS_dt').val() > $('#endE_dt').val()) {
			fnAlertMessage('시작일은 종료일과 같거나 과거이어야 합니다.');
			return false;
		}
	</c:if>
		fnPopupConfirmOpen('mngLicenceSave');
	}

	function updateLicence(e) {
		popupMessageDown('mngLicenceSave.style02.zi3', 'mngLicenceSave.zi2');

		var frm = document.updateLic;
		frm.submit();
	}

	function loanApp() {
		if($('#loanStd').val() == null || $('#loanStd').val() == '') {
			fnAlertMessage('학생수를 입력하여주십시요.');
			$('#loanStd').focus();
			return false;
		}
		if($('#loanStartDt').val() == null || $('#loanStartDt').val() == '') {
			fnAlertMessage('시작일을 입력하세요.');
			return false;
		}
		if($('#loanEndDt').val() == null || $('#loanEndDt').val() == '') {
			fnAlertMessage('종료일을 입력하세요.');
			return false;
		}
		if($('#loanStartDt').val() > $('#loanEndDt').val()) {
			fnAlertMessage('시작일은 종료일과 같거나 과거이어야 합니다.');
			return false;
		}

		var frmData = $('#applicationForm').serialize();

		$.ajax({
			url    : "/mng/licence/licenceApplicationAjax",
			type   : "post",
			data   : frmData,
			success: function(data){
				popupMessageDown('mngLicenceExtensionApp.style01.zi5', 'mngLicenceExtensionApp.zi4');
				fnAlertUrlMessage("라이선스 연장신청 되었습니다.", "/mng/licence/infoDetail?seq_lic=${licDetail.seq_lic}");
			},
			error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />