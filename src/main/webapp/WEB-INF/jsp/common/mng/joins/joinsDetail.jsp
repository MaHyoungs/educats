<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="1" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="1" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngJoins">
		<h1 class="title"><span>${organInfo.schorg_nm}</span> 참여현황</h1>
		<c:if test="${adm.auth_typ_cd_adm_grp eq 'Y'}">
			<a href="/mng/joins"><span class="prevList">이전목록</span></a>
		</c:if>
		<form id="searchForm" name="searchForm" action="/mng/joinsDetail" method="post">
			<input type="hidden" name="searchOrg" value="${map.searchOrg}"/>
			<input type="hidden" name="searchTyp" value="${map.searchTyp}"/>
			<div class="searchWrap">
				<div class="section">
					<label for="searchYear">학년도</label>
					<div class="select">
						<select id="searchYear" class="" name="searchYear">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${yearList}">
								<option value="${rs.code}" <c:if test="${map.searchYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchLic">라이선스연차</label>
					<div class="select">
						<select id="searchLic" class="" name="searchLic">
							<option value="all">전체</option>
							<option value="1" <c:if test="${map.searchLic eq '1'}">selected="selected"</c:if>>1</option>
							<option value="2" <c:if test="${map.searchLic eq '2'}">selected="selected"</c:if>>2</option>
							<option value="3" <c:if test="${map.searchLic eq '3'}">selected="selected"</c:if>>3</option>
							<option value="4" <c:if test="${map.searchLic eq '4'}">selected="selected"</c:if>>4</option>
							<option value="5" <c:if test="${map.searchLic eq '5'}">selected="selected"</c:if>>5</option>
							<option value="6" <c:if test="${map.searchLic eq '6'}">selected="selected"</c:if>>6</option>
							<option value="7" <c:if test="${map.searchLic eq '7'}">selected="selected"</c:if>>7</option>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchTme">검사회차</label>
					<div class="select">
						<select id="searchTme" class="" name="searchTme">
							<option value="all">전체</option>
							<c:if test="${map.searchTyp eq 'SV01'}">
								<c:forEach var="rs" items="${tmeList}" begin="0" end="1">
									<option value="${rs.code}" <c:if test="${map.searchTme eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</c:if>
							<c:if test="${map.searchTyp eq 'SV02'}">
								<c:forEach var="rs" items="${tmeList}">
									<option value="${rs.code}" <c:if test="${map.searchTme eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</c:if>
						</select>
					</div>
				</div>
				<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">
					<div class="section">
						<label for="searchGrade">학년</label>
						<div class="select">
							<select id="searchGrade" class="" name="searchGrade" onchange="doClass(this.value)">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${gradeist}">
									<option value="${rs.code}" <c:if test="${map.searchGrade eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</c:if>
				<div class="section">
					<label for="searchClass">
						<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">반</c:if>
						<c:if test="${organInfo.schorg_typ_cd eq 'ORG01'}">학급</c:if>
					</label>
					<div class="select <c:if test="${organInfo.schorg_typ_cd eq 'ORG01'}">wid180</c:if>">
						<select id="searchClass" class="wid100p" name="searchClass">
							<option value="all">전체</option>
							<c:if test="${organInfo.schorg_typ_cd eq 'ORG01'}">
								<c:forEach var="rs" items="${classList}">
									<option value="${rs.st_class}" <c:if test="${map.searchClass eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
								</c:forEach>
							</c:if>
							<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">
								<c:if test="${not empty map.searchGrade}">
									<c:forEach var="rs" items="${classList}">
										<option value="${rs.st_class}" <c:if test="${map.searchClass eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
									</c:forEach>
								</c:if>
							</c:if>
						</select>
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">
						<col width="12%" />
						<col width="10%" />
						<col width="13%" />
						<col width="8%" />
						<col width="10%" />
						<col width="13%" />
						<col width="10%" />
						<col width="13%" />
						<col width="11%" />
					</c:if>
					<c:if test="${organInfo.schorg_typ_cd eq 'ORG01'}">
						<col width="14%" />
						<col width="11%" />
						<col width="13%" />
						<col width="14%" />
						<col width="11%" />
						<col width="13%" />
						<col width="12%" />
						<col width="14%" />
					</c:if>
				</colgroup>
				<thead>
					<tr>
						<th>학년도</th>
						<c:if test="${organInfo.schorg_typ_cd eq 'ORG01'}">
							<th>학급</th>
						</c:if>
						<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">
							<th>학년</th>
							<th>반</th>
						</c:if>
						<th>라이선스연차</th>
						<th>검사회차</th>
						<th>총학생수</th>
						<th>검사참여</th>
						<th>검사미참여</th>
						<th>참여율</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${joinsDetail}">
						<tr>
							<td>${rs.st_year_cd}</td>
							<c:if test="${organInfo.schorg_typ_cd ne 'ORG01'}">
								<td>${fn:replace(rs.st_grade_cd, '0', '')}</td>
							</c:if>
							<td><a href="javascript:void(0)" onclick="moreDetail('${rs.schorg_id}','${rs.survey_typ_cd}','${rs.lic_max}','${rs.tme_cd}','${rs.st_year_cd}','${rs.seq_class}','${fn:replace(rs.st_grade_cd, '0', '')}','${rs.st_class}','${organInfo.schorg_typ_cd}')" class="layerpopupOpen" data-popup-class="mngJoins">${rs.st_class}</a></td>
							<td>${rs.lic_max}</td>
							<td>${fn:replace(rs.tme_cd, '0', '')}</td>
							<td>${rs.totalCnt}</td>
							<td>${rs.joinCnt}</td>
							<td>${rs.noJoinCnt}</td>
							<td>${rs.joinPer}%</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<c:import url="/mng/classJoin" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<script>
	function doSearch() {
		document.searchForm.submit();
	}

	function doClass(val) {
		$.ajax({
			url    : "/mng/manage/classListAjaxss",
			type   : "post",
			data   : {nowYear : '${map.searchYear}', grade : val, org_id : '${map.searchOrg}'},
			success: function(data){
				var json = jsonListSet(data);

				$('#searchClass option').remove();
				$('#searchClass').append($('<option value="all">전체</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
					$('#searchClass').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />