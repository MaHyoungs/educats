<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="1" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="1" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngJoins">
		<h1 class="title">참여현황</h1>
		<form id="searchForm" name="searchForm" action="/mng/joins" method="post">
			<div class="searchWrap">
				<div class="section">
					<label for="join_typ">검사명</label>
					<div class="select">
						<select id="join_typ" class="" name="join_typ">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${surveyList}">
								<option value="${rs.code}" <c:if test="${map.join_typ eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="join_year">학년도</label>
					<div class="select">
						<select id="join_year" class="" name="join_year">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${yearList}">
								<option value="${rs.code}" <c:if test="${map.join_year eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="join_lic">라이선스연차</label>
					<div class="select">
						<select id="join_lic" class="" name="join_lic">
							<%--<option value="all">전체</option>
							<c:forEach var="rs" items="${yearList}">
								<option value="${rs.code}" <c:if test="${map.join_lic eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>--%>
							<option value="all">전체</option>
							<option value="1" <c:if test="${map.join_lic eq '1'}">selected="selected"</c:if>>1</option>
							<option value="2" <c:if test="${map.join_lic eq '2'}">selected="selected"</c:if>>2</option>
							<option value="3" <c:if test="${map.join_lic eq '3'}">selected="selected"</c:if>>3</option>
							<option value="4" <c:if test="${map.join_lic eq '4'}">selected="selected"</c:if>>4</option>
							<option value="5" <c:if test="${map.join_lic eq '5'}">selected="selected"</c:if>>5</option>
							<option value="6" <c:if test="${map.join_lic eq '6'}">selected="selected"</c:if>>6</option>
							<option value="7" <c:if test="${map.join_lic eq '7'}">selected="selected"</c:if>>7</option>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="join_tme">검사회차</label>
					<div class="select">
						<select id="join_tme" class="" name="join_tme">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${tmeList}">
								<option value="${rs.code}" <c:if test="${map.join_tme eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="join_organ">학교/기관명</label>
					<div class="select wid180">
						<input type="text" id="join_organ" name="join_organ" class="wid100p" value="${map.join_organ}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="searchJoin()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="6%" />
					<col width="17%" />
					<col width="17%" />
					<col width="10%" />
					<col width="9%" />
					<col width="9%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>학교/기관명</th>
						<th>검사명</th>
						<th>학년도</th>
						<th>라이선스연차</th>
						<th>검사회차</th>
						<th>총학생수</th>
						<th>검사참여</th>
						<th>검사미참여</th>
						<th>참여율</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${joinsList}">
						<tr>
							<th>${rs.num}</th>
							<td><a href="/mng/joinsDetail?searchOrg=${rs.schorg_id}&searchTyp=${rs.survey_typ_cd}&searchTme=${rs.tme_cd}&searchYear=${rs.st_year_cd}&searchLic=${rs.lic_max}">${rs.schorg_nm}</a></td>
							<td>${rs.survey_typ_nm}</td>
							<td>${rs.st_year_cd}</td>
							<td>${rs.lic_max}</td>
							<td>${fn:replace(rs.tme_cd, '0', '')}차</td>
							<td>${rs.totalCnt}</td>
							<td>${rs.joinCnt}</td>
							<td>${rs.noJoinCnt}</td>
							<td>${rs.joinPer}%</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:if test="${msg eq 'E'}" >
				fnAlertMessage("잘못된 경로로 접속하셨습니다.");
			</c:if>
		</c:if>
	});

	function searchJoin() {
		document.searchForm.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />