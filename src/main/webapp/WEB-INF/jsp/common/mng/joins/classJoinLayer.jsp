<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="layerBg mngJoins"></div>
<div class="layerPopup mngJoins style01">
	<div class="layerHeader">
		<span class="title">참여현황</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<h1 id="titles" class="title"></h1>
		<div class="conContainer">
			<table id="joinInfo" class="tMngTopInfo mB20" summary="">
				<caption></caption>
				<colgroup>
					<col width="100" />
					<col width="120" />
					<col width="100" />
					<col width="120" />
					<col width="130" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th rowspan="2">검사명</th>
						<c:if test="${map.searchTyp eq 'SV01'}">
							<td rowspan="2">진로탐색검사</td>
						</c:if>
						<c:if test="${map.searchTyp eq 'SV02'}">
							<td rowspan="2">자기주도학습역량검사</td>
						</c:if>
						<td rowspan="2" id="total" class="sky">총학생수</td>
						<td>25명</td>
						<td class="sky">1회차 검사참여</td>
						<td>25명(100%)</td>
					</tr>
					<tr>
						<td>25명</td>
						<td class="blue">2회차 검사참여</td>
						<td>25명(100%)</td>
					</tr>
				</tbody>
			</table>
			<form id="search2Form" name="seach2Form" action="/mng/classInfo" method="post" onsubmit="return false;">
				<input type="hidden" id="search2Org" name="searchOrg" value="${map.searchOrg}"/>
				<input type="hidden" id="search2Typ" name="searchTyp" value="${map.searchTyp}"/>
				<input type="hidden" id="search2SeqClass" name="searchSeqClass" value="${map.searchSeqClass}"/>
				<input type="hidden" id="search2Group" name="searchGroup" value="${map.searchGroup}"/>
				<input type="hidden" id="search2g" name="searchg" value="${map.g}"/>
				<input type="hidden" id="search2cc" name="searchcc" value="${map.c}"/>
				<div class="searchWrap mB20">
					<div class="section2">
						<label for="searchYears">학년도</label>
						<div class="select ">
							<select id="searchYears" class="" name="searchYear">
								<c:forEach var="rs" items="${searchYeaList}">
									<option value="${rs.code}" <c:if test="${map.searchYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="section2">
						<label for="searchLics">연차</label>
						<div class="select ">
							<select id="searchLics" class="" name="searchLic">
								<option value="all">전체</option>
								<option value="1" <c:if test="${map.searchLic eq '1'}">selected="selected"</c:if>>1</option>
								<option value="2" <c:if test="${map.searchLic eq '2'}">selected="selected"</c:if>>2</option>
								<option value="3" <c:if test="${map.searchLic eq '3'}">selected="selected"</c:if>>3</option>
								<option value="4" <c:if test="${map.searchLic eq '4'}">selected="selected"</c:if>>4</option>
								<option value="5" <c:if test="${map.searchLic eq '5'}">selected="selected"</c:if>>5</option>
								<option value="6" <c:if test="${map.searchLic eq '6'}">selected="selected"</c:if>>6</option>
								<option value="7" <c:if test="${map.searchLic eq '7'}">selected="selected"</c:if>>7</option>
							</select>
						</div>
					</div>
					<div class="section2">
						<label for="searchTmes">회차</label>
						<div class="select ">
							<select id="searchTmes" class="" name="searchTme">
								<option value="all">전체</option>
								<c:if test="${map.searchTyp eq 'SV01'}">
									<c:forEach var="rs" items="${searchtmeList}" begin="0" end="1">
										<option value="${rs.code}" <c:if test="${map.searchTme eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</c:if>
								<c:if test="${map.searchTyp eq 'SV02'}">
									<c:forEach var="rs" items="${searchtmeList}">
										<option value="${rs.code}" <c:if test="${map.searchTme eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
					</div>
					<div class="section2">
						<label for="searchStateYn">검사여부</label>
						<div class="select ">
							<select id="searchStateYn" class="" name="searchStateYn">
								<option value="all">전체</option>
								<option value="Y" <c:if test="${map.searchStateYn eq 'Y'}">selected="selected"</c:if>>Y</option>
								<option value="N" <c:if test="${map.searchStateYn eq 'N'}">selected="selected"</c:if>>N</option>
							</select>
						</div>
					</div>
					<div class="section2">
						<label for="searchNames">이름</label>
						<div class="select wid85">
							<input type="text" id="searchNames" name="searchName" class="wid85" value="${map.searchName}" />
						</div>
					</div>
					<div class="section2">
						<span class="saBtn"><a href="javascript:void(0)" class="btnMngJoinsSearch" onclick="doSearchs()">검색</a></span>
					</div>
				</div>
			</form>
			<div class="tableWrap">
				<table id="joinList" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="8%" />
						<col width="12%" />
						<col width="24%" />
						<col width="9%" />
						<col width="9%" />
						<col width="12%" />
						<col width="12%" />
						<col width="14%" />
					</colgroup>
					<thead>
						<tr>
							<th>번호</th>
							<th>이름</th>
							<th>이메일</th>
							<th>연차</th>
							<th>검사회차</th>
							<th>진행여부</th>
							<th>검사일자</th>
							<th>검사결과</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagings2" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="#" class="btnSurveyLayerStart layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	// 레이어
	function moreDetail(org, typ, max, tme, yea, cla, g, c, p, currentPage) {
		$('#joinInfo > tbody').children().remove();
		$('#searchg').val(g);
		$('#searchcc').val(c);
		$('#search2SeqClass').val(cla);
		$.ajax({
			url    : "/mng/classJoinInfo",
			type   : "post",
			data   : {searchOrg:org, searchTyp:typ, searchLic:max, searchTme:tme, searchYear:yea, searchSeqClass:cla, searchGroup:p},
			success: function(data){
				var json = jsonListSet(data);
				detailList(org, typ, max, tme, yea, cla, g, c, p, currentPage, 'all', null);
				if(p != 'ORG01') {
					$('#titles').text('${organInfo.schorg_nm}' + " " + g + "학년 " + c + "반");
				} else if(p == 'ORG01') {
					$('#titles').text('${organInfo.schorg_nm}' + " " + c + "학급");
				}
				var surveynm = '';
				<c:if test="${map.searchTyp eq 'SV01'}">
					surveynm = '진로탐색검사';
				</c:if>
				<c:if test="${map.searchTyp eq 'SV02'}">
					surveynm = '자기주도학습역량검사';
				</c:if>
				var infos = '';
				for(i=0; i<json.length; i++) {
					if(i==0) {
						infos = '<tr>' +
								'<th rowspan="' + json.length + '">검사명</th>' +
								'<td rowspan="' + json.length + '">' + surveynm  + '</th>' +
								'<td rowspan="' + json.length + '" id="total" class="sky">총학생수</td>' +
								'<td>' + json[i].totalcnt + '명</td>' +
								'<td class="sky">' + json[i].tme_num + '회차 검사참여</td>' +
								'<td>' + json[i].joincnt + '명(' + json[i].joinper + '%)</td>' +
								'</tr>';
					} else {
						infos += '<tr>' +
								 '<td>' + json[i].totalcnt + '명</td>' +
								 '<td class="blue">' + json[i].tme_num + '회차 검사참여</td>' +
								 '<td>' + json[i].joincnt + '명(' + json[i].joinper + '%)</td>' +
								 '</tr>';

					}

				}
				$('#joinInfo > tbody').append(infos);
				fnLayerResize('mngJoins');
			}, error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});
	}

	function detailList(org, typ, max, tme, yea, cla, g, c, p, currentPage, stats, names) {
		$('#joinList > tbody').children().remove();
		$.ajax({
			url    : "/mng/classJoinList",
			type   : "post",
			data   : {searchOrg:org, searchTyp:typ, searchLic:max, searchTme:tme, searchYear:yea, searchSeqClass:cla, searchGroup:p, currentPage:currentPage, searchStateYn:stats, searchName:names},
			success: function(data){
				var json2 = jsonListSet(data);
				var lists = '';
				var results = '';
				var date = '';
				var num = '';
				for(j=0; j<json2.length; j++) {
					if(json2[j].surveys == 'Y') {
						results = '<span class="saBtn"><a href="javascript:void(0)" onclick=popupwindow("' + json2[j].seq_tme + '","' + json2[j].user_id + '","result","1050","900") class="btnMngJoinsResult">검사결과</a></span>';
						date = json2[j].reg_dt;
					} else {
						results = '<span class="saBtn"></span></td>'
						date = '-';
					}

					if(p == 'ORG01') {
						num = json2[j].num;
					} else {
						num = json2[j].st_number;
					}

					lists += '<tr>' +
								'<td>' + num + '</td>' +
								'<td>' + json2[j].user_nm + '</td>' +
								'<td>' + json2[j].user_id + '</td>' +
								'<td>' + json2[j].lic_max + '</td>' +
								'<td>' + json2[j].tme_num + '차</td>' +
								'<td>' + json2[j].surveys + '</td>' +
								'<td>' + date + '</td>' +
								'<td>' + results + '</td>' +
							'</tr>';
				}
				$('#joinList > tbody').append(lists);
				doPaging2(org, typ, max, tme, yea, cla, g, c, p, currentPage, stats, names);
				fnLayerResize('mngJoins');
			}, error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});
	}

	function doPaging2(org, typ, max, tme, yea, cla, g, cc, p, currentPage, stats, names) {
		$.ajax({
			url : "/mng/classJoinListCnt",
			type : "post",
			data : {searchOrg:org, searchTyp:typ, searchLic:max, searchTme:tme,searchYear:yea,searchSeqClass:cla, g:g, c:cc, searchGroup:p, currentPage:currentPage, searchStateYn:stats, searchName:names},
			dataType: "html",
			success:function(data) {
				$('#pagings2').html(data);
				fnLayerResize('mngJoins');
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doSearchs() {
		var org2 = $('#search2Org').val();
		var typ2 = $('#search2Typ').val();
		var max2 = $('#searchLics').val();
		var tme2 = $('#searchTmes').val();
		var yea2 = $('#searchYears').val();
		var seq2 = $('#search2SeqClass').val();
		var grp2 = $('#search2Group').val();
		var sta2 = $('#searchStateYn').val();
		var nam2 = $('#searchNames').val();
		var gg = $('#searchg').val();
		var ccc = $('#searchcc').val();
		detailList(org2, typ2, max2, tme2, yea2, seq2, gg, ccc, grp2, '1', sta2, nam2);
	}

	function popupwindow(tme, id, title, w, h) {
//		var go = "/mng/surveyResult?seq_tme=" + tme + "&who=" + id + "&adms=Y";
		var go = "/mng/aulandResult?seq_tme=" + tme + "&who=" + id + "&adms=Y";
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		return window.open(go, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	}
</script>