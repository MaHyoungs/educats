<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="4" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="4" />
		<c:param name="menu2Num" value="2" />
	</c:import>

	<div id="content" class="mngSemiInfo">
		<h1 class="title">세미나신청관리</h1>
		<form id="searchForm" name="searchForm" action="/mng/semi/apply" method="post">
			<div class="searchWrap">
				<div class="section">
					<label for="proS_dt">접수일</label>
					<div class="select flexN">
						<input type="text" id="proS_dt" name="proS_dt" readonly="readonly" value="${proS_dt}" class="calendar" />
						<span>~</span>
						<input type="text" id="proE_dt" name="proE_dt" readonly="readonly" value="${proE_dt}" class="calendar" />
					</div>
				</div>
				<div class="section">
					<label for="proOrgan">학교/기관명</label>
					<div class="select wid180">
						<input type="text" id="proOrgan" name="proOrgan" class="wid100p" value="${proOrgan}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="searchPro()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="7%" />
					<col width="21%" />
					<col width="12%" />
					<col width="17%" />
					<col width="15%" />
					<col width="12%" />
					<col width="16%" />
				</colgroup>
				<thead>
				<tr>
					<th>No</th>
					<th>접수일</th>
					<th>학교/기관명</th>
					<th>신청일</th>
					<th>희망시간</th>
					<th>담당자</th>
					<th>연락처</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${semiList}" >
						<tr>
							<td>${rs.num}</td>
							<td>${rs.reg_dts}</td>
							<td><a href="javascript:void(0)" class="layerpopupOpen" onclick="semiDetail('${rs.seq_semina}')" data-popup-class="mngSemiInfoDetail">${rs.schorg_nm}</a></td>
							<td>${rs.start_dt}</td>
							<td>${rs.ampm_nm} ${rs.start_tmes}시</td>
							<td>${rs.user_nm}</td>
							<td>${rs.user_cttpc}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<div class="layerBg mngSemiInfoDetail"></div>
<div class="layerPopup mngSemiInfoDetail style01">
	<div class="layerHeader">
		<span class="title">세미나 신청 상세정보</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<table class="style02" summary="">
					<caption></caption>
					<colgroup>
						<col width="22%" />
						<col width="28%" />
						<col width="22%" />
						<col width="28%" />
					</colgroup>
					<tbody>
						<tr>
							<th>접수일시</th>
							<td colspan="3" class="taL pL20" id="reg_dt"></td>
						</tr>
						<tr>
							<th>신청일자</th>
							<td class="taL pL20" id="start_dt"></td>
							<th>희망시간</th>
							<td class="taL pL20" id="start_tme"></td>
						</tr>
						<tr>
							<th>학교명</th>
							<td colspan="3" class="taL pL20" id="schorg_nm"></td>
						</tr>
						<tr>
							<th>담당자명</th>
							<td colspan="3" class="taL pL20" id="user_nm"></td>
						</tr>
						<tr>
							<th>연락처</th>
							<td colspan="3" class="taL pL20" id="user_cttpc"></td>
						</tr>
						<tr>
							<th>내용</th>
							<td colspan="3" class="taL pL20" id="description"><pre id="desData"></pre></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="deleteSet()" class="btnLayerGray layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#proS_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true});
		$("#proE_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true});
	});

	function searchPro() {
		if($("#proS_dt").val() > $("#proE_dt").val()) {
			fnAlertMessage("시작일은 종료일과 같거나 과거이어야 합니다.");
			return false;
		}
		document.searchForm.submit();
	}

	function semiDetail(seq) {

		$.ajax({
			url : "/mng/semi/infoDetail",
			type : "post",
			data : {seq_semina : seq},
			success:function(data) {
				var json = jsonSet(data);
				$('#reg_dt').text(json[0].reg_dt);
				$('#start_dt').text(json[0].start_dt);
				$('#start_tme').text(json[0].ampm_nm + " " + json[0].start_tmes + "시");
				$('#schorg_nm').text(json[0].schorg_nm);
				$('#user_nm').text(json[0].user_nm);
				$('#user_cttpc').text(json[0].user_cttpc);
				$('#desData').html(json[0].description);
				fnLayerResize('mngSemiInfoDetail');
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}

	// 값 초기화
	function deleteSet() {
		$('#reg_dt').text("");
		$('#start_dt').text("");
		$('#start_tme').text("");
		$('#schorg_nm').text("");
		$('#user_nm').text("");
		$('#user_cttpc').text("");
		$('#desData').html("");
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />