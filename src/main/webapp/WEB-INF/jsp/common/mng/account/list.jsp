<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="4" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="4" />
		<c:param name="menu2Num" value="4" />
	</c:import>

	<div id="content" class="mngSemiInfo">
		<h1 class="title">계정관리</h1>
		<form id="searchForm" name="searchForm" action="/mng/account/list" method="post">
			<div class="searchWrap">
				<div class="section">
					<label for="searchAuth">권한</label>
					<div class="select">
						<select id="searchAuth" name="searchAuth">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${authList}">
								<option value="${rs.code}" <c:if test="${map.searchAuth eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchAuth2">소속</label>
					<div class="select">
						<select id="searchAuth2" name="searchAuth2">
							<option value="all">전체</option>
							<c:if test="${map.searchAuth eq '08' or map.searchAuth eq '10'}">
								<c:forEach var="rss" items="${organList}">
									<option value="${rss.salescp_id}" <c:if test="${map.searchAuth2 eq rss.salescp_id}">selected="selected"</c:if>>${rss.company_nm}</option>
								</c:forEach>
							</c:if>
							<c:if test="${map.searchAuth eq '06'}">
								<c:forEach var="rss" items="${organList}">
									<option value="${rss.schorg_id}" <c:if test="${map.searchAuth2 eq rss.schorg_id}">selected="selected"</c:if>>${rss.schorg_nm}</option>
								</c:forEach>
							</c:if>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchId">ID</label>
					<div class="select wid100 mR38">
						<input type="text" id="searchId" name="searchId" class="wid100" value="${map.searchId}"/>
					</div>
				</div>
				<div class="section">
					<label for="searchName">이름</label>
					<div class="select wid100 mR38">
						<input type="text" id="searchName" name="searchName" class="wid100" value="${map.searchName}"/>
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="searchUsr()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tabSection">
			<span class="saBtn"><a href="#" class="btnMngClassTabGray layerpopupOpen" data-popup-class="mngAccountListInsert">등록</a></span>
		</div>
		<div class="tableWrap">
			<table class="style05" summary="">
				<caption></caption>
				<colgroup>
					<col width="10%" />
					<col width="13%" />
					<col width="20%" />
					<col width="17%" />
					<col width="21%" />
					<col width="19%" />
				</colgroup>
				<thead>
					<tr>
						<th>No.</th>
						<th>이름</th>
						<th>이메일</th>
						<th>연락처</th>
						<th>소속</th>
						<th>권한</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${accountList}" varStatus="sts">
						<tr>
							<td>${rs.num}</td>
							<td><a href="javascript:void(0)" onclick="fnPopupAccountDetail($(this).attr('data-popup-class'), '${rs.seq_adm}');" data-popup-class="mngAccountListDetail">${rs.user_nm}</a></td>
							<td>${rs.user_id}</td>
							<td>${rs.hp_num}</td>
							<c:if test="${not empty rs.company_nm}">
								<td>${rs.company_nm}</td>
							</c:if>
							<c:if test="${empty rs.company_nm}">
								<td>${rs.schorg_nm}</td>
							</c:if>
							<td>${rs.auth_typ_nm}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<%--계정 상세--%>
<div class="layerBg mngAccountListDetail"></div>
<div class="layerPopup mngAccountListDetail style01">
	<div class="layerHeader">
		<span class="title">계정 상세</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="userUpdate" name="userUpdate" method="post">
					<input type="hidden" id="user_seq" name="user_seq" />
					<table class="style06" summary="">
						<caption></caption>
						<colgroup>
							<col width="31%" />
							<col width="37%" />
							<col width="32%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_tchId">ID(이메일)</th>
								<td>
									<ul>
										<li name="user_id" class="wid220"></li>
									</ul>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_tchPass">현재 비밀번호</th>
								<td><input type="password" name="password" class="wid208" readonly placeholder="현재 비밀번호" /></td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_tchRepass">새 비밀번호</th>
								<td><input type="password" id="newPassword" name="newPassword" class="wid208" placeholder="비밀번호 변경" /></td>
								<td class="cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상.</td>
							</tr>
							<tr>
								<th class="icon_tchRepassCheck">새 비밀번호 확인</th>
								<td><input type="password" id="newPasswordChk" name="newPasswordChk" class="wid208" placeholder="비밀번호 확인" /></td>
								<td><span id="errorTxt2" class="cl_ec7063"></span></td>
							</tr>
							<tr>
								<th class="icon_tchName">이름</th>
								<td><input type="text" id="usrNm" name="user_nm" class="wid128" /></td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_auth">권한</th>
								<td>
									<select id="authSelects" class="wid220 hei36" name="auth_typ_cd">
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_hp">휴대폰</th>
								<td><input type="text" id="usrHp" name="hp_num" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" maxlength=11 class="wid208" /></td>
								<td class="cl_ec7063">* 숫자만 입력가능 합니다.</td>
							</tr>
							<tr>
								<th class="icon_tchSchool">소속</th>
								<td>
									<div class="select">
										<select id="organSelects" class="wid220 hei36" name="organ_id">
										</select>
									</div>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_comment">설명</th>
								<td colspan="2" rowspan="2"><textarea name="description" class="wid100p hei100p"></textarea></td>
							</tr>
							<tr>
								<th></th>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn"><a href="#" class="btnLayerGreen layerpopupOpen" data-popup-class="mngAccountListDetailDelete">삭제</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="updateChks()" class="btnLayerGreen">수정</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="deleteSets()" class="btnLayerGray layerClose">취소</a></span>
		</div>
	</div>
</div>

<%--계정 등록--%>
<div class="layerBg mngAccountListInsert"></div>
<div class="layerPopup mngAccountListInsert style01">
	<div class="layerHeader">
		<span class="title">계정 등록</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="userInsert" name="userInsert" action="/mng/account/insert" method="post">
					<table class="style06" summary="">
						<caption></caption>
						<colgroup>
							<col width="31%" />
							<col width="37%" />
							<col width="32%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_tchId">ID(이메일)</th>
								<td><input type="text" id="accountUserId" name="user_id" class="wid208" /></td>
								<td><span id="errorEmail" class="cl_ec7063"></span></td>
								<td><input type="hidden" name="seq_adm"/></td>
							</tr>
							<tr>
								<th class="icon_tchRepass">비밀번호</th>
								<td><input type="password" id="accountPassword" name="password" class="wid208" placeholder="비밀번호 변경" /></td>
								<td class="cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상.</td>
							</tr>
							<tr>
								<th class="icon_tchRepassCheck">비밀번호 확인</th>
								<td><input type="password" id="accountPasswordChk" name="passwordChk" class="wid208" placeholder="비밀번호 확인" /></td>
								<td><span id="errorTxt" class="cl_ec7063"></span></td>
							</tr>
							<tr>
								<th class="icon_tchName">이름</th>
								<td><input type="text" id="accountUserNm" name="user_nm" class="wid128" /></td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_auth">권한</th>
								<td>
									<select id="authSelect" class="wid220 hei36" name="auth_typ_cd">
											<option value="N">선택</option>
										<c:forEach var="rs" items="${authList}">
											<option value="${rs.code}">${rs.code_nm}</option>
										</c:forEach>
									</select>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_hp">휴대폰</th>
								<td><input type="text" id="accountHp" name="hp_num" class="wid208" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" maxlength=11 /></td>
							</tr>
							<tr>
								<th class="icon_tchSchool">소속</th>
								<td>
									<div class="select">
										<select id="organSelect" class="wid220 hei36" name="organ_id">
											<option value="N">선택</option>
										</select>
									</div>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_comment">설명</th>
								<td colspan="2" rowspan="2"><textarea id="accountDescription" name="description" class="wid100p hei100p"></textarea></td>
							</tr>
							<tr>
								<th></th>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn"><a href="javascript:void(0)" onclick="insertChk()" class="btnLayerGreen">등록</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="deleteSet()" class="btnLayerGray layerClose">취소</a></span>
		</div>
	</div>
</div>

<div class="layerBg mngAccountListInsertSubmit zi4"></div>
<div class="layerPopup mngAccountListInsertSubmit style02 zi5">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			등록 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doInsert(this);" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngAccountListDetailSubmit zi4"></div>
<div class="layerPopup mngAccountListDetailSubmit style02 zi5">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			수정 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdate()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngAccountListDetailDelete zi4"></div>
<div class="layerPopup mngAccountListDetailDelete style02 zi5">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			삭제 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doDelete()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<script>
	$(function(){

		<c:if test="${not empty map.msg}">
			<c:if test="${map.msg eq 'N'}">
				fnAlertMessage("동일한 아이디가 있습니다.");
			</c:if>
			<c:if test="${map.msg eq 'D'}">
				fnAlertMessage("아이디를 삭제하였습니다.");
			</c:if>
			<c:if test="${map.msg eq 'U'}">
				fnAlertMessage("계정을 수정하였습니다.");
			</c:if>
		</c:if>
		$('#authSelect').change(function(){
			$.ajax({
				url    : "/mng/values/authSelectAjax",
				type   : "post",
				data   : {auth_typ_cd : $('#authSelect').val()},
				success: function(data){
					var json = jsonListSet(data);
					$('#organSelect option').remove();
					var opt = $('<option value="N">선택</option>');
					$('#organSelect').append(opt);
					var auth = $('#authSelect').val();
					if(auth == '08' || auth == '10'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].salescp_id + '">' + json[i].company_nm + '</option>');
							$('#organSelect').append(opt);
						}
					}
					if(auth == '06'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].schorg_id + '">' + json[i].schorg_nm + '</option>');
							$('#organSelect').append(opt);
						}
					}
				},
				error:function(e) {
					fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				}
			});
		});

		$('#authSelects').change(function(){
			$.ajax({
				url    : "/mng/values/authSelectAjax",
				type   : "post",
				data   : {auth_typ_cd : $('#authSelects').val()},
				success: function(data){
					var json = jsonListSet(data);
					$('#organSelects option').remove();
					var opt = $('<option value="">선택</option>');
					$('#organSelects').append(opt);
					var auth = $('#authSelects').val();
					if(auth == '08' || auth == '10'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].salescp_id + '">' + json[i].company_nm + '</option>');
							$('#organSelects').append(opt);
						}
					}
					if(auth == '06'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].schorg_id + '">' + json[i].schorg_nm + '</option>');
							$('#organSelects').append(opt);
						}
					}
				},
				error:function(e) {
					fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				}
			});
		});

		$('#searchAuth').change(function(){
			$.ajax({
				url    : "/mng/values/authSelectAjax",
				type   : "post",
				data   : {auth_typ_cd : $('#searchAuth').val()},
				success: function(data){
					var json = jsonListSet(data);
					$('#searchAuth2 option').remove();
					$('#searchAuth2').append($('<option value="all">전체</option>'));
					var auth = $('#searchAuth').val();
					if(auth == '08' || auth == '10'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].salescp_id + '">' + json[i].company_nm + '</option>');
							$('#searchAuth2').append(opt);
						}
					}
					if(auth == '06'){
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].schorg_id + '">' + json[i].schorg_nm + '</option>');
							$('#searchAuth2').append(opt);
						}
					}
				},
				error:function(e) {
					fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				}
			});
		});

		$('#accountUserId').keyup(function(){
			emailcheck($('#accountUserId').val());
		});

		$('#accountPasswordChk').keyup(function() {
			var pswd = $.trim($('#accountPassword').val());
			var pswdChk = $.trim($('#accountPasswordChk').val());

			if (pswd.length > 0 && pswdChk.length > 0) {
				if (pswd != pswdChk) {
					document.getElementById("errorTxt").className= "cl_ec7063";
					$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
				} else {
					$('#errorTxt').text("* 비밀번호와 일치합니다.");
					document.getElementById("errorTxt").className= "cl_839ab5";
				}
			} else {
				$('#errorTxt').text("");
			}
		});
		
		$('#newPasswordChk').keyup(function() {
			var npswd = $.trim($('#newPassword').val());
			var npswdChk = $.trim($('#newPasswordChk').val());

			if (npswd.length > 0 && npswdChk.length > 0) {
				if(npswd != npswdChk){
					document.getElementById("errorTxt2").className= "cl_ec7063";
					$('#errorTxt2').text("* 비밀번호와 일치하지 않습니다.");
				}else{					
					document.getElementById("errorTxt2").className= "cl_839ab5";
					$('#errorTxt2').text("");
				}
			} else {
				$('#errorTxt2').text("");
			}
		});
	});

	function searchUsr() {
		document.searchForm.submit();
	}

	// 계정관리
	function fnPopupAccountDetail(popupId, id){
		var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;
		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);

		$('#authSelects option').remove();
		$('#organSelects option').remove();
		$('#user_seq').val(id);
		$('.mngAccountListDetail tbody tr [name=user_id]').text("");
		$('.mngAccountListDetail tbody tr [name=seq_adm]').val("");
		$('.mngAccountListDetail tbody tr [name=password]').val("");
		$('.mngAccountListDetail tbody tr [name=newPassword]').val("");
		$('.mngAccountListDetail tbody tr [name=newPasswordChk]').val("");
		$('.mngAccountListDetail tbody tr [name=user_nm]').val("");
		$('.mngAccountListDetail tbody tr [name=hp_num]').val("");
		$('.mngAccountListDetail tbody tr [name=description]').val("");

		$.ajax({
			url    : "/mng/account/detail",
			type   : "post",
			data   : {seq_adm : id},
			success: function(data){
				var json = jsonSet(data);
				$('#user_seq').val(json[0].seq_adm);
				$('.mngAccountListDetail tbody tr [name=user_id]').text(json[0].user_id);
				$('.mngAccountListDetail tbody tr [name=seq_adm]').val(json[0].seq_adm);
				$('.mngAccountListDetail tbody tr [name=password]').val(json[0].password);
				$('.mngAccountListDetail tbody tr [name=user_nm]').val(json[0].user_nm);
				$('.mngAccountListDetail tbody tr [name=hp_num]').val(json[0].hp_num);
				$('.mngAccountListDetail tbody tr [name=description]').val(json[0].description);

				<c:forEach var="rs" items="${authList}">
					var code = "${rs.code}";
					if(json[0].auth_typ_cd == '${rs.code}') {
						var opt = $('<option value="' + code + '" selected="selected">' + '${rs.code_nm}' + '</option>');
						$('#authSelects').append(opt);
					} else {
						var opt = $('<option value="' + code + '">' + '${rs.code_nm}' + '</option>');
						$('#authSelects').append(opt);
					}
				</c:forEach>

				if(json[0].salescp_id != null) {
					var obs = $('<option value="' + json[0].salescp_id + '" selected="selected">' + json[0].company_nm + '</option>');
					$('#organSelects').append(obs);
				} else {
					var obs = $('<option value="' + json[0].schorg_id  + '" selected="selected">' + json[0].schorg_nm + '</option>');
					$('#organSelects').append(obs);
				}
			}, error: function(e){
				// 오류 메시지 출력
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}

	function insertChk() {
		if($('#accountUserId').val().length < 1) {
			fnAlertMessage('아이디를 입력하세요.');
			return false;
		}

		if($('#accountPassword').val().length < 1) {
			fnAlertMessage('비밀번호를 입력하세요.');
			return false;
		}

		if($('#accountPasswordChk').val().length < 1) {
			document.getElementById("errorTxt").className= "cl_ec7063";
			$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			$('#accountPasswordChk').focus();
			return false;
		}

		if(!chkPwdAll($.trim($('#accountPassword').val()))) {
			$('#accountPassword').val('');
			$('#accountPasswordChk').val('');
			$('#accountPassword').focus();
			return false;
		}

		if($('#accountPassword').val() != $('#accountPasswordChk').val()){
			document.getElementById("errorTxt").className= "cl_ec7063";
			$('#errorTxt').text("* 비밀번호와 일치하지 않습니다.");
			$('#accountPasswordChk').focus();
			return false;
		}


		if($('#accountUserNm').val().length < 1) {
			fnAlertMessage('이름을 입력하세요.');
			return false;
		}

		if($('#authSelect').val() == 'N') {
			fnAlertMessage('권한을 선택하세요.');
			return false;
		}

		if($('#accountHp').val().length < 1) {
			fnAlertMessage('휴대폰을 입력하세요.');
			return false;
		}

		if($('#organSelect').val() == 'N') {
			fnAlertMessage('소속을 선택하세요.');
			return false;
		}

		if(!emailcheck($('#accountUserId').val())) {
			return false;
		} else {
			$.ajax({
				url : "/mng/account/checkDupUseridAjax",
				type : "POST",
				data : {user_id : $('#accountUserId').val()},
				success:function(res) {
					if(res == 'fail'){
						fnAlertMessage('동일한 아이디가 있습니다.');
						return false;
					} else {
						fnPopupConfirmOpen('mngAccountListInsertSubmit');
					}
				},
				error:function(e) {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			});
		}
	}

	function doInsert(e) {
		popupMessageDown('mngAccountListInsertSubmit.style02.zi5', 'mngAccountListInsertSubmit.zi4');

		document.userInsert.submit();
	}
	
	function updateChks() {
		if($('input[name=password]').val().length < 1) {
			fnAlertMessage("현재 비밀번호를 입력하세요.");
		}
		
		if($('#newPassword').val().length > 0) {
			if(!chkPwdAll($.trim($('#newPassword').val()))){
				$('#newPassword').val('');
				$('#newPasswordChk').val('');
				$('#newPassword').focus();
				return false;
			}
		}
		
		if($('#newPassword').val().length < 1 && $('#newPasswordChk').val().length > 0) {
			$('#newPassword').focus();
			return false;
		}
				
		if($('#newPassword').val().length > 0 && $('#newPasswordChk').val().length > 0 && $('#newPassword').val() != $('#newPasswordChk').val()) {
			document.getElementById("errorTxt2").className= "cl_ec7063";
			$('#errorTxt2').text("* 비밀번호와 일치하지 않습니다.");
			
			$('#newPasswordChk').focus();
			return false;
		}
		
		if($('#usrNm').val().length < 1) {
			fnAlertMessage("이름을 입력하여 주십시요.");
			return false;
		}
		
		if($('#usrHp').val().length < 1) {
			fnAlertMessage('휴대폰을 입력하세요.');
			return false;
		}
		
		if($('#organSelects').val() == '') {
			fnAlertMessage('소속을 선택하세요.');
			return false;
		}
		
		fnPopupConfirmOpen('mngAccountListDetailSubmit');
	}

	function doUpdate() {
		var forms = document.userUpdate;
		forms.action="/mng/account/update"
		forms.submit();
	}

	function doDelete() {
		var forms = document.userUpdate;
		forms.action="/mng/account/delete"
		forms.submit();
	}

	// 등록팝업 값 초기화
	function deleteSet() {
		$('#accountUserId').val("");
		$('#accountPassword').val("");
		$('#accountPasswordChk').val("");
		$('#accountUserNm').val("");
		$('#authSelect').val("");
		$('#accountHp').val("");
		$('#organSelect option').remove();
		var opt = $('<option value="">선택</option>');
		$('#organSelect').append(opt);
		$('#accountDescription').val("");
	}

	// 계정상세팝업 값 초기화
	function deleteSets() {
		$('#authSelects option').remove();
		$('#organSelects option').remove();
		$('.mngAccountListDetail tbody tr [name=user_id]').text("");
		$('.mngAccountListDetail tbody tr [name=seq_adm]').val("");
		$('.mngAccountListDetail tbody tr [name=password]').val("");
		$('.mngAccountListDetail tbody tr [name=newPassword]').val("");
		$('.mngAccountListDetail tbody tr [name=newPasswordChk]').val("");
		$('.mngAccountListDetail tbody tr [name=user_nm]').val("");
		$('.mngAccountListDetail tbody tr [name=hp_num]').val("");
		$('.mngAccountListDetail tbody tr [name=description]').val("");
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />