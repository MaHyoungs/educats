<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="4" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="4" />
		<c:param name="menu2Num" value="3" />
		<c:param name="menu3Num" value="1" />
	</c:import>

	<div id="content" class="mngCodeOrgan">
		<h1 class="title">학교/기관코드 관리</h1>
		<div>
			<form id="organ" name="organ" action="/mng/code/insertOrgan" method="post" >
				<div class="searchWrap">
					<div class="searchBlock searchBlockLeft">
						<div class="section">
							<label for="schorg_typ_cd">구분</label>
							<div class="select">
								<select id="schorg_typ_cd" class="schorg_typ_cd" name="schorg_typ_cd">
									<option value="">선택</option>
									<c:forEach var="rs" items="${schorgList}">
										<option value="${rs.code}">${rs.code_nm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="section">
							<label for="are_cd">시/도</label>
							<div class="select">
								<select id="are_cd" class="are_cd" name="are_cd">
									<option value="">선택</option>
									<c:forEach var="rs" items="${areaList}">
										<option value="${rs.code}">${rs.code_nm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="section">
							<label for="schorg_nm">학교/기관명</label>
							<div class="select wid180">
								<input type="text" id="schorg_nm" name="schorg_nm" class="wid100p" />
							</div>
						</div>
						<div class="section">
							<label for="schorg_id">코드</label>
							<div class="section">
								<div class="select input">
									<input type="text" id="schorg_id" name="schorg_id" value="" />
								</div>
							</div>
						</div>
						<div class="section">
							<label for="salescp_id">영업사</label>
							<div class="select">
								<select id="salescp_id" class="salescp_id" name="salescp_id">
									<option value="">선택</option>
									<c:forEach var="rs" items="${salesList}">
										<option value="${rs.salescp_id}">${rs.company_nm}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<div class="section">
							<label for="survey_typ">검사</label>
							<div class="select">
								<select id="survey_typ" class="survey_typ" name="survey_typ">
									<option value="">선택</option>
									<c:forEach var="rs" items="${surveyList}">
										<option value="${rs.code}">${rs.code_nm}</option>
									</c:forEach>
									<option value="SV03">모든 검사</option>
								</select>
							</div>
						</div>
					</div>
					<div class="searchBlock searchBlockRight">
						<div class="section">
							<span class="saBtn"><a href="javascript:void(0)" onclick="goInsert(); return false;" class="btnMngJoinsSearch">등록</a></span>
						</div>
					</div>
				</div>
			</form>
		</div>
		<div class="tableWrap">
			<table class="style05" summary="">
				<caption></caption>
				<colgroup>
					<col width="11%" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
					<col width="11%" />
					<col width="16%" />
					<col width="17%" />
				</colgroup>
				<thead>
				<tr>
					<th>구분</th>
					<th>시/도</th>
					<th>학교/기관명</th>
					<th>학교/기관코드</th>
					<th>영업사</th>
					<th>검사</th>
					<th>수정/삭제</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="orList" items="${organCodeList}" varStatus="sts">
					<form id="organs${sts.count}" name="organs" action="/mng/code/doUpdateOrganAjax" method="post">
						<tr>
							<td>
								<div id="schorg_typ_read${sts.count}">
									<c:forEach var="rs" items="${schorgList}">
										<c:if test="${rs.code eq orList.schorg_typ_cd}">${rs.code_nm}</c:if>
									</c:forEach>
								</div>
								<div class="select" id="schorg_typ_select${sts.count}" style="display:none">
									<select id="schorg_typ_cd${sts.count}" class="schorg_typ_cd" name="schorg_typ_cd">
										<c:forEach var="rs" items="${schorgList}">
											<option value="${rs.code}" <c:if test="${rs.code eq orList.schorg_typ_cd}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
								</div>
							</td>
							<td>
								<div id="are_read${sts.count}">
									<c:forEach var="rs" items="${areaList}">
										<c:if test="${rs.code eq orList.are_cd}">${rs.code_nm}</c:if>
									</c:forEach>
								</div>
								<div class="select" id="are_select${sts.count}" style="display:none">
									<select id="are_cd${sts.count}" class="are_cd" name="are_cd">
										<c:forEach var="rs" items="${areaList}">
											<option value="${rs.code}" <c:if test="${rs.code eq orList.are_cd}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
								</div>
							</td>
							<td>
								<div id="schorg_read${sts.count}">
									${orList.schorg_nm}
								</div>
								<div class="select input" id="schorg_input${sts.count}" style="display:none">
									<input type="text" id="schorg_nm${sts.count}" name="schorg_nm" value="${orList.schorg_nm}" class="wid140" />
								</div>
							</td>
							<td>
								<div class="select input">${orList.schorg_id}</div>
								<input type="hidden" id="schorg_id${sts.count}" name="schorg_id" value="${orList.schorg_id}" />
							</td>
							<td>
								<div id="company_read${sts.count}">									
									<c:forEach var="rs" items="${salesList}">
										<c:if test="${rs.salescp_id eq orList.salescp_id}">${rs.company_nm}</c:if>
									</c:forEach>
								</div>
								<div class="select" id="company_select${sts.count}" style="display:none">
									<select id="salescp_id${sts.count}" class="salescp_id" name="salescp_id">
										<c:forEach var="rs" items="${salesList}">
											<option value="${rs.salescp_id}" <c:if test="${rs.salescp_id eq orList.salescp_id}">selected="selected"</c:if>>${rs.company_nm}</option>
										</c:forEach>
									</select>
								</div>
							</td>
							<td>
								<div id="survey_typ_read${sts.count}">									
									<c:choose>
										<c:when test="${orList.auCnt gt 0 and orList.selfCnt gt 0}">
											모든 검사
										</c:when>
										<c:when test="${orList.auCnt ge 0 and orList.selfCnt lt 1}">
											진로탐색검사
										</c:when>
										<c:when test="${orList.selfCnt ge 0 and orList.auCnt lt 1}">
											자기주도학습
										</c:when>
									</c:choose>
								</div>
								<div class="select" id="survey_typ_select${sts.count}" style="display:none">
									<select id="survey_typ${sts.count}" class="survey_typ" name="survey_typ">
									<c:choose>
										<c:when test="${orList.auCnt gt 0 and orList.selfCnt gt 0}">
											<option value="SV03">모든 검사</option>
											<input type="hidden" name="survey_typ_org" value="SV03">
										</c:when>
										<c:when test="${orList.auCnt ge 0 and orList.selfCnt lt 1}">
											<option value="SV01">진로탐색검사</option>
											<option value="SV03">모든 검사</option>
											<input type="hidden" name="survey_typ_org" value="SV01">
										</c:when>
										<c:when test="${orList.selfCnt ge 0 and orList.auCnt lt 1}">
											<option value="SV02">자기주도학습</option>
											<option value="SV03">모든 검사</option>
											<input type="hidden" name="survey_typ_org" value="SV02">
										</c:when>
										<c:otherwise>
											<option value="SV01">진로탐색검사</option>
											<option value="SV02">자기주도학습</option>
											<option value="SV03">모든 검사</option>
										</c:otherwise>
									</c:choose>
									</select>
								</div>
							</td>
							<td>
								<span class="saBtn" id="uptBtn1_${sts.count}"><a href="javascript:void(0)" onclick="setUpdateMode('${sts.count}');" class="btnGreen">수정</a></span>
								<span class="saBtn" id="uptBtn2_${sts.count}" style="display: none;"><a href="javascript:void(0)" onclick="setForm('${sts.count}')" class="btnGreen layerpopupOpen" data-popup-class="mngCodeOrganModify">수정</a></span>
								<span class="saBtn" id="delBtn_${sts.count}"><a href="javascript:void(0)" onclick="setDeleteValue('${orList.schorg_id}');" class="btnGray layerpopupOpen">삭제</a></span>
								<span class="saBtn" id="cleBtn_${sts.count}" style="display: none;"><a href="javascript:void(0)" onclick="setRestoreMode('${sts.count}');" class="btnGray">취소</a></span>
							</td>
						</tr>
					</form>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<div class="layerBg mngCodeOrganModify zi2"></div>
<div class="layerPopup mngCodeOrganModify style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			수정 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="chkUpdate(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngCodeOrganDelete zi2"></div>
<div class="layerPopup mngCodeOrganDelete style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			삭제 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="goDelete(); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<input type="hidden" id="form_cnt" name="form_cnt"/>
<form name="organ2" id="organ2" action="/mng/code/deleteOrgan" method="post" >
	<input type="hidden" id="delete_id" name="delete_id"/>
</form>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:choose>
			<c:when test="${msg eq 'Y'}" >
				fnAlertMessage("학교/기관코드를 등록하였습니다.");
			</c:when>
			<c:when test="${msg eq 'N'}" >
				fnAlertMessage("동일한 학교/기관코드가 있습니다.");
			</c:when>
			<c:when test="${msg eq 'D'}" >
				fnAlertMessage("학교/기관코드가 삭제되었습니다.");
			</c:when>
			<c:when test="${msg eq 'F'}" >
				fnAlertMessage("학교/기관코드 삭제가 실패하였습니다.");
			</c:when>
			</c:choose>
		</c:if>
	});

	function goInsert() {
		var frm = document.organ;
		if(frm.schorg_typ_cd.value == "") {
			fnAlertMessage("구분 값을 입력하여 주십시요.");
			return false;
		}
		if(frm.are_cd.value == "") {
			fnAlertMessage("시/도 값을 입력하여 주십시요.");
			return false;
		}
		if(frm.schorg_nm.value == "") {
			fnAlertMessage("학교/기관명 값을 입력하여 주십시요.");
			return false;
		}
		if(frm.schorg_id.value == "") {
			fnAlertMessage("코드 값을 입력하여 주십시요.");
			return false;
		}
		if(frm.salescp_id.value == "") {
			fnAlertMessage("영업사 값을 입력하여 주십시요.");
			return false;
		}
		if(frm.survey_typ.value == "") {
			fnAlertMessage("검사 값을 입력하여 주십시요.");
			return false;
		}
		
		$.ajax({
			url : "/mng/code/organIdDupChkAjax",
			type : "POST",
			data : {schorg_id : frm.schorg_id.value},
			success:function(msg) {
				if (msg == "succ") {
					frm.submit();
				} else {
					fnAlertMessage("동일한 학교/기관코드가 있습니다.");
					return false;
				}
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function setForm(cnt) {
		$("#form_cnt").val(cnt);
	}
	
	function chkUpdate(e) {
		var cnt = $("#form_cnt").val();

		var before = $.trim($('#schorg_typ_read'+cnt).text());
		var after = $('#schorg_typ_cd'+cnt+' option:selected').text();

		$.ajax({
			url : "/mng/code/updateChkOrganAjax",
			type : "POST",
			data : {schorg_id:$('#schorg_id'+cnt).val()},
			success:function(msg) {
				if (before != after && msg == "fail") {
					fnAlertMessage("학급이 개설된 경우 학교/기관의 구분속성을 수정할 수 없습니다.");
				} else {
					goUpdate(e);
				}
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function goUpdate(e) {
		var frmData = $('#organs' + $("#form_cnt").val()).serialize();

		$.ajax({
			url : "/mng/code/doUpdateOrganAjax",
			type : "POST",
			data : frmData,
			success:function(msg) {
				if (msg != "fail") {
					var _target = $(e).parents(".layerPopup").attr("data-popup-this");
					$(e).parents(".layerPopup." + _target).css({"display":"none"});
					$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});

					fnAlertMessage("수정되었습니다.");

					// 변경된 값으로 출력 처리
					var cnt = $("#form_cnt").val();
					setRestoreMode(cnt);
					var organCodeInfo = jsonSet(msg)[0];					
					$("#schorg_typ_read"+cnt).text(organCodeInfo.schorg_typ_read);
					$("#are_read"+cnt).text(organCodeInfo.are_read);
					$("#schorg_read"+cnt).text(organCodeInfo.schorg_read);
					$("#company_read"+cnt).text(organCodeInfo.company_read);
					var survey_typ_read = "";
					var auCnt = parseInt(organCodeInfo.aucnt);
					var selfCnt = parseInt(organCodeInfo.selfcnt);
					if (auCnt > 0 && selfCnt > 0) {
						survey_typ_read = "모든 검사";
					} else if(auCnt >= 0 && selfCnt < 1) {
						survey_typ_read = "진로탐색검사";
					} else if(selfCnt >= 0 && auCnt < 1) {
						survey_typ_read = "자기주도학습";
					}
					$("#survey_typ_read"+cnt).text(survey_typ_read);
				}
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function goDelete() {
		var frm = document.organ2;
		frm.submit();
	}

	function setUpdateMode(cnt) {
		$("#uptBtn1_"+cnt).css("display", "none");
		$("#uptBtn2_"+cnt).css("display", "inline");
		$("#delBtn_"+cnt).css("display", "none");
		$("#cleBtn_"+cnt).css("display", "inline");
		
		// 변경 전 값으로 원복 처리
		// 구분
		var schorg_typ_read = $.trim($("#schorg_typ_read"+cnt).text());
		var schorg_typ_cd = '';
		$("#schorg_typ_cd"+cnt).children("option").each(function(){
			if ($(this).html() == schorg_typ_read) schorg_typ_cd = $(this).val();
		});
		$("#schorg_typ_cd"+cnt).val(schorg_typ_cd);
		
		// 시/도
		var are_read = $.trim($("#are_read"+cnt).text());
		var are_cd = '';
		$("#are_cd"+cnt).children("option").each(function(){
			if ($(this).html() == are_read) are_cd = $(this).val();
		});
		$("#are_cd"+cnt).val(are_cd);
		
		// 학교/기관명
		$('#schorg_nm'+cnt).val($.trim($("#schorg_read"+cnt).text()));
	
		// 영업사
		var company_read = $.trim($("#company_read"+cnt).text());
		var salescp_id = '';
		$("#salescp_id"+cnt).children("option").each(function(){
			if ($(this).html() == company_read) salescp_id = $(this).val();
		});
		$("#salescp_id"+cnt).val(salescp_id);
		
		// 상태
		var survey_typ_read = $.trim($("#survey_typ_read"+cnt).text());
		var survey_typ = '';
		$("#survey_typ"+cnt).children("option").each(function(){
			if ($(this).html() == survey_typ_read) survey_typ = $(this).val();
		});
		$("#survey_typ"+cnt).val(survey_typ);
				
		$("#schorg_typ_read"+cnt).hide();
		$("#are_read"+cnt).hide();
		$("#schorg_read"+cnt).hide();
		$("#company_read"+cnt).hide();
		$("#survey_typ_read"+cnt).hide();
		
		$("#schorg_typ_select"+cnt).show();
		$("#are_select"+cnt).show();
		$("#schorg_input"+cnt).show();
		$("#company_select"+cnt).show();
		$("#survey_typ_select"+cnt).show();
	}

	function setDeleteValue(id) {

		$.ajax({
			url    : "/mng/code/useOrganChkAjax",
			type   : "post",
			data   : {schorg_id:id},
			success: function(data){
				if(data == 'Y') {
					fnAlertMessage('라이선스가 등록된 학교/기관은 삭제할 수 없습니다.');
					return false;
				} else {
					fnPopupConfirmOpen('mngCodeOrganDelete');
				}
			},
			error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});

		$("#delete_id").val(id);
	}

	function setRestoreMode(cnt) {
		$("#uptBtn1_"+cnt).css("display", "inline");
		$("#uptBtn2_"+cnt).css("display", "none");
		$("#delBtn_"+cnt).css("display", "inline");
		$("#cleBtn_"+cnt).css("display", "none");
		
		$("#schorg_typ_read"+cnt).show();
		$("#are_read"+cnt).show();
		$("#schorg_read"+cnt).show();
		$("#company_read"+cnt).show();
		$("#survey_typ_read"+cnt).show();
		
		$("#schorg_typ_select"+cnt).hide();
		$("#are_select"+cnt).hide();
		$("#schorg_input"+cnt).hide();
		$("#company_select"+cnt).hide();
		$("#survey_typ_select"+cnt).hide();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />