<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="4" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="4" />
		<c:param name="menu2Num" value="3" />
		<c:param name="menu3Num" value="2" />
	</c:import>

	<div id="content" class="mngCodeBusiness">
		<h1 class="title">영업사코드 관리</h1>
		<div>
			<form name="sales" id="sales" action="/mng/code/insertBusiness" method="post" >
				<div class="searchWrap">
					<div class="section">
						<label for="company_nm">회사명</label>
						<div class="select wid200">
							<input type="text" id="company_nm" name="company_nm" class="wid100p" />
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" onclick="goInsert(); return false;" class="btnMngJoinsSearch">등록</a></span>
					</div>
				</div>
			</form>
		</div>
		<div class="tableWrap">
			<table class="style05" summary="">
				<caption></caption>
				<colgroup>
					<col width="27%" />
					<col width="43%" />
					<col width="30%" />
				</colgroup>
				<thead>
				<tr>
					<th>회사명</th>
					<th>회사코드</th>
					<th>수정/삭제</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${businessCodeList}" varStatus="sts" >
						<tr id="${sts.count}">
							<td>
								<div id="company_read${sts.count}">
									${rs.company_nm}
								</div>
								<div class="select input" id="company_input${sts.count}" style="display:none">
									<input type="text" id="company_nm${sts.count}" name="company_nm" value="${rs.company_nm}" class="wid140" />
								</div>
							</td>
							<td>${rs.salescp_id}</td>
							<td>
								<span class="saBtn" id="uptBtn1_${sts.count}"><a href="javascript:void(0)" onclick="setUpdateMode('${sts.count}');" class="btnGreen">수정</a></span>
								<span class="saBtn" id="uptBtn2_${sts.count}" style="display: none;"><a href="javascript:void(0)" onclick="setUpdateValue('${rs.salescp_id}', '${sts.count}');" class="btnGreen layerpopupOpen" data-popup-class="mngCodeBusinessModify">수정</a></span>
								<span class="saBtn" id="delBtn_${sts.count}"><a href="javascript:void(0)" onclick="setDeleteValue('${rs.salescp_id}');" class="btnGray layerpopupOpen" data-popup-class="mngCodeBusinessDelete">삭제</a></span>
								<span class="saBtn" id="cleBtn_${sts.count}" style="display: none;"><a href="javascript:void(0)" onclick="setRestoreMode('${sts.count}');" class="btnGray">취소</a></span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>
<%--수정 팝업--%>
<div class="layerBg mngCodeBusinessModify zi2"></div>
<div class="layerPopup mngCodeBusinessModify style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			수정 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="goUpdate(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<%--삭제 팜업--%>
<div class="layerBg mngCodeBusinessDelete zi2"></div>
<div class="layerPopup mngCodeBusinessDelete style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			삭제 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="goDelete(); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<input type="hidden" id="update_nm" name="update_nm"/>
<input type="hidden" id="update_id" name="update_id"/>
<input type="hidden" id="update_cnt" name="update_cnt"/>
<form name="sales2" id="sales2" action="/mng/code/deleteBusiness" method="post" >
	<input type="hidden" id="delete_id" name="delete_id"/>
</form>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:choose>
			<c:when test="${msg eq 'Y'}" >
				fnAlertMessage("영업사 코드를 등록하였습니다.");
			</c:when>
			<c:when test="${msg eq 'N'}" >
				fnAlertMessage("동일한 회사명이 있습니다.");
			</c:when>
			<c:when test="${msg eq 'D'}" >
				fnAlertMessage("영업사 코드가 삭제되었습니다.");
			</c:when>
			<c:when test="${msg eq 'F'}" >
				fnAlertMessage("영업사 코드 삭제가 실패하였습니다.");
			</c:when>
			</c:choose>
		</c:if>
	});

	function goInsert() {
		var frm = document.sales;
		if(frm.company_nm.value == "") {
			fnAlertMessage("회사명을 입력하여 주십시요.");
			return false;
		}

		if(checkKey(frm.company_nm.value)) {
			frm.submit();
		} else {
			fnAlertMessage("허용하지 않는 문자가 있습니다.");
		}
	}

	function goUpdate(e) {
		$.ajax({
			url : "/mng/code/doUpdateBusinessAjax",
			type : "POST",
			data : {update_id:$("#update_id").val(), update_nm:$("#update_nm").val()},
			success:function(msg) {
				
				if (msg != "fail") {
					var _target = $(e).parents(".layerPopup").attr("data-popup-this");
					$(e).parents(".layerPopup." + _target).css({"display":"none"});
					$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});
	
					fnAlertMessage("수정되었습니다.");
					
					// 변경된 값으로 출력 처리
					var cnt = $("#update_cnt").val();
					setRestoreMode(cnt);
					$("#company_read"+cnt).text($("#update_nm").val());
				}
			},
			error:function(e) {
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.")
				return false;
			}
		});
	}

	function goDelete() {
		var frm = document.sales2;
		frm.submit();
	}
	
	function setUpdateMode(cnt) {
		$("#uptBtn1_"+cnt).css("display", "none");
		$("#uptBtn2_"+cnt).css("display", "inline");
		$("#delBtn_"+cnt).css("display", "none");
		$("#cleBtn_"+cnt).css("display", "inline");
		
		// 변경 전 값으로 원복 처리
		$('tbody tr#' + cnt+' input[name=company_nm]').val($.trim($("#company_read"+cnt).text()));
		
		$("#company_read"+cnt).hide();
		$("#company_input"+cnt).show();
	}

	function setUpdateValue(id, cnt) {
		$("#update_id").val(id);
		$("#update_nm").val($('tbody tr#' + cnt+' input[name=company_nm]').val());
		$("#update_cnt").val(cnt);
	}

	function setDeleteValue(id) {
		$("#delete_id").val(id);
	}
	
	function setRestoreMode(cnt) {
		$("#uptBtn1_"+cnt).css("display", "inline");
		$("#uptBtn2_"+cnt).css("display", "none");
		$("#delBtn_"+cnt).css("display", "inline");
		$("#cleBtn_"+cnt).css("display", "none");
		
		$("#company_input"+cnt).hide();
		$("#company_read"+cnt).show();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />