<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="4" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="4" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngSemiInfo">
		<h1 class="title">현금영수증 신청관리</h1>
		<div>
			<form id="searchForm" name="searchForm" action="/mng/cash/receipt" method="post">
				<div class="searchWrap">
					<div class="section">
						<label for="proS_dt">신청일</label>
						<div class="select flexN">
							<input type="text" id="proS_dt" name="proS_dt" readonly="readonly" value="${map.proS_dt}" class="calendar" />
							<span>~</span>
							<input type="text" id="proE_dt" name="proE_dt" readonly="readonly" value="${map.proE_dt}" class="calendar" />
						</div>
					</div>
					<div class="section">
						<label for="proName">ID/입금자명</label>
						<div class="select wid180">
							<input type="text" id="proName" name="proName" class="wid100p" value="${map.proName}" />
						</div>
					</div>
					<div class="section">
						<span class="saBtn"><a href="javascript:void(0)" onclick="searchPro()" class="btnMngJoinsSearch">검색</a></span>
					</div>
				</div>
			</form>
		</div>
		<div class="tabSection">
			<span class="saBtn"><a href="javascript:void(0)" class="btnMngClassTabGreen layerpopupOpen" data-popup-class="mngCashReceiptTrue">처리완료</a></span>
		</div>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="5%" />
					<col width="18%" />
					<col width="15%" />
					<col width="10%" />
					<col width="14%" />
					<col width="14%" />
					<col width="14%" />
					<col width="10%" />
				</colgroup>
				<thead>
				<tr>
					<th>
						<div class="select checkbox">
							<input type="checkbox" name="checkAll"  id="checkAll" value="" />
						</div>
					</th>
					<th>신청일시</th>
					<th>ID</th>
					<th>입금자명</th>
					<th>거래자구분</th>
					<th>현금영수증<br/>신청구분</th>
					<th>현금영수증<br/>신청번호</th>
					<th>처리여부</th>
				</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${cashList}" >
						<tr>
							<td>
								<div class="select checkbox">
									<c:if test="${rs.process_yn eq 'Y'}" >
										<input type="checkbox" name="uncheckItem" value="${rs.seq_cash}" disabled="true"/>
									</c:if>
									<c:if test="${rs.process_yn eq 'N'}" >
										<input type="checkbox" name="checkItem" value="${rs.seq_cash}"/>
									</c:if>
								</div>
							</td>
							<td>${rs.reg_dt}</td>
							<td>${rs.user_id}</td>
							<td>${rs.user_nm}</td>
							<td>${rs.user_type_txt}</td>
							<td>${rs.receipt_type_txt}</td>
							<td>${rs.receipt_number}</td>
							<td>${rs.process_yn}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<form id="statCdUpdate" name="statCdUpdate" action="/mng/cash/receiptComplete" method="post">
	<input type="hidden" id="valList" name="valList" value=""/>
</form>

<div class="layerBg mngCashReceiptTrue zi2"></div>
<div class="layerPopup mngCashReceiptTrue style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			현금영수증신청을 처리완료 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doComplete(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<script>
	$(document).ready(function(){
		$("#proS_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true});
		$("#proE_dt").datepicker({dateFormat: "yy-mm-dd", changeYear: true, changeMonth: true});
	});

	$("#checkAll").click(function(){
		if($("#checkAll").prop("checked")){
			$("input:checkbox[name='checkItem']").prop("checked", true);
		}else{
			$("input:checkbox[name='checkItem']").prop("checked", false);
		}
	});

	function searchPro() {
		if($("#proS_dt").val() > $("#proE_dt").val()) {
			fnAlertMessage("시작일은 종료일과 같거나 과거이어야 합니다.");
			return false;
		}
		document.searchForm.submit();
	}

	function doComplete(e) {
		var _target = $(e).parents(".layerPopup").attr("data-popup-this");
		$(e).parents(".layerPopup." + _target).css({"display":"none"});
		$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});
		var valList = '';
		$("input:checkbox[name='checkItem']:checked").each(function() {
			valList += ',' + $(this).val();
		});
		if(valList == '') {
			fnAlertMessage("처리완료할 항목을 선택하여 주십시요.");
			return false;
		}
		valList = valList.substr(1);
		$("#valList").val(valList);

		document.statCdUpdate.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />