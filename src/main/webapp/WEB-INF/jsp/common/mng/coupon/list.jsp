<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="5" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="5" />
		<c:param name="menu2Num" value="2" />
	</c:import>

	<div id="content" class="mngJoins">
		<h1 class="title">쿠폰관리</h1>
		<div class="functionWrap">
			<span class="saBtn"><a href="#" class="btnCouponPublish layerpopupOpen" data-popup-class="mngCouponAddClass">쿠폰 발행</a></span>
		</div>
		<form id="searchForm2" name="searchForm2" action="/mng/coupon/list" method="post" onsubmit="return false;">
			<div class="searchWrap">
				<div class="section">
					<label for="searchSDt">발행일</label>
					<div class="select">
						<input type="text" id="searchSDt" name="searchSDt" value="${map.searchSDt}" readonly="readonly" class="calendar" />
						<span>~</span>
						<input type="text" id="searchEDt" name="searchEDt" value="${map.searchEDt}" readonly="readonly" class="calendar" />
					</div>
				</div>
				<div class="section">
					<label for="searchSales">발행대상</label>
					<div class="select wid180">
						<input type="text" id="searchSales" name="searchSales" class="wid100p" value="${map.searchSales}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="8%" />
					<col width="17%" />
					<col width="11%" />
					<col width="8%" />
					<col width="15%" />
					<col width="11%" />
					<col width="10%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>발행일시</th>
						<th>검사종류</th>
						<th>검사횟수</th>
						<th>발행대상</th>
						<th>유효기간</th>
						<th>발행수</th>
						<th>가입수</th>
						<th>검사진행수</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${couponList}" varStatus="sts">
						<tr>
							<th>${rs.num}</th>
							<td><a href="/mng/coupon/publishHistory?seq_coupon_master=${rs.seq_coupon_master}">${rs.reg_dt}</a></td>
							<td>${rs.survey_typ_nm}</td>
							<td><c:choose><c:when test="${rs.survey_max eq '999999'}">N</c:when><c:otherwise>${rs.survey_max}</c:otherwise></c:choose></td>
							<td>${rs.company_nm}</td>
							<td>${rs.valid_start_dt} ~ <br/> ${rs.valid_end_dt}</td>
							<td>${rs.publish_count}</td>
							<td>${rs.use_cnt}</td>
							<td>${rs.survey_cnt}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<div class="layerBg mngCouponAddClass"></div>
<div class="layerPopup mngCouponAddClass style01">
	<div class="layerHeader">
		<span class="title">쿠폰 발행</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="couponForm" name="couponForm" action="/mng/coupon/publish" method="post">
					<input type="hidden" name="survey_typ_cd" value="SV01" />
					<input type="hidden" id="salescpId" name="salescp_id" />
					<input type="hidden" id="checkLimitState" name="check_limit_state" value="N"/>
					<input type="hidden" id="validLimitState" name="valid_limit_state" value="N"/>
					<table class="style02" summary="">
						<caption></caption>
						<colgroup>
							<col width="30%" />
							<col width="70%" />
						</colgroup>
						<tbody>
							<tr>
								<th>검사종류</th>
								<td class="taL pL20">진로탐색검사</td>
							</tr>
							<tr>
								<th>검사횟수</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select input">
											<input type="text" id="surveyMax" name="survey_max" maxlength="5" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" class="wid170" />
										</div>&nbsp;
										<div class="select checkbox">
											<input type="checkbox" name="check_limit"  id="checkLimit"/>
										</div>&nbsp;
										<label class="mouse-type" for="checkLimit">제한 없음</label>
									</div>
								</td>
							</tr>
							<tr>
								<th>발행대상</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select input">
											<input type="text" id="companyNm" name="company_nm" readonly="readonly" class="wid170" />&nbsp;
											<span class="saBtn"><a href="javascript:void(0)" onclick="initSales('', '1')" class="btnGray layerpopupOpen" data-popup-class="mngChoiceSales">선택</a></span>
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>발행수</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select input">
											<input type="text" id="publishCount" name="publish_count" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" class="wid170" />
										</div>
									</div>
								</td>
							</tr>
							<tr>
								<th>유효기간</th>
								<td class="taL pL20">
									<div class="section">
										<div class="select calendar">
											<input type="text" id="validStartDt" readonly="readonly" name="valid_start_dt" class="calendar" />
											<span>~</span>
											<input type="text" id="validEndDt" readonly="readonly" name="valid_end_dt" class="calendar" />
										</div>
										<div class="select checkbox">
												<input type="checkbox" name="valid_limit"  id="validLimit" />
										</div>&nbsp; <label class="mouse-type" for="validLimit">유효기간 없음</label>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<a href="javascript:void(0)" onclick="insertChk()" class="btnLayerGreen">등록</a>
				<a href="javascript:void(0)" onclick="doReset()" class="btnLayerGray layerClose">취소</a>
			</span>
		</div>
	</div>
</div>

<%-- Layer --%>
<div class="layerBg mngCounponPublishAdd zi2"></div>
<div class="layerPopup mngCounponPublishAdd style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			쿠폰을 발행하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doInsert(); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<c:import url="/mng/layer/publisherSelect" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<script>
	var d = new Date();

	function leadingZeros(n, digits) {
		var zero = '';
		n = n.toString();

		if (n.length < digits) {
			for (i = 0; i < digits - n.length; i++)
				zero += '0';
		}
		return zero + n;
	}

	$('#checkLimit').click(function() {
		if($("#checkLimit").prop("checked")){
			$('#checkLimitState').val("Y");
			$('#surveyMax').prop("disabled", true);
		}else{
			$('#checkLimitState').val("N");
			$('#surveyMax').prop("disabled", false);
		}
	});

	$('#validLimit').click(function() {
		if($("#validLimit").prop("checked")){
			$('#validLimitState').val("Y");
			$('#validStartDt').val(d.getFullYear() + "-" + leadingZeros(d.getMonth() + 1, 2) + "-" + leadingZeros(d.getDate()));
			$('#validEndDt').val("9999-12-30");
			$('#validStartDt').prop("disabled", true);
			$('#validEndDt').prop("disabled", true);
		}else{
			$('#validLimitState').val("N");
			$('#validStartDt').val("");
			$('#validEndDt').val("");
			$('#validStartDt').prop("disabled", false);
			$('#validEndDt').prop("disabled", false);
		}
	});

	function insertChk() {
		if($('#checkLimitState').val() == "N") {
			if($('#surveyMax').val() == '') {
				fnAlertMessage('검사횟수를 입력하여주십시요.');
				$('#surveyMax').focus();
				return false;
			}
		}

		if($('#companyNm').val() == '') {
			fnAlertMessage('발행대상을 선택하여주십시요.');
			return false;
		}

		if($('#publishCount').val() == '') {
			fnAlertMessage('발행수를 입력하여주십시요.');
			$('#publishCount').focus();
			return false;
		}

		if($('#validLimitState').val() == "N") {
			if($('#validStartDt').val() == null || $('#validStartDt').val() == '') {
				fnAlertMessage('시작일을 입력하세요.');
				return false;
			}
			if($('#validEndDt').val() == null || $('#validEndDt').val() == '') {
				fnAlertMessage('종료일을 입력하세요.');
				return false;
			}
			if($('#validStartDt').val() > $('#validEndDt').val()) {
				fnAlertMessage('시작일은 종료일과 같거나 과거이어야 합니다.');
				return false;
			}
		}

		fnPopupConfirmOpen('mngCounponPublishAdd');

	}

	function doInsert() {
		document.couponForm.submit();
	}

	function doReset() {
		document.couponForm.reset();
		$('#checkLimitState').val("N");
		$('#surveyMax').prop("disabled", false);
		$('#validLimitState').val("N");
		$('#validStartDt').prop("disabled", false);
		$('#validEndDt').prop("disabled", false);
	}

	function doSearch() {

			if($('#searchSDt').val() != ''){
				if($('#searchEDt').val() == null || $('#searchEDt').val() == ''){
					fnAlertMessage('발행종료일을 입력하세요.');
					return false;
				}
			}

			if($('#searchEDt').val() != ''){
				if($('#searchSDt').val() == null || $('#searchSDt').val() == ''){
					fnAlertMessage('발행시작일을 입력하세요.');
					return false;
				}
			}

			if($('#searchSDt').val() != '' && $('#searchEDt').val() != ''){
				if($('#searchSDt').val() > $('#searchEDt').val()){
					fnAlertMessage('발행시작일은 발행종료일과 같거나 과거이어야 합니다.');
					return false;
				}
			}
		document.searchForm2.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />