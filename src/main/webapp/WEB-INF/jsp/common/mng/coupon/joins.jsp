<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="5" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="5" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngJoins">
		<h1 class="title">쿠폰회원 참여현황</h1>
		<form id="searchForm" name="searchForm" action="/mng/coupon/joins" method="post">
			<div class="searchWrap">
				<div class="section">
					<label for="searchSurveyYn">검사진행여부</label>
					<div class="select">
						<select id="searchSurveyYn" class="searchSurveyYn" name="searchSurveyYn">
							<option value="all">전체</option>
							<option value="Y" <c:if test="${map.searchSurveyYn eq 'Y'}">selected="selected"</c:if>>Y</option>
							<option value="N" <c:if test="${map.searchSurveyYn eq 'N'}">selected="selected"</c:if>>N</option>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchSchoolDiv">학교구분</label>
					<div class="select">
						<select id="searchSchoolDiv" class="searchSchoolDiv" name="searchSchoolDiv" onchange="doSelectGrade(this.value)">
							<option value="all">전체</option>
							<option value="03" <c:if test="${map.searchSchoolDiv eq '03'}">selected="selected"</c:if>>고등학교</option>
							<option value="02" <c:if test="${map.searchSchoolDiv eq '02'}">selected="selected"</c:if>>중학교</option>
							<option value="01" <c:if test="${map.searchSchoolDiv eq '01'}">selected="selected"</c:if>>초등학교</option>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchGrade">학년</label>
					<div class="select">
						<select id="searchGrade" class="searchGrade" name="searchGrade">
							<option value="all">전체</option>
							<c:forEach var="gs" items="${gradeList}" varStatus="sts">
								<option value="${gs.code}" <c:if test="${map.searchGrade eq gs.code}">selected="selected"</c:if>>${gs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchNm">이름/ID</label>
					<div class="select wid180">
						<input type="text" id="searchNm" name="searchNm" class="wid100p" value="${map.searchNm}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="8%" />
					<col width="15%" />
					<col width="11%" />
					<col width="18%" />
					<col width="11%" />
					<col width="11%" />
					<col width="11%" />
					<col width="15%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>가입일</th>
						<th>이름</th>
						<th>ID(이메일)</th>
						<th>학교구분</th>
						<th>학년</th>
						<th>검사진행여부</th>
						<th>최근검사일자</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${list}" varStatus="sts">
						<tr>
							<th>${rs.num}</th>
							<td>${rs.reg_dt}</td>
							<td><a href="javascript:void(0)" onclick="fnPopupAccountDetail('${rs.seq_user}') ">${rs.user_nm}</a></td>
							<td>${rs.user_id}</td>
							<td><c:choose><c:when test="${rs.schlvl_cd eq '03'}">고등학교</c:when><c:when test="${rs.schlvl_cd eq '02'}">중학교</c:when><c:otherwise>초등학교</c:otherwise></c:choose></td>
							<td>${rs.st_grade}학년</td>
							<td><c:choose><c:when test="${empty rs.last_survey_dt}">N</c:when><c:otherwise>Y</c:otherwise></c:choose></td>
							<td>${rs.last_survey_dt}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<%--쿠폰회원 상세정보--%>
<div class="layerBg mngCouponAccountDetail"></div>
<div class="layerPopup mngCouponAccountDetail style01">
	<div class="layerHeader">
		<span class="title">계정 상세</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="userUpdate" name="userUpdate" method="post">
					<input type="hidden" id="changeSeq" name="changeSeq" />
					<table class="style06" summary="">
						<caption></caption>
						<colgroup>
							<col width="31%" />
							<col width="37%" />
							<col width="32%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_tchName">이름</th>
								<td>
									<ul>
										<li id="userNm" name="user_nm" class="wid240"></li>
									</ul>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_tchId">ID(이메일)</th>
								<td>
									<ul>
										<li id="userId" name="user_id" class="wid240"></li>
									</ul>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_tchRepass">새 비밀번호</th>
								<td><input type="password" id="changePassword" name="changePassword" class="wid208" placeholder="비밀번호 변경" /></td>
								<td class="cl_839ab5">영문소문자, 영문대문자 또는<br/>특수문자, 숫자 포함 8자리이상.</td>
							</tr>
							<tr>
								<th class="icon_tchRepassCheck">새 비밀번호 확인</th>
								<td><input type="password" id="changePasswordChk" name="changePasswordChk" class="wid208" placeholder="비밀번호 확인" /></td>
								<td><span id="errorTxt" class="info"></span></td>
							</tr>
							<tr>
								<th class="icon_tchSchool">학교정보</th>
								<td>
									<ul>
										<li id="userSch" name="userSch" class="wid240"></li>
									</ul>
								</td>
								<td></td>
							</tr>
							<tr>
								<th class="icon_member_comment">검사진행여부</th>
								<td>
									<ul>
										<li id="userSurvey" name="userSurvey" class="wid240"></li>
									</ul>
								</td>
								<td><span id="historyBtn" class="saBtn ml20"><a href="javascript:void(0)" onclick="surveyHistory();" class="btnCouponPublish layerpopupOpen" data-popup-class="mngCouponAddClass">검사이력</a></span></td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn"><a href="javascript:void(0)" onclick="chkPas()" class="btnLayerGreen">수정</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="doReset('userUpdate')" class="btnLayerGray layerClose">취소</a></span>
		</div>
	</div>
</div>

<div class="layerBg mngManageMemberSave zi2"></div>
<div class="layerPopup mngManageMemberSave style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			비밀번호를 수정하시겠습니까?<br/>관리자 계정 확인 후 수정됩니다.
		</div>
		<div class="tableWrap pB30">
			<table class="style03" summary="">
				<caption></caption>
				<colgroup>
					<col width="33%" />
					<col width="66%" />
				</colgroup>
				<tbody>
					<tr>
						<th class="icon_tchId">ID</th>
						<td colspan="4">
							<input type="text" id="adminId" />
						</td>
					</tr>
					<tr>
						<th class="icon_tchPass">비밀번호</th>
						<td colspan="4">
							<input type="password" id="adminPassword" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onClick="chkAdmin(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" onclick="doResets()" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg mngSurveyHistory"></div>
<div class="layerPopup mngSurveyHistory style01">
	<div class="layerHeader">
		<span class="title">검사이력</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<h1 id="titles" class="title"></h1>
		<div class="conContainer">
			<div class="tableWrap">
				<table id="surHistory" class="style01" summary="">
					<caption></caption>
					<colgroup>
						<col width="10%" />
						<col width="40%" />
						<col width="25%" />
						<col width="25%" />
					</colgroup>
					<thead>
						<tr>
							<th>No.</th>
							<th>검사일시</th>
							<th>검사명</th>
							<th>검사결과</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
				<div id="pagings2" class="paging"></div>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
			<a href="#" class="btnLayerGreen layerClose">닫기</a>
			</span>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#changePasswordChk').keyup(function() {
			if($('#changePassword').val() != $('#changePasswordChk').val()) {
				document.getElementById("errorTxt").className= "info";
				$('#errorTxt').text("*비밀번호와 일치하지 않습니다.");
			} else {
				$('#errorTxt').text("*비밀번호와 일치합니다.");
				document.getElementById("errorTxt").className= "info_ok";
			}
		});
	});

	function doSearch() {
		document.searchForm.submit();
	}

	function doSelectGrade(e) {
		$('#searchGrade option').remove();
		if(e == '01') {
			$('#searchGrade').append(
					"<option value='all'>전체</option>" +
					"<option value='01'>1</option>" +
					"<option value='02'>2</option>" +
					"<option value='03'>3</option>" +
					"<option value='04'>4</option>" +
					"<option value='05'>5</option>" +
					"<option value='06'>6</option>"
			)
		} else if( e == 'all') {
			$('#searchGrade').append("<option value='all'>전체</option>")
		} else {
			$('#searchGrade').append(
					"<option value='all'>전체</option>" +
					"<option value='01'>1</option>" +
					"<option value='02'>2</option>" +
					"<option value='03'>3</option>"
			)
		}
	}

	function fnPopupOpen(popupId, zIndex){
		var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	function fnPopupAccountDetail(seq) {
		$('#historyBtn').css({"display":"none"});
		fnPopupOpen('mngCouponAccountDetail');

		$.ajax({
			url    : "/mng/coupon/joinsInfoAjax",
			type   : "post",
			data   : {seq_user : seq},
			success: function(data){
				var json = jsonSet(data);
				$('#changeSeq').val(json[0].seq_user);
				$('#userNm').text(json[0].user_nm);
				$('#userId').text(json[0].user_id);
				$('#userSch').text(json[0].schlvl_nm + " " + json[0].st_grade + "학년");

				if(json[0].survey_count > 0) {
					if(json[0].survey_max == '999999'){
						$('#userSurvey').text("Y (총 N회차 중 " + json[0].survey_count + "회차 검사완료)");
					} else {
						$('#userSurvey').text("Y (총 " + json[0].survey_max + "회차 중 " + json[0].survey_count + "회차 검사완료)");
					}
					$('#historyBtn').css({"display":"block"});
				} else {
					$('#userSurvey').text("N");
				}

			}, error: function(e){
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}

	function chkPas() {

		if(!chkPwdAll($.trim($('#changePassword').val()))) {
			$('#changePassword').val('');
			$('#changePasswordChk').val('');
			$('#changePassword').focus();
			return false;
		}
		if(document.getElementById("errorTxt").className != 'info_ok') {
			return false;
		}
		fnPopupOpen('mngManageMemberSave');
	}

	function chkAdmin(e) {
		$.ajax({
			url    : "/mng/manage/adminChkAjax",
			type   : "post",
			data   : {user_id : $('#adminId').val(), password : $('#adminPassword').val()},
			success: function(data){
				if(data != 'Y') {
					fnAlertMessage("관리자 계정확인에 실패하였습니다.")
				} else {
					popupMessageDown('mngManageMemberSave.style02.zi3', 'mngManageMemberSave.zi2');

					var frmData = $('#userUpdate').serialize();

					$.ajax({
						url    : "/mng/manage/passChangeAjax",
						type   : "post",
						data   : frmData,
						success: function(data){
							popupMessageDown('mngCouponAccountDetail.style01', 'mngCouponAccountDetail');
							fnAlertMessage("비밀번호를 재설정하였습니다.");
						},
						error:function(e) {
							fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
						}
					});
				}
			},
			error:function(e) {
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
			}
		});
	}

	function doReset(frm) {
		document.getElementById(frm).reset();

	}

	function doResets() {
		$('#adminId').val("");
		$('#adminPassword').val("");
	}

	function surveyHistory() {
		popupMessageDown('mngCouponAccountDetail.style01', 'mngCouponAccountDetail');
		fnPopupOpen('mngSurveyHistory');
		var seq = $('#changeSeq').val();
		doHistory(seq, '1');
	}

	function popupwindow(tme, id, title, w, h) {
		var go = "/mng/aulandResult?seq_tme=" + tme + "&who=" + id + "&adms=Y";
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		return window.open(go, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	}

	function doHistory(seq, page) {
		$('#surHistory > tbody').children().remove();
		$.ajax({
			url    : "/mng/coupon/joinsHistoryAjax",
			type   : "post",
			data   : {seq_user:seq, currentPage:page},
			success: function(data){
				var json = jsonListSet(data);

				$('#titles').text($('#userNm').text() + "님의 검사이력");

				var infos = '';
				for(i=0; i<json.length; i++) {
						infos += '<tr>' +
								'<td>' + json[i].num  + '</th>' +
								'<td>' + json[i].reg_dt  + '</th>' +
								'<td>' + json[i].survey_typ_nm  + '</th>' +
								'<td><span class="saBtn"><a href="javascript:void(0)" onclick=popupwindow("' + json[i].seq_tme + '","' + json[i].user_id + '","result","1050","900") class="btnMngJoinsResult">검사결과</a></span></th>'
								'</tr>';
				}
				$('#surHistory > tbody').append(infos);
				doPaging2(seq, page);
			}, error: function(e){
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
				return false;
			}
		});
	}

	function doPaging2(seq, page) {
		$.ajax({
			url : "/mng/coupon/joinsHistoryCntAjax",
			type : "post",
			data : {seq_user:seq, currentPage:page},
			dataType: "html",
			success:function(data) {
				$('#pagings2').html(data);
				fnLayerResize('mngSurveyHistory');
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />