<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
	String title = "쿠폰_" + request.getAttribute("company_nm") + "_" + request.getAttribute("reg_dt");
	response.setHeader("Content-Disposition", "attachment; filename="+new String((title).getBytes("KSC5601"),"8859_1")+".xls");
	response.setHeader("Content-Description", "JSP Generated Data");
	response.setContentType("application/octet-stream");
%>

<html>
	<head>
	  <title><%=title%></title>
	  <meta http-equiv="Content-type" content="application/vns.ms-excel;charset=UTF-8">
	<style type="text/css">
	<!--
	body {font-size:10px;margin:0px;font-family:돋움;}
	table {border-collapse: collapse; width:1200px; padding:10px;margin-bottom:20px;}
	table th {background:#edf3fa;border-top:1px solid #5d99d2;border-bottom:1px solid #b6cce2; font-weight:normal;color:#5e8dbf;font-weight:bold; height:23px;text-align:center;border-right:1px solid #dfdfdf;}
	table th.caption {padding-top:10px;width:100%;height:40px;font-size:20px;color:#5e8dbf;font-weight:bold;background:#fff;border:0;}
	table td {background:#ffffff;border-bottom:1px solid #dfdfdf; font-size:10px; height:18px;border-right:1px solid #dfdfdf;padding-left:5px;text-align:center;}
	table td.alignL{text-align:left;padding-left:15px;}
	table tr td {mso-number-format:"\@";}

	-->
	</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th class="caption" colspan="21"><%=title%>_발행현황</th>
				</tr>
				<tr>
					<th>No</th>
					<th colspan="4">쿠폰번호</th>
					<th colspan="4">유효기간</th>
					<th colspan="2">검사종류</th>
					<th colspan="2">검사횟수</th>
					<th colspan="2">발행대상</th>
					<th colspan="2">가입여부</th>
					<th colspan="2">검사진행여부</th>
					<th colspan="2">가입자정보</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach var="rs" items="${list}" varStatus="sts">
				<tr>
					<td>${rs.num2}</td>
					<td colspan="4">${rs.coupon_code}</td>
					<td colspan="4">${rs.valid_start_dt} ~ ${rs.valid_end_dt}</td>
					<td colspan="2">${rs.survey_typ_nm}</td>
					<td colspan="2"><c:choose><c:when test="${rs.survey_max eq '999999'}">N</c:when><c:otherwise>${rs.survey_max}</c:otherwise></c:choose></td>
					<td colspan="2">${rs.company_nm}</td>
					<td colspan="2">${rs.use_yn}</td>
					<td colspan="2"><c:choose><c:when test="${rs.survey_count gt '0'}">Y (${rs.survey_count}/<c:choose><c:when test="${rs.survey_max eq '999999'}">N</c:when><c:otherwise>${rs.survey_max}</c:otherwise></c:choose>)</c:when><c:otherwise>N</c:otherwise></c:choose></td>
					<td colspan="2">${rs.user_nm}</td>
				</tr>
			</c:forEach>
			</tbody>
		</table>
	</body>
</html>