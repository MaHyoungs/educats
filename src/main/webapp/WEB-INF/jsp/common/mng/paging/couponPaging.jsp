<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="custom" uri="/tid/custom-taglib"%>

<c:set var="page2" value="1" />
<c:if test="${!empty param.currentPage}"><c:set var="page2" value="${param.currentPage}" /></c:if>
<c:set var="indexPerPage" value="5" />
<c:if test="${!empty param.indexPerPage}"><c:set var="indexPerPage" value="${param.indexPerPage}" /></c:if>

    <custom:paging name="paging" recordsPerPage="${recordsPerPage}" totalRecordCount="${totalRecordCount}" currentPage="${page2}" indexPerPage="${indexPerPage}">
         <%-- First --%>
        <c:if test="${!empty paging.firstPage.href}">
            <span class="start"><a href="javascript:void(0)" onclick="initSales('', '${paging.firstPage.index}');">&lt;&lt;</a></span>
        </c:if>
        <%-- Prev --%>
        <c:if test="${!empty paging.previousPage.href}">
            <span class="prev"><a href="javascript:void(0)" onclick="initSales('', '${paging.previousPage.index}')">&lt;</a> </span>
        </c:if>
        <%-- Number --%>
        <c:forEach var="numbering" items="${paging.pages}" varStatus="status">
            <c:choose>
                <c:when test="${numbering.index eq paging.currentPage}">
                    <span class="on">${numbering.index}</span>
                </c:when>
                <c:otherwise>
                    <span><a href="javascript:void(0)" onclick="initSales('', '${numbering.index}')" title="${numbering.index} 페이지">${numbering.index}</a></span>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <%-- Next --%>
        <c:if test="${!empty paging.nextPage.href}">
            <span class="next"><a href="javascript:void(0)" onclick="initSales('', '${paging.nextPage.index}')">&gt;</a></span>
        </c:if>
        <%-- Last --%>
        <c:if test="${!empty paging.lastPage.href}">
            <span class="end"><a href="javascript:void(0)" onclick="initSales('', '${paging.lastPage.index}')">&gt;&gt;</a></span>
        </c:if>
    </custom:paging>
