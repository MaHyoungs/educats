<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="2" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="2" />
		<c:param name="menu2Num" value="3" />
	</c:import>

	<div id="content" class="mngManagePlan">
		<h1 class="title">검사일정관리</h1>
		<div class="tableWrap">
			<table class="tMngClassTopInfo mB20" summary="">
				<caption></caption>
				<colgroup>
					<col width="18%" />
					<col width="19%" />
					<col width="13%" />
					<col width="18%" />
					<col width="32%" />
				</colgroup>
				<tbody>
				<tr>
					<th>학교/기관명</th>
					<td>${schInfo.schorg_nm}</td>
					<c:if test="${adm.auth_typ_cd_adm_grp eq 'Y'}">
						<td><span class="saBtn flL"><a href="#" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassClass">학교/기관 선택</a></span></td>
					</c:if>
					<c:if test="${adm.auth_typ_cd_adm_grp ne 'Y'}">
						<td><span class="saBtn flL"></span></td>
					</c:if>
					<td class="sky">검사명</td>
					<c:if test="${empty schInfo}">
						<td> - </td>
					</c:if>
					<c:if test="${not empty schInfo}">
						<td>${schInfo.survey_typ_nm} (${schInfo.lic_max}차)</td>
					</c:if>
				</tr>
				</tbody>
			</table>
		</div>
		<h1 class="info">* 회차별 검사기간을 설정해주세요.</h1>
		<h1 class="info">* 2차 검사는 1차 검사 종료 이후 가능하며, 라이선스기간 내에서 검사기간을 설정할 수 있습니다.</h1>
		<h1 class="subTitle">검사기간 설정</h1>
		<div class="tableWrap">
			<form id="plan" name="plan" action="/mng/manage/updatePlan" method="post">
				<input type="hidden" name="nowYear" value="${map.nowYear}" />
				<input type="hidden" name="lic_id" value="${schInfo.seq_lic}" />
				<input type="hidden" name="org_id" value="${schInfo.schorg_id}" />
				<table id="tmeDatas" class="style02" summary="">
					<caption></caption>
					<colgroup>
						<col width="18%" />
						<col width="82%" />
					</colgroup>
					<tbody>
					<c:if test="${not empty tmeList}">
						<c:forEach var="rs" items="${tmeList}" varStatus="sts">
							<tr class="relayItem${sts.count}">
								<th>${rs.num}회차</th>
								<td class="disFlex">
									<div class="select">
										<input type="hidden" id="old_proc_typ${sts.count}" name="old_proc_typ${sts.count}" value="${rs.stat_cd}" />
										<select id="proc_typ${sts.count}" name="proc_typ" onchange="selection(this.value, '${sts.count}')">
											<c:forEach var="proc" items="${procList}">
												<option value="${proc.code}" <c:if test="${proc.code eq rs.stat_cd}">selected="selected"</c:if>>${proc.code_nm}</option>
											</c:forEach>
										</select>
									</div>
									<div class="select">
										<label for="st_year_cd${sts.count}">학년도</label>
										<select id="st_year_cd${sts.count}" name="st_year_cd">
											<option value="${rs.st_year_cd}">${rs.st_year_cd}</option>
										</select>
									</div>
									<div class="select calendar">
										<input type="text" id="tmeS_dt${sts.count}" name="tmeS_dt" value="${rs.start_dt}" class="calendar"/>
										~
										<input type="text" id="tmeE_dt${sts.count}" name="tmeE_dt" value="${rs.end_dt}" class="calendar"/>
									</div>

									<div class="select checkbox">
										<input type="checkbox" id="continue${sts.count}" name="continueChk" onclick="dateContinue('${sts.count}', '${schInfo.start_dt}', '${schInfo.end_dt}')" class="poR top4" />
										<input type="hidden" name="seq_tme" value="${rs.seq_tme}"/>
										<label for="continue${sts.count}">계속</label>
									</div>
								</td>
							</tr>
						</c:forEach>
					</c:if>
					</tbody>
				</table>
			</form>
		</div>
		<div class="btnArea">
			<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGreen layerpopupOpen" onclick="check('mngManagePlanSaveSubmit')">저장</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="cancels()" class="btnLayerGray">취소</a></span>
		</div>
	</div>
</div>

<div class="layerBg mngManagePlanSaveSubmit zi2"></div>
<div class="layerPopup mngManagePlanSaveSubmit style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			저장하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doSave()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>

<c:import url="/mng/layer/schoolOrgan" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
	<c:param name="nowYear" value="${map.nowYear}"/>
</c:import>

<script>
	$(document).ready(function(){
		<c:if test="${empty schInfo}">
			var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup.mngManageClassClass").width()
				,popupHei = $(".layerPopup.mngManageClassClass").height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

			$(".layerBg.mngManageClassClass").css({"display":"block"}).attr("data-popup-this", 'mngManageClassClass');
			$(".layerPopup.mngManageClassClass").css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", 'mngManageClassClass');
		</c:if>

		<c:forEach var="rss" items="${tmeList}" varStatus="sts">
			// 회차별 날자
			$("#tmeS_dt" + ${sts.count}).datepicker({dateFormat: "yy-mm-dd", minDate: new Date('${schInfo.start_dt}'), maxDate: new Date('${schInfo.end_dt}'), changeYear: true, changeMonth: true});
			$("#tmeE_dt" + ${sts.count}).datepicker({dateFormat: "yy-mm-dd", minDate: new Date('${schInfo.start_dt}'), maxDate: new Date('${schInfo.end_dt}'), changeYear: true, changeMonth: true});

			// 대기 & 완료 disabled 처리
			<c:if test="${rss.stat_cd eq '01'}">
				$('.relayItem' + ${sts.count + 1} + ' input, .relayItem' + ${sts.count + 1} + ' select').attr('disabled', true);
			</c:if>
			<c:if test="${rss.stat_cd eq '03'}">
				$('.relayItem' + ${sts.count} + ' input, .relayItem' + ${sts.count} + ' select').attr('disabled', true);
			</c:if>
		</c:forEach>
	});

	function fnPopupOpen(popupId, zIndex){
		var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	$(".messageClose, .messageBg").click(function(e){
		e.preventDefault();
		$(this).parents(".messageLayerWrap").remove();
	});

	function check(layer){
		var cnt = 0;
		var yn = true;
		$('select[name=proc_typ] option:selected').each(function(){
			var ids = $(this).parent().attr("id");
			ids = ids.replace(/proc_typ/gi, '');

			if($(this).attr('value') == '01'){
				if($('#tmeS_dt' + ids).val() != "" || $('#tmeE_dt' + ids).val() != ""){
					if($('#tmeS_dt' + ids).val() == "") {
						fnAlertMessage('시작일을 입력하세요.');
						yn = false;
						return false;
					}
					if($('#tmeE_dt' + ids).val() == "") {
						fnAlertMessage('종료일을 입력하세요.');
						yn = false;
						return false;
					}
					if($('#tmeS_dt' + ids).val() > $('#tmeE_dt' + ids).val()){
						fnAlertMessage('시작일은 종료일과 같거나 과거이어야 합니다.');
						yn = false;
						return false;
					}
				}
			}
			if($(this).attr('value') != '01'){
				if($('#tmeS_dt' + ids).val() == "" || $('#tmeE_dt' + ids).val() == ""){
					fnAlertMessage(ids + '회차 시작일/종료일을 입력하세요.');
					yn = false;
					return false;
				}
				if($('#tmeS_dt' + ids).val() > $('#tmeE_dt' + ids).val()){
					fnAlertMessage('시작일은 종료일과 같거나 과거이어야 합니다.');
					yn = false;
					return false;
				}
			}
			if($(this).attr('value') == '02'){
				var dus = $(this).parent().attr("id");
				dus = dus.replace(/proc_typ/gi, '');
				if(dus > 1){
					if($('#proc_typ' + (dus - 1)).val() != '03'){
						fnAlertMessage('이전 회차가 완료상태가 아닙니다.');
						yn = false;
						return false;
					}
				}
				cnt++;
			}
		});
		if(cnt > 1){
			fnAlertMessage('진행중인 회차를 하나만 선택하세요.');
			yn = false;
			return false;
		}
		if(yn == true){
			fnPopupOpen(layer);
		}
	}

	function doSave() {
		popupMessageDown('mngManagePlanSaveSubmit.style02.zi3', 'mngManagePlanSaveSubmit.zi2');
		var frm = document.plan;
		frm.submit();
	}

	function cancels() {
		location.href = "/mng/manage/plan?seq_lic=${schInfo.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${map.nowYear}";
	}

	function selection(e, num) {
		if(e == '02') {
			$('#proc_typ' + (num -1)).val('03');
		}
	}

	function dateContinue(sts, sdt, edt) {
		$('#tmeS_dt' + sts).val(sdt);
		$('#tmeE_dt' + sts).val(edt);
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />