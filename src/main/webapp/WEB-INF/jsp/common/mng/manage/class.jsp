<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="2" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="2" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngManageClass">
		<h1 class="title">학교/기관 선택</h1>
		<h1 class="info">* 관리할 학교 또는 기관을 선택하세요.</h1>
		<form id="searchForm" name="searchForm" action="/mng/manage/class" method="post">
			<div class="searchWrap">
				<div class="section">
					<label for="searchArea">지역</label>
					<div class="select">
						<select id="searchArea" class="" name="searchArea">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${areaList}">
								<option value="${rs.code}" <c:if test="${rs.code eq map.searchArea}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchDiv">구분</label>
					<div class="select">
						<select id="searchDiv" class="" name="searchDiv">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${schList}">
								<option value="${rs.code}" <c:if test="${rs.code eq map.searchDiv}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchNm">학교/기관명</label>
					<div class="select wid180">
						<input type="text" id="searchNm" name="searchNm" class="wid100p" value="${map.searchNm}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="16%" />
					<col width="23%" />
					<col width="38%" />
					<col width="23%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>학교/기관명</th>
						<th>구분</th>
						<th>지역</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach var="rs" items="${organList}" >
					<tr>
						<th>${rs.num}</th>
						<td><a href="/mng/manage/classConfig?org_id=${rs.schorg_id}&nowYear=${map.nowYear}">${rs.schorg_nm}</a></td>
						<td>${rs.schorg_typ_nm}</td>
						<td>${rs.are_nm}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<script>
	function doSearch() {
		document.searchForm.submit();
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />