<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="2" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="2" />
		<c:param name="menu2Num" value="2" />
	</c:import>

	<div id="content" class="mngManageClass">
		<h1 class="title">선생님 관리</h1>
		<div class="tableWrap">
			<table class="tMngClassTopInfo mB20" summary="">
				<caption></caption>
				<colgroup>
					<col width="17%" />
					<col width="20%" />
					<col width="63%" />
				</colgroup>
				<tbody>
					<tr>
						<th>학교/기관명</th>
						<td>${schInfo.schorg_nm}</td>
						<c:if test="${adm.auth_typ_cd_adm_grp eq 'Y'}">
							<td><span class="saBtn flL"><a href="#" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassClass">학교/기관 선택</a></span></td>
						</c:if>
						<c:if test="${adm.auth_typ_cd_adm_grp ne 'Y'}">
							<td><span class="saBtn flL"></span></td>
						</c:if>
					</tr>
				</tbody>
			</table>
		</div>
		<form id="tchSeach" name="tchSeach" action="/mng/manage/teacher" method="post">
			<input type="hidden" name="seq_lic" value="${map.seq_lic}" />
			<input type="hidden" name="org_id" value="${schInfo.schorg_id}" />
			<div class="searchWrap">
				<div class="section">
					<label for="nowYear">학년도</label>
					<div class="select">
						<select id="nowYear" name="nowYear">
							<option value="all">전체</option>
							<c:forEach var="rs" items="${yearList}">
								<option value="${rs.code}" <c:if test="${map.nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="confrmTyp">상태</label>
					<div class="select">
						<select id="confrmTyp" class="" name="confrmTyp">
							<option value="all" <c:if test="${map.confrmTyp eq 'all'}">selected="selected"</c:if>>전체</option>
							<option value="Y" <c:if test="${map.confrmTyp eq 'Y'}">selected="selected"</c:if>>승인</option>
							<option value="N" <c:if test="${map.confrmTyp eq 'N'}">selected="selected"</c:if>>반려</option>
							<option value="L" <c:if test="${map.confrmTyp eq 'L'}">selected="selected"</c:if>>대기</option>
						</select>
					</div>
				</div>
				<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
					<div class="section">
						<label for="grade">학년</label>
						<div class="select">
							<select id="grade" name="grade" onchange="doClass(this.value)">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${gradeist}">
									<option value="${rs.code}" <c:if test="${map.grade eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</c:if>
				<div class="section">
					<label for="classs">
						<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">반</c:if>
						<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">학급</c:if>
					</label>
					<div class="select">
						<select id="classs" name="classs">
							<option value="all">전체</option>
							<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<c:forEach var="rs" items="${classList}">
									<option value="${rs.st_class}" <c:if test="${map.classs eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
								</c:forEach>
							</c:if>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<c:if test="${not empty map.grade}">
									<c:forEach var="rs" items="${classList}">
										<option value="${rs.st_class}" <c:if test="${map.classs eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
									</c:forEach>
								</c:if>
							</c:if>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchNames">이름</label>
					<div class="select wid100">
						<input type="text" id="searchNames" name="searchNames" class="wid100" value="${map.searchNames}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tabSection">
			<span class="saBtn"><a href="#" class="btnMngClassTabGray layerpopupOpen" data-popup-class="mngManageTeacherAdd">선생님 등록</a></span>
		</div>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="5%" />
					<col width="10%" />
					<col width="25%" />
					<col width="10%" />
					<c:choose>
						<c:when test="${schInfo.schorg_typ_cd eq 'ORG01'}">
							<col width="20%" />
						</c:when>
						<c:otherwise>
							<col width="10%" />
							<col width="10%" />
						</c:otherwise>
					</c:choose>
					<col width="10%" />
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>이름</th>
						<th>이메일</th>
						<th>학년도</th>
						<c:choose>
							<c:when test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<th>학급</th>
							</c:when>
							<c:otherwise>
								<th>학년</th>
								<th>반</th>
							</c:otherwise>
						</c:choose>
						<th>상태</th>
						<th>승인/반려</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="rs" items="${teacherList}" varStatus="sts">
						<tr>
							<td>${rs.num}</td>
							<td><a href="javascript:void(0)" onclick="tchDetail('${rs.seq_user}', '${rs.user_id}', '${rs.user_nm}','${rs.confm_yn}', '${rs.st_year_cd}', '${rs.schorg_id}', '${schInfo.schorg_typ_cd}')" class="layerpopupOpen" data-popup-class="mngManageTeacherInfoSchool">${rs.user_nm}</a></td>
							<td>${rs.user_id}</td>
							<td>${rs.st_year_cd}</td>
							<c:choose>
								<c:when test="${schInfo.schorg_typ_cd eq 'ORG01'}">
									<td>${rs.st_class}</td>
								</c:when>
								<c:otherwise>
									<td>${rs.st_grade}</td>
									<td>${rs.st_class}</td>
								</c:otherwise>
							</c:choose>
							<c:if test="${rs.user_type eq 'R'}">
								<td>승인</td>
								<td>&nbsp;</td>
							</c:if>
							<c:if test="${rs.user_type eq 'N'}">
								<c:if test="${empty rs.confm_yn}">
									<td>대기</td>
									<td>
										<span class="saBtn"><a href="javascript:void(0)" onclick="doTchSet('N','Y', '${rs.seq_user}', '${rs.st_year_cd}','${rs.st_grade_cd}', '${rs.st_class}')" class="btnMngTeacherApprove layerpopupOpen" data-popup-class="mngManageTeacherApprove">승인</a></span>
										<span class="saBtn"><a href="javascript:void(0)" onclick="doTchSet('N','N', '${rs.seq_user}', '${rs.st_year_cd}','${rs.st_grade_cd}', '${rs.st_class}')" class="btnMngTeacherReturn layerpopupOpen" data-popup-class="mngManageTeacherReturn">반려</a></span>
									</td>
								</c:if>
								<c:if test="${rs.confm_yn eq 'N'}">
									<td>반려</td>
									<td>&nbsp;</td>
								</c:if>
								<c:if test="${rs.confm_yn eq 'Y'}">
									<td>승인</td>
									<td>&nbsp;</td>
								</c:if>
							</c:if>
							<c:if test="${rs.user_type eq 'H'}">
								<c:if test="${rs.stat_cd eq '01'}">
									<td>대기</td>
									<td>
										<span class="saBtn"><a href="javascript:void(0)" onclick="doTchSet('H', 'Y', '${rs.seq_user}', '${rs.st_year_cd}','${rs.st_grade_cd}', '${rs.st_class}')" class="btnMngTeacherApprove layerpopupOpen" data-popup-class="mngManageTeacherApprove">승인</a></span>
										<span class="saBtn"><a href="javascript:void(0)" onclick="doTchSet('H', 'N', '${rs.seq_user}', '${rs.st_year_cd}','${rs.st_grade_cd}', '${rs.st_class}')" class="btnMngTeacherReturn layerpopupOpen" data-popup-class="mngManageTeacherReturn">반려</a></span>
									</td>
								</c:if>
								<c:if test="${rs.stat_cd eq '02'}">
									<td>반려</td>
									<td>&nbsp;</td>
								</c:if>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
		</div>
	</div>
</div>

<form id="setDataaa" name="setDataaa" action="/mng/manage/tchUpdateAjax" method="post">
	<input type="hidden" id="typ" name="typ" />
	<input type="hidden" id="seq" name="seq" />
</form>

<%-- Layer --%>
<div class="layerBg mngManageTeacherApprove zi2"></div>
<div class="layerPopup mngManageTeacherApprove style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청을 승인 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdatees(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg mngManageTeacherReturn zi2"></div>
<div class="layerPopup mngManageTeacherReturn style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			신청을 반려 하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdatees(this); return false;" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<form id="tchForm" name="tchForm" action="/mng/manage/tchUpdateAjax" method="post">
	<input type="hidden" id="tchTyp" name="tchTyp" />
	<input type="hidden" id="tchCon" name="tchCon" />
	<input type="hidden" id="tchSeq" name="tchSeq" />
	<input type="hidden" id="tchYea" name="tchYea" />
	<input type="hidden" id="tchGra" name="tchGra" />
	<input type="hidden" id="tchCla" name="tchCla" />
	<input type="hidden" id="tchOrg" name="tchOrg" value="${schInfo.schorg_id}" />
	<input type="hidden" id="orgTyp" name="orgTyp" value="${schInfo.schorg_typ_cd}" />
</form>

<c:import url="/mng/layer/teacherInsert" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<c:import url="/mng/layer/teacherDetail" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<c:import url="/mng/layer/schoolOrganB" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
	<c:param name="nowYear" value="${map.nowYear}"/>
</c:import>

<script>
	$(document).ready(function(){
		<c:if test="${empty schInfo}">
			var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup.mngManageClassClass").width()
				,popupHei = $(".layerPopup.mngManageClassClass").height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;
			$(".layerBg.mngManageClassClass").css({"display":"block"}).attr("data-popup-this", 'mngManageClassClass');
			$(".layerPopup.mngManageClassClass").css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", 'mngManageClassClass');
		</c:if>
		
		<c:if test="${not empty param.msg}">
			<c:choose>
			<c:when test="${param.msg eq 'N'}">
				fnAlertMessage("동일한 아이디가 있습니다.");
			</c:when>
			<c:when test="${param.msg eq 'F'}">
				fnAlertMessage("선택된 학급에 배정할 수 있는 선생님은 최대 3명입니다.");
			</c:when>
			<c:when test="${param.msg eq 'E'}">
				fnAlertMessage("존재하지 않는 학급을 선택했습니다.");
			</c:when>
			</c:choose>
		</c:if>
	});

	function doSearch() {
		var frm = document.tchSeach;
		frm.submit();
	}

	function doTchSet(typ, con, seq, yea, gra, cla) {
		$('#tchTyp').val(typ);
		$('#tchCon').val(con);
		$('#tchSeq').val(seq);
		$('#tchYea').val(yea);
		$('#tchGra').val(gra);
		$('#tchCla').val(cla);
	}

	function doUpdatees(e) {
		var frmData = $('#tchForm').serialize();

		$.ajax({
			url    : "/mng/manage/tchUpdateAjax",
			type   : "post",
			data   : frmData,
			success: function(data){
				if(data == "N") {
					popupMessageDown('mngManageTeacherApprove.style02.zi3', 'mngManageTeacherApprove.zi2');
					popupMessageDown('mngManageTeacherReturn.style02.zi3', 'mngManageTeacherReturn.zi2');
					fnAlertMessage("신청 학급에 선생님들이 모두 정해져있습니다.");
					return false;
				} else if(data == "deny") {
					popupMessageDown('mngManageTeacherApprove.style02.zi3', 'mngManageTeacherApprove.zi2');
					popupMessageDown('mngManageTeacherReturn.style02.zi3', 'mngManageTeacherReturn.zi2');
					fnAlertUrlMessage("반려 처리 되었습니다.", "/mng/manage/teacher?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}");
				} else {
					popupMessageDown('mngManageTeacherApprove.style02.zi3', 'mngManageTeacherApprove.zi2');
					popupMessageDown('mngManageTeacherReturn.style02.zi3', 'mngManageTeacherReturn.zi2');
					fnAlertUrlMessage("승인 처리 되었습니다.", "/mng/manage/teacher?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}");
				}
			},
			error: function(e) {
				return false;
			}
		});
	}

	function doClass(val) {
		$.ajax({
			url    : "/mng/manage/classListAjaxss",
			type   : "post",
			data   : {nowYear : '${map.nowYear}', grade : val, org_id : '${map.org_id}'},
			success: function(data){
				var json = jsonListSet(data);

				$('#classs option').remove();
				$('#classs').append($('<option value="all">전체</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
					$('#classs').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />