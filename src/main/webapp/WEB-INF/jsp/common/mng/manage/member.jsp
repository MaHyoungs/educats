<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="2" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="2" />
		<c:param name="menu2Num" value="4" />
	</c:import>

	<div id="content" class="mngManageMember">
		<h1 class="title">구성원관리</h1>
		<div class="tableWrap">
			<table class="tMngClassTopInfo mB20" summary="">
				<caption></caption>
				<colgroup>
					<col width="18%" />
					<col width="19%" />
					<col width="13%" />
					<col width="18%" />
					<col width="32%" />
				</colgroup>
				<tbody>
					<tr>
						<th>학교/기관명</th>
						<td>${schInfo.schorg_nm}</td>
						<c:if test="${adm.auth_typ_cd_adm_grp eq 'Y'}">
							<td><span class="saBtn flL"><a href="#" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassClass">학교/기관 선택</a></span></td>
						</c:if>
						<c:if test="${adm.auth_typ_cd_adm_grp ne 'Y'}">
							<td><span class="saBtn flL"></span></td>
						</c:if>
						<td class="sky">학년도</td>
						<td>
							<div class="select">
								<select id="nowYear" name="nowYear" onchange="doYearChange(this.value)">
									<c:forEach var="rs" items="${yearList}">
										<option value="${rs.code}" <c:if test="${map.nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</select>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<form id="memSeach" name="memSeach" action="/mng/manage/member" method="post">
			<input type="hidden" name="seq_lic" value="${map.seq_lic}"/>
			<input type="hidden" name="org_id" value="${schInfo.schorg_id}"/>
			<div class="searchWrap">
				<div class="section">
					<label for="userTyp">구분</label>
					<div class="select">
						<select id="userTyp" name="userTyp">
							<option value="all">전체</option>
							<option value="04" <c:if test="${map.userTyp eq '04'}">selected="selected"</c:if>>선생님</option>
							<option value="02" <c:if test="${map.userTyp eq '02'}">selected="selected"</c:if>>학생</option>
						</select>
					</div>
				</div>
				<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
					<div class="section">
						<label for="grade">학년</label>
						<div class="select">
							<select id="grade" name="grade" onchange="doClass(this.value)">
								<option value="all">전체</option>
								<c:forEach var="rs" items="${gradeist}">
									<option value="${rs.code}" <c:if test="${map.grade eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</div>
					</div>
				</c:if>
				<div class="section">
					<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
						<label for="classs">반</label>
					</c:if>
					<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
						<label for="classs">학급</label>
					</c:if>
					<div class="select">
						<select id="classs" name="classs">
							<option value="all">전체</option>
							<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<c:forEach var="rs" items="${classList}">
									<option value="${rs.st_class}" <c:if test="${map.classs eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
								</c:forEach>
							</c:if>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<c:if test="${not empty map.grade}">
									<c:forEach var="rs" items="${classList}">
										<option value="${rs.st_class}" <c:if test="${map.classs eq rs.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
									</c:forEach>
								</c:if>
							</c:if>
						</select>
					</div>
				</div>
				<div class="section">
					<label for="searchNames">이름</label>
					<div class="select wid100">
						<input type="text" id="searchNames" name="searchNames" class="wid100" value="${map.searchNames}" />
					</div>
				</div>
				<div class="section">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnMngJoinsSearch">검색</a></span>
				</div>
			</div>
		</form>
		<div class="tableWrap">
			<table class="style01" summary="">
				<caption></caption>
				<colgroup>
					<col width="6%" />
					<col width="11%" />
					<col width="23%" />
					<col width="9%" />
					<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
						<col width="13%" />
						<col width="9%" />
						<col width="9%" />
					</c:if>
					<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
						<col width="31%" />
					</c:if>
					<col width="20%" />
				</colgroup>
				<thead>
					<tr>
						<th>No</th>
						<th>이름</th>
						<th>이메일</th>
						<th>구분</th>
						<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
							<th>학년</th>
							<th>반</th>
							<th>번호</th>
						</c:if>
						<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
							<th>학급</th>
						</c:if>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="ls" items="${memberList}" varStatus="sts" >
						<tr>
							<td>${ls.num}</td>
							<%--<td><a href="javascript:void(0)" class="layerpopupOpen" data-popup-class="mngManageTeacherInfoSchool">${ls.user_nm}</a></td>--%>
							<td>${ls.user_nm}</td>
							<td>${ls.user_id}</td>
							<td>
								<c:if test="${ls.user_typ_cd eq '02'}">학생</c:if>
								<c:if test="${ls.user_typ_cd eq '04'}">선생님</c:if>
							</td>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<c:if test="${ls.user_typ_cd eq '02'}">
									<td>${ls.st_grade}</td>
									<td>${ls.st_class}</td>
									<td>${ls.st_number}</td>
								</c:if>
								<c:if test="${ls.user_typ_cd eq '04'}">
									<td>-</td>
									<td>-</td>
									<td>-</td>
								</c:if>
							</c:if>
							<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<td>${ls.st_class}</td>
							</c:if>
							<td>
								<span class="saBtn"><a href="javascript:void(0)" onclick="memSet('${ls.seq_user}', '${ls.user_id}', '${ls.user_nm}')" class="btnMngGreen layerpopupOpen" data-popup-class="mngManageMemberPassowrdChange">비밀번호 재설정</a></span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:if test="${not empty schInfo}">
				<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
			</c:if>
		</div>
	</div>
</div>

<div class="layerBg mngManageMemberPassowrdChange"></div>
<div class="layerPopup mngManageMemberPassowrdChange style01">
	<div class="layerHeader">
		<span class="title">비밀번호 재설정</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="conContainer">
			<div class="tableWrap">
				<form id="passwordForm" name="passwordForm" action="/mng/manage/passChangeAjax" method="post">
					<input type="hidden" id="changeSeq" name="changeSeq" />
					<table class="style03 wid495 mA20" summary="">
						<caption></caption>
						<colgroup>
							<col width="33%" />
							<col width="66%" />
						</colgroup>
						<tbody>
							<tr>
								<th class="icon_tchName">이름</th>
								<td colspan="4">
									<ul>
										<li id="selectNm"></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th class="icon_tchId">ID(이메일)</th>
								<td colspan="4">
									<ul>
										<li id="selectId"></li>
									</ul>
								</td>
							</tr>
							<tr>
								<th class="icon_tchPass">비밀번호</th>
								<td colspan="4">
									<input type="password" id="changePassword" name="changePassword" />
									<span id="ruleTxt" class="info">*영문소문자, 영문대문자 또는 특수문자, 숫자 포함 8자리 이상</span>
								</td>
							</tr>
							<tr>
								<th class="icon_tchRepassCheck">비밀번호 확인</th>
								<td colspan="4">
									<input type="password" id="changePasswordChk" name="changePasswordChk" />
									<span id="errorTxt" class="info"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="conFooter">
			<span class="saBtn">
				<span class="saBtn"><a href="javascript:void(0)" onclick=chkPas() class="btnLayerGreen">수정</a></span>
				<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
			</span>
		</div>
	</div>
</div>

<div class="layerBg mngManageMemberSave zi2"></div>
<div class="layerPopup mngManageMemberSave style02 zi3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			비밀번호를 수정하시겠습니까?<br/>관리자 계정 확인 후 수정됩니다.
		</div>
		<div class="tableWrap pB30">
			<table class="style03" summary="">
				<caption></caption>
				<colgroup>
					<col width="33%" />
					<col width="66%" />
				</colgroup>
				<tbody>
					<tr>
						<th class="icon_tchId">ID</th>
						<td colspan="4">
							<input type="text" id="adminId" />
						</td>
					</tr>
					<tr>
						<th class="icon_tchPass">비밀번호</th>
						<td colspan="4">
							<input type="password" id="adminPassword" />
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onClick="chkAdmin(this)" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%--
<input type="hidden" id="memSeq" name="memSeq" />
<input type="hidden" id="memNm" name="memNm" />
<input type="hidden" id="memId" name="memId" />
--%>

<c:import url="/mng/layer/schoolOrganB" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
	<c:param name="nowYear" value="${map.nowYear}"/>
</c:import>

<!-- main End -->
<script>
	$(document).ready(function(){
		<c:if test="${empty schInfo}">
			var winWid = $(window).width()
					,winHei = $(window).height()
					,popupWid = $(".layerPopup.mngManageClassClass").width()
					,popupHei = $(".layerPopup.mngManageClassClass").height()
					,popupTop = (winHei-popupHei)/2
					,popupLeft = (winWid-popupWid)/2;

			$(".layerBg.mngManageClassClass").css({"display":"block"}).attr("data-popup-this", 'mngManageClassClass');
			$(".layerPopup.mngManageClassClass").css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", 'mngManageClassClass');
		</c:if>

		$('#changePasswordChk').keyup(function() {
			if($('#changePassword').val() != $('#changePasswordChk').val()) {
				document.getElementById("errorTxt").className= "info";
				$('#errorTxt').text("*비밀번호와 일치하지 않습니다.");
			} else {
				$('#errorTxt').text("*비밀번호와 일치합니다.");
				document.getElementById("errorTxt").className= "info_ok";
			}
		});
	});

	function fnPopupOpen(popupId, zIndex){
		var winWid = $(window).width()
				,winHei = $(window).height()
				,popupWid = $(".layerPopup."+popupId).width()
				,popupHei = $(".layerPopup."+popupId).height()
				,popupTop = (winHei-popupHei)/2
				,popupLeft = (winWid-popupWid)/2;

		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	function doYearChange(year) {
		<c:if test="${empty map.seq_lic}">
			document.location.href = "/mng/manage/member?org_id=${schInfo.schorg_id}&nowYear=" + year;
		</c:if>
		<c:if test="${not empty map.seq_lic}">
			document.location.href = "/mng/manage/member?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=" + year;
		</c:if>
	}

	function doSearch() {
		var frm = document.memSeach;
		frm.submit();
	}

	function memSet(seq, id, nm) {
		$('#changeSeq').val(seq);
		$('#selectNm').text(nm + " 선생님");
		$('#selectId').text(id);
	}

	function chkPas() {

		if(!chkPwdAll($.trim($('#changePassword').val()))) {
			$('#changePassword').val('');
			$('#changePasswordChk').val('');
			$('#changePassword').focus();
			return false;
		}
		if(document.getElementById("errorTxt").className != 'info_ok') {
			return false;
		}
		fnPopupOpen('mngManageMemberSave');
	}

	function chkAdmin(e) {
		$.ajax({
			url    : "/mng/manage/adminChkAjax",
			type   : "post",
			data   : {user_id : $('#adminId').val(), password : $('#adminPassword').val()},
			success: function(data){
				if(data != 'Y') {
					fnAlertMessage("관리자 계정확인에 실패하였습니다.")
				} else {
					popupMessageDown('mngManageMemberSave.style02.zi3', 'mngManageMemberSave.zi2');

					var frmData = $('#passwordForm').serialize();

					$.ajax({
						url    : "/mng/manage/passChangeAjax",
						type   : "post",
						data   : frmData,
						success: function(data){
							popupMessageDown('mngManageMemberPassowrdChange.style01', 'mngManageMemberPassowrdChange');
							fnAlertMessage("비밀번호를 재설정하였습니다.");
						},
						error:function(e) {
							fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
						}
					});
				}
			},
			error:function(e) {
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
			}
		});
	}

	function doClass(val) {
		$.ajax({
			url    : "/mng/manage/classListAjaxss",
			type   : "post",
			data   : {nowYear : '${map.nowYear}', grade : val, org_id : '${map.org_id}'},
			success: function(data){
				var json = jsonListSet(data);

				$('#classs option').remove();
				$('#classs').append($('<option value="all">전체</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
					$('#classs').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />