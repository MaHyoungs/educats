<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/mng/top" charEncoding="utf-8">
	<c:param name="menu1Num" value="2" />
</c:import>

<div id="container" class="adminContainer">
	<c:import url="/mng/left" charEncoding="utf-8">
		<c:param name="menu1Num" value="2" />
		<c:param name="menu2Num" value="1" />
	</c:import>

	<div id="content" class="mngManageClass">
		<h1 class="title">학급관리</h1>
		<div class="tableWrap">
			<table class="tMngClassTopInfo mB20" summary="">
				<caption></caption>
				<colgroup>
					<col width="17%" />
					<col width="20%" />
					<col width="13%" />
					<col width="17%" />
					<col width="33%" />
				</colgroup>
				<tbody>
					<tr>
						<th>학교/기관명</th>
						<td>${schInfo.schorg_nm}</td>
						<c:if test="${adm.auth_typ_cd_adm_grp eq 'Y'}">
							<td><span class="saBtn"><a href="#" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassClass">학교/기관 선택</a></span></td>
						</c:if>
						<c:if test="${adm.auth_typ_cd_adm_grp ne 'Y'}">
							<td><span class="saBtn"></span></td>
						</c:if>
						<td class="sky">학년도</td>
						<td>
							<select id="nowYear" name="nowYear" onchange="doYearChange(this.value)">
								<c:forEach var="rs" items="${yearList}">
									<option value="${rs.code}" <c:if test="${nowYear eq rs.code}">selected="selected"</c:if>>${rs.code_nm}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<h1 class="subTitle">학급설정</h1>
		<div class="tabSection">
			<span class="saBtn"><a href="javascript:void(0)" onclick="doAdd()" class="btnMngClassTabGray">학급추가</a></span>
			<c:if test="${empty map.seq_lic}">
				<span class="saBtn"><a href="/mng/manage/classConfig?org_id=${schInfo.schorg_id}&nowYear=${nowYear}" class="btnMngClassTabGray">초기화</a></span>
			</c:if>
			<c:if test="${not empty map.seq_lic}">
				<span class="saBtn"><a href="/mng/manage/classConfig?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=${nowYear}" class="btnMngClassTabGray">초기화</a></span>
			</c:if>
			<span class="saBtn"><a href="javascript:void(0)" onclick="doDataSet()" class="btnMngClassTabGreen layerpopupOpen">저장</a></span>
		</div>
		<div class="tableWrap">
			<table id="classLists" class="style01" summary="">
				<caption></caption>
				<colgroup>
					<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
						<col width="20%" />
						<col width="23%" />
						<col width="23%" />
						<col width="23%" />
						<col width="11%" />
					</c:if>
					<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
						<col width="15%" />
						<col width="14%" />
						<col width="20%" />
						<col width="20%" />
						<col width="20%" />
						<col width="11%" />
					</c:if>
				</colgroup>
				<thead>
					<tr>
						<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
							<th>학급명</th>
						</c:if>
						<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
							<th>학년</th>
							<th>반</th>
						</c:if>
						<th>담당선생님1</th>
						<th>담당선생님2</th>
						<th>담당선생님3</th>
						<th>삭제</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="cList" items="${classList}" varStatus="sts">
						<tr>
							<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
								<td class="classRow">
									<select id="st_grade_cd" name="st_grade_cd" onchange="changeGrade(this, this.value, '${schInfo.schorg_typ_cd}', '${nowYear}', '${schInfo.schorg_id}')">
										<c:forEach var="rs" items="${gradeist}">
											<option value="${rs.code}" <c:if test="${rs.code eq cList.st_grade_cd}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:forEach>
									</select>
									<input type="hidden" name="org_st_grade_cd" value="${cList.st_grade_cd}" />
								</td>
								<td>
									<input type="text" name="st_class" value="${cList.st_class}" onchange="changeClass(this, this.value, '${schInfo.schorg_typ_cd}', '${nowYear}', '${schInfo.schorg_id}')"/>
								</td>
								<input type="hidden" name="org_st_class" value="${cList.st_class}" />
							</c:if>
							<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
								<td>
									<input type="text" name="st_class" value="${cList.st_class}" onchange="changeClass(this, this.value, '${schInfo.schorg_typ_cd}', '${nowYear}', '${schInfo.schorg_id}')"/>
									<input type="hidden" name="org_st_class" value="${cList.st_class}" />
								</td>
							</c:if>
								<input type="hidden" name="seqClass" value="${cList.seq_class}" />
							<c:if test="${not empty classTeacherList}">
								<c:forEach var="cTeachList" items="${classTeacherList}">
										<c:if test="${cList.seq_class eq cTeachList.seq_class and cList.cnt eq '3'}">
											<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
											<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
										</c:if>
										<c:if test="${cList.seq_class eq cTeachList.seq_class and cList.cnt eq '2'}">
											<c:if test="${cList.lowNum eq '1' and cList.maxNum eq '2'}">
												<c:if test="${cTeachList.tch_num eq '1'}">
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
												</c:if>
												<c:if test="${cTeachList.tch_num eq '2'}">
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
													<td><input type="text" readonly="readonly" name="tch_num3" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid3" value="" />
												</c:if>
											</c:if>
											<c:if test="${cList.lowNum eq '1' and cList.maxNum eq '3'}">
												<c:if test="${cTeachList.tch_num eq '1'}">
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
													<td><input type="text" readonly="readonly" name="tch_num2" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid2" value="" />
												</c:if>
												<c:if test="${cTeachList.tch_num eq '3'}">
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
												</c:if>
											</c:if>
											<c:if test="${cList.lowNum eq '2' and cList.maxNum eq '3'}">
												<c:if test="${cTeachList.tch_num eq '2'}">
													<td><input type="text" readonly="readonly" name="tch_num1" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid1" value="" />
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
												</c:if>
												<c:if test="${cTeachList.tch_num eq '3'}">
													<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num${cTeachList.tch_num}')"></i></div><input type="text" readonly="readonly" name="tch_num${cTeachList.tch_num}" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num${cTeachList.tch_num}')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
													<input type="hidden" name="tch_hid${cTeachList.tch_num}" value="${cTeachList.seq_user}" />
												</c:if>
											</c:if>
										</c:if>
										<c:if test="${cList.seq_class eq cTeachList.seq_class and cList.cnt eq '1'}">
											<c:if test="${cList.maxNum eq '1'}" >
												<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num1')"></i></div><input type="text" readonly="readonly" name="tch_num1" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num1')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid1" value="${cTeachList.seq_user}" />
												<td><input type="text" readonly="readonly" name="tch_num2" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num2')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid2" value="" />
												<td><input type="text" readonly="readonly" name="tch_num3" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid3" value="" />
											</c:if>
											<c:if test="${cList.maxNum eq '2'}" >
												<td><input type="text" readonly="readonly" name="tch_num1" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num1')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid1" value="" />
												<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num2')"></i></div><input type="text" readonly="readonly" name="tch_num2" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num2')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid2" value="${cTeachList.seq_user}" />
												<td><input type="text" readonly="readonly" name="tch_num3" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid3" value="" />
											</c:if>
											<c:if test="${cList.maxNum eq '3'}" >
												<td><input type="text" readonly="readonly" name="tch_num1" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num1')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid1" value="" />
												<td><input type="text" readonly="readonly" name="tch_num2" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num2')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid2" value="" />
												<td><div class="resetWrap"><i class="icon icon_inputReset" onclick="deleteTch(this, 'tch_num3')"></i></div><input type="text" readonly="readonly" name="tch_num3" value="${cTeachList.user_nm}" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
												<input type="hidden" name="tch_hid3" value="${cTeachList.seq_user}" />
											</c:if>
										</c:if>
										<c:if test="${cList.seq_class eq cTeachList.seq_class and cList.cnt eq '0'}">
											<td><input type="text" readonly="readonly" name="tch_num1" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num1')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
											<input type="hidden" name="tch_hid1" value="" />
											<td><input type="text" readonly="readonly" name="tch_num2" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num2')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
											<input type="hidden" name="tch_hid2" value="" />
											<td><input type="text" readonly="readonly" name="tch_num3" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
											<input type="hidden" name="tch_hid3" value="" />
										</c:if>
								</c:forEach>
							</c:if>
							<c:if test="${empty classTeacherList}">
								<td><input type="text" readonly="readonly" name="tch_num1" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num1')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
								<input type="hidden" name="tch_hid1" value="" />
								<td><input type="text" readonly="readonly" name="tch_num2" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num2')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
								<input type="hidden" name="tch_hid2" value="" />
								<td><input type="text" readonly="readonly" name="tch_num3" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick="position(this, 'tch_num3')" class="btnMngClassChoice layerpopupOpen" data-popup-class="mngManageClassTeacher">선택</a></span></td>
								<input type="hidden" name="tch_hid3" value="" />
							</c:if>
							<td>
								<span class="saBtn"><a href="javascript:void(0)" onclick="doUseCheck(this)" class="btnMngClassDelete layerpopupOpen">삭제</a></span>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<c:import url="/mng/layer/teacherSelect" charEncoding="utf-8">
	<c:param name="schorg_id" value="${schInfo.schorg_id}"/>
	<c:param name="returnUrl" value="${returnUrl}"/>
</c:import>

<input type="hidden" id="positions" name="positions" />
<input type="hidden" id="positionn" name="positionn" />
<input type="hidden" id="numbering" name="numbering" />
<input type="hidden" id="classDuple" name="classDuple" value="N" />
<input type="hidden" id="clasChks" name="clasChks" value="N" />
<input type="hidden" id="delChk" name="delChk" value="N" />
<input type="hidden" id="delSeq" name="delSeq" />
<input type="hidden" id="delPosi" name="delPosi" />

<%-- Layer --%>
<div class="layerBg mngManageClassSave"></div>
<div class="layerPopup mngManageClassSave style02">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			저장하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doSave()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<%-- Layer --%>
<div class="layerBg mngManageClassDel"></div>
<div class="layerPopup mngManageClassDel style02">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			삭제하시겠습니까?
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doDelete()" class="btnLayerGreen">확인</a></span>
		<span class="saBtn"><a href="#" class="btnLayerGray layerClose">취소</a></span>
	</div>
</div>
<%-- Layer --%>

<form id="classForm" name="classForm" action="/mng/manage/classConfigSave" method="post">
	<input type="hidden" name="seq_lic" value="${map.seq_lic}" />
	<input type="hidden" name="org_id" value="${schInfo.schorg_id}" />
	<input type="hidden" name="nowYear" value="${map.nowYear}" />
	<input type="hidden" name="orgtyp" value="${schInfo.schorg_typ_cd}" />
	<input type="hidden" id="seqClassList" name="seqClassList"/>
	<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		<input type="hidden" id="orgGradeList" name="orgGradeList"/>
		<input type="hidden" id="gradeList" name="gradeList"/>
	</c:if>
	<input type="hidden" id="classList" name="classList"/>
	<input type="hidden" id="orgClassList" name="orgClassList"/>
	<input type="hidden" id="tchNum1" name="tchNum1"/>
	<input type="hidden" id="tchNum2" name="tchNum2"/>
	<input type="hidden" id="tchNum3" name="tchNum3"/>
</form>

<c:import url="/mng/layer/schoolOrganB" charEncoding="utf-8">
	<c:param name="returnUrl" value="${returnUrl}"/>
	<c:param name="nowYear" value="${map.nowYear}"/>
</c:import>

<script>
	function fnPopupOpen(popupId, zIndex){
		var  winWid    = $(window).width()
			,winHei    = $(window).height()
			,popupWid  = $(".layerPopup."+popupId).width()
			,popupHei  = $(".layerPopup."+popupId).height()
			,popupTop  = (winHei-popupHei)/2
			,popupLeft = (winWid-popupWid)/2;

		/**
		 * 팝업 배경 블럭
		 */
		$(".layerBg."+popupId).css({"display":"block"}).attr("data-popup-this", popupId);
		/**
		 * 해당 클래스 가지는 팝업 표출
		 */
		$(".layerPopup."+popupId).css({"display":"block", "top":popupTop, "left":popupLeft}).attr("data-popup-this", popupId);
	}

	$(".messageClose, .messageBg").click(function(e){
		e.preventDefault();
		$(this).parents(".messageLayerWrap").remove();
	});

	function doAdd() {
		<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
			var years = '';
			<c:forEach var="rs" items="${gradeist}">
				years += "<option value='" + '0' + ${rs.code} + "'>" + ${rs.code_nm} + "</option>";
			</c:forEach>

			$('#classLists > tbody').append(
					'<tr>' +
					'<td class="classRow">' +
					'<select id="st_grade_cd" name="st_grade_cd">' + years + '</select>' +
					'<input type="hidden" name="org_st_grade_cd" value="" />' +
					'</td>' +
					'<td>' +
					'<input type="text" name="st_class" value="" />' +
					'<input type="hidden" name="org_st_class" value="" />' +
					'</td>' +
					'<input type="hidden" name="seqClass" value="new" />' +
					'<td><input type="text" name="tch_num1" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num1"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid1" value="" />' +
					'<td><input type="text" name="tch_num2" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num2"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid2" value="" />' +
					'<td><input type="text" name="tch_num3" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num3"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid3" value="" />' +
					'<td><span class="saBtn"><a href="javascript:void(0)" onclick="doUseCheck(this)" class="btnMngClassDelete layerpopupOpen" >삭제</a></span></td>' +
					'</tr>'
			);

			</c:if>
			<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
			$('#classLists > tbody').append(
					'<tr>' +
					'<td>' +
					'<input type="text" name="st_class" value="" />' +
					'<input type="hidden" name="org_st_class" value="" />' +
					'</td>' +
					'<input type="hidden" name="seqClass" value="new" />' +
					'<td><input type="text" name="tch_num1" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num1"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid1" value="" />' +
					'<td><input type="text" name="tch_num2" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num2"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid2" value="" />' +
					'<td><input type="text" name="tch_num3" readonly="readonly" value="" /><span class="saBtn"><a href="javascript:void(0)" onclick=position(this,"'+"tch_num3"+'") class="btnMngClassChoice layerpopupOpen2" data-popup-class="mngManageClassTeacher">선택</a></span></td>' +
					'<input type="hidden" name="tch_hid3" value="" />' +
					'<td><span class="saBtn"><a href="javascript:void(0)" onclick="doUseCheck(this)" class="btnMngClassDelete layerpopupOpen" >삭제</a></span></td>' +
					'</tr>'
			);
		</c:if>
	}

	function doDataSet() {
		var classList = '';
		var orgClassList = '';
		var seqClassList = '';
		var tchNum1 = '';
		var tchNum2 = '';
		var tchNum3 = '';
		$('#clasChks').val("N");

		$('input[name="seqClass"]').each(function (index){
			seqClassList += ',' + $(this).val();
		});
		<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		var gradeList = '';
		var orgGradeList = '';
		$('select[name="st_grade_cd"]').each(function (index){
			gradeList += ',' + $(this).val();
		});
		$('input[name="org_st_grade_cd"]').each(function (index){
			orgGradeList += ',-' + $(this).val();
		});
		</c:if>
		$('input[name="st_class"]').each(function (index){
			classList += ',' + $(this).val();
			if($.trim($(this).val()) == '') {
				$('#clasChks').val("Y");
			}
		});
		$('input[name="org_st_class"]').each(function (index){
			orgClassList += ',-' + $(this).val();
		});
		$('input[name="tch_hid1"]').each(function (index){
			tchNum1 += ',-' + $(this).val();
		});
		$('input[name="tch_hid2"]').each(function (index){
			tchNum2 += ',-' + $(this).val();
		});
		$('input[name="tch_hid3"]').each(function (index){
			tchNum3 += ',-' + $(this).val();
		});
		seqClassList = seqClassList.substr(1);
		<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		gradeList = gradeList.substr(1);
		orgGradeList = orgGradeList.substr(1);
		</c:if>
		classList = classList.substr(1);
		orgClassList = orgClassList.substr(1);
		tchNum1 = tchNum1.substr(1);
		tchNum2 = tchNum2.substr(1);
		tchNum3 = tchNum3.substr(1);

		$('#classDuple').val("N");
		var unique_check = {};
		var compare = new Array();
		var cla = classList.split(',');
		<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		var gra = gradeList.split(',');
		for(var x=0; x<gra.length; x++){
			compare[x] = gra[x] + ',' + cla[x];
		}
		for(var y=0; y<compare.length; y++) {
			if(typeof unique_check[compare[y]] == "undefined") {
				unique_check[compare[y]] = 0;
			} else {
				$('#classDuple').val("Y");
				break;
			}
		}
		</c:if>
		<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
		for(var y=0; y<cla.length; y++) {
			if(typeof unique_check[cla[y]] == "undefined") {
				unique_check[cla[y]] = 0;
			} else {
				$('#classDuple').val("Y");
				break;
			}
		}
		</c:if>

		$('#seqClassList').val(seqClassList);
		<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
		$('#orgGradeList').val(orgGradeList);
		$('#gradeList').val(gra);
		</c:if>
		$('#classList').val(cla);
		$('#orgClassList').val(orgClassList);
		$('#tchNum1').val(tchNum1);
		$('#tchNum2').val(tchNum2);
		$('#tchNum3').val(tchNum3);

		if($('#clasChks').val() == "Y") {
			<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
			fnAlertMessage("반을 입력하여 주십시요.");
			</c:if>
			<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
			fnAlertMessage("학급을 입력하여 주십시요.");
			</c:if>
			$(".layerPopup.mngManageClassSave.style02").css({"display":"none"});
			$(".layerBg.mngManageClassSave").css({"display":"none"});
			return false;
		}
		if($('#classDuple').val() == "Y") {
			fnAlertMessage("하나 이상의 같은 학년/반을 저장할 수 없습니다.");
			$(".layerPopup.mngManageClassSave.style02").css({"display":"none"});
			$(".layerBg.mngManageClassSave").css({"display":"none"});
			return false;
		}

		fnPopupOpen('mngManageClassSave');
	}

	var delay = 1000;
	var submitted = false;

	function doSave() {
		if(submitted == true) { return; }
		setTimeout ('doSubmit()', delay);
		submitted = true;
	}

	function doSubmit() {
		document.classForm.submit();
	}

	function doUseCheck(r) {
		$('#delChk').val("N");
		$('#delSeq').val('');
		$('#delPosi').val('');
		var b = r.parentNode.parentNode.parentNode.rowIndex -1;
		var i = r.parentNode.parentNode.parentNode.rowIndex;
		var seqCla = $("#classLists tbody tr:eq(" + b + ") input[name=seqClass]").val();
		$('#delPosi').val(i);
		$('#delSeq').val(seqCla);
		if(seqCla == "new"){
			$('#delChk').val("N");
		} else {
			$.ajax({
				url    : "/mng/manage/classUseChk",
				type   : "post",
				data   : {seq_class:seqCla},
				success: function(data){
					if(data == "Y") {
						$('#delChk').val("Y");
					}
				},
				error: function(e) {
					return false;
				}
			});
		}
		fnPopupOpen('mngManageClassDel');
	}

	function doDelete() {
		popupMessageDown('mngManageClassDel.style02', 'mngManageClassDel');
		var pso = $('#delPosi').val();
		if($('#delChk').val() == 'Y'){
			fnAlertMessage('사용중인 학급은 삭제할 수 없습니다.');
			return false;
		} else {
			if($('#delSeq').val() != ''){
				$.ajax({
					url    : "/mng/manage/classDelete",
					type   : "post",
					data   : {seq_class: $('#delSeq').val()},
					success: function(data){
						document.getElementById("classLists").deleteRow(pso);
						fnAlertMessage('삭제되었습니다.');
					},
					error  : function(e){
						return false;
					}
				});
			}
		}
	}

	function doYearChange(year) {
		<c:if test="${empty map.seq_lic}">
			document.location.href = "/mng/manage/classConfig?org_id=${schInfo.schorg_id}&nowYear=" + year;
		</c:if>
		<c:if test="${not empty map.seq_lic}">
			document.location.href = "/mng/manage/classConfig?seq_lic=${map.seq_lic}&org_id=${schInfo.schorg_id}&nowYear=" + year;
		</c:if>
	}

	function position(e, pos) {
		$('#positions').val(pos);
		var idx = e.parentNode.parentNode.parentNode.rowIndex -1;
		$('#numbering').val(idx);
	}

	function setName(num, id, nm) {
		var pos = $('#positions').val();
		var idx = $('#numbering').val();
		var hid = "tch_hid" + pos.substring(pos.length -1, pos.length);

		for(var i=1; i<4; i++) {
			if(pos != ('tch_num' + i) ) {
				if($("#classLists tbody tr:eq(" + idx + ") input[name=tch_hid" + i + "]").val() == num) {
					fnAlertMessage("이미 담당선생님으로 등록이 되어 있습니다. \n 다른 선생님을 선택하여 주십시요.");
					return false;
				}
			}
		}
		$("#classLists tbody tr:eq(" + idx + ") input[name="+ pos + "]").val(nm);
		$("#classLists tbody tr:eq(" + idx + ") input[name="+ hid + "]").val(num);
		$(".layerPopup.mngManageClassTeacher.style01").css({"display":"none"});
		$(".layerBg.mngManageClassTeacher").css({"display":"none"});
	}

	function deleteTch(e, val) {
		var idn = e.parentNode.parentNode.parentNode.rowIndex -1;
		var ids = val.replace("tch_num", "");
		$("#classLists tbody tr:eq(" + idn + ") input[name=" + val + "]").val('');
		$("#classLists tbody tr:eq(" + idn + ") input[name=tch_hid" + ids + "]").val('');
	}

	function changeGrade(e, val, typ, year, org) {
		var bidx = e.parentNode.parentNode.rowIndex -1;
		var orgGra = $("#classLists tbody tr:eq(" + bidx + ") input[name=org_st_grade_cd]").val();
		var claVal = $("#classLists tbody tr:eq(" + bidx + ") input[name=st_class]").val();
		var seq = $("#classLists tbody tr:eq(" + bidx + ") input[name=seqClass]").val()
		$.ajax({
			url    : "/mng/manage/classDupleChk",
			type   : "post",
			data   : {schorg_id:org, st_year_cd:year, st_grade_cd:val, st_class:claVal, org_typ:typ, seq_class:seq},
			success: function(data){
				if(data == "D") {
					$("#classLists tbody tr:eq(" + bidx + ") select[name=st_grade_cd]").val(orgGra);
					fnAlertMessage("이미 등록되어있는 학년/반이 있습니다.");
					return false;
				}
				if(data == "U") {
					$("#classLists tbody tr:eq(" + bidx + ") select[name=st_grade_cd]").val(orgGra);
						fnAlertMessage("사용중인 학급은 변경할 수 없습니다.");
					return false;
				}
			},
			error: function(e) {
				return false;
			}
		});
	}
	function changeClass(e, val, typ, year, org) {
		var cidx = e.parentNode.parentNode.rowIndex - 1;
		var orgCla = $("#classLists tbody tr:eq(" + cidx + ") input[name=org_st_class]").val();
		var GraVal = $("#classLists tbody tr:eq(" + cidx + ") select[name=st_grade_cd]").val();
		var seq = $("#classLists tbody tr:eq(" + cidx + ") input[name=seqClass]").val()
		$.ajax({
			url    : "/mng/manage/classDupleChk",
			type   : "post",
			data   : {schorg_id:org, st_year_cd:year, st_grade_cd:GraVal, st_class:val, org_typ:typ, seq_class:seq},
			success: function(data){
				if(data == "D") {
					$("#classLists tbody tr:eq(" + cidx + ") input[name=st_class]").val(orgCla);
					<c:if test="${schInfo.schorg_typ_cd ne 'ORG01'}">
						fnAlertMessage("이미 등록되어있는 학년/반이 있습니다.");
					</c:if>
					<c:if test="${schInfo.schorg_typ_cd eq 'ORG01'}">
						fnAlertMessage("이미 등록되어있는 학급이 있습니다.");
					</c:if>
					return false;
				}
				if(data == "U") {
					$("#classLists tbody tr:eq(" + cidx + ") input[name=st_class]").val(orgCla);
						fnAlertMessage("사용중인 학급은 변경할 수 없습니다.");
					return false;
				}
			},
			error: function(e) {
				return false;
			}
		});
	}
</script>

<c:import url="/mng/bottom" charEncoding="utf-8" />