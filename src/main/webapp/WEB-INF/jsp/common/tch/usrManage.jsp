<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="teacherContainer">
	<div id="content" class="tchUsrManage">
		<span class="header">학생관리</span>
		<span class="info">* 우리반 학생인지 확인 후, 승인하여 주시기 바랍니다, 승인 후 학생이 서비스를 이용할 수 있습니다.</span>
		<form id="tchManage" name="tchManage" action="/tch/usrManage" method="post">
			<div class="presentSearch searchWrap">
				<div class="select">
					<label for="stYears" class="">학년도</label>
					<span id="year" class="select">
						<select id="stYears" name="st_year" class="" onchange="doClaChange(this.value, '${user.schorg_id}', '${user.seq_user}', '${user.schorg_typ_cd}')">
							<c:forEach var="rs" items="${yearList}" >
								<option value="${rs.code}" <c:if test="${rs.code eq map.st_year}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>
						</select>
					</span>
				</div>
				<div class="select">
					<label for="stClasss" class="">담당학급</label>
					<span id="class" class="select">
						<select id="stClasss" name="st_class" class="">
							<option value="a">선택</option>
							<c:forEach var="rs" items="${yearToClass}" varStatus="sts" >
								<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
									<option value="${rs.seq_class}" <c:if test="${rs.seq_class eq map.st_class}">selected="selected"</c:if>>${rs.st_grade}학년 ${rs.st_class}반</option>
								</c:if>
								<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
									<option value="${rs.seq_class}" <c:if test="${rs.seq_class eq map.st_class}">selected="selected"</c:if>>${rs.st_class}</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
				</div>
				<div class="select">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doState()" class="btnUsrManagePresentSearch">현황조회</a></span>
				</div>
			</div>
		</form>
		<c:if test="${not empty classInfo}" >
			<div class="usrManageList">
				<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
					<div class="title">${map.st_year}학년도 ${classInfo.st_grade}학년 ${classInfo.st_class}반 학생 가입 신청</div>
				</c:if>
				<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
					<div class="title">${map.st_year}학년도 ${classInfo.st_class} 학생 가입 신청</div>
				</c:if>
				<form id="tchSearch" name="tchSearch" action="/tch/usrManage" method="post">
					<input type="hidden" id="tchYears" name="st_year" value="${map.st_year}" />
					<input type="hidden" name="st_class" value="${map.st_class}" />
					<div class="searchWrap">
						<div class="select">
							<label for="turn" class="">상태</label>
							<span id="turn" class="select">
								<select name="confrmTyp" class="">
									<option value="all" <c:if test="${map.confrmTyp eq 'all'}">selected="selected"</c:if>>전체</option>
									<option value="Y" <c:if test="${map.confrmTyp eq 'Y'}">selected="selected"</c:if>>승인</option>
									<option value="N" <c:if test="${map.confrmTyp eq 'N'}">selected="selected"</c:if>>반려</option>
									<option value="L" <c:if test="${map.confrmTyp eq 'L'}">selected="selected"</c:if>>대기</option>
								</select>
							</span>
						</div>
						<div class="select">
							<label for="name" class="">이름</label>
							<span id="name" class="select">
								<input type="text" name="usrNm" class="" value="${map.usrNm}" />
							</span>
						</div>
						<div class="select">
							<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnUsrManageListSearch">검색</a></span>
						</div>
					</div>
				</form>
				<div class="listWrap">
					<table class="tUsrManageList" summary="">
						<caption></caption>
						<colgroup>
							<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
								<col width="120" />
							</c:if>
							<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
								<col width="60" />
								<col width="60" />
							</c:if>
							<col width="60" />
							<col width="100" />
							<col width="180" />
							<col width="180" />
							<col width="80" />
							<col width="200" />
						</colgroup>
						<thead>
							<tr>
								<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
									<th>학급</th>
								</c:if>
								<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
									<th>학년</th>
									<th>반</th>
								</c:if>
								<th>번호</th>
								<th>이름</th>
								<th>이메일</th>
								<th>신청일시</th>
								<th>상태</th>
								<th>승인/반려</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="rs" items="${classUserList}">
								<tr>
									<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
										<td>${rs.st_grade}</td>
									</c:if>
									<td>${rs.st_class}</td>
									<td>${rs.st_number}</td>
									<td>${rs.user_nm}</td>
									<td>${rs.user_id}</td>
									<td>${rs.reg_dt}</td>
									<c:if test="${rs.user_type eq 'R'}">
										<c:if test="${empty rs.confm_yn}">
											<td>대기</td>
											<td>
												<span class="saBtn"><a href="javascript:void(0)" onclick="doSet('R', 'Y', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}', '${rs.st_number_cd}', '${user.schorg_typ_cd}')" class="btnUsrManageApp layerpopupOpen" data-popup-class="usrManage">승인</a></span>
												<span class="saBtn"><a href="javascript:void(0)" onclick="doSet('R', 'N', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}', '${rs.st_number_cd}', '${user.schorg_typ_cd}')" class="btnUsrManageCen layerpopupOpen" data-popup-class="usrManage2">반려</a></span>
											</td>
										</c:if>
										<c:if test="${rs.confm_yn eq 'Y'}">
											<td>승인</td>
											<td></td>
										</c:if>
										<c:if test="${rs.confm_yn eq 'N'}">
											<td>반려</td>
											<td></td>
										</c:if>
									</c:if>
									<c:if test="${rs.user_type eq 'H'}">
										<c:if test="${rs.stat_cd eq '01'}">
											<td>대기</td>
											<td>
												<span class="saBtn"><a href="javascript:void(0)" onclick="doSet('H', '03', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}', '${rs.st_number_cd}', '${user.schorg_typ_cd}')" class="btnUsrManageApp layerpopupOpen" data-popup-class="usrManage">승인</a></span>
												<span class="saBtn"><a href="javascript:void(0)" onclick="doSet('H', '02', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}', '${rs.st_number_cd}', '${user.schorg_typ_cd}')" class="btnUsrManageCen layerpopupOpen" data-popup-class="usrManage2">반려</a></span>
											</td>
										</c:if>
										<c:if test="${rs.stat_cd eq '03'}">
											<td>승인</td>
											<td></td>
										</c:if>
										<c:if test="${rs.stat_cd eq '02'}">
											<td>반려</td>
											<td></td>
										</c:if>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<c:if test="${!empty recordsPerPage}">
						<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
					</c:if>
				</div>
			</div>
		</c:if>
	</div>
</div>
<form id="setData" name="setData" action="/tch/usrUpdateAjax" method="post">
	<input type="hidden" id="usTp" name="usTp" />
	<input type="hidden" id="typ" name="typ" />
	<input type="hidden" id="seq" name="seq" />
	<input type="hidden" id="stYea" name="stYea" />
	<input type="hidden" id="stGra" name="stGra" />
	<input type="hidden" id="stCla" name="stCla" />
	<input type="hidden" id="stNum" name="stNum" />
	<input type="hidden" id="userOrgTyp" name="userOrgTyp" />

</form>
<div class="layerBg usrManage"></div>
<div class="layerPopup usrManage">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">신청을 승인 하시겠습니까?</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doSave(this); return false;" class="btnUsrManageLayerSumbit">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg usrManage2"></div>
<div class="layerPopup usrManage2">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">신청을 반려 하시겠습니까?</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doSave(this); return false;" class="btnUsrManageLayerSumbit">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<script>
	$(document).ready(function(){
		<c:if test="${not empty map.msg}" >
			<c:if test="${map.msg eq 'Y'}">
				fnAlertMessage("승인 처리 되었습니다.");
			</c:if>
			<c:if test="${map.msg eq 'N'}">
				fnAlertMessage("반려 처리 되었습니다.");
			</c:if>
			<c:if test="${map.msg eq 'O'}">
				fnAlertMessage("라이선스 허용 학생수를 초과하였습니다.");
			</c:if>
			<c:if test="${map.msg eq 'X'}">
				fnAlertMessage("라이선스가 존재하지않아 승인할 수 없습니다.");
			</c:if>

		</c:if>
	});

	function doClaChange(yea, sch, seq, typ) {
		$('#stClasss option').remove();

		$.ajax({
			url    : "/tch/yearToClassAjax",
			type   : "post",
			data   : {st_year: yea, schorg_id :sch, seq_user:seq, schorg_typ_cd:typ},
			success: function(data){
				var json = jsonListSet(data);

				if(json.length == 0){
					$('#stClasss').append('<option value="a">선택</option>');
				} else {
					<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
						for(i = 0; i < json.length; i++){
							var	opt = $('<option value="' + json[i].seq_class + '">' + json[i].st_grade + '학년 ' + json[i].st_class + '반</option>');
							$('#stClasss').append(opt);
						}
					</c:if>
					<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
						for(i = 0; i < json.length; i++){
							var opt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
							$('#stClasss').append(opt);
						}
					</c:if>
				}
			},
			error:function(e) {
				fnAlertMessage("잠시 후, 다시 시도해 주십시요.");
			}
		});
	}

	function doState() {
		popupMessageDown('messageLayer.zi11', 'messageBg.zi10');

		if($('#stClasss').val() == 'a') {
			fnAlertMessage('담당학급을 선택하여 주십시요.');
			return false;
		}

		document.tchManage.submit();
	}

	function doSet(usTp, typ, seq, yea, gra, cla, num, orgTyp) {
		$('#usTp').val(usTp);
		$('#typ').val(typ);
		$('#seq').val(seq);
		$('#stYea').val(yea);
		$('#stGra').val(gra);
		$('#stCla').val(cla);
		$('#stNum').val(num);
		$('#userOrgTyp').val(orgTyp);
	}

	var delay = 1000;
	var submitted = false;

	function doSave(e) {
		var _target = $(e).parents(".layerPopup").attr("data-popup-this");
		$(e).parents(".layerPopup." + _target).css({"display":"none"});
		$(e).parents(".layerPopup." + _target).siblings(".layerBg." + _target).css({"display":"none"});

		if(submitted == true) { return; }
		setTimeout ('doUpdate()', delay);
		submitted = true;
	}

	function doUpdate() {
		var frmData = $('#setData').serialize();
		$.ajax({
			url    : "/tch/usrUpdateAjax",
			type   : "post",
			data   : frmData,
			success: function(msg){
				if (msg == 'confirm') {
					location.href = "/tch/usrManage?st_year=${map.st_year}&st_class=${map.st_class}&msg=Y";
				} else if (msg == 'deny') {
					location.href = "/tch/usrManage?st_year=${map.st_year}&st_class=${map.st_class}&msg=N";
				} else if (msg == 'over') {
					location.href = "/tch/usrManage?st_year=${map.st_year}&st_class=${map.st_class}&msg=O";
				} else if (msg == 'nolic') {
					location.href = "/tch/usrManage?st_year=${map.st_year}&st_class=${map.st_class}&msg=X";
				} else {
					// 오류 메시지 출력
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			},
			error:function(e) {
				// 오류 메시지 출력
				fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
				return false;
			}
		});
	}

	function doSearch() {
		document.tchSearch.submit();
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />