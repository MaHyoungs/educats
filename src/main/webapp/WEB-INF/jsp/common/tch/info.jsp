<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="teacherContainer">
	<div id="content" class="tchInfo">
		<span class="header">내 정보관리</span>
		<div class="container">
			<form id="tchInfo" name="tchInfo" action="/tch/infoUpdate" method="post">
				<input type="hidden" name="seq_user" value="${user.seq_user}" />
				<table class="tTchInfo" summary="">
					<caption></caption>
					<colgroup>
						<col width="204" />
						<col width="300" />
						<col width="234" />
					</colgroup>
					<tbody>
						<tr>
							<th><i class="icon_tchName"></i><span>이름</span></th>
							<td colspan="2">
								<ul>
									<li>${user.user_nm} 선생님</li>
								</ul>
							</td>
						</tr>
						<tr>
							<th><i class="icon_tchId"></i><span>아이디</span></th>
							<td colspan="2">
								<ul>
									<li>${user.user_id}</li>
								</ul>
							</td>
						</tr>
						<tr class="pT30">
							<th><i class="icon_tchPass"></i><span>현재 비밀번호</span></th>
							<td><input type="password" id="password" name="password" placeholder="현재 비밀번호" height="34" /></td>
							<td></td>
						</tr>
						<tr>
							<th><i class="icon_tchRepass"></i><span>비밀번호 변경</span></th>
							<td><input type="password" id="new_password" name="new_password" placeholder="비밀번호 변경" /></td>
							<td><span class="cl_839ab5">6~15자리 국문, 영문, 숫자 혼합</span></td>
						</tr>
						<tr>
							<th><i class="icon_tchRepassCheck"></i><span>비밀번호 변경 확인</span></th>
							<td><input type="password" id="new_passwordChk" name="new_passwordChk" placeholder="비밀번호 확인" /></td>
							<td><span id="errorTxt" class="cl_ec7063"></span></td>
						</tr>
						<tr class="pT30">
							<th><i class="icon_tchSchool"></i><span><c:if test="${user.schorg_typ_cd ne 'ORG01'}">학교</c:if><c:if test="${user.schorg_typ_cd eq 'ORG01'}">기관</c:if></span></th>
							<td colspan="2">
								<ul>
									<li>${user.schorg_nm}</li>
								</ul>
							</td>
						</tr>
						<tr class="pT30">
							<th><i class="icon_tchNowClass"></i><span>현재 담당학급</span></th>
							<td colspan="2">
								<ul>
										<c:forEach var="rs" items="${manageClassList}">
											<c:if test="${rs.st_year_cd eq user.st_year_cd}">
												<li>
													<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
														${rs.st_year_cd}학년도 ${rs.st_grade}학년 ${rs.st_class}반
													</c:if>
													<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
														${rs.st_year_cd}학년도 ${rs.st_class}반
													</c:if>
													<c:choose>
														<c:when test="${rs.user_type eq 'R'}">
															<span class="status ok">(승인완료)</span>
														</c:when>
														<c:when test="${rs.user_type eq 'H'}">
															<c:if test="${rs.stat_cd eq '01'}">
																<span class="status ready">(대기)</span>
																<div class="btnArea">
																	<span class="saBtn"><a href="#" class="btnTchInfoModifyApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nowClassModify">수정요청</a></span>
																</div>
															</c:if>
															<c:if test="${rs.stat_cd eq '02'}">
																<span class="status no">(반려)</span>
																<div class="btnArea">
																	<span class="saBtn"><a href="#" class="btnTchInfoModifyApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nowClassModify">수정요청</a></span>
																</div>
															</c:if>
															<c:if test="${rs.stat_cd eq '03'}">
																<span class="status ok">(승인완료)</span>
															</c:if>
														</c:when>
														<c:otherwise>
															<c:if test="${empty rs.confm_yn}">
																<span class="status ready">(대기)</span>
																<div class="btnArea">
																	<span class="saBtn"><a href="#" class="btnTchInfoModifyApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nowClassModify">수정요청</a></span>
																</div>
															</c:if>
															<c:if test="${rs.confm_yn eq 'N'}">
																<span class="status no">(반려)</span>
																<div class="btnArea">
																	<span class="saBtn"><a href="#" class="btnTchInfoModifyApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nowClassModify">수정요청</a></span>
																</div>
															</c:if>
															<c:if test="${rs.confm_yn eq 'Y'}">
																<span class="status ok">(승인완료)</span>
															</c:if>
														</c:otherwise>
													</c:choose>
												</li>
											</c:if>
										</c:forEach>
								</ul>
							</td>
						</tr>
						<tr class="pT30">
							<th><i class="icon_tchNextClass"></i><span>다음 학년도 담당학급</span></th>
							<td colspan="2">
								<ul>
									<c:forEach var="rs" items="${manageClassList}">
										<c:if test="${rs.st_year_cd eq (user.st_year_cd + 1)}">
											<li>
												<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
														${rs.st_year_cd}학년도 ${rs.st_grade}학년 ${rs.st_class}반
													</c:if>
													<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
														${rs.st_year_cd}학년도 ${rs.st_class}반
													</c:if>
												<c:choose>
													<c:when test="${rs.user_type eq 'R'}">
														<span class="status ok">(승인완료)</span>
													</c:when>
													<c:when test="${rs.user_type eq 'H'}">
														<c:if test="${rs.stat_cd eq '01'}">
															<span class="status ready">(대기)</span>
															<div class="btnArea">
																<span class="saBtn"><a href="#" class="btnTchInfoReApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nextClassReApp">재신청</a></span>
																<span class="saBtn"><a href="javascript:void(0)" onclick="doDeleteYn('${rs.user_type}', '${user.user_id}', '${user.schorg_typ_cd}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" class="btnTchInfoDel layerpopupOpen" data-popup-class="usrManage3">삭제</a></span>
															</div>
														</c:if>
														<c:if test="${rs.stat_cd eq '02'}">
															<span class="status no">(반려)</span>
															<div class="btnArea">
																<span class="saBtn"><a href="#" class="btnTchInfoReApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nextClassReApp">재신청</a></span>
																<span class="saBtn"><a href="javascript:void(0)" onclick="doDeleteYn('${rs.user_type}', '${user.user_id}', '${user.schorg_typ_cd}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" class="btnTchInfoDel layerpopupOpen" data-popup-class="usrManage3">삭제</a></span>
															</div>
														</c:if>
														<c:if test="${rs.stat_cd eq '03'}">
															<span class="status ok">(승인완료)</span>
														</c:if>
													</c:when>
													<c:otherwise>
														<c:if test="${empty rs.confm_yn}">
															<span class="status ready">(대기)</span>
															<div class="btnArea">
																<span class="saBtn"><a href="#" class="btnTchInfoReApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nextClassReApp">재신청</a></span>
																<span class="saBtn"><a href="javascript:void(0)" class="btnTchInfoDel">삭제</a></span>
															</div>
														</c:if>
														<c:if test="${rs.confm_yn eq 'N'}">
															<span class="status no">(반려)</span>
															<div class="btnArea">
																<span class="saBtn"><a href="#" class="btnTchInfoReApp layerpopupOpen" onclick="modify('${rs.user_type}', '${rs.seq_user}', '${rs.st_year_cd}', '${rs.st_grade_cd}', '${rs.st_class}')" data-popup-class="nextClassReApp">재신청</a></span>
																<span class="saBtn"><a href="javascript:void(0)" class="btnTchInfoDel">삭제</a></span>
															</div>
														</c:if>
														<c:if test="${rs.confm_yn eq 'Y'}">
															<span class="status ok">(승인완료)</span>
														</c:if>
													</c:otherwise>
												</c:choose>
											</li>
										</c:if>
									</c:forEach>
									<c:if test="${user.confm_yn eq 'Y'}">
										<li>
											<div class="btnArea">
												<span class="saBtn"><a href="#" class="btnTchInfoNextClassAdd layerpopupOpen" onclick="modify('T', '${user.seq_user}', '${user.st_year_cd + 1}', '${user.st_grade_cd}', '${user.st_class}')" data-popup-class="nextClassApp">다음 학년도 학급 신청</a></span>
											</div>
										</li>
									</c:if>
								</ul>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</div>
		<div class="footer">
			<span class="saBtn"><a href="javascript:void(0)" onclick="doUpdate()" class="btnTchInfoMdify">수정</a></span>
			<span class="saBtn"><a href="javascript:void(0)" onclick="cancel()" class="btnTchInfoCancel">취소</a></span>
		</div>
	</div>
</div>

<form id="deleteForm" name="deleteForm" action="tch/tchDeleteAjax" method="post">
	<input type="hidden" id="delTyp" name="delTyp" />
	<input type="hidden" id="delSch" name="delSch" />
	<input type="hidden" id="delId" name="delId" />
	<input type="hidden" id="delYea" name="delYea" />
	<input type="hidden" id="delGra" name="delGra" />
	<input type="hidden" id="delCla" name="delCla" />
</form>

<form id="modifyForm" name="modifyForm" action="tch/tchUpdateAjax" method="post">
	<input type="hidden" id="userTyp" name="userTyp" />
	<input type="hidden" id="userSeq" name="userSeq" />
	<input type="hidden" id="userYea" name="userYea" />
	<input type="hidden" id="userGra" name="userGra" />
	<input type="hidden" id="userCla" name="userCla" />
	<input type="hidden" id="newGra" name="newGra" />
	<input type="hidden" id="newCla" name="newCla" />
	<input type="hidden" id="subTyp" name="subTyp" />
	<input type="hidden" id="userOrg" name="userOrg" value="${user.schorg_id}" />
	<input type="hidden" id="userOrgTyp" name="userOrgTyp" value="${user.schorg_typ_cd}" />
</form>

<div class="layerBg nextClassApp"></div>
<div class="layerPopup nextClassApp tchInfoLayer">
	<div class="layerHeader">
		<span class="title">다음 학년도 학급 신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			다음 학년도 학급을 선택하세요.<br/>
			학교관리자의 승인 후 확정됩니다.
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
				<label for="modifyNr" class="mR5">학년</label>
				<span class="select wid120 mR12">
					<select id="modifyNr" onchange="doGraChange('T', this.value, (${user.st_year_cd} + 1))" class="wid120">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
					<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
					</c:if>
					</select>
				</span>
			</c:if>
			<label for="modifyTr" class="mR5">반</label>
			<span class="select wid120">
				<select id="modifyTr" class="wid120">
					<option value="99">선택</option>
				</select>
			</span>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('T')" class="btnUsrManageLayerSumbit">신청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg nowClassModify"></div>
<div class="layerPopup nowClassModify tchInfoLayer">
	<div class="layerHeader">
		<span class="title">현재 학년도 담당학급 수정요청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			담당학급이 일치하지 않으면<br/>
			서비스 이용 승인이 되지 않습니다.<br/>
			수정하여 다시 요청하시겠습니까?
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
				<label for="modifyGra" class="mR5">학년</label>
				<span class="select wid120 mR12">
					<select id="modifyGra" name="modifyGra" onchange="doGraChange('C', this.value, '${user.st_year_cd}')" class="wid120">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
					<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
					</c:if>
					</select>
				</span>
			</c:if>
			<label for="modifyCla" class="mR5">반</label>
			<span class="select wid120">
				<select id="modifyCla" name="modifyCla" class="wid120">
					<option value="99">선택</option>
				</select>
			</span>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('C')" class="btnUsrManageLayerModify">수정요청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg nextClassReApp"></div>
<div class="layerPopup nextClassReApp tchInfoLayer">
	<div class="layerHeader">
		<span class="title">다음 학년도 학급 재신청</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">
		<div class="message">
			담당학급이 일치하지 않으면<br/>
			서비스 이용 승인이 되지 않습니다.<br/>
			수정하여 다시 요청하시겠습니까?
		</div>
		<div class="selectWrap">
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
				<label for="modifyGr" class="mR5">학년</label>
				<span class="select wid120 mR12">
					<select id="modifyGr" onchange="doGraChange('N', this.value, (${user.st_year_cd} + 1))" class="wid120">
						<option value="99">선택</option>
						<option value="01">1</option>
						<option value="02">2</option>
						<option value="03">3</option>
					<c:if test="${user.schorg_typ_cd eq 'SCH01'}">
						<option value="04">4</option>
						<option value="05">5</option>
						<option value="06">6</option>
					</c:if>
					</select>
				</span>
			</c:if>
			<label for="modifyCl" class="mR5">반</label>
			<span class="select wid120">
				<select id="modifyCl" class="wid120">
					<option value="99">선택</option>
				</select>
			</span>
		</div>
	</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doNowClass('N')" class="btnUsrManageLayerSumbit">재신청</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<div class="layerBg usrManage3"></div>
<div class="layerPopup usrManage3">
	<div class="layerHeader">
		<span class="title">알림</span>
		<a href="#" class="layerClose"><img src="/template/common/images/layerpopup/btn_layerpopup_tch_close.png" alt="" /></a>
	</div>
	<div class="layerContainer">신청을 삭제 하시겠습니까?</div>
	<div class="layerFooter">
		<span class="saBtn"><a href="javascript:void(0)" onclick="doDelete(); return false;" class="btnUsrManageLayerSumbit">확인</a></span>
		<span class="saBtn"><a href="javascript:void(0)" class="btnUsrManageLayerCancel layerClose">취소</a></span>
	</div>
</div>

<script>
	$(document).ready(function(){
		<c:if test="${not empty msg}">
			<c:choose>
				<c:when test="${msg eq 'RT'}">
					fnAlertUrlMessage("가입신청이 반려되었습니다. <br/> 내정보관리에서 다시 신청하세요.", "/tch/info");
				</c:when>
				<c:when test="${msg eq 'BT'}">
					fnAlertUrlMessage("가입신청 승인 이전에는 <br/> 일부 메뉴 이용이 제한됩니다.", "/tch/info");
				</c:when>
				<c:when test="${msg eq 'Y'}">
					fnAlertMessage("수정되었습니다.");
				</c:when>
				<c:otherwise>
					fnAlertMessage('${msg}');
				</c:otherwise>
			</c:choose>
		</c:if>
		$('#new_passwordChk').keyup(function() {
			if($('#new_password').val() != $('#new_passwordChk').val()) {
				document.getElementById("errorTxt").className= "cl_ec7063";
				$('#errorTxt').text("*비밀번호와 일치하지 않습니다.");
			} else {
				$('#errorTxt').text("*비밀번호와 일치합니다.");
				document.getElementById("errorTxt").className= "cl_839ab5";
			}
		});
	});
	function cancel() {
		location.href = "/tch/info";
	}
	function doUpdate() {

		var chkPassword = $('#password').val();
		$.ajax({
			url    : "/tch/passwordChk",
			type   : "post",
			data   : {chkId:'${user.user_id}', chkSeqUser:'${user.seq_user}', chkPassword:chkPassword},
			success: function(data){
				if(data == 'N') {
					fnAlertMessage('현재 비밀번호가 일치하지 않습니다.');
					return false;
				} else {
					if($('#new_password').val() != $('#new_passwordChk').val()) {
						fnAlertMessage('비밀번호와 비밀번호 확인이 일치하지 않습니다.');
						return false;
					}
					if(!chkPwdAll($.trim($('#new_password').val()))) {
						$('#new_password').val('');
						$('#new_passwordChk').val('');
						$('#new_password').focus();
						return false;
					}
					document.tchInfo.submit();

				}
			},
			error:function(e) {
				return false;
			}
		});

	}

	function modify(typ, seq, yea, gra, cla) {
		$('#userTyp').val(typ);
		$('#userSeq').val(seq);
		$('#userYea').val(yea);
		$('#userGra').val(gra);
		$('#userCla').val(cla);
		<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
			$.ajax({
				url    : "/tch/classListAjax2",
				type   : "post",
				data   : {st_year_cd : yea, schorg_id : '${user.schorg_id}'},
				success: function(data){
					var json = jsonListSet(data);
					if(typ == 'C'){
						$('#modifyCla option').remove();
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#modifyCla').append(cpt);
						}
					} else if(typ == 'N'){
						$('#modifyCl option').remove();
						$('#modifyCla option').remove();
						$('#modifyCl').append($('<option value="99">선택</option>'));
						$('#modifyCla').append($('<option value="99">선택</option>'));
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#modifyCl').append(cpt);
							$('#modifyCla').append(cpt);
						}
					} else if(typ == 'H'){
						$('#modifyCl option').remove();
						$('#modifyCl').append($('<option value="99">선택</option>'));
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#modifyCl').append(cpt);
						}
					} else {
						$('#modifyTr option').remove();
						$('#modifyTr').append($('<option value="99">선택</option>'));
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
							$('#modifyTr').append(cpt);
						}
					}
				},
				error:function(e) {
					return false;
				}
			});
		</c:if>
	}

	function doGraChange(tp, gra, yea) {
		$.ajax({
			url    : "/tch/classListAjax2",
			type   : "post",
			data   : {st_year_cd : yea, st_grade_cd : gra, schorg_id : '${user.schorg_id}'},
			success: function(data){
				var json = jsonListSet(data);

				if(tp == 'C'){
					$('#modifyCla option').remove();
					$('#modifyCla').append($('<option value="99">선택</option>'));
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCla').append(cpt);
					}
				} else if(tp == 'N'){
					$('#modifyCl option').remove();
					$('#modifyCl').append($('<option value="99">선택</option>'));
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyCl').append(cpt);
					}
				} else {
					$('#modifyTr option').remove();
					$('#modifyTr').append($('<option value="99">선택</option>'));
					for(i = 0; i < json.length; i++){
						var cpt = $('<option value="' + json[i].st_class + '">' + json[i].st_class + '</option>');
						$('#modifyTr').append(cpt);
					}
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doNowClass(typs) {

		$('#subTyp').val(typs);

		if(typs == 'C') {
			$('#newGra').val($('#modifyGra').val());
			$('#newCla').val($('#modifyCla').val());
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
				if($('#modifyGra').val() == '99') {
					fnAlertMessage("학년을 선택하여 주십시요.");
					return false;
				}
				if($('#modifyCla').val() == '99') {
					fnAlertMessage("반을 선택하여 주십시요.");
					return false;
				}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
			if($('#modifyCla').val() == '99') {
				fnAlertMessage("학급을 선택하여 주십시요.");
				return false;
			}
			</c:if>
		} else if(typs == 'N') {
			$('#newGra').val($('#modifyGr').val());
			$('#newCla').val($('#modifyCl').val());
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
			if($('#modifyGr').val() == '99') {
				fnAlertMessage("학년을 선택하여 주십시요.");
				return false;
			}
			if($('#modifyCl').val() == '99') {
				fnAlertMessage("반을 선택하여 주십시요.");
				return false;
			}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
			if($('#modifyCl').val() == '99') {
				fnAlertMessage("학급을 선택하여 주십시요.");
				return false;
			}
			</c:if>
		} else {
			$('#newGra').val($('#modifyNr').val());
			$('#newCla').val($('#modifyTr').val());
			<c:if test="${user.schorg_typ_cd ne 'ORG01'}">
			if($('#modifyNr').val() == '99') {
				fnAlertMessage("학년을 선택하여 주십시요.");
				return false;
			}
			if($('#modifyTr').val() == '99') {
				fnAlertMessage("반을 선택하여 주십시요.");
				return false;
			}
			</c:if>
			<c:if test="${user.schorg_typ_cd eq 'ORG01'}">
			if($('#modifyTr').val() == '99') {
				fnAlertMessage("학급을 선택하여 주십시요.");
				return false;
			}
			</c:if>
		}
		var frm = $('#modifyForm').serialize();
		$.ajax({
			url    : "/tch/tchUpdateAjax",
			type   : "post",
			data   : frm,
			success: function(data){
				if(data == "duple") {
					fnAlertMessage("신청하신 학급을 중복 신청하였습니다.");
					return false;
				} else if(data =="up") {
					if(typs == 'C'){
						popupMessageDown('nowClassModify.tchInfoLayer', 'nowClassModify');
					} else if(typs == 'N') {
						popupMessageDown('nextClassReApp.tchInfoLayer', 'nextClassReApp');
					} else{
						popupMessageDown('nextClassApp.tchInfoLayer', 'nextClassApp');
					}
					fnAlertUrlMessage("신청하였습니다.", "/tch/info");
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	function doDeleteYn(typ, id, sch, yea, gra, cla) {
		$('#delTyp').val(typ);
		$('#delId').val(id);
		$('#delSch').val(sch);
		$('#delYea').val(yea);
		$('#delGra').val(gra);
		$('#delCla').val(cla);
	}

	function doDelete() {
		var delForm = $('#deleteForm').serialize();
		popupMessageDown('usrManage3', 'usrManage3');
		$.ajax({
			url    : "/tch/tchDeleteAjax",
			type   : "post",
			data   : delForm,
			success: function(data){
				if(data == "succ") {
					fnAlertUrlMessage("삭제하였습니다.", "/tch/info");
					return false;
				} else {
					fnAlertMessage("실패하였습니다. <br/> 잠시 후, 다시 시도해 주시기 바랍니다.");
					return false;
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

</script>

<c:import url="/footer" charEncoding="utf-8" />