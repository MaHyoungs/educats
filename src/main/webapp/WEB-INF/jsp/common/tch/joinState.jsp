<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:import url="/header" charEncoding="utf-8" />

<div id="container" class="teacherContainer">
	<div id="content" class="tchJoinState">
		<span class="header">참여현황</span>
		<form id="tchState" name="tchState" action="/tch/joinState" method="post">
			<input type="hidden" id="searchOrg" name="searchOrg" value="${user.schorg_id}">
			<div class="presentSearch searchWrap">
				<div class="select">
					<label for="searchTyp" class="mR7">검사종류</label>
					<span class="select wid200 mR10">
						<select id="searchTyp" name="searchTyp" class="wid200" onchange="doLic(this.value, '${user.schorg_id}')">
							<c:choose>
								<c:when test="${sv01y eq 'Y' and sv02y eq 'Y'}">
									<c:forEach var="rs" items="${surveyList}" >
										<c:if test="${rs.code eq map.searchTyp}">
											<c:set var="typ_nm" value="${rs.code_nm}"/>
										</c:if>
											<option value="${rs.code}" <c:if test="${rs.code eq map.searchTyp}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</c:when>
								<c:when test="${sv01y eq 'Y' and sv02y eq 'N'}">
									<c:forEach var="rs" items="${surveyList}" >
										<c:if test="${rs.code eq 'SV01'}">
											<c:set var="typ_nm" value="${rs.code_nm}"/>
											<option value="${rs.code}" <c:if test="${rs.code eq map.searchTyp}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:if>
									</c:forEach>
								</c:when>
								<c:when test="${sv01y eq 'N' and sv02y eq 'Y'}">
									<c:forEach var="rs" items="${surveyList}" >
										<c:if test="${rs.code eq 'SV02'}">
											<c:set var="typ_nm" value="${rs.code_nm}"/>
											<option value="${rs.code}" <c:if test="${rs.code eq map.searchTyp}">selected="selected"</c:if>>${rs.code_nm}</option>
										</c:if>
									</c:forEach>
								</c:when>
								<c:otherwise>
									<c:forEach var="rs" items="${surveyList}" >
										<c:if test="${rs.code eq map.st_typ}">
											<c:set var="typ_nm" value="${rs.code_nm}"/>
										</c:if>
										<option value="${rs.code}" <c:if test="${rs.code eq map.searchTyp}">selected="selected"</c:if>>${rs.code_nm}</option>
									</c:forEach>
								</c:otherwise>
							</c:choose>
						</select>
					</span>
				</div>
				<div class="select">
					<label for="searchLic" class="mR7">연차</label>
					<span class="select wid100 mR10">
						<select id="searchLic" name="searchLic" class="wid100" onchange="doYear(this.value, '${user.schorg_id}')">
							<option value="N">선택</option>
							<c:if test="${not empty map.searchLic}">
								<c:forEach var="lc" items="${licList}">
									<option value="${lc.lic_max}" <c:if test="${lc.lic_max eq map.searchLic}">selected="selected"</c:if>>${lc.lic_max}차</option>
								</c:forEach>
							</c:if>
						</select>
					</span>
				</div>
				<div class="select">
					<label for="searchYear" class="mR7">학년도</label>
					<span class="select wid85 mR10">
						<select id="searchYear" name="searchYear" class="wid85">
							<c:if test="${empty map.searchYear}">
								<option value="N">선택</option>
							</c:if>
							<c:if test="${not empty map.searchYear}">
								<option value="${map.searchYear}" selected="selected">${map.searchYear}</option>
							</c:if>
							<%--<c:forEach var="rs" items="${yearList}" >
								<option value="${rs.code}" <c:if test="${rs.code eq map.searchYear}">selected="selected"</c:if>>${rs.code_nm}</option>
							</c:forEach>--%>
						</select>
					</span>
				</div>
				<div class="select">
					<label for="searchSeqClass" class="mR8">담당학급</label>
					<span class="select wid100 mR10">
						<select id="searchSeqClass" name="searchSeqClass" class="wid100">
							<option value="N">선택</option>
							<c:forEach var="rs" items="${yearToClass}" varStatus="sts" >
								<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
									<option value="${rs.seq_class}" <c:if test="${rs.seq_class eq map.searchSeqClass}">selected="selected"</c:if>>${rs.st_grade}학년 ${rs.st_class}반</option>
								</c:if>
								<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
									<option value="${rs.seq_class}" <c:if test="${rs.seq_class eq map.searchSeqClass}">selected="selected"</c:if>>${rs.st_class}</option>
								</c:if>
							</c:forEach>
						</select>
					</span>
				</div>
				<div class="select">
					<span class="saBtn"><a href="javascript:void(0)" onclick="doSearch()" class="btnPresentSearch">현황조회</a></span>
				</div>
			</div>
		</form>
		<c:if test="${not empty classInfo}">
			<div class="joinPresent">
				<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
					<div class="title">${map.searchYear}학년도 ${classInfo.st_grade}학년 ${classInfo.st_class}반 ${typ_nm} 참여현황</div>
				</c:if>
				<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
					<div class="title">${map.searchYear}학년도 ${classInfo.st_class}반 ${typ_nm} 참여현황</div>
				</c:if>
				<table class="tJoinPresent" summary="">
					<caption></caption>
					<colgroup>
						<col width="154" />
						<col width="153" />
						<col width="154" />
						<col width="152" />
						<col width="154" />
						<col width="153" />
					</colgroup>
					<tbody>
						<c:forEach var="stLt" items="${classJoinState}" varStatus="sts">
							<tr>
								<c:if test="${sts.count eq '1'}">
									<th rowspan="${classJoinStateCnt}">검사회차</th>
								</c:if>
								<td>${stLt.tme_num}회차</td>
								<th class="sky">총 학생수</th>
								<td>${stLt.totalCnt}명</td>
								<th class="sky">${stLt.tme_num}회차 검사참여</th>
								<td>${stLt.joincnt}명(${stLt.joinper}%)</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="joinStateList">
				<form id="searchJoin" name="searchJoin" action="/tch/joinState" method="post">
					<input type="hidden" name="searchOrg" value="${user.schorg_id}">
					<input type="hidden" name="searchGroup" value="${user.schorg_typ_cd}">
					<input type="hidden" name="searchYear" value="${map.searchYear}" />
					<input type="hidden" name="searchSeqClass" value="${map.searchSeqClass}" />
					<input type="hidden" name="searchTyp" value="${map.searchTyp}" />
					<input type="hidden" name="searchLic" value="${map.searchLic}" />

					<div class="searchWrap">
						<div class="section">
							<label for="searchTme" class="">검사회차</label>
							<span class="select">
								<c:choose>
									<c:when test="${empty map.searchTme}">
										<c:set var="searchTme" value="0${classJoinStateCnt}" />
									</c:when>
									<c:otherwise>
										<c:set var="searchTme" value="${map.searchTme}" />
									</c:otherwise>
								</c:choose>
								<select id="searchTme" name="searchTme">
									<option value="all">전체</option>
									<option value="01" <c:if test="${searchTme eq '01'}">selected="selected"</c:if>>1</option>
									<option value="02" <c:if test="${searchTme eq '02'}">selected="selected"</c:if>>2</option>
									<c:if test="${map.searchTyp eq 'SV02'}">
										<option value="03" <c:if test="${searchTme eq '03'}">selected="selected"</c:if>>3</option>
										<option value="04" <c:if test="${searchTme eq '04'}">selected="selected"</c:if>>4</option>
									</c:if>
								</select>
							</span>
						</div>
						<div class="section">
							<label for="searchStateYn" class="">검사진행여부</label>
							<span class="select">
								<select id="searchStateYn" name="searchStateYn" class="">
									<option value="all">전체</option>
									<option value="Y" <c:if test="${map.searchStateYn eq 'Y'}">selected="selected"</c:if>>Y</option>
									<option value="N" <c:if test="${map.searchStateYn eq 'N'}">selected="selected"</c:if>>N</option>
								</select>
							</span>
						</div>
						<div class="section">
							<label for="searchName" class="">이름</label>
							<span class="select">
								<input id="searchName" name="searchName" type="text" value="${map.searchName}" />
							</span>
						</div>
						<div class="section">
							<span class="saBtn"><a href="javascript:void(0)" onclick="doSearchList()" class="btnJoinStateListSearch">검색</a></span>
						</div>
					</div>
				</form>
				<div class="listWrap">
					<table class="tJoinStateList" summary="">
						<caption></caption>
						<colgroup>
							<col width="58" />
							<col width="121" />
							<col width="184" />
							<col width="58" />
							<col width="80" />
							<col width="128" />
							<col width="132" />
							<col width="165" />
						</colgroup>
						<thead>
							<tr>
								<th>번호</th>
								<th>이름</th>
								<th>이메일</th>
								<th>연차</th>
								<th>검사회차</th>
								<th>검사진행여부</th>
								<th>검사일자</th>
								<th>검사결과</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="cList" items="${classJoinList}">
								<c:if test="${not empty cList.user_id}">
									<tr>
										<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
											<td>${cList.st_number}</td>
										</c:if>
										<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
											<td>${cList.num}</td>
										</c:if>
										<td>${cList.user_nm}</td>
										<td>${cList.user_id}</td>
										<td>${cList.lic_max}</td>
										<td>${cList.tme_num}</td>
										<td>${cList.surveys}</td>
										<td>${cList.reg_dt}</td>
										<c:if test="${cList.surveys eq 'Y'}">
											<td>
												<%--
												<span class="saBtn"><a href="/au/surveyResult?seq_tme=${cList.seq_tme}&who=${cList.user_id}" class="btnJoinStateResult">결과보기</a></span></td>
												<span class="saBtn"><a href="/au/aulandResult?seq_tme=${cList.seq_tme}&who=${cList.user_id}" class="btnJoinStateResult">결과보기</a></span>
												--%>
												<span class="saBtn"><a href="javascript:void(0)" onclick="popupwindow('${cList.seq_tme}','${cList.user_id}','result','1050','900')" class="btnMngJoinsResult">검사결과</a></span>
											</td>
										</c:if>
										<c:if test="${cList.surveys eq 'N'}">
											<td></td>
										</c:if>
									</tr>
								</c:if>
							</c:forEach>
						</tbody>
					</table>
					<%@ include file="/WEB-INF/jsp/common/cmm/paging/paging.jsp" %>
				</div>
			</div>
		</c:if>
	</div>
</div>
--${map.searchLic}--
<script>
	$(document).ready(function(){
		<c:if test="${not empty map.msg}">
			<c:choose>
				<c:when test="${map.msg eq 'H'}">
					fnAlertMessage("라이선스기간이 만료되었습니다. <br/> 만료 이전의 서비스만 이용 가능합니다.");
				</c:when>
				<c:otherwise>
					fnAlertMessage('${map.msg}');
				</c:otherwise>
			</c:choose>
		</c:if>
		<c:if test="${empty map.searchLic}">
			doLic($('#searchTyp').val(), '${user.schorg_id}');
		</c:if>
	});

	/**
	 * 현황조회
	 */
	function doSearch() {
		if($('#searchLic').val() == 'N') {
			fnAlertMessage('연차를 선택하여 주십시요.');
			return false;
		}

		if($('#searchSeqClass').val() == 'N') {
			fnAlertMessage('담당학급을 선택하여 주십시요.');
			return false;
		}

		document.tchState.submit();
	}

	/**
	 * 검색
	 */
	function doSearchList() {
		document.searchJoin.submit();
	}

	/**
	 * 01.검사종류에 따른 연차 목록 Ajax
	 * @param typ
	 * @param org
	 */
	function doLic(typ, org) {
		$('#searchYear option').remove();
		$('#searchYear').append($('<option value="N">선택</option>'));
		$('#searchSeqClass option').remove();
		$('#searchSeqClass').append($('<option value="N">선택</option>'));
		$('#searchLic option').remove();

		$.ajax({
			url    : "/tch/surveyToLicAjax",
			type   : "post",
			data   : {survey_typ_cd:typ, schorg_id:org},
			success: function(data){
				var json = jsonListSet(data);
				$('#searchLic').append($('<option value="N">선택</option>'));
				for(i = 0; i < json.length; i++){
					var cpt = $('<option value="' + json[i].lic_max + '">' + json[i].lic_max + '차</option>');
					$('#searchLic').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	/**
	 * 02.연차에 따른 학년도 Ajax
	 * @param lic
	 * @param org
	 */
	function doYear(lic, org) {
		var typ = $('#searchTyp').val();

		$.ajax({
			url    : "/tch/licToYearAjax",
			type   : "post",
			data   : {lic_max:lic, survey_typ_cd:typ, schorg_id:org},
			success: function(data){
				var json = jsonSet(data);
				if(json[0].st_year_cd != $('#searchYear').val()){
					$('#searchYear option').remove();
					var cpt = $('<option value="' + json[0].st_year_cd + '" selected="selected" >' + json[0].st_year_cd + '</option>');
					$('#searchYear').append(cpt);

					doClass(json[0].st_year_cd, org);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	/**
	 * 03.학년도에 따른 담당학급 목록 Ajax
	 * @param yea
	 * @param org
	 */
	function doClass(yea, org) {
		$('#searchSeqClass option').remove();

		$.ajax({
			url    : "/tch/classListAjax",
			type   : "post",
			data   : {st_year:yea, schorg_id:org, seq_user:'${user.seq_user}', schorg_typ_cd:'${user.schorg_typ_cd}'},
			success: function(data){
				var json = jsonListSet(data);
				$('#searchSeqClass').append($('<option value="N">선택</option>'));
				for(i = 0; i < json.length; i++){
					<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
					var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_grade + '학년 ' + json[i].st_class + '반</option>');
					</c:if>
					<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
					var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
					</c:if>
					$('#searchSeqClass').append(cpt);
				}
			},
			error:function(e) {
				return false;
			}
		});
	}

	/**
	 * (구)학년도에 따른 담당학급리스트
	 * @param typ
	 * @param org
	 */
/*
	function doClaChange(yea, sch, seq, typ) {
		$('#searchSeqClass option').remove();

		$.ajax({
			url    : "/tch/yearToClassAjax",
			type   : "post",
			data   : {st_year: yea, schorg_id :sch, seq_user:seq, schorg_typ_cd:typ},
			success: function(data){
				var json = jsonListSet(data);
				if(json == '') {
					$('#searchSeqClass').append($('<option value="N">선택</option>'));
				} else {
					$('#searchSeqClass').append($('<option value="N">선택</option>'));
					<c:if test="${user.schorg_typ_cd ne 'ORG01'}" >
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_grade + '학년 ' + json[i].st_class + '반</option>');
							$('#searchSeqClass').append(cpt);
						}
					</c:if>
					<c:if test="${user.schorg_typ_cd eq 'ORG01'}" >
						for(i = 0; i < json.length; i++){
							var cpt = $('<option value="' + json[i].seq_class + '">' + json[i].st_class + '</option>');
							$('#searchSeqClass').append(cpt);
						}
					</c:if>
				}
			},
			error:function(e) {
				return false;
			}
		});
	}
*/

	function popupwindow(tme, id, title, w, h) {
//		var go = "/au/surveyResult?seq_tme=" + tme + "&who=" + id + "&tchs=Y";
		var go = "/au/aulandResult?seq_tme=" + tme + "&who=" + id + "&tchs=Y";
		var left = (screen.width/2)-(w/2);
		var top = (screen.height/2)-(h/2);
		return window.open(go, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
	}
</script>

<c:import url="/footer" charEncoding="utf-8" />