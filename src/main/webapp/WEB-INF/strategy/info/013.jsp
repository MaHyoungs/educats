<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">암기기억전략이란</p>
	<p class="stxt1">암기기억전략이란 <b>학습한 결과를 기억을 담당하는 우리의 뇌(해마)가 잘 받아드리도록 입력하고, 필요한 상황에서 잘 회상(인출)해 낼 수 있도록 일련의 기억 저장고(단기기억 ⇛ 장기기억)에 저장하는 기술</b>을 말합니다.</p> 
	<p class="lst1 bgbl1">암기란 기억할 내용을 소리 내어 말하거나 머릿속으로 되뇌어 의식적으로 반복하여 회상하는 것(시연 rehearsal)으로 장기기억으로 저장되기 이전의 단계를 말함</p>
	<p class="lst1 bgbl1">기억이란 기억할 내용을 장기기억에 오래 보관하기 위해, 새로운 범주나 단위로 묶어, 기억하기 쉬운 형태로 만드는 방법(조직화)과 기억할 내용을 기존에 내가 알고 있는 경험이나 다른 정보와 관련시켜 의미망(semantic network)을 만들어(정교화) 기억하는 기술을 말함</p>
	<br>   
	<p class="stxt1">다음은 앳킨슨-쉬프린의 기억의 모델입니다.</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/013/info_01.jpg" alt="앳킨슨-쉬프린의 기억의 모델" /></p>
</div>

<div class="bdln pt40">
	<p class="stit4">암기기억전략을 이루는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li>선택적 주의력</li>
			<li>시공간 지각력</li>
			<li>학습내용 조직화 범주화 </li>
			<li>학습내용 유의미화</li>
			<li>반복과 시연 </li>
			<li>작절한 휴식과 수면 </li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">암기기억 전략이 학습 성취에 미치는 영향</p>
	<p class="stxt1">질문과 대답을 통해, 암기기억을 더 잘 할수 있는 방법을 알아볼까요?</p>
	<p class="bgq1">암기기억 전략이 공부에 꼭 필요한가요?</p>  
	<p class="bga1">YES!!!입니다.</p>
	<p class="lst3 bgbl3">질 높은 학습이 이루지기 위해 암기·기억전략에 대한 이해와 적용이 필수적입니다.</p>
	<p class="lst3 bgbl3">학습은 기억의 양과 질을 통해서만 그 결과를 측정할 수 있습니다. </p>
	<p class="lst3 bgbl3">기억력은 성공적인 학습자와 열등한 학습자를 확연하게 구분하는 역할을 합니다.</p> 
	<p class="lst3 bgbl3">기억력과 학업성취와의 상관이 매우 높다는 연구결과가 많습니다.</p>
	
	<p class="bgq2">암기기억의 방법이 있나요?</p>
	<p class="bga1">물론 있습니다. 본 세종시 자기주도학습 홈페이지에서 제공되는 암기, 기억전략강화법을 꼭 여러분의 것으로 만든다면 여러분도 암기왕, 기억왕이 될 수 있습니다.</p>
	<p class="quotation">참고) 암기기억전략 웹툰도 참고하세요. 네이버 웹툰의 인기작가가 암기.기억전략을 여러분에게 쏙 쏙 이해되게 알려줄 겁니다.</p>
	<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}${browserMode}/cont/webtoon/cont_webtoon_list?id_measurement=MSRMNT_013" class="btn_webContents">암기기억전략 웹툰 보러 가기</a></div>
</div>

<p class="stit5"></p>