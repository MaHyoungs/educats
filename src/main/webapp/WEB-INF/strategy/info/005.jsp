<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">
		동기(motive)란 라틴어 'movere'에서 유래한 것으로 행동을 발생시키는 원인이 되고, 행동의 방향과 목표를 제시하며, 행동의 수준이나 강도를 결정하는 심리적 상태입니다.<br>
 		학습동기(Learning Motivation)란 학습상황에서 학습자 스스로 과제를 선택하고, 선택한 과제를 해결하기 위해 지속적인 노력을 기울이며, 어려운 상황에 직면하더라도 끈기를 보이는 힘의 근원이라고 정의합니다.(Schunk, 1990)
	</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/info_01.jpg" alt=""></p>
</div>

<div class="bdln pt40">
	<p class="stit4">구성요소</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 지적호기심</li>
			<li> 목표의식</li>
			<li> 도전의식</li>
			<li> 경쟁의식</li>
			<li> 성취목표</li>
			<li> 자기효능감</li>
			<li> 사회적 지지</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학업 성취에 미치는 영향</p>
	<p class="stxt1">
		질문과 대답을 통해, 자기효능감에 대해 알아볼까요?
	</p>
	<p class="bgq1">학습동기는 무엇인가요?</p>
	<p class="bga1">학습 동기란, 우리로 하여금 공부 하게 만드는 마음 상태, 혹은 의지, 행위를 하게 하는 무언가입니다.</p>
	<p class="bgq2">학습동기에는 어떤 것이 있나요?</p>
	<p class="bga2">외재적동기, 내재적동기로 나눌수 있습니다.</p>
	<p class="lst3 bgbl3">  외재적 학습동기: 외부에서 보상(상, 선물, 용돈 등)을 얻기 위해 공부하는 것.</p>
	<p class="lst3 bgbl3"> 내재적 학습동기: 학습자 스스로 내면적인 보상과 성취감을 얻기 위해 공부하는 것.</p>
	<p class="bgq3">학습동기는 왜 중요한가요?</p>
	<p class="bga3">학생들이 학습에 대한 충분한 동기화가 형성 되었을 때 학습에 필요한 다양한 학습전략도 적절히 활용해 학업성취를 향상 시킬 수 있다.
학습에 대한 동기화가 되어있지 않으면 아무리 우수한 학습기술을 보유하고 있다 하더라도 좋은 결과에 이룰 수 없다. 그러므로 학습동기는 학업성취에 영향을 주는 핵심 변인이라고 할 수 있다.</p>
	<p class="bgq4">무엇이 학습동기를 강화하나요?</p>
	<p class="bga4">구체적인 목표입니다. 목표가 뚜렷할 때, 우리는 학습동기를 강하게 가지고, 또 오래 유지할 수 있습니다. (다음 명언을 참고하세요.)</p>
	<p class="quotation mt40">
		성공한 사람들은 실패한 사람들이 하기 싫어하는 일을 하는 습관을 가지고 있다. 그들도 그런 일을 하고 싶지 않은 것은 마찬가지다. 
 다만, 그들은 자기가 가진 <span class="red">목표의 힘으로 하기 싫다는 생각을 극복한다.</span> &nbsp;&nbsp;-알버트 그레이-
	</p>
	<p class="bgq5">학습동기의 지속을 막는 잘못된 습관은 왜 생기나요?</p>
	<p class="bga5">오랫동안 반복해서 생긴 굳어진 잘못된 습관을 고치는 방법에는 어떤 것이 있을까요? 좋은 행동이든 나쁜 행동이든, 그 행동을 오랫동안 반복하면 습관으로 형성됩니다. 그렇게 되면 어느새 자기도 모르게 원하지 않는 행동이 습관처럼 반복되어 나타납니다. 예를 들면 습관적으로 지각하기, 습관적으로 TV 보기, 습관적으로 스마트폰 하기 등.
 이러한 나쁜 습관들은 학습동기가 실제 학습 행위로 실현 및 지속되는 것을 가로 막는 원인이 됩니다. </p>
 	<p class="bgq6">그러면 이러한 잘못된 습관을 고치는 방법은 없을까요?</p>
	<p class="bga6">있습니다. 다음을 실천해 보세요.</p>
		<p class="lst3 bgbl3 ml100"><span class="bold">고치고 싶은 잘못된 습관이 되버린 행동을 세분화합니다.</span><br>
		습관이 되버린 잘못된 행동은 우리 몸에 베어 있습니다. 이 고치고 싶은 행동을 없애기 위해서는 그 행동을 단계별로 쪼개어 보세요. 단계별로 쪼개어진 작은 행동들을 점점 바꾸다 보면, 전체 행동이 어느새 모두 바뀌어 있습니다. 
		</p>
		<p class="lst3 bgbl3 ml100"><span class="bold">자기 행동들을 기록하세요.</span><br>
		시간대별로 자기가 행동을 할 때 마다, 스스로를 모니터링 하여 기록을 하세요. 자기 행동 보정의 중요한 단서가 됩니다. 기록표를 보고, 실천이 안되었을 때는 반성도 하고, 잘 하고 있을 때는 스스로에게 칭찬도 해주세요. 
		</p>
		<p class="lst3 bgbl3 ml100"><span class="bold">행동수정을 잘 한 스스로에게 적절한 상을 주세요.</span><br>
		예로 반복된 나쁜 습관을 안 했을 경우, 나에게 맞는 칭찬과 상을 주도록 하세요. 칭찬을 꼭 타인으로부터 들어야 하는 것은 아닙니다. 스스로에게 칭찬하고 격려해 주는 사람이 진정한 성공자입니다. 
		</p>
	
</div>

<p class="stit5"></p>