<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="sb2_con">
	<p class="tit">자기효능감 이란?</p>
	<p class="tit2">정의</p>
	<p class="txt1">
		자기 효능감이란 과제를 끝마치고 목표에 도달할 수 있는 자신의
		능력에 대한 스스로의 평가를 뜻합니다. 
		예를 들어 특정 과목 목표 점수 달성하기, 숙제 만족스럽게 
		끝마치기, 계속 공부하기 위한 요구 조건들 만족하기 등 스스로 
		성공적으로 할 수 있으리라는 신념을 자기 효능감이라고 합니다. 
	</p>

	<p class="tit2">구성요소</p>
	<p class="txt1">
		자아개념 / 자아존중감 / 자신감 / 귀인성향 / 과제지속력 / 
		성실성 / 자아성찰 / 사회성 / 낙관성 / 자기실현  
	</p>

	<p class="tit2">학습 성취에 미치는 영</p>
	<p class="txt2">
		<span class="nbg">1</span>
		<span class="nbt">자기 효능감은 실제 능력보다 과대평가하게 만든다</span>
		<span class="nbt2">ex) 가장 적정 수준의 자기 효능감 : 실제보다 15% 상위 난도</span>
   	</p>

	<p class="txt2">
		<span class="nbg">2</span>
		<span class="nbt">높은 자기 효능감은 동기에 따라 긍정적일 수도 부정적일 수도 있다.</span>
		<span class="nbt2">긍정 요인 : 과제 해결을 위해 더 많이, 더 오랫동안 노력
   			부정 요인 : 낯선 주제에 대해 배우려는 준비 부족 </span>
   	</p>

	<p class="txt2 pb">
		<span class="nbg">3</span>
		<span class="nbt">낮은 자기 효능감은 과제를 실제보다 어렵게 판단한다.</span>
	</p>

	<p class="txt2">
		<span class="nbg">4</span>
		<span class="nbt">건강과 관련된 선택들은 자기 효능감에 따라 행동을 변화 시킨다.</span>
		<span class="nbt2"> ex) 흡연, 운동, 다이어트, 치아 건강, 안전띠 착용, 유방암 자가 진단 등</span>
   	</p>

   	<div class="bot_btn bdt"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>
</div>