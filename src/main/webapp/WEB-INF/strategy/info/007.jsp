<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">효과적인 수업참여 전략이란</p>
	<p class="stxt1">효과적인 수업참여란 수업장면에서 학생이 학습목표를 달성하기 위해 수업에 적극적으로 참여하고, 학습 효과를 높이기 위해 수업 중 주어진 과제에 주의를 집중하는 행동을 가리킵니다.</p> 
	<p class="lst2 bgbl2">뇌과학적으로 우리의 뇌는 각 감각기관으로부터 받은 정보를 뇌의 감각 기관인 뉴런에 저장하게 됩니다.</p>
 	<p class="lst2 bgbl2">나중에 기억 저장소에서 기억해 낼 때, <font class="bold">시각, 청각, 여러 감각 기관이 동원된 '수업'을 통해, 눈이나 귀 등의 하나의 감각 기관에 의존하여 혼자 공부할 때 보다 더 빠르고 정확히 해당 학습내용을 이해할 수 있습니다. 이렇게 충분한 이해를 기반으로 하여 학습한 내용은 기억과 응용에서도 우리 뇌는 능력을 더 잘 발휘할 수 있습니다.</font></p>
</div>

<div class="bdln pt40">
	<p class="stit4">수업참여 전략을 이루는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li>주의집중하기</li>
			<li>수업중 노트하기</li>
			<li>적극 질문하기</li>
			<li>발표하기</li>
			<li>학습기술 적용하기</li>
			<li>선택적 주의집중력</li>
			<li>복습하기</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학업 성취에 미치는 영향</p>
	<p class="stit7"><span>수업은 우리 두뇌에게 이해할 수 있는 기회를 제공해 주는 가장 강력한 학습 방법입니다.</span></p> 
	<p class="stxt1"><b>질문과 대답을 통해, 효과적인 수업참여 전략을 알아볼까요?</b><br/>
	여러분 학습전략에 대한 8개의 웹툰이, 여러분들로 하여금 더 재미있게 학습전략을 익힐 수 있도록 도와줍니다. 꼭, 학습전략-수업참여전략 편을 꼭 보시기 바랍니다.</p>
	<p class="tabAreaPcenter"><a href="${pageContext.request.contextPath}${browserMode}/cont/webtoon/cont_webtoon_list?id_measurement=MSRMNT_007" target="" class="cnt_btn">수업참여 웹툰 보러가기</a></p>
	<p class="bgq1">수업 참여 전략이 왜 중요한가요?</p>
	<p class="bga1">수업은 공부의 핵심이라고 할 수 있지요. 구체적으로는 다음과 같은 잇점이 있습니다.</p>
	<p class="lst3 bgbl3">수업 중 다양한 학습기술(learning arts)을 사용하여 이해력과 기억력을 강화시켜줍니다.</p>
	<p class="lst3 bgbl3">적극적인 수업 참여활동은 수업 중 교사와 상호작용을 통해 수업내용을 이해하고 기억하는 데 효과적입니다.</p>
	<p class="lst3 bgbl3">교사와 적극적인 상호작용을 통해 학업적 효능감을 높게 유지할 수 있습니다.</p>
	
	<p class="bgq2">학원과 야간자습으로 너무 피곤해요. 혼자 공부하는 게 차라리 더 진도도 많이 나가고 좋지 않나요?</p> 
	<p class="bga2">효과적인 수업참여 활동은 학업성취를 향상시키는데 중요한 요인으로 작용합니다. 혼자 하는 공부는 수업의 앞, 뒤의 예습과 복습, 그리고 시험공부 평소의 부진한 부분에 대한 보강 및 실력 향상의 부수적인 학습활동은 되지만, 학습의 꽃은 수업입니다. 특히, 새로운 학습 정보를 받아들이는 학교 수업은, 학교 내신이 매우 중요한 시점에서 곧 시험성적표라고 할 수 있습니다.</p>
	
	<p class="bgq3">관심 없는 과목 수업에는 집중이 안되요. 어떻게 해야 하나요? </p>
	<p class="bga3">오히려 그 과목의 수업에 적극 참여하면, 거꾸로 그 과목을 좋아하게 된답니다. 선생님에 대해서는 늘 최고의 존경심과 긍정의 마인드로 대하세요. 그렇게 했을 때, 제일 이익을 보는 분은 선생님이 아니라, 바로 여러분입니다. 선생님&수업을 좋아하고 적극참여하게 되면, 그 과목을 좋아하게 되고, 그러면 어느새 성적도 쑥쑥 올라갑니다.</p> 
	<p class="lst3 bgbl3">적극적인 수업참여와 주의집중(student engagement)은 학습 활동과 관련하여 특정 과목에 대한 강한 관심과 흥미를 유발하는 정서적 반응을 나타냅니다. </p>
	<p class="lst3 bgbl3">이는 높은 학업적 효능감을 자극하여 학습활동에 자신감을 갖게 하고 학습동기를 형성하게 합니다. </p>

</div>

<p class="stit5"></p>