<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">자연관찰 지능이란</p>
	<p class="stxt1">다중지능에서 자연관찰지능이란 <font class="bold">자연친화적 지능으로서 동식물에 대한 관심</font>과 자연환경에 있는 규칙성을 관찰하고 이해할 수 있는 능력입니다. 즉, 환경 관찰을 잘 하고, 이를 이해하는 능력입니다.</p> 
	<p class="lst2 bgbl2">새, 꽃, 나무 등 동․식물에 관심이 많습니다.</p> 
   	<p class="lst2 bgbl2">동․식물의 습성과 생리에 깊은 관심을 보입니다.</p> 
  	<p class="lst2 bgbl2">자연물들을 오랫동안 관찰하곤 합니다.</p>
  	<p class="lst2 bgbl2">동․식물을 기르는데 남들보다 다른 열정을 가집니다.</p>
  	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">자연주의 지능을 이루는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li>식물에 대한 관심</li>
			<li>동물에 대한 관심</li>
			<li>환경 생태계관심</li>
			<li>자연친화력</li>
			<li>환경문제</li>
			<li>자연생태계에 대한 관심</li>
			<li>동식물 관리에 애정</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">자연관찰지능이 우수한 학습자의 추천</p>
	<p class="lst2 bgbl2">관찰, 관계지각(분류, 수집, 상호관계), 가설설정 및 실험,  자연학습(야외 자연 활동, 야외 체험학습)을 통해 공부하기</p>
	<p class="lst2 bgbl2">학습 자료를 수집하고 분류하기</p>
</div>

<div class="bdln pt40">
	<p class="stit4">공부법</p>
	<p class="lst2 bgbl2">오감(시각, 청각, 후각, 미각, 촉각)을 통해 사물이나 현상을 주의 깊게 관찰하기</p>
	<p class="lst2 bgbl2">학습내용의 관계를 자연(동물, 식물, 사물)의 비유로 상상하기</p>
	<p class="lst2 bgbl2">배운 혹은 배우게 될 지식이 자연세계와 어떻게 조화를 이루는지 생각해보기</p>
	<p class="lst2 bgbl2">몇 가지 단서들이 있으면 조금씩 추적하여 답을 찾기</p>
	<p class="lst2 bgbl2">정원을 가꾸거나 좋은 생각의 씨앗을 뿌리고 키우는 것을 상상하며 공부하기</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="bgq1">자연주의지능은 자연과의 상호작용을 잘 하는 것을 말하나요?</p>
	<p class="bga1">자연주의지능은 자연과의 상호작용을 잘하는 것만을 말하지 않습니다. 자연주의지능은 현상을 잘 관찰하고, 이를 구분하고 분석하는 능력과 관련되어 있습니다. 예를 들면, 우는소리로 벌레 이름을 구별할 수 있습니다.</p>
	
	<p class="bgq2">자연주의지능이 우수한 학생들은 어떤 특징이 있나요?</p>
	<p class="bga2">자연주의지능이 우수한 학생들은 다음과 같은 특징이 있습니다.</p>
	<p class="lst3 bgbl3">자연주의지능이 우수한 학생은 관찰하는 능력이 우수합니다.<br/>
	예) 예술 작품 감상을 통해 자신의 인생에 대한 교훈을 얻으며, 미적 체험활동을 통한 상상력 학습에 탁월합니다.</p>
	<p class="lst3 bgbl3">자연주의지능이 우수한 학생은 현상을 잘 분류합니다.<br/>
	예) 하늘의 구름 모양을 보고 토끼, 비행기, 백두산 등 친숙한 사물의 모습을 발견합니다.</p>
	<p class="lst3 bgbl3">자연주의지능은 다른 지능과 밀접한 연관을 가집니다.<br/>
	예) 바다를 좋아하는 사람은 파도 소리를 들으며 노래의 리듬을 만들어 내고 갈매기를 보며 시구를 떠올리기도 합니다.</p>
	<p class="lst3 bgbl3">자연관찰지능이 우수한 학생들은 조직이나 모임에서 다른 학생들과 잘 어울리거나 융화합니다. </p>
	<p class="bgq3">자연주의지능이 우수한 학생들은 호기심이 많아 관찰을 즐긴다고 하는데요.<br/>이것은 어떤 학습방법과 관련 지으면 좋을까요?
	<p class="bga3">맞아요. 자연주의지능이 우수한 학생은 호기심이 많고 뭐든 관찰하려는 경향을 보입니다. 따라서 발산적 사고 및 상상력이 풍부합니다. 심도 있는 관찰을 요구하는 ‘상상력 학습’은 이러한 학생에게 적합한 학습 방법입니다. 따라서, 자연관찰지능이 높은 학생은 호기심을 바탕으로 한 관찰을 즐기므로, 창의적인 아이디어를 떠올리는 특징이 있습니다.</p>
	<p class="quotation">
	 *발산적 사고(divergent thinking)란?
	 <font class="red">확산적 사고</font>라고도 하며, 길포드(Guilford)가 연구한 지능에 대한 가설적 모형에서 쓰여진 용어입니다.
	  창의적 사고로 유창성, 융통성, 독창성의 특성이 있습니다. 즉, 이미 기억된 정보로부터 새롭고 신기하며 다양하고 관습에서 벗어난 생각과 해결책을 생성하는 생산적 사고를 말합니다.
	  ↔ 반대 개념으로는 수렴적 사고가 있습니다.
	</p>
	<p class="stxt1">간단하게 그림으로 나타내면 다음과 같습니다.</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/022/info_01.jpg" alt="확산적사고와 수렴적사고" /></p>
	<p class="quotation">창의성이 부족한 학생들은 자연친화적 활동(등산, 화초가꾸기, 동식물 기르기, 야영하기, 텃밭 가꾸기 등)을 통해 확산적 사고를 키울 수 있는 훈련을 받도록 합니다.</p>
</div>

<div class="bdln pt40">
	<p class="stit4">자연주의 지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="lst2 bgbl2">연구원 - 생명공학자, 유전공학자, 생물학자, 식물학자, 천문학자, 조류학자</p>
	<p class="lst2 bgbl2">의사 – 외과의사, 수의사</p>
	<p class="lst2 bgbl2">환경운동가</p>
	<p class="lst2 bgbl2">농장운영자</p>
	<p class="lst2 bgbl2">동식물 – 조련사, 식물 전문가, 화훼장식가, 화훼디자이너</p>
	<p class="lst2 bgbl2">교육 – 생물교사, 지구과학교사</p>
</div>
<p class="stit5"></p>