<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">예습이란, 본격적인 학습을 하기에 앞서 공부할 내용을 미리 살펴보고 학습목표에 도달할 수 있도록 준비 자세를 갖추는 것을 말합니다. 다시 말하면, 공부할 내용을 미리 살펴보는 것으로 마치 도착해야 할 목표(학습목표)를 사전에 내비게이션으로 확인하는 것과 같습니다.</p>
	<p class="stxt1">복습이란, 학습한 내용을 확실하게 자기 것으로 만들기(기억의 공고화) 위해서 행하는 일련의 학습 후 활동을 말합니다. 다시 말하면, 학습의 완성이라는 목표지점을 향하여 끊임없는 반복훈련을 통해 공부한 내용을 확실하게 자기 것으로 만드는 작업입니다.</p>
</div>

<div class="bdln pt40">
	<p class="stit4">효과적인 예습과 복습활동을 위한 동일 구성요소</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 학습계획성</li>
			<li> 학습준비성</li>
			<li> 생활규칙성</li>
			<li> 목표지향성</li>
			<li> 학습당일 복습습관</li>
			<li> 자기감찰</li>
			<li> 기억의 공고화</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="bgq1">예습은 필요한가요?</p>
	<p class="bga1">네, 꼭 필요합니다. 예습의 고전적인 효과는 다음과 같습니다.</p>
	<p class="lst3 bgbl1">첫째, 아는 것과 모르는 것을 구분하기 위함입니다.</p>
	<p class="lst3 bold">예습을 통해 자신이 아는 것과 모르는 것을 구분하여, 학습과정에 참여 할 수 있어 빠르게 이해할 수 있게 합니다.</p>
	
	<p class="lst3 bgbl1">둘째, 수업의 집중력을 높이기 위함입니다.</p>
	<p class="lst3 bold">예습 시 생긴 질문을 확인하는 것이 수업입니다. 그러므로 예습을 한 학생들은 수업시간에 해당 지식을 구경만 하는 것이 아니라, 이미 자신의 것으로 소화하고 있습니다.</p>
	<p class="lst3 bgbl1">셋째, 뇌과학적으로 예습을 통해 불완전한 내용 일부를 미리 봄으로써 뇌는 완성되지 않은 상태의 정보를 인식하게 되고 스스로 정보의 퍼즐을 완성하기 위해 수업시간에 새로 익힌 정보와 조합하여 학습의 심화 효과를 보게 됩니다.</p>
	<br>
	<p class="lst3">예습 한 상태에서 두뇌가 정보를 연합하여 저장하는 과정을 그림으로 볼까요?</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/info_01.jpg" alt=""></p>
	
	<p class="bgq2">복습은 꼭 해야 하나요? 학원도 가야하고, 숙제도 해야 하는데요.</p>
	<p class="bga2">다음 두 가지 이유로 꼭 복습을 하셔야 합니다. 시간을 내서요.</p>
	<p class="lst3 bgbl1">계획적이고 규칙적인 복습활동은 기억의 망각 활동을 줄여 학습내용에 대한 이해와 기억을 증진시킵니다. 아래 에빙하우스의 망각곡선을 참고하세요.</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/info_02.jpg" alt=""></p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/info_03.jpg" alt=""></p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/info_04.jpg" alt=""></p>
	
	<p class="lst3">독일 심리학자 에빙하우스는 불규칙한 무작위의 단어를 학생들에게 암기하게 한 후 망각율을 측정해 본 결과 수업 직후인 1시간 이내가 망각율이 가장 높게 나타났습니다. <b>따라서 단기기억을 장기기억으로 변환되는 가장 중요한 복습 시점은 쉬는 시간 또는 수업 직후 또는 방과후 집에 가서 (그날 안에) 하는 것이 효과적입니다.</b></p>
	<p class="lst3 bgbl1"> 복습활동을 통하여 학습 내용을 의미 있는 아이디어로 재조직하고 변형시켜 문제해결능력을 길러줍니다.</p>
</div>

<p class="stit5"></p>