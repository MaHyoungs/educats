<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1"> 학습전략에서 주의집중이란 “학습자가 학습의 과정에서 외부로부터 들어오는 여러 자극들을 선별하여 학습하려는 대상에 자신의 의식과 주의를 기울일 수 있는 능력”입니다. 즉, ‘일정한 시간 동안 지속적으로 한곳에 모든 정신을 집중해서 몰입하는 능력’을 의미합니다.</p>
	<p class="stxt1"> 주의집중은 외부의 방해가 되는 자극으로부터, 흐트러짐 없이 학습에 집중하는 것뿐만 아니라, 인생에 있어서 많은 유혹으로부터 내면의 일관성을 유지하는 것 또한 포함합니다. </p> 
</div>

<div class="bdln pt40">
	<p class="stit4">주의집중력을 구성하는 요소</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 선택적 주의력</li>
			<li> 정보처리능력</li>
			<li> 자기통제력</li>
			<li> 과제 지속력</li>
			<li> 성취감</li>
			<li> 충동조절능력</li>
			<li> 결과기대</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="stit7"><span>학업 장면에서 집중력은 학업성취의 바로미터가 됩니다.</span></p>
	<p class="bgq1">집중력과 학업성취가 관계가 있나요?</p>
	<p class="bga1">절대적으로 관계 있지요. 학업성취를 결정하는 학습자 개인의 주요 변인으로 주의 집중력은 가장 필수적인 요소라고 할 수 있습니다. 인간의 학습과정을 정보처리 관점에서 살펴 볼 때 학습과정의 초기단계에서 가장 결정적 영향을 미치는 것은 주의집중입니다. </p>
	<p class="bgq2">주의집중력을 높이려면 어떻게 해야 하나요?
	<p class="bga2">본 세종 Smart-아이 자기주도적학습 웹툰이나 학습력 강화법에서 제시한 방법들을 꼭 매일매일 읽고, 여러분의 것으로 만드십시오. 더불어서 본 홈페이지에서 제공되는 뇌파음원 중 집중력을 강화하는 뇌파음원을 들으면, 주의집중에 많은 도움이 됨이 많은 연구에서 밝혀졌습니다. 뇌에 도움이 되는 음식, 수면, 뇌파, 운동. 이 모든 것이 모여서 여러분의 주의집중과 학습 성취를 결정짓는데 영향을 미칩니다.
	<p class="quotation"><span>구슬이 서말이라도 꿰어야 보배!<br/>
	세종시 학생들에게만 제공되는 자기주도학습 학습자료(최고의 명강의 인터넷 강좌), 웹툰, (설문을 바탕으로 한 맞춤식) 학습력강화법, 뇌파음원, 무료로 제공되는 전자도서관의 도서들로 집중력과 학습력을 쑥쑥 키우십시오.</span></p>
</div>
<p class="tabAreaPcenter"><a href="${pageContext.request.contextPath}${browserMode}/cont/sound/cont_sound_home" class="cnt_btn">뇌파음원 듣기 바로가기</a></p>

<p class="stit5"></p>