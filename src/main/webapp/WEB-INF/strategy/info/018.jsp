<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">음악적 지능이란</p>
	<p class="stxt1">다중지능에서 음악지능이란 <font class="bold">음악적 표현 형식을 지각하고(음악애호가), 변별하고(음악비평가), 변형하고(작곡가), 표현하는(연주자) 능력</font> 을 말합니다.음악지능이 뛰어난 사람들은 곡을 뇌에서 인지할 때 논리수학지능인 논리체계 또는 공간지능인 공간체계로 변화시켜 인지하게 됩니다. 태아는 엄마 뱃속에 있는 10개월 동안 엄마의 심장박동을 통해 세상을 처음으로 인식하게 됩니다. 따라서 인간은 태어나면서부터 음악적 능력을 통해 사물을 인식하고 사고하게 됩니다.</p>  
	<p class="quotation"><span>
	- 음조 : 음의 강약, 빠르고 느린 것<br/>
	- 음정 : 높이가 다른 두 음 사이의 간격<br/>
	- 음절 : 종합된 음의 느낌을 주는 말소리의 단위<br/>
	- 음색 : 음을 만드는 구성요소의 차이로 생기는 감각적 특색<br/>
	- 리듬 : 음의 규칙적인 흐름<br/>
	- 화성 : 일정한 법칙에 따른 화음의 연결
	</span></p>
	<p class="stxt1 bold">음악지능이 뛰어난 사람들의 특징</p>
	<p class="lst2 bgbl2">음의 리듬을 느낄 수 있습니다. </p>
	<p class="lst2 bgbl2">음절에 맞는 화성을 잘 찾아냅니다. </p>
	<p class="lst2 bgbl2">화성의 흐름 (C → F → G → C)을 바탕으로 음을 표현합니다.</p>
	<p class="lst2 bgbl2">여러 악기들의 음색을 잘 파악하고, 자신의 생각을 조화로운 음색으로 표현할 수 있습니다.</p>
	<p class="lst2 bgbl2">발성을 잘 합니다.</p>
	<p class="lst2 bgbl2">가창력이 뛰어납니다.</p>
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">음악적 지능을 이루는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li>가창능력</li>
			<li>연주능력(악기 다루는 기술)</li>
			<li>작곡능력</li>
			<li>감상능력</li>
			<li>음악적 감수성</li>
			<li>리듬감수성</li>
			<li>멜로디 감수성</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">음악지능이 우수한 학습자의 추천 공부법</p>
	<p class="lst2 bgbl2">노래하기와 악기 연주하기 등과 같은 편안한 소리 환경을 조성하여 공부하기</p>
	<p class="lst2 bgbl2">학습내용의 요지를 추려 노래, 랩 음악, 리듬치기, 창의 리듬 형태로 바꾸기</p>
	<p class="lst2 bgbl2">노랫가락이나 높낮이 등을 통해 암기하기</p>
	<p class="lst2 bgbl2">공부 전이나 후에 음악을 듣거나 악기 연주하기</p>
	<p class="lst2 bgbl2">기억해야 하는 새로운 정보를 리듬이나 노래에 맞추어 암기하기</p>
	<p class="lst2 bgbl2">영어단어 암기시 음악적 지능을 활용한 발음 암기하기 (예)germ이라는 단어의 발음인 ‘<font class="bold">점</font>’을 활용하여 ‘세균이 점점 퍼져나간다’라는 힌트를 만들 수 있습니다. 또한 <font class="bold">찬트(chant)</font>나 팝송을 통한 영어단어 암기도 음악적 지능과 관련이 있습니다.)</p>
	<p class="lst2 bgbl2">천천히 큰 소리로 말하거나 빨리 말하면서 공부하기</p>
	<p class="lst2 bgbl2">학습할 내용과 관계가 있는 음악을 찾아보기</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="lst1 bgbl1">학습과정에서 나타나는 대표적인 음악적 지능의 유형은 노래부르기, 악기 연주하기, 녹음하기, 지휘하기, 작곡하기, 즉흥 연주하기, 편곡하기, 각색하기, 듣기, 음을 구별하기, 조율하기, 관현악 작곡하기, 음악 분석하기, 음악 비평하기 등입니다.</p>
	
	<p class="lst1 bgbl1">음악적 지능이 우수한 경우에도 개인마다 노래하기가 뛰어난 사람, 악기 연주가 우수한 사람, 작곡하는 능력이 뛰어난 사람으로 구분될 수 있습니다.</p>
	
	<p class="lst1 bgbl1">수업장면에서 멜로디나 리듬을 이용하는 수업에 유리합니다. 이러한 특성으로 인해, 리듬을 이용하여 암기하는 것이나 음악을 들으면서 공부하면 많은 도움이 됩니다. ‘시’의 운율(리듬)을 살려 시를 이해하도록 합니다.</p>
</div>

<div class="bdln pt40">
	<p class="stit4">음악적 지능이 높은 학습자에게 유망한 진로적성 및 직업</p>
	<p class="lst2 bgbl2">노래 – 가수, 성악가, 오페라가수</p>
	<p class="lst2 bgbl2">악기 – 반주자, 교향곡 연주가, 피아노 조율사</p>
	<p class="lst2 bgbl2">작곡 – 작곡가, 영화 음악 작곡가</p>
	<p class="lst2 bgbl2">음악 치료사</p>
	<p class="lst2 bgbl2">음향 기술자</p>
	<p class="lst2 bgbl2">음악 평론가</p>
	<p class="lst2 bgbl2">음악 공연 연출가</p>
	<p class="lst2 bgbl2">지휘자</p>
	<p class="lst2 bgbl2">음악 교사</p>
	<p class="lst2 bgbl2">음반 제작자</p>
</div>

<p class="stit5"></p>