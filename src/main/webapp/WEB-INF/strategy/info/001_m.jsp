<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="sb2_con">
	<p class="tit">자기효능감 이란?</p>
	<p class="tit2">정의</p>
	<p class=txt1nobottom>
		자기 스스로를 소중하게 여기는 마음입니다. 자기 효능감이 높은 사람은 스스로를 믿고, 어떤 과제를 끝까지 완수할 수 있는 능력이 강한 사람입니다.
	</p>
	<p class=txt1nobottom>
		예를 들어, 나에게 주어진 어려운 과제를 중간에 포기하지 않고 끝까지 완주하는 것을 말합니다.
	</p>
	<p class="txt2">	
		<span class="nbg">1</span>
		<span class="nbt">중간·기말고사 목표 달성하기</span>
	</p>
	<p class="txt2">
		<span class="nbg">2</span>
		<span class="nbt">매일 일기쓰기를 거르지 않기</span>
	</p>
	<p class="txt2">
		<span class="nbg">3</span>
		<span class="nbt">스스로 정한 생활 계획표 실천하기</span>
	</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/001/info_01.jpg" alt=""></p>
	<p class="onlybottom"></p>
	<p class="tit2">구성요소</p>
	<p class="txt1">
		자신감 / 귀인성향 / 자기조절 / 자기존중감 / 높은 과제 난이도 선호 
		과제지속력 / 낙관성  
	</p>

	<p class="tit2">학습 성취에 미치는 영향</p>
	<p class="txt1nobottom">
		자기효능감은 실제 본인의 능력으로 달성하기 어려운 학습 과제를 완수할 수 있게 도와줍니다. 
	</p>
	<p class="txt1nobottom">
		질문과 대답을 통해, 자기효능감에 대해 알아볼까요? 
	</p>
	<p class="txt2">
		<span class="nbg">1</span>
		<span class="nbt">자기효능감을 고려할 때, 가장 적정한 수준의 목표 달성 수준을 어떻게 되나요?</span>
		<span class="nbt2">실제 능력보다 약 15% 상위 난도로 목표를 설정하는 것이 성취동기 달성에 가장 효과적라고 연구 결과에서 밝혀졌습니다.</span>
   	</p>

	<p class="txt2">
		<span class="nbg">2</span>
		<span class="nbt">자기 효능감이 높은 것은 무조건 좋은가요?</span>
		<span class="nbt2">꼭 그렇다고 할 수는 없지만, 자기효능감은 학업에 긍정적 인 역할을 합니다.<br>
   			긍정 요인 : 자기효능감이 높은 사람은 주어진 과제에 대해 두려워하기보다, 해결에 대한 자신감을 갖고 과제를 대하므로, 더 많은 과제를, 더 적극적으로 해결할 수 있습니다.<br>
   			부정 요인 : 자기 효능감이 과도하게 높은 사람은, 간혹 주어진 과제에 대해 자신감이 높은 나머지, 본인이 이미 다 알고 있다고 자만하고, 배우려는 태도가 부족할 수도 있습니다.
   		</span>
   	</p>

	<p class="txt1nobottombold">
		자기효능감은 학습전략처럼 구체적으로 드러나지 않지만, 학업에 있어서 가장 큰 영향을 미치는 요소 중의 하나입니다. 
	</p>
	<p class="txt1nobottombold">
		자, 그러면 여러분의 자기효능감 정도와 자기효능감을 높이기 위한 방법을 알아 볼까요? 
	</p>

   	<div class="bot_btn bdt"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>
</div>