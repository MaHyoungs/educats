<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">다중지능에서 공간지능이란 시각적·공간적 세계를 정확하게 지각하는 능력과 그런 지각을 통해 형태를 바꾸는 능력을 말합니다.</p>
	<p class="stxt1">이 지능에는 색, 선, 모양, 형태, 공간과 이런 요소들 간에 존재하는 관계에 대한 감수성이 포함됩니다. 또 추상적인 것을 구체화하는 시각화 능력, 시각적·공간적 아이디어를 기하학적으로 표현하는 능력, 자신을 어떤 공간상에 적절하게 위치시키는 능력 등이 포함됩니다.</p>
	<p class="stxt1">예술가, 건축가, 실내장식가, 발명가와 지리 안내원, 항해사, 비행기 조종사는 뛰어난 공간지능을 보이는 직업군입니다. </p>
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">다중지능 중 공간 지능을 구성하는 요소</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 공간 인식력</li>
			<li> 공간을 통한 예술작업</li>
			<li> 공간적 전환능력</li>
			<li> 시각화 능력</li>
			<li> 그림그리기</li>
		</ul> 
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="lst2 bgbl2">공간적 지능이 뛰어난 학습자들은 시각적으로 표현하는 그리기, 만들기, 디자인을 좋아하며, 어떠한 정보에 대해 그림 또는 이미지, 공간적 배열을 통해 유연한 다면적 정보를 상황에 맞게 유기적으로 잘 변환합니다. </p>
	<p class="lst2 bgbl2">공간지능은 상상력과 창의성을 발육시키는 기름진 땅과 같습니다. 공간지능의 주요 기능은 시각 세계를 정확히 지각할 수 있도록 하며 적합한 물리적 자극이 없이도 시간적 경험을 재창조 할 수 있도록 합니다.  </p>
	<p class="lst2 bgbl2">공간적 지능의 발달은 풍부한 사고력을 길러주며, 사고의 유연성과 유창성을 높게 합니다. </p>
	<p class="lst2 bgbl2">공간적 지능의 발달은 정보의 획득과 처리를 유연하게 하고, 주어진  문제를 다면적 시각에서 해결하도록 돕습니다.</p>
	<p class="lst2 bgbl2">공간적 지능은 인지과정에서 다양한 정보처리 방법을 통해 학습효과를 높여줍니다. 특히, 정보의 장기기억으로 전환을 원활하게 하는 기능을 합니다.</p>
</div>
<div class="bdln pt40">
	<p class="stit4">공간지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="stxt1">공간지능은 독립적인 역할보다 다른 지능과 조합을 통해 시너지효과를 높여줍니다. 다양한 지능이 어우러진 아름다운 오케스트라의 하모니의 주인공 중 하나가 바로 공간지능입니다.</p>
	<p class="lst3 bgbl3">예술계: 미술교사, 화가, 디자이너(시각, 패션, 웹, 문자, 로고, 자동차 등), 조각가, 공예가, 코디네이터, 애니메이터, 그래픽 예술가 </p>
	<p class="lst3 bgbl3">건축: 건축가, 설계사, 실내 장식가, 목수, 제도사 </p>
	<p class="lst3 bgbl3">조경: 조경사 </p>
	<p class="lst3 bgbl3">광고업</p>
	<p class="lst3 bgbl3">의류: 재봉사 </p>
	<p class="lst3 bgbl3">미용사</p>
	<p class="lst3 bgbl3">발명가, 탐험가, 항해사 </p>
	<p class="lst3 bgbl3">비행사</p>
	<p class="lst3 bgbl3">사진사, 서예가 </p>
	<p class="lst3 bgbl3">일러스트레이트, 큐레이터, 치과의사, 외과의사, 요리사, 푸드스타일리스트, 비디오감독, 비디오 저널리스트, 홈페이지 제작자 </p>
</div>

<p class="stit5"></p>