<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">다중지능에서 자기성찰(자기이해)지능이란 자기 자신을 이해하고 느낄 수 있는 인지적 능력을 말합니다.</p>
	<p class="bgq1">자기성찰과 자기이해의 차이는 무엇일까요?</p>
	<p class="bga1">자기성찰은 자신을 되돌아보고 반성하여 자신의 행동 변화를 가져오는 것이고, 자기이해는 현재의 자신의 상태를 객관적 시각으로 살펴 보는 것입니다.</p>
	
	<p class="lst3 bgbl3">“나는 누구인가?”</p>
	<p class="lst3 bgbl3">“나는 어떤 감정을 가졌는가?”</p>
	<p class="lst3 bgbl3">“나는 왜 이렇게 행동하는가?”와 같이 자기 존재에 대해 이해하는 능력입니다.</p>
	<p class="stxt5"> 즉, 자기성찰지능이 높은 사람은</p>
	<p class="lst3 bgbl3">자신의 감정을 남에게 잘 전달합니다.</p> 
	<p class="lst3 bgbl3">자기 자신에 대해 자신감이 있습니다. </p>
	<p class="lst3 bgbl3">열정을 가지고 일합니다.</p>
	<p class="lst3 bgbl3">자신에게 적절한 목표를 설정할 수 있습니다.</p> 
	<p class="lst3 bgbl3">자기조절과 통제를 잘 합니다.</p> 
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">다중지능에서 자기이해(성찰)지능을 구성하는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 자기이해력</li>
			<li> 반성적 성찰력</li>
			<li> 대인문제해결력</li>
			<li> 자기조절력</li>
			<li> 자기존중감</li>
			<li> 자기효능감 </li>
			<li> 목표지향성</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">자기성찰지능이 우수한 학습자의 추천 공부법</p>
	<p class="lst2 bgbl2">자기존중감(칭찬릴레이), 초인지(학습하는 방법에 대한 학습), 정서지능, 저널 쓰기, 목표설정, 자기주도적 학습법을 활용하기</p>
	<p class="lst2 bgbl2">학습한 내용을 자신만의 이해된 언어로 표현하기</p>
	<p class="lst2 bgbl2">혼자서 공부하기</p>
	<p class="lst2 bgbl2">자신의 희망, 감정 혹은 경험을 학습내용과 결합시키기</p>
	<p class="lst2 bgbl2">학습이나 토의 또는 프로젝트를 수행하는 동안 잠깐(1분) 반성하기</p>
	<p class="lst2 bgbl2">여러 가지 정서를 느끼고 표현하면서 공부하기</p>
	<p class="lst2 bgbl2">긍정적인 혼잣말로 스스로를 격려하면서 공부하기</p>
	<p class="lst2 bgbl2">학습내용이 자신이 이전에 경험했던 바와 얼마나 같거나 다른지 파악하기</p>
	<p class="lst2 bgbl2">왜 그 지식과 내용에 대해 공부해야 되는지 생각해보기</p>
	<p class="lst2 bgbl2">각 내용에 대한 자신의 입장을 생각해보기</p>
	<p class="lst2 bgbl2">학습에 있어서 자신의 강점을 파악하고 강점을 살려 공부하기</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="bgq1"> 자기성찰 지능이 높은 학생의 특징은 무엇일까요?</p>
	<p class="bga1">&nbsp;</p>
	<p class="lst3 bgbl3">자기성찰지능이 우수한 학생은 자기 효능감, 자신감, 실천력이 높고 정서적으로 안정되어 있습니다.</p>
	<p class="lst3 bgbl3">자기성찰지능이 우수한 학생은 내재적 학습동기가 높으며, 시간관리를 비롯하여 대체로 양호한 학습습관을 가집니다.</p>
	<p class="lst3 bgbl3">자기성찰지능은 자신의 감정과 의견, 태도를 엄격히 통제할 수 있는 능력이고, 이는 학업성취도에 많은 영향을 미칩니다. </p>
	
	<p class="bgq2">자기성찰 지능을 학습에 활용할 수 있는 방법은요?</p>
	<p class="bga2">학습 과정에서 학습 플래너나 학습 성찰저널과 같은 인지도구를 사용하면 행동 수정과 변화에 효과적입니다.</p>
	<p class="tabAreaPcenter"><a href="${pageContext.request.contextPath}${browserMode}/mypg/strategy/mypg_strategy_home?seq_answer_history=${seq_answer_history}&id_measurement=MSRMNT_011&tip=true" class="cnt_btn">학습 플래너 보러가기</a></p>
	<p class="quotation"><span>인지도구(cognitive tools)란?<br/>- 학습을 촉진하는 학습도구 (예 : 특정 과목에 특화된 기능성 노트, 기능성 볼펜, 학습 플래너 등)</span></p>
</div>

<div class="bdln pt40">
	<p class="stit4">자기성찰지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="lst2 bgbl2">정신과의사, 정신분석학자, 심리학자, 심리치료사, 임상심리학자 </p>
	<p class="lst2 bgbl2">자기개발서 작가나 연설가 </p>
	<p class="lst2 bgbl2">철학자, 신학자, 성직자, 정신적 지도자 등</p>
	<p class="lst2 bgbl2">전기(傳記)작가, 소설가 </p>
	<p class="lst2 bgbl2">예술인, 작가, 작곡가 </p>
	<p class="lst2 bgbl2">자기인식 훈련프로그램 지도자</p>
</div>

<p class="stit5"></p>