<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">언어적 지능이란</p>
	<p class="lst1 bgbl1">
		 다중지능에서 언어지능이란 문자나 언어라는 상징을 잘 활용할 수 있는 능력입니다. 즉, <b>말을 잘 하거나 글을 잘 쓰는 능력</b>입니다.
	</p>
	<p class="lst2 bgbl2">말로써 다른 사람을 잘 설득합니다.</p>
	<p class="lst2 bgbl2">글로 된 내용을 잘 읽고 이해합니다. 물론 책도 좋아할겁니다.</p> 
	<p class="lst2 bgbl2">토론과 발표도 잘하고 즐겨합니다.</p>
	<p class="lst2 bgbl2">언어적 감수성이 발달해 글쓰기도 잘 합니다.</p>
	<p class="lst1 bgbl1">
		 언어 중추는 브로카 영역과 베로니케 영역으로 나눌 수 있습니다.
	</p>
	<p class="quotation">브로카 영역은 말/글을 만들어 내는 곳(speech production)<br/>
	베르니케 영역은 말/글을 이해하는 곳(speech comprehension)입니다.</p> 
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/015/info_00.jpg" alt="언어지능을 이해하는 공식" /></p>
	<br><br>
	<p class="lst2 bgbl2">브로카 영역이 손상된 환자는 의사나 가족이 하는 말은 제대로 이해할 수 있어도 자신은 말을 할 수 없는 상태가 됩니다. 본인은 말을 하려고 하지만 '우∼우, 아∼아' 등 의미 없는 소리를 내게 됩니다.</p>
	<p class="lst2 bgbl2">반면 베르니케 영역이 손상된 환자는 다른 사람의 말을 제대로 이해할 수가 없고, 언어적 소리를 내기는 하지만 전혀 이해할 수 없는 말을 하게 됩니다.</p>
	<p class="stxt1 bold">이처럼, 언어지능이 높다함은 말이나 글을 잘 표현하는 것(브로카:발화)뿐만 아니라 다른 사람의 말을 듣거나 글을 읽고 잘 이해하는 능력(베르니케:이해)도 포함됩니다.</p> 
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/015/info_01.jpg" alt="뇌과학 아이란" /></p>
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">언어 지능을 이루는 요소들</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 언어적 민감성</li>
			<li> 대인 설득력</li>
			<li> 작문 능력</li>
			<li> 언어학습능력</li>
			<li> 발표력</li>
			<li> 언어구사력</li>
			<li> 질문능력</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">언어지능을 높일 수 있는 추천 공부법</p>
	<p class="stxt1">궁극적인 언어 지능 강화를 위해서는 현상을 고지곳대로(uncritically) 받아들이지 말고 <font class="bold">왜(why)</font>라는 의문점을 자주 갖는 습관이 중요합니다. 의문점이 들면 철학하는 훈련이 중요합니다. 가장 손쉽게 철학할 수 있는 습관으로는 독서(input)와 일기 쓰기(output)를 실천하는 것입니다.</p> 
	<p class="lst1 bgbl1">다음 학습전략들을 공부할 때 마다 적용해 봅니다. 언어지능 강화를 위한 공부법입니다.</p>
	<p class="lst2 bgbl2">학습을 할 때는 기계적 암기보다는 비판적 사고를 통한 이해하기</p>
	<p class="lst2 bgbl2">배운 내용을 다시 자신의 이해된 말로 바꾸어 자세하게 정리하기</p>
	<p class="lst2 bgbl2">내용을 잘 기억하기 위해 자신만의 줄임말(약어)을 만들거나, 핵심이 되는 주요 단어 찾아내기 위한 인출 단서 만들기</p>
	<p class="lst2 bgbl2">학습내용을 기억하는 데에 도움이 될 만한 적절한 이야기를 찾아보거나 나만의 스토리텔링 구성하기</p>
	<p class="lst2 bgbl2">공부할 내용의 목차와 점검표를 만들어 공부하기</p>
	<p class="lst2 bgbl2">학습내용을 여러 번 말로 반복하여 암기하거나 녹음해서 다시 듣기</p>
	<p class="lst1 bgbl1">하브루타(Havruta) 학습법이란?</p>
 	<p class="stxt1">친구와 짝을 지어 토론과 논쟁을 통해 사고의 깊이를 심화하는 “스스로 가르치기(self-teaching)”를 활용한 학습법입니다.<br/>
 	하브루타를 실천하기 위한 방법으로는, 스스로 교사가 되어 가르치며 학습하기, 듣기만 하는 공부에서 적극 참여하고 질문하는 공부로 전환하기입니다.</p>
	<p class="stxt1">우리의 뇌는 격렬한 대화와 토론의 과정에서 격동하게 되고, 더욱 활성화됩니다. 기존에 학습한 지식이 더욱 공고해 지며 깨닫지 못했던 사고의 깊이 또한 발전하게 됩니다.<br/>
	<p class="quotation"><span>- 마이클 롱(Michael Long, 1984)의 상호작용 가설<br/>
	발화(output)를 염두에 두면, 이해(input)에 대한 흡수가 촉진된다고 합니다. 
	이 가설에 의하면 효과적인 외국어 습득 과정으로 듣기 학습만 하면 언젠가는 저절로 말할 수(발화) 있다는 기존 학설을 뒤엎고 발화의 기회를 줄 때 더 강력한 이해가 가능하다는 것을 주장했다. 
	<br/>이는 곧 말하기(대화)를 통해 사고력의 심화와 지식의 공고화가 촉진된다는 것을 의미한다. <br/><br/>
	- 또한 선생님에게 칭찬받기 보다는 친구들에게 인정받기 위한 욕구인 동료 압력(peer pressure)이 있기 때문에, 친구 앞에서 가르치기는 매우 효과적인 학습법이 될 수 있습니다.</span></p>
	<p class="stxt1">세종시 한솔고등학교는 교내 하브루타 학습실을 제공하고 있습니다. 이 곳에서 학생들은 적극적이고 활동적인 토론 중심의 학습을 할 수 있겠죠?</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/015/info_02.jpg" alt="세종시 한솔고등학교는 교내 하브루타 학습실" /></p>
</div>
<div class="bdln pt40">
	<p class="stit4">학업 성취에 미치는 영향</p>
	<p class="bgq1">언어지능이 높은 사람의 특징은 무엇입니까?</p>
	<p class="bga1">말과 글로 세상을 이해하는 능력이 뛰어난 사람들입니다.<br/>
		언어지능은 다른 지능과 독립적이면서 연계성도 높습니다.<br/>
		언어지능은 이른바 이야기꾼들이 가지고 있는 능력입니다. 삶의 다양한 모습들을 언어로 풀어내는 작가들을 언어지능이 뛰어난 대표적 인물로 꼽을 수 있습니다. 청중을 휘어잡는 언변이 뛰어난 사람들도 언어지능이 높다고 할 수 있습니다.<br/>
	</p>
	<p class="lst1 bgbl1">
		<b>언어지능이 높은 사람들의 주된 특징</b>은 다음과 같습니다.
	</p>
	<p class="lst2 bgbl2">질문, 특히 ‘왜‘ 라고 묻는 유형의 질문을 자주 합니다. 
	<p class="lst2 bgbl2">말하기를 즐깁니다. 
	<p class="lst2 bgbl2">좋은 어휘력을 가지고 있습니다. 
	<p class="lst2 bgbl2">두 가지 이상의 외국어를 구사하기도 합니다. 
	<p class="lst2 bgbl2">새로운 언어를 쉽게 배웁니다. 
	<p class="lst2 bgbl2">단어 게임, 말장난, 시 낭송, 말로 다른 사람 웃기는 일 등을 즐깁니다. 
	<p class="lst2 bgbl2">책 등을 읽는 것을 즐깁니다. 
	<p class="lst2 bgbl2">다양한 종류의 글쓰기를 즐깁니다. 
	<p class="lst2 bgbl2">언어의 기능을 잘 이해합니다.
	
	<p class="bgq2">언어지능이 공부에 미치는 영향은 무엇입니까?</p>
	<p class="bga2">언어 지능은 모든 과목의 근간이 됩니다.</p>
		<p class="lst3 bgbl3">조지 레이코프, 마크 존슨과 같은 인지과학자들에 의하면 언어의 은유성이 모든 창의성의 근간이자 핵심으로 보았습니다. 자연과학이나 수학에서의 개념들과 공식 또한 은유적 사고(상징)으로부터 나온다고 보았습니다. </p>
		<p class="lst3 bgbl3">수업장면에서 이해력이 빠르고, 강의식 수업에 유리합니다.</p> 
		<p class="lst3 bgbl3">학습 주제를 이야기나 토론을 하면서 공부하면 학습 효과가 더욱  좋습니다. 따라서, 하브루타처럼 남을 가르치면서 하는 공부가 언어지능이 높은 사람에게는 매우 좋은 학습법이 됩니다.</p> 
	</p>
	<p class="bgq3">학교에서 하는 활동 중 언어지능과 관련된 학습활동에는 어떤 것이 있나요?</p> 
	<p class="bga3">학습 과정에서 나타나는 대표적인 언어지능과 관련된 인지적 특성과 유형은 ‘말하기, 토론하기, 글짓기, 발표하기’입니다.</p> 
</div>
<div class="bdln pt40">
	<p class="stit4">언어지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="lst2 bgbl2">문학가-시인, 소설가, 극작가, 시나리오작가, 동화작가, 문학평론가, 사서</p>
	<p class="lst2 bgbl2">방송인-아나운서, 성우, MC, 희극인, 쇼호스트</p>
	<p class="lst2 bgbl2">언로인-기자, 프로듀서, 뉴스캐스터, 연출가, 편집자, 리포터</p>
	<p class="lst2 bgbl2">법조인-판사, 검사, 변호사, 법학자, 변리사</p>
	<p class="lst2 bgbl2">정치인-국회의원, 시도의원, 웅변가</p>
	<p class="lst2 bgbl2">언어학자-교사, 교수, 연구원</p>
	<p class="lst2 bgbl2">광고문안-카피리스트 </p>
	<p class="lst2 bgbl2">기업-영업사원, 판매원, 홍보담당, 경영자</p> 
	<p class="lst2 bgbl2">교육-학원 강사, 동기부여 전문가, 컨설턴트</p>
	<p class="lst2 bgbl2">번역가, 통역사</p>
	<p class="lst2 bgbl2">외교관 </p>
	<p class="lst2 bgbl2">컨설턴트</p>
	<p class="lst2 bgbl2">종교인(성직자)</p>
</div>
<p class="stit5"></p>