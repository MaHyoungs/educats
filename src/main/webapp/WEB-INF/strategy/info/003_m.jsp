<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="sb2_con">
	<p class="tit">우울감 이란?</p>
	<p class=txt1nobottom>
		우울감(증; Depression)이란 자신에 대한 부정적인 인식의 결과를 의미하며 근심, 침울, 실패감, 자존감저하, 자기비하, 무기력 및 무가치감 등을 나타내는 감정 상태이다.
	</p>
	<p class=txt1nobottom>
		우울감은 흔히 ‘마음의 감기’라고 부를 정도로 흔한 심리적 불편감이자  정상적인 생활인으로서 기능을 감소시키고 조그마한 부정적인 자극에도 과잉 반응을 하도록 촉발하는 요인이다.
	</p>
	<p class="onlybottom"></p>
	<p class="tit2">구성요소</p>
	<p class="txt2">	
		<span class="nbg">1</span>
		<span class="nbt">하루 중 대부분이 우울하다</span>
	</p>
	<p class="txt2">
		<span class="nbg">2</span>
		<span class="nbt">생활 속에서 흥미와 즐거움이 전혀 느껴지지 않는다</span>
	</p>
	<p class="txt2">
		<span class="nbg">3</span>
		<span class="nbt">식욕의 급격한 변화가 있거나, 체중이 갑자기 감소하거나 증가하였다</span>
	</p>
	<p class="txt2">
		<span class="nbg">4</span>
		<span class="nbt">불면증 또는 수면 과다 상태이다</span>
	</p>
	<p class="txt2">
		<span class="nbg">5</span>
		<span class="nbt">거의 매일 초조함을 느끼거나  신체 동작이 느려진다</span>
	</p>
	<p class="txt2">
		<span class="nbg">6</span>
		<span class="nbt">반복된 피로감과 무력감이 든다</span>
	</p>
	<p class="txt2">
		<span class="nbg">7</span>
		<span class="nbt">나 스스로를 가치없다고 생각하는 부적절한 죄책감이 자주 든다</span>
	</p>
	<p class="txt2">
		<span class="nbg">8</span>
		<span class="nbt">학습 시 사고력 및 집중력 급격히 저하된다</span>
	</p>
	<p class="txt2">
		<span class="nbg">9</span>
		<span class="nbt">자살하고 싶다는 생각이 수시로 든다</span>
	</p>
	<p class="txt1">
		※ 위 예시는 DSM­5 (APA-미국 정신의학회, 정신장애 진단 및 통계 편람) 우울진단 기준에 따르며, 위 <font color="red">9개 증상 가운데 5개 또는 그 이상이 (단 ①,②번 항목 중 하나가 반드시 포함되는 경우) 연속 2주 이상 지속되는 경우에는 <우울증></font> 판별 요소에 해당됨.
	</p>

	<p class="tit2">학습 성취에 미치는 영향</p>
	<p class="txt1nobottom">
		청소년기의 우울감은 심할 경우, 학업뿐만 아니라 전반적인 생활에도 심각한 영향을 미칠 수 있습니다. 
	</p>
	<p class="txt1nobottom">
		질문과 대답을 통해, 우울감에 대해 알아볼까요? 
	</p>
	<p class="txt2">
		<span class="nbg">1</span>
		<span class="nbt">우울감의 증상은 어떤가요?</span>
		<span class="nbt2">청소년기의 우울감은 자신감과 삶의 의욕이 없고, 무기력하게 피곤해하고, 평소에 하던 일을 수행하는 데 어려움을 갖게 하여 전반적인 학교생활에 부적응하도록 만들기 쉽습니다. 이로 인한 체중감소, 식욕부진(또는 폭식), 소화 장애, 두통 등과 같은 신체증상을 보이기도 합니다.</span>
   	</p>

	<p class="txt2">
		<span class="nbg">2</span>
		<span class="nbt">우울감은 학업에 어떤 영향을 미치나요?</span>
		<span class="nbt2">우울한 청소년은 그렇지 않은 청소년에 비해, 새롭거나 복잡한 과제를 배우는데 주의집중 유지의 어려움이나 동기부족, 활력의 저하와 같은 특성들로 인하여 제시된 과제를 완료하는 데 곤란을 경험하게 됩니다.<br>
		우울감은 기억력을 감퇴시키고 새로운 정보와 기존 정보의 연합(통합)을 저해합니다. 이는 궁극적으로 저조한 학업 성취로 이어진다. 이러한 저조한 학업성취로 인한 성공경험의 부족은 이후 자기효능감에 부정적인 영향을 미치는 악순환을 초래할 수 있습니다.
   		</span>
   	</p>

	<p class="txt1nobottombold">
		청소년기 우울감! 반드시 극복하여야겠지요! 
	</p>
	<p class="txt1nobottombold">
		자, 그러면 나의 우울감 정도와 우울감을 극복하기 위한 방법을 알아볼까요? 
	</p>

   	<div class="bot_btn bdt"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>
</div>