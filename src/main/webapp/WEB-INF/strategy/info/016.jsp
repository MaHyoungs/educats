<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">다중지능에서 논리­수학적지능이란 숫자를 효과적으로 사용하는 능력(예: 수학자, 회계사, 통계학자 등)과 잘 추론하는(예: 과학자, 컴퓨터 프로그래머, 논리학자 등) 능력을 말합니다. </p>
	<p class="lst2 bgbl2">수학적 계산을 잘 한다.</p>
	<p class="lst2 bgbl2">일상생활에서 수학을 효과적으로 잘 사용한다.</p>
	<p class="lst2 bgbl2">기술과 전략을 사용하는 게임을 좋아한다.</p>
	<p class="lst2 bgbl2">과학적 사고로 탐구에 흥미와 관심이 많다.</p>
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">다중지능 중 논리·수학적 지능을 이루는 요소들</p>
	<p class="stxt1">
		 <ul class="lstbl">
			<li> 수학적 자신감</li>
			<li> 수리적 패턴인식</li>
			<li> 논리적 문제해결</li>
			<li> 과학적 사고</li>
			<li> 실험 정신</li>
			<li> 분석적 사고(규칙과 질서)</li>
			<li> 수리적 민감성(추론능력)</li>
		</ul> 
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="stxt2">
		<p class="lst2 bgbl2"> 논리(과학적 방법/사고), 연역적 논리(삼단논법, 벤다이어그램), 귀납적 추리(유추)의 방법을 사용하여 공부하기 </p>
		<p class="lst2 bgbl2"> 사고(중재학습, 질문전략), 수학적 사고(유형화, 그래프), 숫자(평균, 백분율, 측정, 계   산, 확률, 기하학)를 통해 공부하기</p>
		<p class="lst2 bgbl2"> 소크라테스식 문답법, 실험 등을 통해 공부하기</p>
		<p class="lst2 bgbl2"> ‘누가, 언제, 어디서, 무엇을, 어떻게, 왜’의 육하원칙에 따라 질문하며 공부하기</p>
		<p class="lst2 bgbl2"> 번호를 매겨 가며 암기하기</p>
		<p class="lst2 bgbl2"> 틀린 문제나 잘못한 것의 이유와 개선할 방법에 대해 생각하기</p>
		<p class="lst2 bgbl2"> 단계를 나누어 상세히 설명하기</p>
		<p class="lst2 bgbl2"> ‘만약~한다면’을 가정하여 생각하고 실험하기</p>
		<p class="lst2 bgbl2"> 개념의 관계성을 파악할 수 있는 일정한 규칙을 찾아보기</p>
		<p class="lst2 bgbl2"> 정보를 수집하고 비교하고 비판하기</p> 
	</p>
</div>
<div class="bdln pt40">
	<p class="stit4">논리수학적지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="stxt2">
		<p class="lst2 bgbl2">학자-수학자, 물리학자, 과학자, 통계학자, 논리학자, 회계사, 세무사, 생활설계사, 법조인 </p>
		<p class="lst2 bgbl2">금융분야-은행원, 회계사, 세무사, 생활설계사, 증권사 애널리스트, 펀드매니저, 외환딜러</p>
		<p class="lst2 bgbl2">공학자-컴퓨터 프로그래머, 엔지니어 </p>
		<p class="lst2 bgbl2">의사, 약사</p>
		<p class="lst2 bgbl2">교사-수학 및 과학교사, 상업교사</p>
		<p class="lst2 bgbl2">연구원</p>
		<p class="lst2 bgbl2">탐정, 정보기관원</p>
		<p class="lst2 bgbl2">법조인-판사, 검사, 변호사, 법학자</p>
		<p class="lst2 bgbl2">전략 컨설턴트, 기업의 기획담당 및 분석가</p>
	</p>
</div>

<p class="stit5"></p>