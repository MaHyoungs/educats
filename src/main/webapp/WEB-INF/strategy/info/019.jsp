<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt3">
		<p class="stxt1"> 다중지능에서 신체­운동지능이란 춤, 운동과 같은 동작을 잘 이해하고 활용할 수 있는 능력입니다. 신체를 이용하여 자신의 생각과 감정을 표현할 수 있는 능력을 말합니다.</p>
<!-- 		<p class="stxt1">예)</p>  -->
		<p class="lst2 bgbl2">몸으로 자신의 생각을 잘 표현합니다.</p>
		<p class="lst2 bgbl2">춤추는 모습을 보는 것을 즐기고, 잘 따라합니다.</p>
		<p class="lst2 bgbl2">세밀하게 분해하고 조립하는 것을 좋아합니다.</p>
		<p class="lst2 bgbl2">손 끝이 매우 민감하여 남들이 만들지 못하는 것을 잘 만듭니다.</p>
	</p>
	<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('015_01')">다중지능 알아보기 클릭</a></p>
</div>

<div class="bdln pt40">
	<p class="stit4">신체운동 지능을 구성하는 요소</p>
	<p class="stxt1">
		<ul class="lstbl">
			<li> 운동감각능력</li>
			<li> 신체 협응력(리듬)</li>
			<li> 숙달능력(지구력)</li>
			<li> 신체 표현력</li>
			<li> 주의집중력</li>
			<li> 직접체험활동(신체 연출력)</li>
			<li> 도구활용능력(신체균형감)</li>
		</ul>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">신체운동지능이 우수한 학습자의 추천 공부법</p>
	<p class="lst2 bgbl2">드라마(연극, 역할극, 시뮬레이션), 동작, 무용, 게임, 신체교육(체험학습, 현장학습)을 활용하기</p>
	<p class="lst2 bgbl2">직접 만지거나 움직여 보거나 하는 등의 신체적인 감각이나 활동을 통해 공부하기</p>
	<p class="lst2 bgbl2">공부하는 중간 중간 몸을 풀 수 있는 시간 가지기</p>
	<p class="lst2 bgbl2">선생님이 수업 시간에 한 행동, 말 등을 정확하게 흉내 내며 공부하기</p>
	<p class="lst2 bgbl2">사물을 직접 만져보고 여러 가지를 상상하며 움직여보기</p>
	<p class="lst2 bgbl2">배운 내용을 신체를 이용해 설명하기</p>
	<p class="lst2 bgbl2">배운 내용을 반복해 쓰면서 공부하기</p>
	<p class="lst2 bgbl2">배운기술이나 방법을 그대로 반복해서 연습하기</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="lst1 bgbl1">신체운동지능을 활용한 학습 활동은 <span class="bold">집중력을 높여주고, 이해와 기억을 증진</span>시키는데 매우 중요한 역할을 합니다. 몸을 움직이면서 공부하는 것을 선호합니다.</p>
	<p class="lst1 bgbl1">신체운동지능은 <span class="bold">동작 능력과 표현 능력</span>으로 구분할 수 있습니다. 동작 능력은 근육을 이용하여 의도된 행동을 잘 하거나 기계 또는 도구를 능숙하게 다루는 능력이고, 표현 능력은 제스처나 표정을 통해 자신의 감정이나 의사를 잘 드러내는 능력이다. 다른 사람이 춤을 추는 것을 보고, 그대로 따라할 수 있습니다.</p>
	<p class="lst1 bgbl1">신체활동은 통한 몰입(in flow)의 경험은 <span class="bold">정신적 및 신체적 에너지가 필요한 활동과 적절한 기술을 요구하는 활동</span>에서 나타납니다. 운동할 때, 뛰어난 집중력을 보입니다.</p>
	<p class="lst1 bgbl1">규칙적이고 반복적인 신체 활동은 <span class="bold">뇌신경 활동을 촉진</span>합니다. 뇌의 감각 기능을 자극해 집중력과 지구력을 높여 학습의 몰입도를 증가시킵니다. 따라서 규칙적인 운동은 원활한 산소공급을 통해 두뇌 활동을 활성화하여 짧은 시간에 주의를 집중할 수 있도록 합니다.</p>
	<p class="lst1 bgbl1">신체운동은 <span class="bold">인지능력과 감성지능 그리고 정서지능 발달</span>에도 영향을 주고 <span class="bold">자아존중감에 영향</span>을 미칩니다. </p>
</div>

<div class="bdln pt40">
	<p class="stit4">신체운동지능이 높은 학습자에게 유망한 진로 적성 및 직업</p>
	<p class="lst2 bgbl2">운동선수</p>
	<p class="lst2 bgbl2">스포츠해설가 - 체육교사</p>
	<p class="lst2 bgbl2">댄서 – 발레리나, 치어리더</p>
	<p class="lst2 bgbl2">배우 – 배우, 뮤지컬 배우</p>
	<p class="lst2 bgbl2">외과 의사</p>
	<p class="lst2 bgbl2">세공인 – 보석 세공인, 유물 복원인</p>
	<p class="lst2 bgbl2">군인</p>
	<p class="lst2 bgbl2">스포츠 에이전트</p>
	<p class="lst2 bgbl2">마사지사</p>
	<p class="lst2 bgbl2">예술인 – 조각가, 예술가</p>
	<p class="lst2 bgbl2">기술자 – 자동차 정비 기술자, 비행기 정비 기술자</p>
	<p class="lst2 bgbl2">카레이서</p>
	<p class="lst2 bgbl2">파일럿</p>
</div>

<p class="stit5"></p>