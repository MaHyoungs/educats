<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bdln">
	<p class="stit4">정의</p>
	<p class="stxt1">
		자기 스스로를 소중하게 여기는 마음입니다. 자기 효능감이 높은 사람은 스스로를 믿고, 어떤 과제를 끝까지 완수할 수 있는 능력이 강한 사람입니다.
	</p>
	<p class="stxt1">
		예를 들어, 나에게 주어진 어려운 과제를 중간에 포기하지 않고 끝까지 완주하는 것을 말합니다. 
	</p>
	<p class="lst1 bgnum1"> 중간·기말고사 목표 달성하기</p>
	<p class="lst1 bgnum2"> 매일 일기쓰기를 거르지 않기</p>
	<p class="lst1 bgnum3"> 스스로 정한 생활 계획표 실천하기</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/001/info_01.jpg" alt=""></p>
</div>

<div class="bdln pt40">
	<p class="stit4">구성요소</p>
	<p class="stxt1">
		자신감 / 귀인성향 / 자기조절 / 자기존중감 / 높은 과제 난이도 선호 / 과제지속력 / 낙관성
	</p>
</div>

<div class="bdln pt40">
	<p class="stit4">학습 성취에 미치는 영향</p>
	<p class="stxt1">
		자기효능감은 실제 본인의 능력으로 달성하기 어려운 학습 과제를 완수할 수 있게 도와줍니다.
	</p>
	<p class="stxt1">
		질문과 대답을 통해, 자기효능감에 대해 알아볼까요?
	</p>
	
	<p class="lst1 bgnum1"> 자기효능감을 고려할 때, 가장 적정한 수준의 목표 달성 수준을 어떻게 되나요?</p>
	<p class="stxt3">실제 능력보다 약 15% 상위 난도로 목표를 설정하는 것이 성취동기 달성에 가장 효과적라고 연구 결과에서 밝혀졌습니다.</p>
	<p class="lst1 bgnum2"> 자기 효능감이 높은 것은 무조건 좋은가요?</p>
	<p class="stxt3">꼭 그렇다고 할 수는 없지만, 자기효능감은 학업에 긍정적 인 역할을 합니다.<br><br>
		긍정 요인 : 자기효능감이 높은 사람은 주어진 과제에 대해 두려워하기보다, 해결에 대한 자신감을 갖고 과제를 대하므로, 더 많은 과제를, 더 적극적으로 해결할 수 있습니다.<br><br>
		부정 요인 : 자기 효능감이 과도하게 높은 사람은, 간혹 주어진 과제에 대해 자신감이 높은 나머지, 본인이 이미 다 알고 있다고 자만하고, 배우려는 태도가 부족할 수도 있습니다.</p>
	<p class="lst1 bgnum3"> 마지막으로 자기 효능감이 낮은 사람은 과제를 실제보다 어렵게 판단한다.</p>
	<p class="stxt1">
		<b>결론) 자기효능감은 학습전략처럼 구체적으로 드러나지 않지만, 학업에 있어서 가장 큰 영향을 미치는 요소 중의 하나입니다.</b>
	</p>
	<p class="stxt1">
		<b>자, 그러면 여러분의 자기효능감 정도와 자기효능감을 높이기 위한 방법을 알아 볼까요?</b>
	</p>
</div>

<div class="bdln pt40">
	<p class="stit6">청소년기의 심한 우울감은 학언뿐만 아니라 전반적인 생활에도 심각한 영향을 미칠 수 있습니다.</p>
	<p class="stxt1">질문과 대답을 통해, 우울감에 대해 알아볼까요?</p>
	<p class="stxt1"><span class="qst">질문1</span><span class="qcnt">우울감의 증상은 어떤가요?</span></p>
	<p class="stxt1"><span class="ans">답변1</span><span class="acnt">청소년기의 우울감은 자신감과 삶의 의욕이 없고, 무기력하게 피곤해하고, 평소에 하던 일을 수행하는 데 어려움을 갖게 하여 전반적인 학교 생활에 부적응하도록 만들기 쉽습니다.</span></p>
	<p class="quotation">
		"우울이란 무엇인가? 그것은 감각에 대한 무능력이며, 우리의 육체가 살아있음에도 불구하고 죽어있는 느낌을 가지는 것이다.
		그것은 슬픔을 경험하는 능력이 없는 것일 뿐만 아니라 기쁨을 경험할 능력도 없는 것을 말한다. 우울한 사람은 만일 그가 슬픔을 느낄 수만 있어도 크게 구원을 받을 것이다." &nbsp;&nbsp;&nbsp;&nbsp;- 에리히 프롬 -
	</p>
	<p class="stxt1"><b>청소년기 우울감! 반드시 극복하여야겠지요!</b></p>
	<p class="stxt1">자, 그러면 나의 우울감 정도와 우울감을 극복하기 위한 방법을 알아볼까요?</p>
	<p class="stxt1">
		<a href="javascript:void(0)" class="cnt_btn" onclick="alert('yo')">나의 우울증을 진단해 볼까요?</a>
	</p>
</div>

<p class="stit5">높은 자기 효능감은 동기에 따라 긍정적일 수도 부정적일 수도 있다. </p>