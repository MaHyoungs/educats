<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link charset="utf-8" href="${pageContext.request.contextPath}/common/css/self_content.css" type="text/css" rel="stylesheet">

<div class="bdln">
	<p class="stit7"><span>먼저 자신의 학습동기 유형을 약식으로 점검해 보도록 해요.</span></p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_01.jpg" alt="나의 학습동기 어느 정도일까?" /></p>
	<p class="stit6">유형 분석</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_02.jpg" alt="유형 분석" /></p>
	<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/005_1" class="btn_webContents">학습동기 체크리스트 다운로드</a></div>
	<br><br>
	
	<p class="stit7"><span>“나는 무엇이 되고 싶은가?”</span></p>
	<p class="stxt1">많은 학생들이 아침부터 저녁까지 정말 열심히 뭔가(?)를 위해서 바쁘고 힘들게 생활하고 있습니다. 아침 일찍부터 시작하는 학교 수업에서부터 늦은 시간까지의 학원, 과외, 숙제 그리고 다른 활동들까지...</p>
	<p class="stxt1">그러나 <span class="bold">“왜 그렇게 하고 있는지? 자신의 목표를 위해 지금 무엇을 준비해야 하는지?”</span> 잘 모르는 경우가 너무 많습니다. </p>
	<p class="stxt1">그러기 때문에 마치 하루살이처럼, 하루하루를 주어진 대로 막연하고 지루하게 보내거나, 구체적인 목표도 없이 그저 그렇게 보내고 있습니다.</p>
	<p class="stxt1"><span class="bold">“내가 무엇이 되고 싶은지? 내가 되고 싶은 것을 이루기 위해서는 어떤 준비가 필요한지?”</span>를 생각해 본 사람과 대충 살아가는 사람은 많은 차이가 있습니다. 목표가 있고, 그것을 달성하기 위해 지금 내가 무엇을 해야 하는지를 안다면 하루하루가 목표를 이루기 위해 준비하는 소중한 시간이 됩니다.</p>  
	<p class="stit6">“나는 무엇이 되고 싶은가요?”</p>
	
	<p class="stit7"><span>나에 대한 이해</span></p>
	<p class="stxt1">자신의 길을 성공적으로 개척한 사람들이 가지고 있는 특징 중 하나는 바로 <span class="bold red">“나 자신”을 잘 아는 것</span>입니다. 내가 무엇을 좋아하는지? 내가 무엇을 잘 하는지? 내가 어떤 것을 할 때 기쁘고 즐거운지? 어떤 활동을 제일 싫어하고 피하고 싶은지? 에 대해 많이 생각하고 아는 것이 중요합니다.</p> 
	<p class="lst1 bgbl1">나는 무엇으로 구성되어 있는지 알아볼까요?<br>(포트폴리오, 노트나 일기장 등에 기록해 보세요.)</p>
	<p class="bgq1">나의 신체/외모는?</p>
	<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst1">예)키가 크다, 작다, 말랐다, 비만이다 등</p>
	<p class="bgq2">나의 성격은?</p>
	<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst1">예)사교적이다, 성실하다, 적극적이다, 모험을 즐긴다 등</p>
	<p class="bgq3">나의 흥미 영역은?</p>
	<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst1">예)만들기, 책 읽기, 생각하기, 글쓰기, 리더십 등</p>
	<p class="bgq4">나의 적성 영역은?</p>
	<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst1">내가 잘 할 수 있는 것 vs. 못 하는 것</p>
	<p class="bgq5">나의 가치관은?</p>
	<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst1">내가 세상에서 가장 중요하다고 여기는 것들 위주로...</p>
	
	<p class="lst1 bgbl1">나를 이해하기 위해 보다 깊게 접근해 볼까요?</p>
	<p class="lst2 bgbl2">나의 적성(Aptitude)입니다.</p>
	<p class="lst2">적성이란 어떤 일을 하는 데 필요한 ‘능력이나 강점’입니다. 이것은 선천적으로 가지고 있는 기질이나 성격적인 측면으로부터 영향을 받을 뿐만 아니라 성장하면서 경험, 환경적인 영향을 통해 형성됩니다. 따라서 적성은 ‘<span class="red">능력적</span>’인 부분을 반영하기 때문에 <span class="red">자신의 강점</span>으로 볼 수 있습니다.</p>
	<p class="lst1">나의 강점은 무엇일까요? </p>
	<p class="lst1 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	
	<p class="lst2 bgbl2">나의 가치관(Value)은?</p>
	<p class="lst2">가치관은 <span class="red">자신이 중요하고 의미 있다고 생각하는 것</span>을 말해요.<br>예를 들어, 대표적인 가치관 양식을 보여드리겠습니다. 자신이 추구하는 가치관에 가까운 곳에 체크해 보도록 해요.</p>
	
	<p class="stit6">※자신의 가치관에 맞는 것을 찾아 보세요^^.</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_03.jpg" alt="자신의 가치관에 맞는 것을 찾아보세요." /></p>
	<p class="stit6">※자신의 가치관에 어울리는 직업과 대표적인 인물도 탐색해 볼까요?</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_04.jpg" alt="자신의 가치관에 어울리는 직업과 대표적인 인물도 탐색해 볼까요?" /></p>
	
	<p class="lst2 bgbl2">나의 흥미(Interest)영역은?</p>
	
	<p class="lst2">흥미는 <span class="red">내가 정말 좋아하는 일</span>을 말해요. 어렵지 않겠지요?</p>
	<p class="lst2">시간가는 줄 모르고 신나게 몰두해본 경험이나 활동이 있었나요?</p>
	
	<p class="lst2">수동적 몰입: </p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst2">예)영화보기, TV시청 등</p>
	
	<p class="lst2">적극적 몰입: </p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p> 
	<p class="lst2">예)수학문제 풀기, 어려운 책 읽기 등</p>
	
	<p class="lst2 bgbl2">성적에 관련 없이 내가 정말 좋아하는 과목과 그 이유는?</p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst2 bgbl2">지금까지 내용을 종합해서 나의 흥미와 관련이 깊은 적당한 직업은 무엇일지 생각해 봅니다.</p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	
	<p class="lst2 bgbl2">부모님의 기대도 중요해요-Parents' View</p>
	
	<p class="lst2">부모님은 내가 어떤 직업을 갖기를 기대하는지 적어보세요?</p>
	<p class="lst2">-아버지가 바라는 직업?</p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	<p class="lst2">-어머니가 바라는 직업?</p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	
	<p class="lst2">부모님 생각과 나의 생각이 얼마나 일치하는지 아래의 표에 체크해 보세요.</p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_05.jpg" alt="퍼센테이지 표" /></p>
	<p class="lst2">내 생각과 부모님 생각이 같은(혹은 다른)이유는 무엇일까?</p>
	<p class="lst2 tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
	
	<p class="stit7 pt40"><span>이제, &lt;자기 이해&gt;로부터 진로탐색 및 목표설정에 필요한 내용을 총 정리해 볼까요?</span></p>
	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/005_06.jpg" alt="진로탐색 및 목표설정 총정리" /></p>
	<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/005_2" class="btn_webContents">나의 가치관 체크리스트 다운로드</a></div>
</div>