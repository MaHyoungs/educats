<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<link charset="utf-8" href="${pageContext.request.contextPath}/common/css/self_content.css" type="text/css" rel="stylesheet">

<div class="bdln">
<p class="stit4">다중지능은 나 자신(즉자)과 타인(타자)을 이해할 수 있는 도구(tools)입니다.</p> 
<p class="lst1 bgbl1 mt0">나에게는 나만의 장점을 깨닫게 해주고</p>
<p class="lst1 bgbl1 mt0">타인에게는 나와 다른 그들만의 장점을 존중하게 해 줍니다.</p> 
<p class="lst1 bgbl1 mt0">사람들 간의 차이점은 각자 소유한 지능(IQ) 때문이 아니라, 일곱 색깔 무지개와 같은 다중지능 프로파일이 다르기 때문입니다.</p>
<p class="lst1 bgbl1 mt0">다중지능은 대인관계지능, 언어지능, 논리수학지능 등 총 8가지 지능으로 구성됩니다. 학생들마다 8가지 지능들을 모두 갖고 있으나, 그 지능들의 조합 정도가 각기 다르기 때문에 각자 독특한 지적 특성을 갖습니다(H.W 가드너, 1983, 1993). 이것은 개개인에 따라 정도의 차이는 있지만, 모든 사람은 8가지 다중지능을 모두 갖추고 있어 이 지능들이 합쳐지면 개성 있는 인지 구조를 지닌다고 합니다.</p> 

<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/015/popup_01.jpg" alt="학생별 지능" /></p>

<p class="stit4">&lt;다중지능 강화 방안&gt;</p>
<p class="stxt1">스마트아이 자기주도학습서비스에서는 각 지능을 강화할 수 있는 뇌기반 음원, 학습전략 웹툰, 전자책 서비스 등을 제공하고 있습니다.</p> 
 
<p class="stit4">&lt;약점지능 강화 방안&gt;</p>
<p class="stxt1 mb0">나의 약점 지능을 강화하려고 할 때 나의 강점 지능을 가지고 약점을 강화해야 합니다.</p>  

<p class="stxt1 mt0">예를 들면, 나의 강점지능이 신체운동지능이고 약점지능이 언어지능이라면, 내가 좋아하는 스포츠의 경기 중계를 혼자 해 보거나 관련 기사를 써 봄으로써 언어지능을 강화시킵니다.</p>

<p class="stxt1">내가 못 하거나 싫어하는 과목을 내가 잘하거나 좋아하는 과목과 연결시켜 학습한다면 좋은 성과를 이루어 낼 수 있습니다.</p> 

<%-- <p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/pop/015_03.jpg" alt="다중지능" /></p> --%>
</div>