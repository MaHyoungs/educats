<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
			<p class="stit4">스마트아이 멘탈 강화법</p>
			<p class="stxt1">회피동기를 극복하기 위해서는 우리 자신의 정신력(Mentality) 자체를 강화해 합니다. 어떻게 하면 우리의 멘탈(정신력)을 강화 시킬 수 있을까요?</p>
			<p class="lst2 bgbl2">나쁜 일은 잠을 자면서 리셋 시킨다. </p>
			<p class="lst2 bgbl2">자신의 가장 멋진 순간을 기억한다.</p>
			<p class="lst2 bgbl2">나의 자존감을 높일 수 있는 문구를 항상 휴대하고 힘들 때마다 보면서 마음을 다잡는다.</p>
			<p class="lst2 bgbl2">나는 강한 사람이야, 행복한 사람이야 라는 자기 암시를 매순간마다 되뇌인다. (긍정적인 자기최면을 건다.)</p>
			<p class="lst2 bgbl2">다른 사람에게 나 자신의 감정을 조정 당하지 않는다. </p>
			<p class="lst2 bgbl2">힘들 때 마다 나를 사랑하는 사람들을 생각하며 긍정의 힘을 얻는다.</p> 
			<p class="lst2 bgbl2">기분 나쁜 일이 있으면 상처받지 말고 적절히 표현을 한다.</p>
			<p class="lst2 bgbl2">자신의 마음을 표현하는 데 인색하지 말자.</p>
			<p class="lst2 bgbl2">흘려듣기를 연습한다. (자신에게 필요한 내용만 기억하는 연습을 한다.)</p>
			<p class="lst2 bgbl2">좋은 친구와 친밀한 관계를 자주 유지한다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="quotation_yellow">
				<b>채팅과 댓글 잦으면 우울·폭력성 심해집니다.</b><br/><br/>
				한국청소년정책연구원이 개최한 ‘한국아동·청소년패널 학술대회’에 의하면, 채팅과 댓글 달기를 많이 하는 청소년은 우울·폭력성 등 심리적인 문제를 겪을 가능성이 높다고 발표되었습니다. <br/><br/>
				이 연구 보고서에 따르면 현재 고등학교 1학년인 학생 2075명(남자 1062명, 여자 1013명)의 2010년부터 2013년까지 인터넷 사용 관련 설문 자료를 분석한 결과, 청소년의 인터넷 사용 유형을 채팅이나 메신저, 댓글 활동이 많은 ‘교류형’, 게임이나 ‘19금 사이트’ 접속이 많은 ‘오락형’, 학습 정보 검색이 주를 이루는 ‘정보추구형’으로 나누어 실험을 실시하였습니다. 
				이 중 교류형 집단의 경우, 우울함과 공격성 등 사회 심리 문제와 상관관계가 있다는 결과를 얻었습니다. 따라서 <b>평소 채팅과 댓글 달기를 많이 하는 청소년의 경우, 우울·폭력성 등 심리적인 문제를 겪을 가능성이 높다고 보았습니다. </b><br/><br/>
				임선아(2014, 인터넷 사용유형이 사회심리문제, 학교적응, 비행에 미치는 영향)에서 인용
				</p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit7"><span>운동과 음악으로 나 자신을 행복하게!</span></p>
				<p class="stxt1">생활과 학업에서 오는 스트레스를 자연스럽게 해소할 수 있도록 
				자신만의 취미생활이나 스포츠클럽 활동 등을 하기를 권합니다.</p>
				<p class="lst1 bgbl1">나만의 취미 및 특기 활동하기</p>
				<p class="lst2 bgbl2">스포츠 종목 1기:</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="lst2 bgbl2">악기 1악기:</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>

				<p class="lst1 bgbl1">건강한 규칙적인 생활 유지</p>
				<p class="lst2 bgbl2">나의 식사시간 : 아침/점심/저녁</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="lst2 bgbl2">나의 수면과 기상시간 : 취침/기상</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>

				<p class="lst1 bgbl1">자기비하(自己卑下: 자기를 비하하고 비난하는 것)’를 하는 사람을 주변 사람들이 사랑할 수는 없습니다.</p>
				<p class="bgq1">최근 스스로를 비하(낮추어 표현)시켰던 혼잣말을 찾아보세요<br>
				ex) ‘난 안 돼’, ‘그 친구는 역시 달라’</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				
				<p class="bgq2">위의 자기비하의 표현을 ‘자기애(self-love)’를 높일 수 있는 말로 바꾸어 봅니다.<br>
				ex)‘왜 안 돼?’, ‘내가 뭐 어때서?’</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				
				<p class="bgq3">자신에게 ‘자기애’를 매일 표현하는 ‘시간과 활동’을 갖도록 합니다.<br>
				ex) 자기와 평화롭게 있는 시간-산책 시간, 하교 시간, 명상 시간 등 자기애를 표현하는 활동-거울을 보며, 나에게 미소 지어주며,  “오늘도 수고했어. 파이팅!” 이라고 격려해 줍니다.</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="stit7"><span>세로토닌 호르몬으로 우울감 안녕!</span></p>
				<p class="stxt1">사람의 감정과 행동은 우리 <뇌(Brain)>와 매우 밀접한 관련이 있어요. 감정을 다루는 뇌에는 여러 가지 신경전달물질이 있거든요. 그 중에서 세로토닌은 우리 뇌에서 행복감도 만들어 주고, 집중력도 높여주는 정말 중요한 호르몬이에요. 그러나 앞으로 세로토닌과 친하게 지내도록 해요^^.</p>
				<p class="stxt1">세로토닌이 부족하게 되면 의욕이 없어지고 자꾸 불안해지면서 기분이 가라앉게 되요. 심하면 우울증의 원인이 되지요.</p>
				<p class="stxt1">이 세로토닌이라는 호르몬은 우리 인체에서 이런 역할을 담당하고 있어요.</p>
				<p class="lst2 bgbl2">조절기능: 공격성, 폭력성, 중독성, 충동을 억제하고 평상심을 유지하게 합니다.</p>
				<p class="lst2 bgbl2">공부와 창조의 기능: 주의집중과 기억력을 향상시켜줍니다. </p>
				<p class="lst2 bgbl2">행복기능: 생기와 의욕을 불러일으키고 편안하고 행복감을 갖게 합니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/003/tip_01.jpg" alt=""></p>
				<p class="stxt1">따라서 평소 세로토닌이 충분히 분비되도록 뇌를 관리하면 생활에서 오는 스트레스, 불안, 초조, 짜증, 분노, 우울감을 극복하는데 아주 효과적입니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="stit7"><span>지금 행복한가요?</span></p>
				<p class="stxt1">행복해야 합니다! 행복하기 위해서 살아가고 있잖아요?
				공부를 왜 하고 있나요? 행복해지기 위해서 하고 있지요?</p>
				<p class="stxt1"> 그럼, 어떤 것이 진정한 행복(Authentic Happiness) 일까요? </p>
				<p class="quotation_gray"><span>긍정심리학자들은 진정한 행복이란,<br/>
				&nbsp;&nbsp;&nbsp;- 첫째, <b>즐거운 삶</b> (Pleasant Life) <br/>
				&nbsp;&nbsp;&nbsp;- 둘째, <b>만족한 삶</b> (Satisfactory Life) <br/>
				&nbsp;&nbsp;&nbsp;- 셋째, <b>의미 있는 삶</b> (Meaningful Life)을 꼽고 있습니다. <br/><br/>
				그 중에서도 특히 마지막 <b>‘의미 있는 삶’</b>을 중요시 하고 있습니다. </span></p>
				<p class="stxt1">예를 들면, 게임 좋아하는 친구들을 잘 보세요. 밤샘 인터넷 게임하고 나면 행복할까요? ‘즐겁고, 만족’했는지는 모르지만, 의미 있는 시간이었다고 말하기는 어렵기 때문에 진정한 행복이라고 할 수가 없습니다. </p>
				<p class="stxt1"> 무엇을 하든 생활에서 의미를 만들어 가도록 해 보세요!</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/002/tip_03.jpg" alt=""></p> 
				<p class="stxt5">행복을 위한 제안!</p>
				<p class="stxt1"> 이렇게 <행복 메뉴>를 꼭 실천해 보도록 하세요. 진짜 행복해 질 것 같지 않나요?</p>
				<p class="quotation"><span>행복을 위한 제안 10가지<br/>
				<br/>
				1. 매일 저녁, 그날 일어난 감사한 일 3가지를 일기에 쓴다.<br/>
				2. 신문에서 감사할 만한 뉴스를 찾아 스크랩한다.<br/>
				3. 평소 감사한 마음을 표현하지 못한 사함을 찾아 감사 편지를 전한다.<br/>
				4. 나에게 하루에 한 가지씩 선물을 준다.<br/>
				5. 하루 한번씩 거울을 보고 크게 소리 내어 웃는다. <br/>
				6. 남에게 하루에 한 번 친절한 행동을 한다.<br/>
				7. 아무도 모르게 좋은 일을 한다.<br/>
				8. 대화하지 않던 이웃에게 말을 건다.<br/>
				9. 좋은 친구나 배우자와 일주일에 한 시간씩 방해 받지 않고 대화한다.<br/>
				10. 연락이 끊겼던 친구에게 전화를 해서 만난다.</span></p>
			</td>
		</tr>
		<tr>
			<th>Way 6.</th>
			<td>
<!-- 				<p class="stit7"><span>세로토닌 호르몬으로 우울한 뇌를 회복시키는 방법</span></p> -->
<!-- 				<p class="lst1 bgbl1">점심식사 후 따뜻한 햇볕을 쬐여, 더 건강한 두뇌 만들기</p> -->
<!-- 				<p class="stxt1">우리의 두뇌에서 분비하는 호르몬인 세르토닌 활성 감소는 우울증의 원인이 된다고 밝혀졌습니다. 따라서 인체내 자연스러운 세르토닌 생성은, 일상생활에서 일정 시간 동안 햇빛에 노출되었을 때 가능하다고 합니다. </p> -->
<!-- 				<p class="lst1 bgbl1">밖으로 나와 상쾌하게 걸어보세요. 우울감이 어느새 사라집니다.</p> -->
<!-- 				<p class="stxt1">우리가 걸어서 발로 뇌를 자극하는 방법은 걷기의 과학이라고 해요. 이것을 전문가들은 <세로토닌 워킹>이라고도 하지. 세로토닌 워킹을 하고 7분 정도 후에 뇌파를 측정해 보면 느린 알파파가 사라지고 대신 10~13 Hz(헤르쯔)의 빠른 알파파가 나타나기 시작한답니다.</p> -->
<%-- 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/003/tip_02.jpg" alt=""></p> --%>
				<p class="lst1 bgbl1 bold"><세로토닌 워킹>은 이렇게 실천해 보세요.</p>
				<p class="lst2 bgbl2">평소보다 조금 빠르고 보폭도 약간 넓게 걸어요.</p>
				<p class="lst2 bgbl2">가슴은 펴고 허리와 등은 반듯하게 해야겠죠.</p>
				<p class="lst2 bgbl2">호흡은 아랫배로 보조에 맞추어 세 번 내쉬고 한번 들이마시고,</p>
				<p class="lst2 bgbl2">얼굴을 스쳐가는 바람도 느껴보고, 주변의 새소리도 느껴보고, 낙엽 밟는 소리에도 주의를 집중해 보세요.</p>
				<p class="lst2 bgbl2">좋아하는 음악을 들으면서 세로토닌 음악에 집중해도 좋고,</p>
				<p class="lst2 bgbl2">이런 상태로 약 5분만 걸어도 행복의 물질인 세로토닌이 분비되기 시작하고, 약15분 정도 지나면 최고조를 느끼게 될 거예요.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/003/tip_03.jpg" alt=""></p>
 				<p class="stxt1">사람은 부정적인 생각을 하게 되면 스트레스 호르몬인 코티죨이 분비되어 우리 뇌가 활성화 되는 것을 방해해요. <font class="bold">세레토닌과 친해지고, 코티죨은 멀리하는 스마트한 세종시 학생이 됩시다!!</font></p>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>