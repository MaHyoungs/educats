<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="stit4">현재 자신의 예습·복습전략이 구체적으로 어느 정도인지 점검해 봅니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_01.jpg" alt=""></p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/014" class="btn_webContents">예복습전략 점검표 다운로드 </a></div>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit7"><span> 학습 플래너 작성으로 예습·복습 시간을 확보하자</span></p>
				<p class="stxt1 bold">계획적이고 규칙적인 예습·복습을 실행을 위한 학습 플래너를 적극 활용하여 자신의 하루 일과를 스스로 조절 통제할 수 있도록 합니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_02.jpg" alt=""></p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/021" class="btn_webContents">학습플래너 양식 다운로드하기 </a></div>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit7"><span>리노트(ReNote) 복습법</span></p>
				<p class="stxt1"><b>일명 리노트(ReNote)라고 할 수 있는 예습·복습노트를 만들어 사용하도록 합니다.</b> 예/복습을 규칙적으로 실천에 옮기기 위해 별도의 노트&lt;ReNote&gt;를 만들어 매일 실천하도록 합니다. 우선, 다음과 같이 시간표를 보고, 리노트를 할 과목을 따로 표시하고, 리노트할 시간을 정리하세요</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_03.jpg" alt=""></p>
				<p class="lst1 bgnum1 bold">나만의 리노트(Re-Note) 만들기</p>
				<p class="lst2 bgbl2">노트를 세로로 반절 접는다.</p>
				<p class="lst1 bgnum2 bold">리노트(Re-Note)를 활용 복습하기</p>
				<p class="lst2 bgbl2">당일 수업 시간표의 맨 마지막 시간에 들었던 과목부터 역순(6교시인 경우; 6교시⟾1교시 순으로-망각율)으로 복습을 한다.</p>
				<p class="lst2 bgbl2">주요과목 위주로 매일 복습을 실행한다.</p>
				<p class="lst2 bgbl2">학습 내용 중 핵심적인 사항을 7개 정도로 간략하게 요약한다. </p>
				<p class="lst2 bgbl2">단, 복습하는데 시간이 많이 걸리는 과목(예: 수학 등)은 맨 나중에 하도록 한다.</p> 
				<p class="lst2 bgbl2">이렇게 매일 약 1시간 30분 정도 탄력적으로 복습시간을 갖도록 한다.  </p>
				<p class="lst2 bgbl2">다음날 역시 똑같은 방식으로 복습을 하는데, 다만 어제 했던 내용을 다시 눈으로 스캔(훑어보는)하는 정도로 가볍게 보고 넘어간다. </p>
				<p class="lst2 bgbl2">이렇게 누적복습을 금요일까지 실행하도록 한다. </p>
				<p class="lst2 bgbl2">주말(토·일요일을 오전, 오후, 저녁시간으로 나누면, 2일×3(오전/오후/저녁)=6등분이 가능) 시간 중 1/6 만(가능한 토요일 오전이 효과적) 한 주간 내용을 총복습 하도록 한다. 시간은 아마 1주간 누적 복습을 하지만 2시간이 채 걸리지 않을 것입니다. </p>
				<p class="lst2 bgbl2">다시 1주일을 이런 방식으로 복습을 실천한다. </p>
				<br>
				<p class="stxt1">이 리노트(ReNote) 복습법을 한 달간만 지속적으로 실천하면 정말 놀라운 효과를 직접 경험할 것입니다. 그림으로 요약하면 다음과 같습니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_04.jpg" alt=""></p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="stit7"><span>&lt;복습 TIP 정리&gt;</span></p>
				<p class="lst1 bgbl1 bold">RIGHT NOW. 바로 할 것. 쉬는시간의 황금의 5분을 잡아라!</p>
				<p class="lst1">공부 직후 정리 노트, 교과서 훑어보기 </p>
				<p class="lst1 bgbl1 bold">자투리시간 활용, 틈틈이</p>
				<p class="lst1">버스 속에서, 걸어가면서, 식사하면서 공부한 내용 떠올려 보기</p>
				<p class="lst1 bgbl1 bold">흔적을 남겨라</p>
				<p class="lst1">다시 보면서 빠진 것, 틀린 것, 색연필, 형광펜 등으로 흔적을 남겨라</p>
				<p class="lst1 bgbl1 bold">자신만의 언어로 정리해라</p>
				<p class="lst1">기계적 암기 말고, ‘의미망(Semantic networks)’ 의도적 연결 시도<br/>ex) 조선의 임금, 태조-정종-태종-세종-문종-단종-세조 (앞글자만 모아서 단어장 만들기)</p>
				<p class="lst1 bgbl1 bold">공부할 양을 줄여 나가라</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_05.jpg" alt=""></p>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="stit7"><span>예습복습은 Smart-아이 학습 동영상(인강)을 활용하라!</span> </p>
				<p class="lst1 bgbl1">배울 내용에 대해 미리 들어보고 수업에 참여하면, 이해의 깊이가 다릅니다.</p>
				<p class="lst1 bgbl1">예전에 배운 내용이나 이번시간에 배운 내용도 예습과 복습은 시간을 절약하고 이해속도는 빠르게, 기억은 오래 할 수 있습니다. </p>
				<p class="stxt1">특히, 단순한 예습 복습의 수준을 넘어선 누진학습이 있을 경우, 즉, 중학생이지만 초등학교 6학년 수학의 도형 단계에서 자신이 없다거나, 고등학생인데 중학교 과학 기초가 없어서 지금 고등학교 과학 수업을 따라갈 수 없다면 세종 Smart-아이 학습자료실에서 제공되는 EBS, 두산 동아 등에서 제공되는 최고의 인강을 들어보세요.</p>
				<p class="quotation_gray mt0">나보다 낮은 학년의 부족한 부분, 모르는 부분을 <b>세종 스마트아이에서 제공하는 인터넷 강의 듣는 것을 부끄러워 할 이유가 없습니다.</b> (헬렌켈러의 스승, 설리반 선생님처럼 여러분의 모든 문제를 해결해 줄 촉진자 역할을 해 줄 여러분만의 일대일 과외 선생님이 계시면 좋겠지만, 그럴 수 있는 경우는 거의 드물죠. 대신! 여러분을 24시간 365일 언제든, 여러분이 콜하면 달려가서 도와줄 수 있는 세종 스마트아이 자기주도적 학습이 있습니다.)<br><br>
				중3 학생이 만약 중1부터 수학을 포기해서, 도저히 수업을 따라 갈 수 없다면, 하루 빨리 중학교 1학년 수학책을 펴서, 모르는 내용을 최대한 빨리 따라 잡아야 지금 나가는 3학년 수업을 이해 할 수 있습니다. 2년치를 어떻게 따라 잡냐구요?   세종 스마트아이 홈페이지에서 제공하는 인터넷 강의 학습자료를 적극 활용해 보세요.<br><br>
				중 1, 일 년 동안 배운 진도를 중 3학생이라면, 한-두달만 집중하면 따라잡을 수 있습니다. 그 이유는 우리두뇌의 인지력과 이해력이 그 간  더 많이 성장해서 예전에는 몰랐던 것이, 이제는 환히 보이는 마술같은 경험을 할 수 있습니다. 문제는 의지입니다. 여러분의 의지가 여러분의 인생을 바꿉니다.<br><br>
				<b>스마트아이 학습자료 초등, 중등, 고등학교 교육과정의 모든 내용을 세종시 학생들이라면 무료로 무제한 활용할 수 있습니다.</b></p>
				<br>
				<p class="lst1 bgblt">불영과 불행!을 기억하라!</p>
				<p class="stit6">“흐르는 물은 웅덩이를 채우지 않고서는 나아가지 않습니다.”</p>
				<p class="quotation"><span>流水之爲物也(유수지위물야) 不盈科不行(불영과불행)<br>
				<br>
				물은 덤벙덤벙 얼마나 빨리 흘러가느냐를 자랑으로 여기지 않고<br>
				웅덩이가 있으면 일단 멈춰 그 웅덩이를 성실히 채운 다음 흐른다는 뜻입니다.<br>
				- 맹자의 말씀</span></p>
				<p class="stxt1">공부도 마찬가지입니다.</p>
				<img src="${pageContext.request.contextPath}/common/img/strategy/014/tip_06.jpg" alt="">
			</td>
		</tr>
		
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>