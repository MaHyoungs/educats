<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="stit7"><span>언제 어디서나 감정을 조절할 수 있는 호흡법</span></p>  
				<p class="stxt1 bold">느리고 깊은 호흡으로 마음을 편안하게! 복식호흡을 통해 불안하고 우울한 기분 벗어던져요</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/004/tip_01.jpg" alt=""></p>    
				<p class="lst2 bgbl2">호흡은 우리의 감정과 관계가 깊어요.</p>
				<p class="lst2 bgbl2">누구나 쉽게 할 수 있는 ‘복식호흡’으로 불안과 우울한 기분을 벗어날 수 있어요.</p>
				<p class="lst2 bgbl2">스트레스를 받아 불안하고, 우울하게 되면 가장 먼저 우리 신체의 근육이 긴장하게 되요. 이때 ‘복식호흡’으로 긴장한 신체를 이완시켜 불안하고 우울한 기분에서 탈출할 수 있어요.</p>
				<p class="lst2 bgbl2">복식호흡을 일정하게 하고 뇌파를 검사해보면 알파파가 나옵니다.</p>
				<p class="lst2 bgbl2">복식호흡을 하고 나면 정신이 맑아져 집중력도 높아져요. 이것은 복식호흡이 자율신경을 조절하기 때문이에요.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/004/tip_02.jpg" alt=""></p>
				<p class="tabAreaPcenter"><a href="${pageContext.request.contextPath}${browserMode}/mypg/sound/mypg_sound_home" class="cnt_btn">나의 뇌파음원듣기 클릭</a></p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit7"><span>마음속 안전지대(Safe-Zone of Mind) 만들기</span></p>
				<p class="lst2 bgbl2">마음의 안전지대 만들기란 시간과 공간의 제약을 받지 않고, 안전과 휴식을 얻을 수 있는 장소를 우리 마음에 존재하게 하는 것입니다. </p>
				<p class="lst2 bgbl2">마음속의 안전지대는 우리 마음에 위안을 줄 수 있고, 불안하고 우울한 마음을 줄여줍니다.</p>
				<p class="lst2 bgbl2">시험 불안 극복에도 좋은 방법이구요. 엄마의 따뜻한 품에서 행복한 기억, 가족과 함께한 행복한 나들이, 목욕할 때의 따끈한 느낌, 침대에 편히 누워 에어콘 바람을 쐬며 휴식을 취하는 장면 등 가장 편안한 자신만의 마음속 안전지대를 만들어 뒀다가 불안할 때 마다 떠올리세요.</p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit7"><span>불안/짜증을 긍정으로 바꿔야 성공한다!</span></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/004/tip_03.jpg" alt=""></p>
				<p class="quotation_gray"><span class="bold">마르텡 셀리멘트의 실험</span> <br/><br/>
				미국의 심리학 교수 마르텡 셀리멘트는 보험회사에 다니는 세일즈맨 200명을 대상으로 성격에 따라 성과가 어떻게 달라지는지 실험을 했어요.<br/><br/>
				사람들을 긍정적 성격, 보통, 부정적 성격으로 분류하여 2년 동안 연구를 했는데. 결과적으로 가장 긍정적인 성격을 가진 그룹이 가장 부정적인 성격을 가진 그룹보다 88%나 성과가 좋았다고 합니다. <br/><br/>
				세일즈맨은 손님들로부터 자주 거절당하는 직업 중 하나이거든. 그래서 자주 의기소침해지고 실망하기 쉽고 거절의 원인을 자기 무능 탓으로 돌리기 쉽지요. 하지만 이 연구 결과에서 긍정적인 성격의 소유자는 거절을 당해도 마음이 심하게 흔들리거나 포기하지 않았다고 해요. 이것이 긍정적인 성격의 장점이지요. </p>
				<p class="stit6">불안, 짜증, 의기소침이 일어날 수 있는 같은 환경에서도<br>마음 먹기에 따라 극복정도가 달랐음을 나타내어 줍니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="stit7"><span>A-B-C 모델로 불안/짜증을 극복하다</span></p>
				<p class="stxt1 bold"> ‘A-B-C’의 <span class="red">생각의 안경</span>을 끼자 </p>
				<p class="stxt1"> ‘색안경’이란 말을 들어 보았지요? 우리 주변에서 벌어지는 사소한 안 좋은 사건을 <span class="">내가 어떤 색상의 색안경을 끼고 보느냐</span>에 따라 내 기분이 달라 보입니다. 우리가 안 좋은 사건을 바라보는 ‘생각의 틀’은 '<span class="bold">A-B-C 모델</span>‘로 이루어져 있습니다.
				<p class="stxt1"> <span class="bold">A(Antecedent Event)</span>는 여러분들이 생활에서 경험하는 안 좋은 사건입니다.   <span class="bold">B(Belief system)</span>는 선행사건에 대해 나도 모르게 자동적 사고(automatic thought)로 내가 생각한 믿음과 해석, <span class="bold">C(Consequence)</span>는 생각과 믿음에 대한 결과(감정과 행동)입니다.</p>
				<p class="stxt1 mt20"> 예를 들면 ‘<span class="">오늘 아침 등교하면서 친구를 보고 반갑게 손을 흔들었는데</span>, <span class="">그 친구가 모른 척하고 지나가 버렸어요.</span>’ 이 사건(A)을 보는 <span class="">두 학생의 생각</span>이 달라집니다.</p>
				<p class="stxt1"> 재석 : A(친구가 그냥 지나갔다.) ⇛ B(<b>뭔가 급한 일이 있나보다.</b>) ⇛ C(<b>나중에 무슨 일이 있었는지 물어봐야지.</b>)
				<br> 명수 : A(친구가 그냥 지나갔다.) ⇛ B(<b>날 무시한다.</b>) ⇛ C(<b>화가 나고 우울해진다.</b>)</p>
				<p class="quotation mt20 bold"><span>
				[선행 사건(A)]  ⇛  [생각 신념(B)]  ⇛  [결과(C)]<br/>
				 •재석: [선행 사건 ] ⇛ [<span class="underline">합리적/긍정적 사고(생각/신념)</span>] ⇛[적절한 정서적 /행동적 결과]<br/>
				•명수:  [선행 사건 ] ⇛ [<span class="underline">비합리적/부정적 사고(생각/신념)</span>] ⇛[부적절한 정서적 /행동적 결과]</span></p>
				<p class="stxt1"> 우리는 이렇게 동일한 사건에 대해 어떤 <span class="">생각 비틀기</span>를 하느냐에 따라 결과가 달라져요. 즉, 어떤 생각의 안경을 쓰느냐에 따라 결과가 달라집니다. </p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/004/tip_04.jpg" alt=""></p>
				<p class="stxt1"> 따라서 <span class="bold">불안과 걱정으로 힘들어하는 친구들은 이렇게 자신의 ‘생각의 틀(색안경)’을 부정적이고 비합리적인 방향으로 바라보기 때문에 그런 결과를 유발</span>한답니다. </p>
				<p class="stxt1"> 여기에 소개된 ABC이론을 잘 기억했다가 자신의 생각을 비관적·부정적으로 비틀지 않도록 합니다. </p>
				<p class="stxt1">사람이 정서적으로 건강하다는 것은 </p>
				<p class="lst3 bgbl3 bold">이렇게 &lt;좋지 않은 사건&gt;A에서 흥분하지 않고 </p>
				<p class="lst3 bgbl3 bold">&lt;합리적인 생각&gt;B라는 안경을 통해서 </p>
				<p class="lst3 bgbl3 bold">&lt;적절한 정서적·행동적 결과&gt;C를 만들어 내는 것입니다. </p>
				<p class="stxt1">우리가 어떤 안 좋은 사건에 대해 화가 나면, 처음 어떤 반응을 할까요? 에이씨! AC!는 잘못된 겁니다. 듣는 사람도 불편하고, 그러니까 화가 나고 분노가 솟아오르는 것입니다. 가운데 중요한 &lt;B: 믿음/해석>이 빠져 있습니다. 이제 나를 화나게 하는 일이 있으면, A<span class="">B</span>C!를 실천하도록 합시다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="stit7"><span>분노 시 의사소통 양식에 대한 점검</span></p>
				<p class="stxt1">다음은 의사소통과 관련된 내용입니다. 특히 <span class="underline">화가 날 때</span> ‘<span class="bold">나는 어떻게 말하고 있는가</span>’를 잘  살펴 본 후 해당 항목에 체크해 봅니다. </p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/004/tip_05.jpg" alt=""></p>
				<p class="stxt1">위 항목에 1개도 체크한 것이 없어야 합니다. 표시된 항목이 많으면 많을수록 더 공부하고 훈련해야 할 것이 많다는 것을 의미합니다.</p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/004" class="btn_webContents">불안짜증 체크리스트 다운로드 </a></div>
			</td>
		</tr>
		
		<tr>
			<th>Way 6.</th>
			<td>
				<p class="stit7"><span>역기능적 사고(Dys-functional Thought)를 경계하라</span></p>
				 
				<p class="stxt1">역기능적 사고란 타당성(Validity)과 유용성(Efficiency)이 부족한 왜곡된 생각을 말합니다. 이러한 <span class="bold">역기능적 사고는 세상을 바라보는 자신의 눈과 마음을 왜곡(굴절시킴)</span>시켜요. 이런 오류에는 이런 다섯 친구들이 있어요. 꼭 경계하고 조심해야 해요!</p>
				
				<p class="lst1 bgblt">흑백논리 또는 이분법적 사고(All-or nothing Thinking)라는 친구가 있어요. </p>
				<p class="stxt1">흑백논리란 ‘Yes!’ 또는 ‘No!’만 있는 거예요. 어떤 일을 <span class="bold">극단적으로 두 개로만 나누어 생각</span>하지요</p>
				<p class="quotation_gray"><span>
				“나는 아무 능력도 없어요.”<br/> 
				“내가 S대학을 들어가 못하면 난 아무 쓸모가 없는 인간이야!”<br/>
				“어떤 일을 하려면 완벽하게 해야지 할 수 없다면, 아예 시작도 하지 마!” <br/>
				이런 생각이나 말을 해 보았지요? 이를 이분법적 사고라고 해요.<br/>
				</span></p>
				
				<p class="stxt1"> 그러면 이런 생각은 어떻게 바꿔야 할까요? 그래요. 바로 퍼센트(%)식 사고를 하는 거예요! 다른 말로, 사고척도화(scaling)라고도 해요. 예를 들어, “<span class="bold">이럴 경우에는 몇 점(0~100%)을 줄 수 있어?</span>” 등으로 유연하게 사고하면 좋겠지요</p> 
				
				<p class="lst1 bgblt">지나친 일반화(과잉일반화; Overgeneralization)라는 친구가 있어요. </p>
				<p class="stxt1">나쁜 일을 지나치게 확대 해석하는 거예요. </p>
				<p class="quotation_gray"><span>“우리 반 여자 얘들은 모두 나를 싫어해.” <br/>
				“내(네)가 하는 일은 항상 그렇지 뭐”<br/>
				 그러면 이런 생각은 어떻게 바꿔야 할까요? </span></p>
				<p class="stxt1">바로 반대 증거(counter-evidence)를 찾아보는 거예요. “<span class="bold">과연 그런가?</span>” 이런 식으로 말이지요. 할 수 있겠지요?</p>
				
				<p class="lst1 bgblt">‘부정적 사실을 강조하는 것’이에요. 멘탈 필터링(Mental Filtering)이라고도 해요. </p>
				<p class="stxt1">좋은(긍정적인) 결과도 있는데, 꼭 나쁜(부정적인) 결과에만 골몰해서 말이나 생각을 하는 경우지요. </p>
				<p class="quotation_gray"><span>중간고사에서 ‘국어 95점, 수학 75, 영어 95점’을 받았는데, “나 이번 시험을 완전히 망쳤어!” 이렇게 말(생각)하는 친구 있지요? 이런 생각은 잘못된 생각 습관이에요. 왜냐하면, 시험을 못 보았다고 생각하는 ‘수학’에만 집착하고, 잘 본 과목은 모두 멘탈 필터링 해 버렸으니까요.</span></p>
				<p class="stxt1">이런 생각은 이렇게 바꾸도록 해요. <span class="bold">‘다른 사람들은 뭐라고 생각할까? 내가 놓치고 있는 것은 뭐지?’</span> 긍정적인 면도 함께 살펴봐야 겠지요.</p>
				
				<p class="lst1 bgblt">‘그릇된 마음 읽기(Mind Reading)’라는 친구가 있어요. </p>
				<p class="stxt1">흔히, ‘독심술’이라고 들어 본 친구들도 있을 거예요. 즉, ‘자신이 다른 사람의 생각을 꿰뚫어 본다.’고 잘못 생각하는 것이에요. 비현실적인 생각을 하거나 지레짐작 하는 자세죠. </p>
				<p class="quotation_gray"><span>어느 날 지각을 하고, “선생님이 이제부터 나를 게으르다고 생각할거야.” “이번 시험에 영어를 잘못 보았으니, 영어 선생님이 나를 무시하실 꺼야.” 이런 식으로 생각하는 버릇을 일컫는 말이지요.</span></p>
				
				<p class="lst1 bgblt">‘비현실적인 예언(Fortune Telling)’을 하는 거예요. </p>
				<p class="stxt1">이를 점쟁이 같다고 해서, ‘점쟁이 오류’라고도 해요. 즉, 좋지 않은 일을 예상하고, 그것이 마치 사실인 것처럼 행동화는 경우지요. </p>
				<p class="quotation_gray"><span>“나는 결코 수학을 잘 할 수 없을 거야.” <br/>
				“내가 저 친구에게 친구하자고 하면 당장 거절할 거야.”<br/>
				이런 식으로, 아직 해보지도 않고, 혼자 속으로 북치고, 장구치고 혼자 모래성을 쌓았다가 혼자 허무는 식이지요.</span></p>
				
				<p class="stxt1">이럴 땐 ‘<span class="bold">그렇게 했을 때, 결과가 좋을 거야.</span>’ 식으로 긍정적 예언을 하면 어떨까요? </p>
				
				<p class="stit6">이렇게 나도 모르게 잘못된 생각 습관(역기능적 사고)들을 갖고 있다 보면, <br/>나도 모르게 우울해지고 불안해지게 됩니다. <br/>혹 주변에 이런 잘못된 사고를 하는 친구들이 있다면, <br/>그러한 잘못된 생각에 휩쓸리지 마시고 늘 긍적적인 생각의 중심을 잡으세요.</p>
			</td>
		</tr>
		
		<tr>
			<th>Way 7.</th>
			<td>
				<p class="stxt1">불안/짜증을 극복하기 위해서는 우리 자신의 정신력(Mentality) 자체를 강화해 합니다. 어떻게 하면 우리의 멘탈(정신력)을 강화 시킬 수 있을까요? </p>
				
				<p class="stit7"><span>(스마트아이 멘탈 강화법)</span></p>
				
				<p class="lst2 bgbl2">나쁜 일은 잠을 자면서 리셋 시킨다. </p>
				<p class="lst2 bgbl2">자신의 가장 멋진 순간을 기억한다.</p>
				<p class="lst2 bgbl2">나의 자존감을 높일 수 있는 문구를 항상 휴대하고 힘들 때마다 보면서 마음을 다잡는다.</p>
				<p class="lst2 bgbl2">나는 강한 사람이야, 행복한 사람이야 라는 자기 암시를 매순간마다 되뇌인다.<br/> (긍정적인 자기최면을 건다.)</p>
				<p class="lst2 bgbl2">다른 사람에게 나 자신의 감정을 조정 당하지 않는다. </p>
				<p class="lst2 bgbl2">힘들 때 마다 나를 사랑하는 사람들을 생각하며 긍정의 힘을 얻는다. </p>
				<p class="lst2 bgbl2">기분 나쁜 일이 있으면 상처받지 말고 적절히 표현을 한다.</p>
				<p class="lst2 bgbl2">자신의 마음을 표현하는 데 인색하지 말자.</p>
				<p class="lst2 bgbl2">흘려듣기를 연습한다. (자신에게 필요한 내용만 기억하는 연습을 한다.)</p>
				<p class="lst2 bgbl2">좋은 친구와 친밀한 관계를 자주 유지한다.  </p>
			</td>
		</tr>
		
		<tr>
			<th>Way 8.</th>
			<td>
				<p class="stit4">불안짜증을 극복할 수 있는 글귀</p>

				<p class="quotation pcenter bold"><span>
				사소해 보이는 상황이 균형에 맞지 않게 <br/>
				격렬한 반응을 촉발할 때가 있다.<br/>
				가령 친구가 나의 어떤 행동을 두고 <br/>
				별 뜻 없이 한 말에 바로 폭발해 버린 경우<br/>
				 감정이 격동될 때는 <br/>
				그것이 과거의 무언가를 환기시키거나 <br/>
				"저 친구가 또 나를 괴롭히는 구나" 식의 생각을 <br/>
				불러일으킬 때가 많다.<br/>
				또 촉발 인자는 대게 자신이 부족하고<br/>
				능력이 부족하다는 느낌과 연결되는데 <br/>
				이것은 일종의 아픈 곳이면서 <br/>
				고통의 근원이기도 하다.<br/>
				첫 번째 가장 중요한 단계를 멈추는 것이다.<br/>
				자극을 받는다고 느낄 때마다 그냥 멈춰라.<br/>
				감정이 격동할 때 멈추는 것은<br/>
				매우 강력하고도 중요한 기술이다.<br/>
				그저 잠깐만 반응을 자제하라.<br/>
				이 순간을 신성한 멈춤이라고 한다.<br/>
				-너의 내면을 검색하라 중에서- <br/>
				</span></p>
				
				<p class="stxt1 bold">우리의 뇌는 항상성을 원하지만 한번 자극받게 된 뇌는 계속적으로 그 자극을 원하기도 한답니다. 화내기가 처음에는 어렵지만, 한번 화를 내기 시작하면, 우리 뇌는 일종의 카타르시스(배출)로 인한 쾌감을 느끼게 되어, 화내기를 반복하게 됩니다.</p>
			</td>
		</tr>
		
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>