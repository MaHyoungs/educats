<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="quotation_yellow">
				<b>밤새지 마란 말이야~ 잠자는 것이 공부하는 것이고, 숙면은 암기의 마지막 단계이다.</b><br/><br/>
				미국 뉴욕대학과 베이징대학의 공동 연구팀은 밤샘 공부보다 숙면이 학습능력과 기억을 증진하는 데 효과가 있다고 발표하였습니다. 연구팀은 잠을 자는 동안 뇌세포 사이에 새로운 연결(뉴런의 연합)이 형성되는 것을 관찰하는데 성공했습니다.
				잠을 잔 뒤 한 시간 동안 회전 막대 위를 걸은 쥐와 자지 못하고 세 시간 동안 계속 걸었던 쥐를 비교 관찰한 결과, 뇌세포에서 확실한 차이가 발견되었습니다. 잠을 잔 쥐의 뇌에서는 뉴런 사이에 새로운 시냅스 형성이 현저히 증가했습니다. 실험의 결과, 잠을 잔 쥐가 훈련을 더 잘해냈고, 이는 수면을 취한 쥐가 학습을 훌륭히 소화했다는 것을 알 수 있습니다.<br/><br/> 
				뉴욕 의과대학의 원 뱌오 간 교수는 <b>“뇌세포는 잠을 자는 동안 쉬고 있는 것이 아니라, 낮 시간에 벌어진 일을 리플레이(재현)하고 있다”</b>고 설명했습니다. 
				간 교수는 <b>"어린이들의 학습을 위해 갖는 중요한 의미 가운데 하나는 무언가를 기억하고자 한다면 뇌세포의 연결망이 필요하다는 것"</b>이라고 강조하면서 <b>"계속 공부하는 것 보다 공부하고 잠을 잘 자는 편이 더 효과적일 것"</b>이라고 말했습니다. 
				</p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit4">시간관리 매트릭스(Matrix)</p>
				<p class="lst1 bgblt"><긴급성>과 <중요성>에 따라 우선순위(priority)를 정하라</p>
				<p class="stxt1"> 우리가 어제와 다른 삶을 살고 싶다면, 반드시 어제까지와는 다른 방식을 <선택>해야 합니다. 당장 즐거움을 주는 긴급한 일과 장기적인 보상을 주는 중요한 일 중에서 한 가지를 선택하는 문제는 매우 어려운 문제입니다. </p>
				<p class="lst1 bgblt">지속적인 시간관리를 위해 이를 효율적으로 도와줄 수 있는 학습 툴(Tool)</p>
				<p class="stxt1">즉 <학습 플래너>가 필요합니다. 학생들의 학습동선에 따라 교육공학적으로 잘 설계 된 학습 플래너 활용은, 시간관리와 학습습관 형성에 매우 좋은 효과를 가져다 줄 것입니다. </p>
				<p class="stxt1 bold">시간관리 매트릭스를 만드는 방법과 학습 플래너를 작성하는 방법은 웹툰 > 시간관리 전략을 확인 하세요.</p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}${browserMode}/cont/webtoon/cont_webtoon_view?id_measurement=MSRMNT_009&chapter=3&pageSize=4" class="btn_webContents">웹툰 보러가기 클릭</a></div>
				<br>
				<p class="stxt1">최고로 성공한 사람들은 중요한 일과 긴급한 일들을 구분해서 합니다. 특히 중요한 일들이 긴급한 상황에 처해지지 않도록, 미리 미리 해 두려고 하는 노력을 많이 하는 사람들입니다. 보통사람들은 중요하고, 긴급한 1사분면이 가장 중요하다고 생각하지만, 고수들은 중요한 일들을 절대 긴급하게 하지 않습니다.</p>
				<p class="stxt1">여러분들에게 중요한 일은 무엇인가요? 실력향상이겠지요. 당장 긴급한 크고 작은 일들도 있겠지만요. <b>당장 급한 일들만 처리하다가 정작 중요한 최종 목표(원하는 고등학교, 대학교)를 놓치는 일이 없도록 이 시간관리의 4분면을 잘 생각해서 우선순위와 에너지분배의 정도를 정해서 시간을 관리하십시오.</b></p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit4">효과적인 시간 계획을 위해 다음 내용을 실천해 봅니다.</p>
				<br>
				<p class="lst2 bgbl2">현재 보내고 있는 하루 시간 사용 내역서를 30분 단위로 기록해서 분석해 봅니다.</p>
				<p class="lst2 bgbl2">생활습관과 시간 사용 패턴을 확인합니다.</p>
				<p class="lst2 bgbl2">목표와 함께 우선순위를 정하도록 합니다.</p>
				<p class="lst2 bgbl2">공부시간의 양을 정하도록 합니다.</p>
				<p class="lst2 bgbl2">공부할 과목의 순서를 정합니다. </p>
				<p class="lst2 bgbl2">학교, 학원, 식사, 수면 등의 고정된 시간을 표시합니다.</p>
				<p class="lst2 bgbl2">복습시간을 꼭 계획합니다.</p>
				<p class="lst2 bgbl2">휴식 및 여가 시간을 계획합니다.</p>
				<p class="lst2 bgbl2">어렵다고 생각하는 과목은 가능한 집중이 잘 되는 시간에 배정합니다.</p>
				<p class="lst2 bgbl2">보여주기 위한 계획표가 아닌, 실천할 수 있는 합리적인 시간 계획을 세운다.</p>
				<p class="lst2 bgbl2">여가 시간에는 학습과 관련된 생각이나 활동은 가능한 하지 않는다.</p>
				<p class="lst2 bgbl2">계획을 실천하기 위해 노력하고 달성한 후에는 자신을 칭찬한다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_01.png" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_02.jpg" alt=""></p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="stit4">직접 계획표를 작성해 봅니다.</p>
				<p class="lst1 bgblt">목표 세우기(한 학기 목표를 세워 봅니다.)</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_03.jpg" alt=""></p>
				<p class="lst1 bgblt">월간 계획 세우기(자신의 월간 계획을 세워 봅니다.)</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_04.jpg" alt=""></p>
				<p class="lst1 bgblt">일일 계획 세우기(나의 하루 계획을 세워봅니다.)</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_05.jpg" alt=""></p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/009_2" class="btn_webContents">시간관리 계획표 다운로드 </a></div>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_06.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_07.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_08.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_09.jpg" alt=""></p>
				<p class="stit6">"꿈에 날짜를 붙이면 목표가 되고, 목표를 쪼개면 계획이 되며, 그 계획들을 실행하면 꿈은 현실이 된다."</p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/021" class="btn_webContents">학습플래너 양식 다운로드 </a></div>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="stit4">자신의 생활습관(내가 개선해야 할 점이 무엇인지)을 점검해 봅니다.</p>
				<p class="stxt1 bold">다음은 생활습관에 관한 내용입니다. 자신에 해당되는 내용에 표시해 봅니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_10.jpg" alt=""></p>
				<p class="stit4">시간 관리를 방해하는 요인을 탐색해 봅니다.</p>
				<p class="stxt1 bold">다음 시간관리를 방해하는 요인입니다. 자신에게 해당되는 내용이면 예, 아니오에 표시해 봅니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/009/tip_11.jpg" alt=""></p>
				<p class="stxt1">‘예’에 표시된 문항이 많을수록 시가관리를 방해하는 항목이 많다는 것입니다. 문제에 대한 이유를 파악하는 것이 해결의 첫 걸음입니다.</p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/009_1" class="btn_webContents">생활습관 점검표 다운로드 </a></div>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>