<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="stit4">자기효능감 Up! 할 수 있는 과제 1</p>
				<p class="stxt1"><b>자신감이 높으면 높은 동기수준을 유지하고, 높은 학업성취를 보입니다.</b></p>
				<p class="stxt1">자신감을 증진시키려면,</p>
				<p class="lst1 bgnum1"> 작은 성취 경험을 가질 수 있도록 해 줘야 합니다.</p>
				<p class="lst1 bgnum2"> 위인전이나 성공한 사람들의 이야기와 같은 대리경험을 통해서,</p>
				<p class="lst1 bgnum3"> 부모나 교사의 언어적 설득(칭찬과 격려)를 통해서,</p>
				<p class="lst1 bgnum4"> 정서적 안정을 통해서 자신감을 높이는 것이 필요합니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit4">자기효능감 Up! 할 수 있는 과제 2</p>
				<p class="stit7"><span>잦은 성공 경험을 위한 목표 세우기</span></p>
				<p class="lst1 bgbl1">이번 주에 꼭 이루고 싶은 목표</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="lst1 bgbl1">이번 달에 꼭 이루고 싶은 목표</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="lst1 bgbl1">이번 학기에 꼭 이루고 싶은 목표</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit4">자기효능감 Up! 할 수 있는 과제 3</p>
				<p class="stit7"><span>에디슨의 놀이법</span></p>
				<p class="quotation">
				"항상 모든 일에서 재미를 찾으세요. 저는 실험을 할 때 놀이를 한다고 생각합니다. 책을 읽을 때도, 일을 할 때도 마찬가지입니다.<br>
				 여러분도 그렇게 살아 보세요. 숙제도, 공부도, 회사 일도 모두 놀이라고 생각해 보세요.<br>
				 그러면 항상 행복하게 살 수 있답니다."  - 토마스 에디슨 -</p>
				<p class="lst1 bgbl1">에디슨의 말에 비춰 보았을 때, 본인은 행복하다고 생각하나요?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="lst1 bgbl1">누구나 행복을 추구합니다. 행복한 삶이되기 위해서는 어떻게 해야 할까요? </p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>