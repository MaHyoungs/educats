<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="stxt1 bold">그래프, 지도, 다양한 시청각 자료, 레고 세트, 예술 재료, 시각적 환상, 카메라, 그림 모음집, 교과 학습할 때나 노트 정리할 때 다양한 종류의 포스트잇을 활용한 학습이 효과적입니다. </p>
				<p class="lst3 bgbl3">색지, 낙서장, 펜</p>
				<p class="lst3 bgbl3">페인트와 페인트 솔</p>
				<p class="lst3 bgbl3">다양한 글쓰기 도구</p>
				<p class="lst3 bgbl3">모형 점토</p>
				<p class="lst3 bgbl3">인형, 인형에 입힐 옷, 버팀대</p>
				<p class="lst3 bgbl3">퍼즐과 레고</p>
				<p class="lst3 bgbl3">지구의와 지도</p>
				<p class="lst3 bgbl3">디지털카메라나 일반 카메라</p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="lst2 bgbl2">교과 학습 시 내용 요약을 마인드맵(mind map)을 활용해 정리 하도록 합니다.</p>
				<p class="lst2 bgbl2">교과 중요 영역을 색상을 활용한 밑줄 긋기(1회독-연필, 2회독-파랑색, 3회독-붉은 색 등)를 활용하도록 합니다.</p>
				<p class="lst2 bgbl2">당일 복습 활동은 마인드맵(mind map)을 활용해 요약 정리하는 학습습관을 갖도록 합니다.</p>
				<p class="lst2 bgbl2">다양한 PPT 발표 수업을 권장합니다.</p>
				<p class="lst3 bgbl3">읽기학습 활동 예에서 포스터 만들기, 광고문 만들기, 지도 만들기, 점토로 핵심 주제 만들기</p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stxt1">공간지능이 강점인 학습자들은 사진, 슬라이드, 영화, 그림에 민감하게 반응합니다. <span class="bold">공간지능을 강화하는 데 유용한 활동</span>들은 다음과 같은 것들이 있습니다.</p>
				<p class="lst2 bgbl2">흥미 있는 주제나 아이디어에 기초한 몽타주를 만들어 보거나 잡지에서 그림을 오려서 광고문을 만듭니다.</p>
				<p class="lst2 bgbl2">핵심, 요점, 주제, 기본 개념을 점토, 그림물감, 색연필을 이용해서 그림으로 표현해 봅니다.</p>
				<p class="quotation_gray"><span><b>스마트티어링(Smart & teering)으로 공간능력 키우기</b>
				<br><br>
				스마트티어링(Smart & teering)은 스마트 디바이스와 오리엔티어링의 합성어로, 산과 숲 등을 지도와 나침반 대신 스마트 디바이스를 통한 교사의 지시에 따라 정해진 지점으로 이동하면서 이벤트를 수행하는 학습 방법으로 흥미 유발, 자연 체험, 공간 지능 학습을 동시에 할 수 있는 교수방법입니다.</span></p>
				<br>
				<p class="stit7 mt40"><span>델 그린드(J.J. Del Grand, 1990)의 공간 능력 키우기</span></p>
				<p class="lst1 bgblt">눈-운동 조정능력 키우기</p>
				<p class="stxt1">  신체의 움직임 또는 신체 부위의 움직임과 시각을 조정할 수 있는 능력을 키웁니다. 옷 입기, 자르기, 공차기, 줄넘기 등의 활동은 몸의 운동과 함께 시각을 조절하는 힘과 집중력을 요구합니다.</p>
				<p class="lst1 bgblt">형(image) - 배경 지각능력 키우기</p>
				<p class="stxt1">  한 그림(배경)에서 특정한 도형(초점:전경)을 인식하는 시각적 능력을 키웁니다. 흔히 착시를 일으키는 그림에서 전경(figure)과 배경(ground)을 식별하는 시각적 변환 행위를 일컫습니다. 숨은 그림 찾기, 부분 그림 완성하기, 닮은 점과 다른 점 찾기 등이 도움이 됩니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/017/tip_00.jpg" alt=""></p>
				<p class="lst1 bgblt">지각적 불변성 능력 키우기</p>
				<p class="stxt1">  물체를 보는 인상은 관점에 따라 다양하나, 물체의 크기나 모양과 같은 불변적인 성질은 변하지 않음을 아는 능력입니다. 육면체를 정면 또는 위에서 보았을 때, 눈이 얻는 상(image)이 각각 다름에도 불구하고 지각적 불변성을 가진 사람은 육면체로 인식한다는 것입니다.</p>
				<p class="lst1 bgblt">공간에서의 위치 지각능력 키우기</p>
				<p class="stxt1"> 한 대상과 관찰자의 관계를 결정할 수 있는 능력입니다. 공간에서의 위치 지각을 개발시키기 위한 활동으로는 도형의 회전과 대칭 이동, 사물 위의 위치 변화, 거울의 상 만들기와 같은 활동을 합니다.</p>
				<p class="lst1 bgblt">공간 관계의 지각능력 키우기</p>
				<p class="stxt1">  관찰자가 둘 이상의 대상들을 그 자체에 대하여 또는 각각 사로에 대하여 볼 수 있는 능력입니다. 블록 쌓기, 목표 지점으로 가는 최단 거리 찾기, 부분들을 조립하기, 도형을 완성하기, 계열을 완성하기와 같은 활동이 있습니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="lst2 bgbl2">점토나 밀가루 반죽 등을 이용하여 수업에 나오는 개념 만들기</p>
				<p class="lst2 bgbl2">플로차트(flow chart), 시각적 맵, 벤다이어그램 등을 활용하여 알고 있는 정보에 새 자료를 연결하기</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/017/tip_01.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/017/tip_02.jpg" alt=""></p>
				<p class="lst2 bgbl2">개념도, 마인드맵 등을 이용하여 요점 정리하기</p>
				<p class="lst2 bgbl2">인형극을 통해 수업에서 배운 개념 강화하기</p>
				<p class="lst2 bgbl2">수업에서 배운 지리적 위치를 공부하기 위해서 지도 이용하기</p>
				<p class="lst2 bgbl2">컴퓨터 소프트웨어를 사용하여 학습 시문집에 삽화 그려넣기</p>
				<p class="lst2 bgbl2">가상현실 소프트웨어 사용하기</p>
				<p class="lst2 bgbl2">기억해야 할 내용을 공간과 연관지어 외우기</p>
				<br>
				<p class="stxt1">시각화, 색깔 단서(공간 지능이 높은 학생은 색에 민감하므로 교수 활동에서 다양한 색상의 분필, 마커, 슬라이드)를 활용하면 효과적입니다. 학생들에게 작문 과제를 줄 때에도 색연필과 색지를 사용하여 글을 쓰게 하는 방법을 제시할 수 있습니다. 또한 교사는 유형, 규칙, 범주를 표시할 때 색깔을 달리하여 학생에게 제시할 수도 있습니다. </p>
			</td>
		</tr>
		
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>