<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="stxt1">먼저 자신의 시험관리 전략 검사를 통해 부족한 부분이 어느 부분인지 구체적으로 점검해 보도록 합니다. 다음은 시험에 임해서 본인의 실력을 발휘할 수 있는 체크리스트입니다. <b>자신의 시험관리 전략 검사를 통해 구체적으로 점검해 보도록 합니다.</b> 아래의 진단 결과에 따라 아니오라고 체크한 문항은 반드시 자기성찰 하도록 합니다. </p>
<!-- 				<p class="stxt1"> -->
<!-- 					<표1. 나의 시험관리 전략 점검> -->
					<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_01.jpg" alt=""></p>
					<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_02.jpg" alt=""></p>
<!-- 				</p> -->
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit7"><span>적당한 스트레스가 최고의 성적을 낳는다.</span></p>
				<p class="stxt1">다음은 여키스-도슨의 곡선으로 스트레스와 시험성적간의 상관관계를 보여주는 실험입니다. 이 시험에 참가한 세 명의 학생인 래리, 모에, 컬리는 미국의 대학입학을 앞둔 고등학생으로 각각 다른 스트레스 상황에 처해 있었습니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_04.jpg" alt=""></p>
				<p class="stxt1">미국의 SAT는 한국의 수능시험에 해당되는 시험으로 래리는 프린스턴 대학 입학 허가를 받은 후, 편안한 마음으로 SAT를 치릅니다. 모에는 학교 성적은 좋은 편이고 SAT 결과까지 좋다면 원하는 대학에 합격이 가능합니다. 컬리는 학교 성적이 좋지 않아서 오직 SAT 결과만으로 대학을 가야하는 과도한 스트레스 상황이었습니다. 이 실험 결과 공부한 내용을 잘 기억한 학생은 바로 모에입니다. 실험에 의하면 <b>적당한 긴장과 스트레스를 가진 학생이 가장 시험을 잘본다는 결과가 나왔습니다. 왜냐하면 적당한 스트레스는 각성 효과를 높이기 때문입니다.</b> (출처: 학습과 기억, 뇌에서 행동까지)</p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit7"><span>시험 한 달 전에 실천해야 할 10가지</span></p>
				<p class="stxt1 bold">시험 전 계획표를 세우도록 합니다. </p>
				<p class="stxt1">다음은 시험 한달 전에 실천해야할 10가지 시험계획표 입니다. 중간고사는 3주(혹은 4주) 전부터 계획을 세우고 시작합니다. 기말고사는 시험 과목과 범위가 많기 때문에 반드시 중고사 대비 기간보다 여유롭게 기간을 계획하도록 합니다. 실천해야할 내용에 체크가 빠진 항목중 반드시 시험계획에 포함시켜 실천하도록 합니다. </p>
				<p class="tabAreaPcenter pt0"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_06.jpg" alt=""></p>
				<p class="stit7"><span>주로 사용할 시험 준비 자료</span></p>
				<p class="stxt1">다음은 시험준비에 필요한 준비자료들 입니다. 각 각의 준비자료의 활용을 체크하며 효율적인 시험준비가 되도록 합니다.</p> 
				<p class="tabAreaPcenter pt0"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_07.jpg" alt=""></p>
				<p class="stit7"><span>시험 전 시험에 대한 정보를 탐색해요!</span></p>
				<p class="tabAreaPcenter pt0"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_08.jpg" alt=""></p>
				<p class="lst1 bgblt">시험 일정 예시</p>
				<p class="stxt1">중간고사 날짜가 5월 2일 이라면, 각 과목별 시험범위와 문제유형과 배점, 선생님이 시험에 대해서 언급하신 내용을 잘 메모하여 정리해 두면, 중요한 내용을 혼자 모르거나 빠뜨려서 시험에 낭패를 보는 것을 예방할 수 있습니다</p>
				<p class="tabAreaPcenter pt0"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_09.jpg" alt=""></p>
				<p class="stxt1">이러한 사전 데이터를 참고하여 시험 스케줄러를 작성합니다 중간고사는 3주전, 기말고사는 4주전부터 시험 스케줄러를 짜서 하루 하루를 최선을 다해서 공부합니다. (학교 내신을 위해서 이렇게까지 해야 하냐구요? 시험기간은 우리의 실력을 가장 많이 높여줄 수 있는 소중한 기회입니다. 시험기간때 한 공부들이 모여서, 수능에서도 그리고 여러분의 앞으로의 삶에서도 중요한 재산이 됩니다)</p>
				<p class="tabAreaPcenter pt0"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_10.jpg" alt=""></p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/010" class="btn_webContents">시험 스케줄러 양식 다운로드 </a></div>
				<br>
				<p class="lst1 bgblt">교과서를 보면 문제와 답이 보입니다
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_11.jpg" alt=""></p>
				<p class="lst1 bgblt">시험공부 하기 전에 기출문제를 먼저 풀어 보세요.</p>
				<p class="lst2 bgbl2">본격적으로 과목별 시험공부에 들어가기 전에 기출문제를 가볍게 풀어 봅니다. 기출 문제를 통해 내가 공부 할 단원의 어느 부분이 중요하고, 문제가 어떤 유형으로 출제 된다는 것을 알 수 있습니다. </p>
				<p class="lst2 bgbl2">내가 반드시 기억해야 할 내용만 압축한 요점 노트를 활용하도록 합니다. 시험 기간 요점 노트를 활용하면 큰 효과를 볼 수 있어요.</p>
				<p class="lst1 bgblt">시험 전날은 이렇게 합니다. </p>
				<p class="lst2 bgbl2">농구, 축구와 같은 과격한 운동을 피해요. 과격한 운동은 근육과 신경을 흥분시켜 학습과 기억을 방해합니다.</p>
				<p class="lst2 bgbl2">우리(타)학교 전년도 기출 문제를 풀어서 최종적으로 정리합니다. 무엇보다 각 단원에서 출제 예상 문제를 뽑으면서 공부하면 더욱 효과적입니다.</p>
				<p class="lst2 bgbl2">시험 당일 필요한 준비물 예를 들어, 지우개, 컴퓨터 사인펜, 휴지 등을 미리 준비해두도록 해요.</p>
				<p class="lst2 bgbl2">시험 전날 밤은 가능한 일찍 자도록 합니다. 충분한 수면을 취하는 것이 시험 당일 기억을 회상하는 데 도움이 됩니다.</p>
				<p class="lst1 bgblt">시험 당일에는 이렇게 해봐요.</p>
				<p class="lst2 bgbl2">가능한 교실에 일찍 도착하도록 합니다. 마음의 여유로움에서 총 복습을 합니다. 이때 요점 카드를 적극 활용합니다.</p>
				<p class="lst2 bgbl2">시험 불안과 기장을 해소합니다. 깊게 심호흡을 하면서 차분히 마음을 가라앉도록 합니다.  </p>
				<p class="lst2 bgbl2">1-2분 정도 시간을 들여 시험문제 전체를 훑어보도록 합니다. </p>
				<p class="lst2 bgbl2">쉬운 문제부터 풀고 나중에 어려운 문제를 풀도록 합니다. 문제를 풀다가 어려운 문제의 해결 방법이 떠오르기도 합니다.</p>
				<p class="lst2 bgbl2">문제를 천천히 꼼꼼하게 읽도록 합니다. 특히, 객관식은 모두 옳음(O)과 틀림(X)의 문제를 가리는 것이므로 표시를 해가며 문제를 풀도록 합니다.</p>
				<p class="lst2 bgbl2">문제를 다 풀고 난 다음 전체적으로 다시 검토해 보도록 해요.</p>
				<p class="lst2 bgbl2">헷갈리는 문제는 ‘확실히 답이다’는 정보가 뒷받침 되지 않으면 고치지 않도록 합니다. </p>
				<p class="lst2 bgbl2">문제가 요구하는(출제의도) 것을 정확히 이해하고 답을 적도록 해요.</p>
				<p class="lst3 bgbl3">‘~을 비교하라’: 두 가지나 그 이상의 사실에 대해 별도로 정의를 내린 다음 공통점과 차이점을 기술하도록 합니다.</p>
				<p class="lst3 bgbl3">‘~을 증명하라’: 제시된 공식이나 개념을 자세한 내용에서 전체적인 것(부분에서 전체로) 적어나가야 해요.</p>
				<p class="lst3 bgbl3">‘~을 기(서)술하라’: 관련된 사실을 하나씩 쓰면서, 적절한 예를 들어주면 좋은 평가를 받아요.</p>
				<p class="lst3 bgbl3">‘~을 나열하라’: 문제가 요구하는 내용을 차례대로 써주면 되요.</p>
				<p class="lst1 bgblt">주관식은 정성스럽고, 선생님이 읽기 쉽게 예쁘게 쓰세요.</p>
				<p class="lst2 bgbl2"> 글씨는 정자체로 채점하는 선생님이 힘들지 않도록 써야 해요. 빈칸은 남기지 말고 알고 있는 내용을 최대한 정성을 다해 적도록 해요.</p>
				<p class="lst1 bgblt">시험이 끝나고 나면, 다음 시간 시험에 대해서만 생각하세요. </p>
				<p class="lst2 bgbl2">시험을 보는 예민한 상황에서 부정적인 생각은 기억을 회상하는 데 뿐만 아니라 다음 시험 문제를 푸는데도 영향을 준답니다. </p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="lst1 bgblt">객관식 문제 풀이 방법</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_12.jpg" alt=""></p>
				<p class="lst1 bgblt">주관식 문제 풀이 요령</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_13.jpg" alt=""></p>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="lst1 bgblt">시험지 분석 및 자기성찰</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_14.jpg" alt=""></p>
				<p class="stxt1"> 모든 일에는 마무리가 중요합니다. </p>
				<p class="stxt1">시험지를 펼쳐 놓고 과목별 틀린 문항수를 찾아서 기록한 다음, 틀린 원인을 찾아서 체크를 합니다. 가장 많이 체크된 문항은 앞으로 자신이 반드시 학습방법으로 개선해야 할 내용입니다. 위 시험지 분석 양식을 통해 구체적으로 자신의 취약점이 어느 부부인지를 찾아내도록 하는 것이 매우 중요한 성과입니다.</p>
				<p class="stxt1"> 잘 된 부분은 현재의 상태를 유지하고, 잘못된 부분은 반드시 실천을 통해 개선되어져야만 다음 시험에 좋은 결과를 약속할 수 있습니다. </p>
				<p class="lst1 bgblt">시험후 성찰 일지 작성하기</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/010/tip_15.jpg" alt=""></p>
				<p class="stxt5">지나간 시험의 점수가 중요한 것은 아니다. 시험을 통해 자신을 반성하고 앞으로 더 잘하겠다는 마음가짐이 자신을 성장시키는 일이며 시험이 주는 교훈(敎訓)이다.</p>
				<p class="lst2 bgbl2">시험점수는 오직 노력과 실력에 의해서 결정된다는 생각을 가진다.</p>
				<p class="lst2 bgbl2">좋은 성적을 얻기 위해서 막연한 기대보다는 실천이 꼭 이루어져야 한다는 사실을 항상 명심하고 매일 학습의 중요성을 깨달아야 한다.</p>
				<p class="lst2 bgbl2">분석 및 반성에서 스스로 깨달은 부분이 있다면, 앞으로 같은 잘못을 하지 않도록 주의하도록 한다. 다음 시험기간에 위의 분석을 중심으로 시험 준비를 철저히 한다.</p>
				<p class="lst2 bgbl2">잘 된 부분은 현재의 상태를 유지하고, 잘 못된 부분은 반드시 실천을 통해서 고쳐져야만 다음 시험에 좋은 성과를 얻을 수 있다.</p>
				<p class="stxt1">성찰 일지를 작성하여 다음 시험에서는 이런 시행착오를 반복하는 일이 없도록 해야 한다. 그리고 반드시 책상 앞에 붙여 놓습니다.</p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/010_1" class="btn_webContents">시험준비상태 점검표 다운로드 </a></div>
			</td>
		</tr>
		
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>