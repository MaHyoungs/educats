<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="tb1">
	<colgroup>
		<col width="15%">
		<col width="85%">
	</colgroup>
	<tbody>
		<tr>
			<th><span class="tit2">방안 1</span></th>
			<td>
				<p class="tit2">운동과 음악으로 나 자신을 행복하게!</p>
				<p class="txt2 mt15">
					생활과 학업에서 오는 스트레스를 자연스럽게 해소할 수 있도록 자신만의 취미생활이나 스포츠클럽 활동 등을 하기를 권합니다.
				</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">나만의 취미 및 특기 활동하기(스포츠 종목 1기, 악기 1기)</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">건강한 규칙적인 생활 유지</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	
		    	<p class="txt2 mt15">
					자기비하(自己卑下: 자기를 비하하고 비난하는 것)’를 하는 사람을 주변 사람들이 사랑할 수는 없습니다.
				</p>
				<br>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">3</span>
						<span class="nbt">
						최근 스스로를 비하(낮추어 표현)시켰던 혼잣말을 찾아보세요<br>
						ex) ‘난 안 돼’, ‘그 친구는 역시 달라’
						</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">4</span>
						<span class="nbt">
						위의 자기비하의 표현을 ‘자기애(self-love)’를 높일 수 있는 말로 바꾸어 봅니다.<br>
						ex)‘왜 안 돼?’, ‘내가 뭐 어때서?’
						</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">5</span>
						<span class="nbt">
						자신에게 ‘자기애’를 매일 표현하는 ‘시간과 활동’을 갖도록 합니다.<br>
						ex) 자기와 평화롭게 있는 시간-산책 시간, 하교 시간, 명상 시간 등 자기애를 표현하는 활동-거울을 보며, 나에게 미소 지어주며,  “오늘도 수고했어. 파이팅!” 이라고 격려해 줍니다.
						</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
			</td>
		</tr>
		<tr>
			<th><span class="tit2">방안 2</span></th>
			<td>
				<p class="tit2">세로토닌 호르몬으로 우울한 뇌를 회복시키는 방법</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">점심식사 후 따뜻한 햇볕을 쬐여, 더 건강한 두뇌 만들기<br><br>
						우리의 두뇌에서 분비하는 호르몬인 세르토닌 활성 감소는 우울증의 원인이 된다고 밝혀졌습니다. 따라서 인체내 자연스러운 세르토닌 생성은, 일상생활에서 일정 시간 동안 햇빛에 노출되었을 때 가능하다고 합니다.</span>
					</p>
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">밖으로 나와 상쾌하게 걸어보세요. 우울감이 어느새 사라집니다.<br><br>
						우리가 걸어서 발로 뇌를 자극하는 방법은 걷기의 과학이라고 해요. 이것을 전문가들은 <세로토닌 워킹>이라고도 하지. 세로토닌 워킹을 하고 7분 정도 후에 뇌파를 측정해 보면 느린 알파파가 사라지고 대신 10~13 Hz(헤르쯔)의 빠른 알파파가 나타나기 시작한답니다.
						</span> 
					</p>
					<p class="txt2_w">
						<span class="nbg">3</span>
						<span class="nbt">세로토닌 워킹은 이렇게 실천해 보세요.<br><br>
						 • 평소보다 조금 빠르고 보폭도 약간 넓게 걸어요<br>
					 	 • 가슴은 펴고 허리와 등은 반듯하게 해야겠죠<br>
						 • 호흡은 아랫배로 보조에 맞추어 세 번 내쉬고 한번 들이마시고.<br>
						 • 얼굴을 스쳐가는 바람도 느껴보고, 주변의 새소리도 느껴보고, 낙엽 밟는 소리에도 주의를 집중해 보세요<br>
						 • 좋아하는 음악을 들으면서 세로토닌 음악에 집중해도 좋고.<br>
						 • 이런 상태로 약 5분만 걸어도 행복의 물질인 세로토닌이 분비되기 시작하고, 약15분 정도 지나면 최고조를 느끼게 될 거예요
						</span> 
						<p class="txt2 mt15">
							사람은 부정적인 생각을 하게 되면 스트레스 호르몬인 코티죨이 분비되어 우리 뇌가 활성화 되는 것을 방해해요. 세레토닌과 친해지고, 코티죨은 멀리하는 스마트한 세종시 학생이 됩시다!!
						</p>
					</p>
		    	</div>
			</td>
		</tr>
	</tbody>
</table>

<div class="bot_btn"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>