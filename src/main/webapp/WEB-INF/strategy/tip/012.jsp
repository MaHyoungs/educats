<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>

				<p class="stit7"><span>효과적인 필기전략</span></p>
				<p class="stxt1">효과적인 Smart-아이 자기주도학습 필기전략에 필요한 다섯 가지 기본 개념에 대해 살펴보도록 합니다. <font class="bold">핵심개념 찾기, 밑줄 긋기, 단어 줄이기, 문장 축약하기, 약호 만들기</font> 마지막으로 이러한 기술을 총체적으로 활용하여 <font class="bold">코넬식 노트 필기</font>를 완성하도록 합니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_01.jpg" alt=""></p>
				
				<p class="stxt5">핵심개념 찾기란?</p>
				<p class="stxt1">핵심 개념 찾기란, 문장에서 명사로 표현된 주어,목적어가 해당합니다. 다음 문장에서 핵심 개념을 찾아볼까요? 이 문장에서의 핵심 개념은 피부색입니다. "<span class="bold">피부색은</span> 인종을 구분하는 가장 큰 특징이다. 백인종, 흑인종 그리고 황인종이란 말은 피부색의 차이에 따라 붙여진 이름이다."</p>
				<br/>
				<p class="stxt5">밑줄 긋기란?
				<p class="stxt1">필기전략의 핵심은 학습내용 중 <font class="bold">핵심개념을 찾아내어 밑줄을 긋는 것입니다.</font> 읽기 과정에서 학습 내용의 핵심 키워드에 밑줄 긋기를 통해 내용을 <font class="bold">입체적(중요한 내용과 그렇지 않은 내용)으로 파악하도록 합니다.</font>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_02.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_03.jpg" alt=""></p>
				
				<p class="stxt5">나만의 약호화 만들기</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_04.jpg" alt=""></p>
				<p class="stxt1"> 선생님의 설명 내용을 글씨로 받아 적는 것은 시간상 한계가 있습니다. 이때 자신의 약호화 된 심볼(상징)을 만들어 활용하면 필기에 많은 수고로움을 덜 수 있습니다. 마치, 가지들이 취재 과정에서 속기록을 작성하면서 활용하는 것과 같습니다. 이러한 예는 이미 여러분들이 수학 공부에서 다양한 수학적 기호를 활용하고 있는 것과 같습니다.</p>
				
				<p class="stit7"><span>먼저 나만의 필기전략 기술 수준을 점검해 보도록 합니다.</span></p>
				<p class="stxt1">아래 설문 문항에 대해 자신이 얼마나 효과적인 필기를 하고 있는지 대해 ‘예’와 ‘아니오’로 점검해 보도록 합니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_05.jpg" alt=""></p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_06.jpg" alt=""></p>
				<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/012_1" class="btn_webContents">필기상태 점검표 다운로드</a></div>
				
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit7 mt20"><span>코넬 노트 필기법은</span></p> 
				<p class="stxt1"> 미국 코넬대학에서 개발한 학습동선에 따라 조직화한 노트 필기방법으로 <font class="bold">효과적인 수업 듣기는 물론 수업 중 발생한 의문 사항들도 놓치지 않고 기록하는 과학적인 방법입니다.</font> 코넬 노트 필기법의 효과는,</p>
				<p class="lst2 bgbl2">학습 내용에 대한 깊이 있는 이해를 돕고</p>
				<p class="lst2 bgbl2">학습 내용을 입체적으로 한눈에 알아볼 수 있도록 하며</p>
				<p class="lst2 bgbl2">학습 내용을 체계적으로 정리하고 조직화 하는 데 도움이 됩니다.</p>
				<p class="lst2 bgbl2">학습 내용을 더 잘 암기․기억할 수 있도록 하고</p>
				<p class="lst2 bgbl2">학습에 대한 적극적인 태도, 책임감, 동기를 증진해 줍니다.</p>
				
				<div class="bot_btn mt40"><a href="${pageContext.request.contextPath}/reference/012" class="btn_webContents">Smart-아이 노트 필기법 양식 다운로드 </a></div>
				
				<p class="stxt5 mt40">코넬식 노트정리 방법</p>
				<p class="lst1 bgblt">수업내용 정리란(가운데)</p>
				<p class="stxt1">  수업 내용을 들으면서 정리 칸에 수업을 들으면서 동시에 필기를 아래 단계에 따라 실시합니다.</p>
				<p class="lst2 bgbl2">제1단계</p>
				<p class="lst3 bgbl3">가장 먼저 그날 <font class="bold">학습할 내용의 제목</font>을 적는다. 강조하여 적습니다.</p>
				<p class="lst3 bgbl3"><font class="bold">대단원, 중단원, 소단원</font> 등을 구분하여 적습니다.</p>
				<p class="lst2 bgbl2">제2단계</p>
				<p class="lst3 bgbl3">단원 제목 아래에 그날 배울 내용의 <font class="bold"><학습목표></font>를 적습니다.</p>
				<p class="lst3 bgbl3">학습목표에는 그날 배울 가장 핵심적인 내용들이 포함되어 있습니다. 따라서 매일 학습목표는 반드시 기록하는 습관을 갖도록 합니다. </p>
				<p class="lst2 bgbl2">제3단계</p>
				<p class="lst3 bgbl3"><font class="bold">수업을 들으며 선생님의 강의 중에 중요하다고 생각되는 내용</font>을 정리칸에 기록합니다.</p>
				<p class="lst3 bgbl3"><font class="bold">노트 여백을 충분히 남기면서</font> 필기하도록 합니다.</p>
				<p class="lst2 bgbl2">제4단계</p>
				<p class="lst3 bgbl3">필기를 할 때에는 <font class="bold">개요번호</font>를 붙이고, 들여쓰기를 해가며 적도록 합니다.</p>
				<p class="lst3 bgbl3">되도록 <font class="bold">조직화</font> 시키면서 필기하도록 해야 나중에 활용이 쉽습니다.</p>
				<p class="lst2 bgbl2">제5단계</p>
				<p class="lst3 bgbl3">잘못 필기한 부분은 지우개나 화이트로 지우지 않고 대신 <font class="bold">취소선을 긋고 고친 내용을 다시 그 위에 기록합니다.</font></p>
				<p class="lst3 bgbl3">이유는 다음과 같습니다. 대체적으로 필기를 할 때 실수하는 이유는 부주의해서 혹은 착각인경우가 많습니다. 그렇다면 심리적으로 시험 볼 때 그 부분에 실수할 가능성이 많습니다. 따라서 <font class="bold">자신이 실수 했던 부분의 흔적(상처)을 남겨둠으로써 같은 실수를 반복하지 않는 학습흔적을 남겨 놓는 것입니다. </font></p>
				<p class="lst3 bgbl3">단순한 철자나 맞춤법이 틀린 경우는 수정하도록 합니다.</p>
				<p class="lst2 bgbl2">제6단계</p>
				<p class="lst3 bgbl3">수업 중 <font class="bold">선생님이 강조한 부분, 반복해서 설명하는 부분은 확실하게 중요한 표시</font>를 해두도록 합니다.  </p>
				<p class="lst2 bgbl2">제7단계</p>
				<p class="lst3 bgbl3">수업 중에<font class="bold"> 중요하다고 생각되는 그림이나 그래프는 직접 그려보도록 합니다.</font> 다만, 꼭 수업 중에 그릴 필요는 없으며, 수업 후 복습하는 과정에서 그려도 좋습니다. </p>
				<p class="lst3 bgbl3">직접 그리다 보면 그림이나 표에 대한 이해도가 훨씬 높아지기 때문입니다.</p>
				<p class="lst2 bgbl2">제8단계</p>
				<p class="lst3 bgbl3">수업 중에 제시된 <font class="bold">다양한 느낌이나 다른 정보도 필기해 보도록 합니다.</font></p>
				<p class="lst3 bgbl3 bold">단순 사실적 정보보다 정서적 뉘앙스가 가미된 정보를 회상할 때 더욱 잘 일어납니다. </p>
				
				<p class="lst1 bgblt">(왼쪽)기억 단서 칸 정리법</p>
				<p class="stxt1"> 상위 8단계에 따라 수업 내용을 정리 칸에 정리하고 나서, 핵심 단어칸을 적습니다. <font class="bold">보통 핵심칸을 적는 시점은 수업이 끝난 직후 가 가장 적합합니다.</font> 수업 내용이 생생하게 머릿속에 남아 있을 때, 수업의 키워드를 뽑아 내어 인출단서로 정리합니다. </p>
				<p class="stxt1"> 만약 핵심단어를 찾지 못했을 경우에는 다음을 참고하여 핵심 단어칸을 적습니다. </p>
				<p class="lst3 bgbl3">선생님이 수업 중에 반복한 단어</p>
				<p class="lst3 bgbl3">학습목표, 목차, 제목 등에 포함된 단어</p>
				<p class="lst3 bgbl3">교과서에 고딕체로 굵게 표시된 단어나 강조된 단어</p>
				
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_07.jpg" alt=""></p>
				
				<p class="stit7"><span>노트한 내용을 보충하고 최종 정리</span></p>
				<p class="stxt1"> 수업 중에 설명한 내용을 들으면서 완벽한 노트틑 정리한다는 것은 누구에게나 불가능합니다. 따라서 수업 중에 정리한 노트틑 스스로 정리하고 보충하는 시간이 필요합니다. <font class="bold">그 시기는 수업을 들은 당일 오후 방과 후에 노트를 정리하는 것이며, 그 과정을 자연스럽게 복습으로 연계합니다.</font> 다음 두 단계에 따라 해보도록 합니다.</p>
				
				<p class="lst1 bgblt">제1단계: 보충하기</p>
				<p class="lst3 bgbl3">교과서나 노트에 모르는 단어가 있으면 찾아서 기록해 둡니다.</p>
				<p class="lst3 bgbl3">빠진 내용을 보충해 둡니다. 모르는 내용은 참고서를 찾아서 알 수 있는 내용으로 기록해 둡니다.</p>
				<p class="lst3 bgbl3">그런 다음, 핵심 단어칸을 다시 살피면서 한 번 더 정리하도록 합니다.</p>
				
				<p class="lst1 bgblt">2단계: 복습하기</p>
				<p class="stxt1"> 여기서 <font class="bold">복습하기란 코넬노트에 필기된 내용을 가지고 이해하고 암기하는 과정을 의미합니다.</font></p>
				<p class="lst3 bgbl3">수업내용 정리 칸에 있는 내용들을 2-3번 정도 정독하면서 읽습니다.</p>
				<p class="lst3 bgbl3">정독하면서 필요한 내용은 암기합니다.</p>
				<p class="lst3 bgbl3">핵심 단어 칸에 있는 인출단서만 보고 그 내용을 떠올려 봅니다(암송법).</p>
				<p class="lst3 bgbl3">암송하는 도중 생각이 나지 않으면 수업 내용 중 정리칸을 보면서 다시 암송해 봅니다. </p>
				<p class="lst3 bgbl3">수업 내용 중 정리 칸에 있는 내용들을 충분히 숙지될 때까지 이 과정을 반복합니다.</p>
				
				<p class="stit7"><span>좋은 노트란?</span></p>
				<p class="stxt1"> <font class="bold">간결성, 능률성, 개인적 개입이 들어간 노트 필기입니다.</font> 어휘, 상징, 선, 이미지, 색 등을 복합적으로 활요한 자신의 사고가 적극적으로 개입된 입체적인 노트필기가 중요합니다. 이러한 요소를 담아낸 필기 방법이 마인드맵(Mind Map)입니다. 이것은 인간의 좌우 뇌를 활용한 노트 필기 방법입니다. 여러분, 세계적인 노트필기의 비법인 코넬노트 정리법을 굳게 마음먹고 1달만 실천해 봅니다. 여러분의 중간, 기말고사 성적이 달라질 겁니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit4">효과적인 노트필기를 위한 노하우(know-how)를 알아보도록 하겠습니다.</p>

				<p class="lst1 bgbl1 bold">과목별로 각기 다른 노트를 준비합니다. </p>
				<p class="lst2 bgbl2">얇은 노트를 여러 권 사용하는 것이 좋습니다. </p>
				<p class="lst2 bgbl2">노트를 항상 사용하기 쉬운 위치에 순서대로 놓는 습관을 갖도록 합니다.</p>
				
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_08.jpg" alt=""></p>
				
				<p class="lst1 bgbl1 bold">노트필기는 수업과 자신을 연결해주는 대화의 창입니다.</p>
				<p class="lst2 bgbl2">배운 내용을 모두 적는 것이 아니라, 중요 부분을 적는 것이 중요합니다.</p>
				<p class="lst2 bgbl2">모든 수업 내용을 필기 없이 기억하는 것은 어렵습니다. </p>
				<p class="lst2 bgbl2">노트필기를 할 때 가능한 여백을 많이 남깁니다.</p>
				
				<p class="stxt1">필기는 뇌과학적으로 우리가 수업에 집중하게 해주고, 차후 복습이나 시험공부때, 수업의 장면을 생생하게 기억할 수 있게 해주는 실마리(clue)를 제공해 주므로 꼭 필요한 학습과정입니다.</p>
				
				<p class="lst1 bgbl1 bold">필기는 자신의 이해된 언어로 의역해서 기록합니다.</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/012/tip_09.jpg" alt=""></p>
				
				<p class="lst1 bgbl1"><font class="bold">노트필기는 새로운 내용을 이전의 알고 있는 내용과 연결시켜 기록합니다.</font> 다양한 예시와 분류, 비교, 유사점과 차이점 등을 잘 정리하도록 합니다.</p>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>


<script>
function hwpDown(){
	alert("down");
}
</script>