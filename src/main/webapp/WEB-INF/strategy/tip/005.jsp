<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
				<p class="tt3 bold">자신의 학습 동기를 점검해 보세요.</p>
<!-- 				<p class="tabAreaPcenter"><a href="javascript:void(0)" class="cnt_btn" onclick="conPopOpen('005_01')">점검 테스트 하기 클릭</a></p> -->
				<p class="tabAreaPright"><a href="javascript:void(0)" class="cnt_btn2" onclick="conPopOpenText('005_01')">점검 테스트 하기 클릭</a></p>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
				<p class="stit7"><span>구체적인 목표를 정하라</span></p>
				<p class="stxt1 red bold">학습동기는 구체적인 목표를 정해서 손으로 자주 쓰는 것에서부터 시작합니다.</p>
				<p class="lst1 bgbl1">미래 구체적인 목표와 꿈을 정하는 것이 얼마나 중요한지를 말해주는 매우 중요한 연구 결과가 있어요. 먼저 한 번 살펴볼까요?</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_01.jpg" alt=""></p>
				<p class="tt3">
					1979년 하버드 대학교 경영대학원 졸업생을 대상으로 "명확한 장래 목표와 그것을 성취할 계획이 있는가?"라는 설문조사를 한 적이 있었습니다. 
					<br>
					<br>이 질문에
				</p>
					<p class="lst3 bgbl3"><font class="bold">졸업생의 3%만이 목표와 계획을 세웠으며 그것을 기록</font>해 두었다고 응답했고, (A그룹)</p>
					<p class="lst3 bgbl3">13%는 목표는 있으나 그것을 종이에 직접 기록하지는 않았다고 했습니다. (B그룹)</p>
					<p class="lst3 bgbl3">나머지 84%는 여름휴가 계획 이외에는 아무런 계획이 없다고 대답했습니다. (C그룹)</p>
				<p class="tt3"> 
				<br>그로부터 10년 후인 1989년, 연구자들은 10년 전의 졸업생을 대상으로 다시 한 번 조사를 했습니다. 그 결과 놀라운 사실을 하나 발견했습니다. </p>
				<p class="lst3 bgbl3 mt20">목표는 있었지만 기록하지 않았던 B그룹은 목표가 전혀 없었던 학생들인 C그룹에 비해 평균수입이 두 배 이상이었고</p>
				<p class="lst3 bgbl3"><font class="bold">명확한 목표와 계획을 세우고 그것을 구체적으로 기록했던 A그룹의 졸업생</font>들은 C그룹 졸업생보다 소득이 평균 <font class="bold">열 배 정도 많았다.</font></p>
				<p class="tt3 mt20"> 이 연구 결과가 정말일까 해서, 같은 미국 동부의 명문대학인 예일대학에서 반론의 가설을 세우고 검증 차 종단연구(비교적 오랜 시간 동안 연구)를 했는데, 오히려 하버드대학의 연구결과를 지지하는 결론이 나왔다고 합니다.</p>
				<p class="quotation">여러분 목표를 설정하고, 종이에 옮겨쓰는 순간, 그 목표 = 꿈은 여러분의 것이 됨을 잊지 마세요!</p>
				
				<p class="stxt1 mt20 bold">세계적으로 유명한 인물들이 구체적인 목표를 글로 쓰고, 후에 큰 성공을 거둔 사례를 살펴볼까요?</p>
				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_02.jpg" alt=""></p>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
				<p class="stit7"><span>구체적인 SMART한 목표 정하는 방법</span></p>
				<p class="stxt1 red bold">SMART한 목표 정하기</p>
 				<p class="tt3">영어 알파벳 첫 글자만 딴 SMART법칙은 smart(똑똑한)라는 단어대로 여러분들을 똑똑하게 목표를 정하도록 도와 줄 거예요.</p>
 				<p class="lst1 bgnum1 bold"> 목표는 구체적이어야(Specific) 해요.</p>
 				<p class="tt3">'나 의사가 되는 게 꿈이야.'가 아니고, 구체적으로 어느 분야의 의사, '난 소아과 의사, 심장 전문의' 등 구체적이어야 합니다. '난 선생님이 꿈이야'가 아니고, 초등학교 선생님, 중·고등학교 영어 선생님처럼 구체적으로 정하는 것이 좋습니다. 구체적일수록 실현가능성이 높거든요!</p>
 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_03.jpg" alt=""></p>
 				<p class="lst1 bgnum2 bold"> 목표는 측정 가능해야(Measurable) 해요.</p>
 				<p class="tt3"> '다이어트 하겠다'가 아니라 '이번 달 5Kg 빼겠다'<br>'이번 중간고사에서 열심히 할 거야'가 아니라 '이번 중간고사에서 영어 성적 95~98점 이상' 이렇게 구체적이어야만 해요. </p>
 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_04.jpg" alt=""></p>
 				<p class="lst1 bgnum3 bold"> 성취 가능한 목표가 되어야(Achievable) 해요. 자신의 강점을 활용해 실현 가능한 목표가 되어야 해요. 목표는 수치화될 때  실행의 파워를 갖습니다.</p>
 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_05.jpg" alt=""></p>
 				<p class="lst1 bgnum4 bold"> 내가 원하고 만족할 수 있는(Rewarding) 목표가 되어야 해요.</p>
 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_06.jpg" alt=""></p>
 				<p class="lst1 bgnum5 bold"> 반드시 시간제한이 있어야만(Time-limeted) 해요.</p>
 				<p class="tt3">언제까지 이루겠다는 목표제한 시간이 설정 되어야 마감시간까지 미루지 않고 최선을 다하려는 노력을 하겠지요.</p>
 				<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/005/tip_07.jpg" alt=""></p>
 				<p class="lst2 bgbl2">이렇게 분명한 목표를 세운다는 것은 자기 삶의 목적지와 방향을 정하는 것이지요.</p>
 				<p class="lst2 bgbl2">우리는 분명한 목적이 있을 때 크고 작은 어려움들을 견딜 수 있거든요.</p>
 				<p class="lst2 bgbl2">롤 모델을 정하는 것도 정해서 그 분의 사진을 책상에 붙여 놓는 것도 좋은 방법이겠지요.</p>
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
				<p class="stit7"><span>SMART한 목표 정하는 절차</span></p>
				<p class="tt3 mt20">다음과 같은 질문을 하면, 여러분이 원하는 꿈(목표)가 무엇인지를 SMART하게 알수 있답니다.</p>
				<p class="stxt1 red bold mt20">자신에 대한 <목표 및 가치 인식>에 대한 탐색적 질문!</p>
				<p class="bgq1">내가 진정으로 원하는 것은 무엇인가?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="bgq2">내가 공부를 해야만 하는 이유는 무엇인가?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="bgq3">내가 대학에 진학해야만 하는 이유는 무엇인가?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<p class="bgq4">나의 구체적인 진로의 방향과 관련된 분야는 무엇인가?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<br>
				<br>
				<p class="stxt1 red bold">진솔하게 내가 진술한 내용이 가치가 있는지 생각해보고, 자신의 내적 동기화로 이어지도록 합니다.</p>
				<p class="tt3 ml27">만일 막연한 생각이 든다면 당신의 미래는 그저 막연하고 자신감을 가지기 어렵습니다.<br>
 				세계적인 경영컨설턴트이자 베스트셀러 저자인 브레이언 트레이시는 ‘목표는 막연한 꿈이 아닌 기술’이라고 강조했습니다. 내가 성공하려면 앞에서 말한 3%의 방식을 따라야 합니다.</p>
				<p class="bgq1"> 나는 지금 무엇을 해야만 하는지, 내가 잘 할 수 있는 일은 무엇인가요?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
 				<p class="bgq2"> 어떤 사람이 되어 어떻게 살고 싶은가?</p>
				<p class="tt3"><input type="text" id="" name="" class="int" disabled="disabled"></p>
				<br>
				<br>
				<p class="stxt1 red bold">어떻게 그것에 도달할지에 대해 미리 생각하고 움직여야 하며, 그러한 마음의 지도를 갖도록 해야 합니다.</p>
				<p class="lst1 bgbl1">공부는 <내가 원하는 것, 가지고 싶은 것, 가고 싶은 곳>을 가장 빠르게 합법적으로 안전하게 성취하게 해 주는 수단입니다.</p>
				<p class="lst1 bgbl1">원하는 목표에 도달하면 스스로에게 보상을 해 줍니다.</p>
				<p class="lst2 bgbl2">한 가지 학습 과제를 마치면 10분간 휴식을 취한다.</p>
				<p class="lst2 bgbl2">한 주를 계획대로 열심히 실천하고 보냈다면, 주말에 자신이 하고 싶은 일에 몰입하여 할 수 있도록 시간을 할애한다.</p>
				<p class="lst1 bgbl1">내가 진학하고 싶은 학교(고교/대학)사진으로 주변을 도배하라! 책상, 화장실, 나의 방문 등에  붙여서 지속적인 동기유발을 강화합니다.</p>
				<p class="quotation">
					소위 데니스 웨이트리 박사가 창안한 &lt;R=VD법칙&gt;입니다. 즉, 생생하게 꿈꾸면 이루어진다! 는 원리입니다.<br> 
					<span class="bold" style="display: block;padding: 0 auto; text-align: center;">[R: Realization(실현), V: Vivid(생생한), D: Dream(꿈)]</span> 
					<br>
					실제 연구결과, 세계적으로 성공한 사람들의 가장 많은 공통점이 바로 이 <R=VD법칙>이었답니다. 내가 이루고자 하는 것을 눈앞에 생생하게 꿈꾸었다는 중요한 사실입니다.<br> 
					- "시크릿"이라는 베스트셀러 책에도 “시각화하면 반드시 이루어 진다” 는 말이 나옵니다.<br>
					- "연금술사"라는 책에서는, “우리가 간절히 원하면 온 우주가 우리를 돕는다”고 했습니다.
				</p>
				<p class="lst1 bgbl1">스스로 책임을 감수하도록 합니다.</p>
				<p class="lst2 bgbl2">내가 자신과의 약속을 지키지 못했을 때는 자신에게 그에 따른 책임을 부여하도록 합니다.</p>
 				<p class="lst2 bgbl2">컴퓨터 1일 안하기, 휴대폰 1일 만지지 않기 등 자신이 좋아하는 일을 스스로 포기하게 하여 책임감을 강제하도록 합니다.</p>
				
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
				<p class="stit7"><span>SMART한 목표! 당장 실천해 볼까요? Just Do It!</span></p>
				<p class="tt3">오늘 당장, 그 결과를 확인할 수 있는 목표 세 가지만 적어 볼까요?
					이때, 구체적이고 측정 가능하며 행동 지향적일 뿐 아니라 실현 가능하며 제한 된 시간 내에 할 수 있는 일이어야 함을 꼭 기억하세요!<br>
				</p>
				<p class="lst1 bgbl1">스터디 그룹을 만들어 공부하세요! 스터디그룹을 만들어 공부하면 학습시간이 지루하지도 않고 학습 역동적이고 흥미로울 수 있습니다.</p>
				<p class="lst2 bgbl2 mt20">스터디 그룹의 장점</p>
				<p class="lst3 bgbl3">내가 서로가 모르는 것을 협동학습을 통해 쉽게 해결할 수도 있다.</p>
				<p class="lst3 bgbl3">내가 맡은 분야(예: 수학, 혹은 일부 단원)에 책임감도 느끼게 되어 더 열심히 하게 된다.</p>
				<p class="lst3 bgbl3">가장 학습효과가 좋다는 하부르타 교수법인 다른 사람에게 가르치기(토론하기)를 할 수 있는 공식적 기회가 주어진다.</p>
				<p class="lst3 bgbl3">친구들 사이에 우정관계도 돈독해지는 보너스를!</p>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>
