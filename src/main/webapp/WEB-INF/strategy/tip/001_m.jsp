<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="tb1">
	<colgroup>
		<col width="15%">
		<col width="85%">
	</colgroup>
	<tbody>
		<tr>
			<th><span class="tit2">방안 1</span></th>
			<td>
				<p class="tit2">잦은 성공 경험을 위한 목표 세우기</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">이번 주에 꼭 이루고 싶은 목표</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">이번 달에 꼭 이루고 싶은 목표</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">3</span>
						<span class="nbt">이번 학기에 꼭 이루고 싶은 목표</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
			</td>
		</tr>
		<tr>
			<th><span class="tit2">방안 2</span></th>
			<td>
				<p class="tit2">에디슨의 놀이법</p>
				<p class="txt2 mt15">
					“항상 모든 일에서 재미를 찾으세요.
					저는 실험을 할 때 놀이를 한다고 생각합니다. 
					책을 읽을 때도, 일을 할 때도 마찬가지입니다.
					여러분도 그렇게 살아 보세요. 
					숙제도, 공부도, 회사 일도 모두 놀이라고 생각해 
					보세요.
					그러면 항상 행복하게 살 수 있답니다“ -- 토마스 에디슨--
				</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">에디슨의 말에 비춰 보았을 때, 본인은 행복하다고 생각하나요?</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">누구나 행복을 추구합니다. 행복한 삶이 되기 위해서는 어떻게 해야 할까요? </span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
			</td>
		</tr>
		<tr>
			<th><span class="tit2">방안 3</span></th>
			<td>
				<p class="tit2">나는 운이 좋다 vs. 나는 운이 나쁘다</p>
				<p class="txt2 mt15">
					일본 마쓰시타 전기의 창업자인 마쓰시타 고노스케는 
					신입사원을 면접할 때 반드시 이런 질문을 했다고 한다. 
					“당신은 그동안 운이 좋았다고 생각합니까?”
					그는 긍정적인 답변을 한 사람만 입사시켰다고 하며,
					실제로 그 사원들이 회사의 중견 간부가 되었을 때 
					회사가 황금기에 들어섰다고 한다.
				</p>
				<p class="txt2 mt15">
					이순신 장군은 명량해전을 앞두고 일본의 대군을 맞으며 
					선조에게 올린 글에서 
					“신에게는 아직 12척의 배가 있습니다.”라고 하면서 
					희망을 버리지 않고 지형과 조류를 이용하여 대승하였다.
				</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">여러분은 평소에 운이 좋다고 생각하나요?</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">그렇게 생각하는 이유는?</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">3</span>
						<span class="nbt">오늘부터 일주일 동안 하루에 한 가지씩 <운이 좋았던 일>을 써 보세요. </span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int" readonly="readonly"></p>
		    	</div>
		    	<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/001/tip_01.jpg" alt=""></p>
			</td>
		</tr>
	</tbody>
</table>

<div class="bot_btn"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>