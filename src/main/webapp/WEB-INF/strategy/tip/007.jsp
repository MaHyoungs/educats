<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="s_tb5">
	<colgroup>
		<col width="159">
		<col width="750">
	</colgroup>
	<tbody>
		<tr>
			<th>Way 1.</th>
			<td>
			<p class="stit4">수업에 임하는 나의 자세는 어떤지 분석해 봅시다.</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_01.jpg" alt="수업에 임하는 나의 자세" /></p>
			<p class="stxt1">긍정형에 체크된 숫자가  부정형에 체크된 숫자보다 많으면 ⇛ 수업참여상태 양호<br/>
			부정형에 체크된 숫자가  긍정형에 체크된 숫자보다 많으면 ⇛  수업참여상태 개선 필요</p>
			<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/007" class="btn_webContents">수업참여상태 체크리스트 다운로드 </a></div>
			</td>
		</tr>
		<tr>
			<th>Way 2.</th>
			<td>
			<p class="stit7"><span>효과적인 수업 듣기의 필요성</span></p>
			<p class="stxt1">여러분들이 가장 많이 접하는 것은, 학교나 학원 같은<font class="bold">교수･학습장면= 수업</font>입니다. 그동안 많은 시간을 유치원부터~고등학교까지 수동적이며 의존적인 학습 환경에 노출되다보니, 우리 자신 스스로가 학습을 주도적으로 이끌어가지 못하는 학습태도를 갖게 되었습니다.<br/><br/>
			효과적인 수업 듣기란 무엇일까요?<br/>
			<font class="bold">수업 중에 집중력을 동원하여 선생님의 수업활동에 적극적으로 함께 참여하는 행동을 말합니다.</font><br/>
			여러분들이 절대시간을 '수업 듣기'에 보내고 있는 상황에서, 어떻게 하면 보다 '효율적･효과적∙매력적'인 수업활동 시간을 만들어 갈 수 있을까요?</p> 
			<p class="quotation_gray">얼마 전 수학능력고사에서 만점을 받은 학생들을 대상으로, 학생들의 전반적인 학교생활에 관한 질의응답 결과, 이들은 학창시절 공부 방법에 대해 한결같이 <font class="bold">‘학교수업에 충실했다’는 말을 강조하고 있었습니다.</font> 이 말은 소위 전설같이 내려오는 ‘진리’가 아닌가요? 청소년기의 절대 시간을 학교와 학원에서 보내고 있는 상황에서, 수업시간을 효율적으로 활용하지 못하고 좋은 결과를 원하는 것은 연목구어(緣木求魚:나무에서 물고기를 구함)라 할 수 있습니다.</p> 
			<p class="stit7 mt60"><span>수업 집중력 높이는 5단계 기술</span></p>
			<p class="stxt1">여러분은 오늘 수업에서 몇 단계 기술까지 수업에 적용했나요?<br/>
			모두 적용했다면 여러분의 오늘 수업은 Perfect!입니다. 수고하셨어요!</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_02.jpg" alt="수업 집중력 높이는 5단계 기술" /></p>
			<p class="stit7"><span>적극적인 수업 참여를 위한 7가지 요소</span></p>
			<p class="lst2 bgbl2">바른 수업태도 갖기</p>
			<p class="lst2 bgbl2">적극적인 청해자 되기</p>
			<p class="lst2 bgbl2">선행학습 내용과 배울 내용을 연계하기</p>
			<p class="lst2 bgbl2">선생님과 심리적 다이아몬드 존에 들어가기</p>
			<p class="lst2 bgbl2">수업의 흐름과 신호어 찾기</p>
			<p class="lst2 bgbl2">수업의 표지판 찾기</p>
			<p class="lst2 bgbl2">교과서에 단권화하기</p>
			<p class="stxt1">이 7가지 요소를 지키기 위한 가장 핵심 요소는 수업에 집중하기입니다.  '수업시간에 집중하는 학생이 공부를 잘한다'는 얘기는 흔하게 들어왔죠? 그러나 어떻게 해야 수업 집중력을 높일 수 있는지 고민하는 학생들은 많지 않아요. 수업에 집중력을 높이려면 어떻게 해야 할까요?</p>
			<p class="stxt1 bold mt40">수업의 목표를 정해 집중력 높여라</p>
			<p class="lst2 bgbl2"><font class="bold">수업 시작 전</font>, 수업을 통해 자신이 무엇을 얻고자 하는지에 대한 입장 정리가 필요합니다. <font class="bold">수업 목표를 간략히 노트에 적어 보는 '준비'가 필요합니다.</font></p>  
			<p class="lst2 bgbl2"><font class="bold">수업시간 동안은 '선생님과 시선 맞추기'</font>와 같은 자세와 관련된 것이나, <font class="bold">'학습 목표와 관련돼 궁금한 점 해결하기'</font> 같은 수업 내용과 관련된 목표가 좋습니다. 구체적이고 성취 가능한 목표를 세워 수업 시간에 목표를 하나씩 이루려고 하다보면 수업에 대한 집중력은 올라갑니다.</p> 
			<p class="lst2 bgbl2">효과적 수업전략은 수업 중에 필기한 부분을 살펴보는 <font class="bold">5분의 수업 후 마무리로 완성됩니다.</font><br/>
			수업이 끝난 후 어떤 내용을 배웠는지 교과서와 노트를 중심으로 다시 한 번 훑어보는 학습습관이 열 걸음 중 마지막 한 걸음에 해당한다고 할 수 있습니다. 단 1~2분이면 충분합니다. 그렇다면 단 1~2분을 활용해 50분 동안 수업한 내용을 일목요연하게 정리하고, 즉석 복습을 통해 기억력을 높여주는 학습 습관이야말로 멋지고 매력적인 학습태도 입니다.</p>
			<p class="quotation_gray"><font class="bold">학습은 <교사의 입>에서가 아닌, <학습자의 머리>에서 이루어져야합니다.</font><br/>
			중국 속담에 <열 걸음 중 아홉 걸음이 반이다.>는 말이 있습니다. 이것은 우리 속담 <시작이 반이다.>와 반대되는 의미로, ‘시작보다는 끝맺음’의 중요성을 강조하는 말입니다.</p> 
			<p class="stit7 mt40"><span>학교 수업 완전 정복 체크리스트</span></p>
			<p class="stxt1">자 오늘 여러분의 수업 집중도를 체크해 보세요. 여러분이 오늘 얼마나 수업에 집중했는지를 스스로 점검할 수 있습니다.</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_03.jpg" alt="학교 수업 완전 정복 체크리스트" /></p>
			<div class="bot_btn mt20"><a href="${pageContext.request.contextPath}/reference/007" class="btn_webContents">수업참여상태 체크리스트 다운로드 </a></div>
			</td>
		</tr>
		<tr>
			<th>Way 3.</th>
			<td>
			<p class="stit7"><span>선생님들은 수업을 어떻게 진행하실까?</span></p>
			<p class="lst2 bgbl2">수업의 결(흐름)을 느껴봅시다.</p>
			<p class="lst2 bgbl2">선생님들이 만드신 교수(수업)설계 내용을 확인해 봅시다.</p>
			<p class="stxt1">수업을 하기 위해 짜 놓은 설계도를 수업지도안이라고 합니다. 선생님들은 이러한 흐름을 염두에 두고, 우리들의 수업을 이끄신답니다. 선생님들의 수업 계획을 알면, 수업에 훨씬 더 잘 적응이 되겠지요?</p> 
			<p class="quotation_gray"><font class="bold"> 가네(Gagne)가 제시한 9단계 수업 절차를 활용한 선생님의 수업 지도안</font><br/><br/> 선생님들은 수업 내용을 학생들에게 잘 전달하기 위해, 이렇게 체계적으로 수업 내용을 구성합니다. 이 수업의 단계를 알고 있으면 수업 내용이 더 재미있고, 수업 중 어디에 집중해야 할지를 알 수 있습니다.
			(선택과 집중의 법칙을 활용해서 수업의 결을 따라 들어보세요. 수업 듣는 것이 훨씬 쉽고 재밌습니다.)</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_04.jpg" alt="9단계 수업 절차를 활용한 선생님의 수업 지도안" /></p> 
			</td>
		</tr>
		<tr>
			<th>Way 4.</th>
			<td>
			<p class="stit7"><span>수업 전  (Before Class)</span></p>
			<p class="stxt1"><b>수업 전 준비자세</b>: 수업에 들어가 전에 이것만큼은 꼭 준비합니다.</p>
			<p class="lst1 bgbl1 bold">수업 전 준비물</p>
			<p class="lst2 bgbl2">교과서, 노트, 필기도구(3색 펜), 형광펜, 샤프, 자(15cm), 포스트잇 등</p>
			<p class="lst2 bgbl2">수업 전에 선생님이 지시한 학습준비물</p>
			
			<p class="lst1 bgbl1 bold">수업 전 마음 가짐</p>
			<p class="lst2 bgbl2">수업에 들어가기 전에 반드시 뇌를 안정시켜야 해요. 공부는 결국 뇌가 합니다. 흥분한 뇌가 학습 뇌로 바뀌는 데는 최소 30분에서 1시간이 소요됩니다.(진흙탕물이 침전되는 것과 같은 원리)</p> 
			<p class="lst2 bgbl2">선생님이 수업 시작에서부터 끝날 때까지 어떻게 수업을 이끌어 가는지 수업 진행 방식을 알고 있어야 합니다.</p>
			<p class="lst2 bgbl2">휴식 시간에 마음을 차분히 가라앉히고 평화롭게 유지해야 합니다.</p>
			<p class="lst2 bgbl2">이 시간에 배울 내용을 간단하게 예습(Survey:훑어보기-지난 시간 배운 내용 회상, 학습목표, 배울 내용 등)하는 것 입니다.</p> 
			
			<p class="lst1 bgbl1 bold">수업 전 몸의 자세</p>
			<p class="lst2 bgbl2">수업은 바른 자세가 중요합니다.(엉덩이를 의자의 뒷부분에 밀착시키고, 허리선을 쭉 편다.)</p>
			<p class="lst2 bgbl2">수업 자세가 바르지 못하면 집중이 어렵고, 시간이 흐르면서 잡념과 졸음에 정신을 양보해 주어야 합니다.</p>
			<p class="lst2 bgbl2">바른 자세는 우리의 뇌에 혈액을 왕성하게 흐르게 하여 집중력과 기억력을 좋게 합니다. 척추가 바로 되어야 뇌에 혈액 공급이 원활하여 해마 등 모든 조직의 활동이 원활합니다.</p>
			<p class="stit7"><span>수업 중 (While Class)</span></p>
			<p class="lst1 bgbl1 bold">수업 이란?</p>
			<p class="lst2 bgbl2">모르는 것(새로운 학습 내용)을 알고, 중요한 내용을 이해하고 암기하는 것입니다.</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_05.jpg" alt="수업이란 아는것과 모르는 것을 구분하는것" /></p>
			<p class="lst1 bgbl1 bold">수업에 임하는 마음과 자세</p>
			<p class="lst2 bgbl2">마음이 경직되지 않고, 유연하고 평화롭게 유지합니다.</p>
			<p class="lst2 bgbl2">모든 시각, 청각적 동선은 선생님에게 집중시킵니다. 선생님과 눈을 마주하려고 하면 졸음과 잡념이 유발되지 않습니다.</p>
			<p class="lst2 bgbl2">항상 필기 준비 자세를 갖추고 있어야 합니다.
			3색 펜 준비(검정: 일반적인 내용 필기, 파랑: 새로운 내용, 창의적 생각 추가, 빨강: 중요한 내용 첨삭)</p>
			<p class="stit7"><span>수업 후 (After Class)</span></p>
			<p class="lst1 bgbl1 bold">황금의 5분을 잡아라</p>
			<p class="lst2 bgbl2">수업 후, 5분의 습관이 우등생과 열등생을 만들기도 합니다</p>
			<p class="lst1 bgbl1 bold">해마의 밥상을 치우지 마라</p>
			<p class="lst2 bgbl2">수업이 끝나자마자 책을 집어넣고 친구들과 장난하거나 떠들기기 시작하면 방금 공부한 내용은 여러분 뇌(해마)가 다 토해버립니다.</p>
			<p class="stxt1 lst1">학습의 뇌인 해마가 아직 공부한 내용을 다 먹지도 못했는데, 상을 치워버리면 어떻게 될까요? 여러분 밥도 덜 먹었는데, 밥상을 모두 치워버리고 엄마가 밖으로 나가버리시면 어떤 기분이 들까요? 생각만 해도 끔찍하죠? 여러분의 학습 뇌인 해마도 마찬가지입니다.<br/> 
			잠깐 동안 차분히 학습한 내용을 정리하고, 내가 이해한 내용과 이해가 안 된 내용을 확인하도록 해야 합니다.<br/>
			나는 한 시간의 수업 중에 대략 어떤 행동을 지금까지 해왔는지 생각해 봅니다. 그리고 이제 효과적인 수업참여전략의 학습 내용대로 수정해 보도록 합니다.  여러분 수업이 신이 날것 입니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 5.</th>
			<td>
			<p class="stit4">수업에서 꼭 잡아야 하는 것들</p>
			<p class="stit7"><span>개념을 잡아라!</span></p>
			<p class="lst1 bgbl1 bold">개념이란 ‘어떤 말의 뜻을 일반적인 말, 보통 하는 말(생활용어)로 풀어서 쉽게 정의하는 말’입니다.</p>
			<p class="lst2 bgbl2">명사(名詞, Noun)란 사람·동물·사물, 유형·무형의 이름을 나타내는 말로, 문장에서 주어, 목적어, 보어로 쓰입니다.</p>
			<p class="lst2 bgbl2">무리수란 ‘어떤 수를 소수로 나타낼 때, 순환하지 않는 무한소수가 되는 수’를 말합니다.</p>
			<p class="lst2 bgbl2 bold">이러한 말이 어렵기 때문에 선생님은 수업 중에 알기 쉬운 말과, 이하기 쉽도록 예를 들어 설명해 줍니다.</p>
			<p class="stit7"><span>학습목표를 잡아라!</span></p>
			<p class="lst2 bgbl2 bold">학습목표는 보통 단원 시작 부분에 교과서에 제시되어 있습니다.</p>
			<p class="lst2 bgbl2">학습목표는 공부의 시작점이자 도달점입니다. 단원에서 학생들이 반드시 알아야 할 내용을 제시해 준거죠. 학습목표를 모르고 수업에 임하는 것은 마치 목적지를 정하지 않고 고속도로를 진입하는 것과 같습니다.</p>  
			<p class="lst2 bgbl2">학습목표는 평가목표가 됩니다.<font class="bold"> 평가는 반드시 학습목표를 성취했는지를 기준으로 평가됩니다. 이는 다른 말로 말하면, 선생님 출제하는 시험문제와 밀접하게 관련이 있다는 중요한 정보입니다. </font>이미 여러분에게 시험문제의 예상 정보를 80%정도 노출시키고 있다는 것이지요. 학습목표의 중요성이 얼마나 중요하지 알고 있어야 합니다. 바로 ‘80:20법칙’이 적용됩니다. 교과서 내용 정보를 100으로 기준 했을 때, 실제 중요한 내용은 20%(핵심 내용)이고, 나머지 80%는 부가적 정보에 불과 합니다. 20%의 핵심내용이 바로 학습목표에 해당합니다.</p>
			<p class="stit7"><span>선생님이 강조하시는 부분을 잡아라!</span></p>
			<p class="lst2 bgbl2">수업 중에 관련 내용을 <font class="bold">칠판에 써서(중요하지 않다면 칠판에 쓸 이유가 전혀 없음) 설명하시거나, 설명 도중 그 부분을 강조하고, 분필을 두드리거나 쓱쓱 밑줄을 긋는 경우</font>등의 선생님의 비언어적 행동까지 포착합니다.</p> 
			<p class="lst2 bgbl2">수업 중에 선생님이<font class="bold"> 직접 중요하다고 고백하는 경우.  ‘중요하다! 암기하라! 이거 시험에 나온다!’고 할 때, 책이나 노트에 반드시 필기와 자신의 중요 표시를 해 두어야 합니다.</font> 여기에다가, 반복해서 설명하시거나 추가로 과제물로 내주는 내용은 매우 중요하다는 것으로, 시험에 출제될 확률이 매우 높다고 할 수 있습니다.</p>
			<p class="stit7"><span>수업 중, 모르는 내용을 잡아라!</span></p>
			<p class="lst2 bgbl2"><font class="bold">수업 중에 이해가 가지 않는 내용은 반드시 즉각적인 표시를 해두도록 합니다.</font> 그리고 누구에게 도움을 청해야 할지도 생각해 보고, 옆에 간단히 표시를 해둡니다. 예를 들어, 선생님이면 <선>, 친구면<친>, 인터넷이면<인> 등으로 표시해 둡니다. 그리고 수업이 끝나고 가능한 빨리 문제를 해결하도록 합니다. <font class="bold">최소한 하루를 넘기면 안됩니다.</font> </p>
			<p class="lst2 bgbl2">문제 해결 학습은 가능한 협동학습(또래)을 하는 것이 효과적입니다. 직접 친구에게 가르침의 도움을 주기도 하고, 받기도 합니다.</p> 
			<p class="lst2 bgbl2">복습할 때 한 번 더 이해를 확인하도록 해야 합니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 6.</th>
			<td>
			<p class="stit4">첫 단추를 잘 끼워야하는 이유(수업 듣기를 잘 해야 하는 이유)</p>
			<p class="stxt1">절대적인 시간을 ‘수업 활동’에 보내고 있는 여러분들이 학교(학원)수업, 즉 수업을 왜! 잘 들어야 하는지 그 이유가 뭘까요?</p> 

			<p class="lst1 bgnum1 bold">하루 일과 중 가장 많은 시간을 수업 받으며 보내기 때문입니다.</p> 
			<p class="stxt1">따라서 수업시간을 어떻게 효율적으로 활용하느냐의 문제는 학업성패와 직접적인 관련을 갖고 있답니다. 수업시간이란? 교과서를 통해 새로운 내용을 익히고, 노트 정리를 통해서는 내용을 조직화하며, 시험문제를 예상해 보는 등 가장 본질적인 <학습활동>이 포함되어 있습니다.</p> 
			
			<p class="lst1 bgnum2 bold">교과 내용에 대한 이해가 훨씬 쉽기 때문입니다.</p> 
			<p class="stxt1">선생님의 자세한 설명을 듣게 되면 처음 배우는 내용도 비교적 쉽고 빠르게 이해할 수 있습니다. </p>
			
			<p class="lst1 bgnum3 bold">자연스러운 반복학습이 가능합니다.</p>
			<p class="stxt1">요즘 많은 학생들이 사교육을 통해 선행학습을 하기 때문에 상대적으로 학교 수업 시간을 소홀히 해서 집중도가 부족합니다. 비록 선행학습을 통해 알고 있는 내용이라도 다시 반복학습을 통해 완전학습으로 자기화가 되어야 합니다.</p> 
			
			<p class="lst1 bgnum4 bold">최고의 교과 전문가인 선생님 수업에 열중해야 합니다.</p>
			<p class="stxt1">간혹 학생들이나 학부모 사이에서 특정교과 교사에 대한 갈등문제로 수업시간을 무용지물로 만들어 가는 경우가 있습니다. 결국 그 피해는 누구에게 돌아올까요? 교사는 국가로부터 엄격한 교육과정과 검증자격을 받은 최고의 전문가라는 사실을 알아야 합니다. 지엽적인 문제로 교과 선생님과 갈등으로 대립하거나 불신하는 모습은 여러분들이 학교생활에서 절대 피해야 할 요소입니다. 교과내용을 아무리 재미없고, 혹시 여러분의 스타일과 맞지 않더라도, 선생님은 담당교과목의 최고 전문가라는 사실을 여러분들은 기억하고 있어야 합니다.</p> 
			
			<p class="lst1 bgnum5 bold">학원 등에서 선행학습을 통해 이미 알고 있다고 자만하여 수업시간을 소홀히 하는 친구들이 있습니다.</p>
			<p class="stxt1">사교육의 부작용을 가장 우려하는 대목이라고도 할 수 있습니다. 상위권학생들은 수업 내용을 이해하지 못하거나 학습 내용에 대해 특별히 ‘학습부하(Learning Load)'를 느끼지는 않습니다. 그렇다고 그들이 수업 시간을 소홀히 보내는 경우도 없습니다. 수업시간을 복습이나 완전학습 시간으로 활용해서, 적극적으로 만들어 갑니다. 그래서 알고 있는 것을 가르쳐도 수업에 열중합니다. 다만, 일부 학생들 중 자신들이 이미 알고 있는 내용을 가르치는 수업을 소홀히 하는 경향이 있다는 것입니다. 이러한 학생들은 결코 상위권 진입이 어려운 친구들입니다.</p> 
			
			<p class="lst1 bgnum6 bold">시험문제의 예측 가능성입니다.</p>
			<p class="stxt1">선생님은 학생들을 위해 수업을 준비하면서 학생이 반드시 알아야 할 내용이 무엇인지 고민하면서 수업을 합니다. 그리고 학생들이 반드시 알아야 할 내용이라고 생각되는 부분을 바탕으로 시험문제를 만들게 됩니다. 그래서 ’수업은 곧 시험문제‘라고 합니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 7.</th>
			<td>
			<p class="stit7"><span>과목별 수업 듣기 방법</span></p>
			<p class="stxt1">학교에서 배우는 교과목은 학문의 성격상 다양한 특성을 가지고 있습니다. 따라서 학습활동도 그 과목의 성격을 파악하고, 여기에 맞춘 공부 방법을 통해 진행한다면 보다 효과적인 학습을 할 수 있습니다.<br/><br/>
			흔히 우리가 주요과목이라고 부르는 <b><font class="bold"><국어･영어･수학･과학></font>의 경우는 평소 주요개념을 이해</b>하고 항상 머릿속에 잘 정리된 상태로 담아두는 것이 중요</b>합니다. 학년이 올라가고 진도가 나갈 때마다 각 개념들을 서로 연결시켜야 합니다. 흔히 기초가 부족한 학생들이 주요과목의 공부를 어려워하는 이유가 여기에 있습니다. 이처럼 위계적 기초를 요하는 주요 과목들은 단기간에 성적을 올리기도 어렵습니다.<br/><br/> 
			따라서 <b><font class="bold"><국어･영어･수학･과학></font>의 수업을 들을 때는 수업시간 동안 개념과 원리를 이해하는 것을 목표</b>로 삼아야 합니다. 주요과목에 대한 실력이 부족하다고 느끼면 반드시 미리 예습을 해두어야 합니다. <b>예습 활동을 통해 수업 시간에 받아들일 수 있는 범위를 최대화</b>할 수 있기 때문입니다. 수업 중에 이해하기 힘든 부분은 반드시 표시해 두었다가 다시 보고, 혼자 해결이 안 될 때는 내용을 이해할만한 친구에게 먼저 질문을 해봅니다. 그래도 해결이 안 될 경우에는  선생님에게 예의를 갖추어 질문하도록 해야 합니다.<br/><br/> 
			질문을 할 때 주의할 점은, 대부분 학생들이 직관적으로 이해가 안 되면, 곧바로 질문을 통해  ‘답’을 알고자하는 습관이 있습니다. 학습 과정에서 질문이 생성되었다면, 일단 <b>질문을 하기 전에 충분히 고민하고 다양한 각도에서 해결하려고 노력</b>해 보아야 합니다. 이러한 과정에서 <내가 알고자 하는 내용>이 명료하게 정리되고, 질문의 핵심을 알 수 있습니다. 그래야 선생님 설명을 들어도 확실하게 머리에 들어오고, 기억도 오래 남게 됩니다. <br/><br/>
			주요과목 외에 다른 과목들은 소위 암기 과목의 성격을 가지고 있습니다. <b>암기 과목이라 해서 기본 이해과정 없이 무조건 외워서 되는 것은 아닙니다.</b> 평소 충실히 수업을 들었다면 시험 때를 활용해 집중적으로 암기해도 무리가 없습니다.<br/> <br/>
			특히 암기 과목의 경우 ‘수업이 곧 시험’인 경우가 많습니다. 따라서 수업 중 선생님이 강조한 내용이나 중요한 힌트, 단서 등을 놓치지 않고 형광펜 같은 것으로 교과서나 노트에 표시를 해 두는 것이 중요합니다. <br/><br/>
			<b><font class="bold"><사회･국사></font>과목은 ‘인과관계’나 ‘내용 흐름을 파악하며 암기’</b>해야 하기 때문에, 수업 시간에 중요한 내용은 교과서 여백을 이용하여 중요한 핵심 단서들을 메모해 두는 자세가 중요합니다.</p>
			</td>
		</tr>
		<tr>
			<th>Way 8.</th>
			<td>
			<p class="stxt1 bold">수업 중 자신을 산만하게 만드는 요인에 대해서 신속하게 대처하는 방법을 알아볼까요?</p>
			<p class="tabAreaPcenter"><img src="${pageContext.request.contextPath}/common/img/strategy/007/tip_06.jpg" alt="수업 중 자신을 산만하게 만드는 요인에 대해서 신속하게 대처하는 방법" /></p>
			</td>
		</tr>
	</tbody>
</table>
<p class="frt"><img src="${pageContext.request.contextPath}/common/img/f_logo.gif" alt="스마트아이"></p>