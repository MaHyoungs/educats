<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<table class="tb1">
	<colgroup>
		<col width="15%">
		<col width="85%">
	</colgroup>
	<tbody>
		<tr>
			<th><span class="tit2">방안 1</span></th>
			<td>
				<p class="tit2">긍정적 태도를 위한 과제 - 목표 세우기</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">자기 효능감은 실제 능력보다 과대평가하게 만든다</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">2학기 때 꼭 이루고 싶은 과제</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">3</span>
						<span class="nbt">올해 안에 이루고 싶은 과제</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int"></p>
		    	</div>
			</td>
		</tr>
		<tr>
			<th><span class="tit2">방안 2</span></th>
			<td>
				<p class="tit2">긍정적 태도를 위한 과제 2 - 생각해 보기 </p>
				<p class="txt2 mt15">
					&lt; 에디슨의 놀이법&gt;
					“항상 모든 일에서 재미를 찾으세요.
					저는 실험을 할 때 놀이를 한다고 생각합니다. 
					책을 읽을 때도, 일을 할 때도 마찬가지입니다.
					여러분도 그렇게 살아 보세요. 
					숙제도, 공부도, 회사 일도 모두 놀이라고 생각해 
					보세요.
					그러면 항상 행복하게 살 수 있답니다“
				</p>
				<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">1</span>
						<span class="nbt">에디슨의 말에 비춰 보았을 때, 본인은 행복하다고 생각하나요?</span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int"></p>
		    	</div>

		    	<div class="txt2">
					<p class="txt2_w">
						<span class="nbg">2</span>
						<span class="nbt">     누구나 행복을 추구합니다. 행복한 삶이 되기 위해서는 어떻게 해야 할까요? </span>
					</p>
					<p class="nbt2"><input type="text" id="" name="" class="int"></p>
		    	</div>
			</td>
		</tr>
	</tbody>
</table>

<div class="bot_btn"><a href="#" onclick="window.scrollTo(0,0)" class="btn_blue">TOP</a></div>