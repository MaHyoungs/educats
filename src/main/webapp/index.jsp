<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lnag="ko">
	<head>
		<title>에듀캣</title>
		<meta name="naver-site-verification" content="fe48f89e6944f2cb1f90629f06031ea0025e56d5"/>
		<meta name="description" content="eduCAT 초/중/고 자기주도학습진단 및 진로탐색검사(인성검사, 진로적성검사) 서비스">
		<meta property="og:type" content="website">
		<meta property="og:title" content="에듀캣">
		<meta property="og:description" content="eduCAT 초/중/고 자기주도학습진단 및 진로탐색검사(인성검사, 진로적성검사) 서비스">
		<meta property="og:image" content="http://educats.co.kr/educat_og.png">
		<meta property="og:url" content="http://educats.co.kr">

		<meta http-equiv="refresh" content="0; /index"/>
	</head>
	<body>
	</body>
</html>